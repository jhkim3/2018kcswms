﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace KSWMS
{
    public partial class Main : Form
    {
        //private Point _imageLocation = new Point(15, 5); // img size = 16_16
        //private Point _imgHitArea = new Point(13, 2); // img size = 16_16
        private Point _imageLocation = new Point(24, 5); // img size = 20_20
        private Point _imgHitArea = new Point(22, 2); // img size = 20_20
        private bool tabchk = false;
        private int C_tabindex;
        private Button TabP_Btn0 = null;
        private Button TabP_Btn1 = null;
        private Button TabP_Btn2 = null;
        private Button TabP_Btn3 = null;
        //private Button TabP_Btn4 = null;
        private TabControl TabP_TabCon1 = null;
        public Panel TabP_Pnl;
        private TabPage TabP;
        public string u_id, u_name, u_ath;

        
        public Main(List<string> user_info)
        {
            InitializeComponent();

            // set the Mode of Drawing as Owner Drawn
            this.tabControl1.DrawMode = TabDrawMode.OwnerDrawFixed;

            // this.tabControl1.ItemSize = new Size(80, 20);
            this.tabControl1.ItemSize = new Size(190, 25);
            //this.tabControl1.SizeMode = TabSizeMode.Fixed;

            // Add the Handler to draw the Image on Tab Pages
            tabControl1.DrawItem += TabControl1_DrawItem;

            statusBar1.Panels[3].Text = user_info[0].ToString();
            statusBar1.Panels[2].Text = user_info[1].ToString();

            u_id   = user_info[0].ToString();
            u_name = user_info[1].ToString();
            u_ath = user_info[2].ToString();

            // 버전 관리
            Assembly assembly = Assembly.GetExecutingAssembly();
            this.Text += ("  BulidTime : " + System.IO.File.GetLastWriteTime(assembly.Location).ToString("yyyy-MM-dd HH:mm"));
        }

 
        private void ExitFunc_Click(object sender, EventArgs e)
        {
            Application.OpenForms["LogIn"].Close();
            //Application.OpenForms["Main"].Close();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            //this.label1.Text = DateTime.Now.ToString();
            statusBar1.Panels[4].Text = DateTime.Now.ToString();
        }

        private void button1_Click(object sender, EventArgs e)
        {

            if (tabControl1.TabCount > 0)
            {
                for (int i = 0; i < tabControl1.TabCount; i++)
                {
                    this.tabControl1.SelectedIndex = i;
                    if (tabControl1.SelectedTab.Name.ToString() == "Form2")
                    {
                        tabchk = true;
                        C_tabindex = i;
                    }
                }

                if (tabchk)
                {
                    this.tabControl1.SelectedIndex = C_tabindex;
                    tabchk = false;
                }
                else
                {
                    tabControl1.TabPages.Add(new MyTabPage(new Form2()));
                    this.tabControl1.SelectedIndex = tabControl1.TabCount - 1;
                    // TabP_Btn0.PerformClick();
                }
            }
            else
            {
                tabControl1.TabPages.Add(new MyTabPage(new Form2()));
                //TabP_Btn0.PerformClick();
            }

        }

        private void Main_Load(object sender, EventArgs e)
        {
            //Changes the size of the ToolStrip botton image
            //this.tlbMain.ImageScalingSize = new Size(16, 16);
            //Disables the image scaling for a particular item
            //this.InqFunc.ImageScaling = ToolStripItemImageScaling.None;
            WindowState = FormWindowState.Maximized;
            Btn_All_Colse();
            //MessageBox.Show(u_ath.Substring(0, 1));
            //treeView1.Nodes.Remove(treeView1.Nodes[0].Nodes[0]);
    }

        private void tabControl1_MouseClick(object sender, MouseEventArgs e)
        {
            TabControl tc = (TabControl)sender;
            Point p = e.Location;
            int _tabWidth = 0;
            _tabWidth = this.tabControl1.GetTabRect(tc.SelectedIndex).Width - (_imgHitArea.X);
            Rectangle r = this.tabControl1.GetTabRect(tc.SelectedIndex);
            r.Offset(_tabWidth, _imgHitArea.Y);
            //r.Width = 16;
            //r.Height = 16;
            r.Width = 20;
            r.Height = 20;
            if (r.Contains(p))
            {
                TabP = (TabPage)tc.TabPages[tc.SelectedIndex];
                tc.TabPages.Remove(TabP);
            }

            Btn_All_Colse();
            SetTabP_Ctl();
        }


        private void TabControl1_DrawItem(object sender, DrawItemEventArgs e)
        {
            try
            {
                // Close Image to draw
                Image img = Resource1.close_20;
                //Image img = new Bitmap("D:\\Close.gif");

                Rectangle r = e.Bounds;
                r = this.tabControl1.GetTabRect(e.Index);
                //r.Offset(2, 2);  // this.tabControl1.ItemSize = new Size(80, 20); 
                r.Offset(2, 5);    // this.tabControl1.ItemSize = new Size(190, 25);


                Brush Title_BackBrush;
                if (e.State == DrawItemState.Selected) Title_BackBrush = new SolidBrush(Color.LightSkyBlue);
                else Title_BackBrush = new SolidBrush(Color.WhiteSmoke);

                Brush Title_ForeBrush = new SolidBrush(Color.Black);
                //Font f = this.Font;
                Font f = new Font("맑은고딕", 10, FontStyle.Bold);

                string title = this.tabControl1.TabPages[e.Index].Text;

                e.Graphics.FillRectangle(Title_BackBrush, e.Bounds);
                e.Graphics.DrawString(title, f, Title_ForeBrush, new PointF(r.X, r.Y));
                e.Graphics.DrawImage(img, new Point(r.X + (this.tabControl1.GetTabRect(e.Index).Width - _imageLocation.X), _imageLocation.Y));
            }
            catch (Exception) { }

        }
        
        private void TabPages_Add(string NodeName)
        {
            List<string> Node_List = new List<string>();
            Node_List = MainTreeNode_Para(treeView1);

            if (NodeName == "110")
            {
                tabControl1.TabPages.Add(new MyTabPage(new KSWMS110(Node_List)));
                Btn_01();
            }
            else if (NodeName == "210") tabControl1.TabPages.Add(new MyTabPage(new KSWMS210(this)));
            else if (NodeName == "220") tabControl1.TabPages.Add(new MyTabPage(new KSWMS220()));
            else if (NodeName == "230") tabControl1.TabPages.Add(new MyTabPage(new KSWMS230()));
            else if (NodeName == "310") tabControl1.TabPages.Add(new MyTabPage(new KSWMS310()));
            else if (NodeName == "320") tabControl1.TabPages.Add(new MyTabPage(new KSWMS320()));
            else if (NodeName == "410") tabControl1.TabPages.Add(new MyTabPage(new KSWMS410()));
            else if (NodeName == "420") tabControl1.TabPages.Add(new MyTabPage(new KSWMS420()));
            else if (NodeName == "430") tabControl1.TabPages.Add(new MyTabPage(new KSWMS430()));
            //else if (NodeName == "440") tabControl1.TabPages.Add(new MyTabPage(new KSWMS440()));
            else if (NodeName == "450") tabControl1.TabPages.Add(new MyTabPage(new KSWMS450()));
            //else if (NodeName == "490") tabControl1.TabPages.Add(new MyTabPage(new KSWMS490()));
            else if (NodeName == "510") tabControl1.TabPages.Add(new MyTabPage(new KSWMS510()));
            //else if (NodeName == "610") tabControl1.TabPages.Add(new MyTabPage(new KSWMS610()));
            else if (NodeName == "620") tabControl1.TabPages.Add(new MyTabPage(new KSWMS620()));
            else if (NodeName == "710") tabControl1.TabPages.Add(new MyTabPage(new KSWMS710(u_name)));
            //else if (NodeName == "720") tabControl1.TabPages.Add(new MyTabPage(new KSWMS720()));
            //else if (NodeName == "990") tabControl1.TabPages.Add(new MyTabPage(new KSWMS990()));
            
        }

        private void SetTabP_Ctl()
        {
            TabP = tabControl1.SelectedTab;
            if (TabP != null)
            {
                if (TabP.Name.ToString() == "KSWMS110")
                {
                    Btn_01();

                    TabP_Pnl = TabP.Controls[0] as Panel;
                    TabP_Btn0 = TabP.Controls[1] as Button;
                    TabP_Btn1 = TabP.Controls[2] as Button;
                    TabP_Btn2 = TabP.Controls[3] as Button;
                    TabP_Btn0.PerformClick();
                    TabP_Btn1.PerformClick();
                }
                else if (TabP.Name.ToString() == "KSWMS210")
                {
                    TabP_Pnl = TabP.Controls[0] as Panel;
                    TabP_Btn0 = TabP.Controls[1] as Button;
                    TabP_Btn1 = TabP.Controls[2] as Button;
                    TabP_Btn2 = TabP.Controls[3] as Button;
                    TabP_Btn3 = TabP.Controls[4] as Button;
                    TabP_Btn0.PerformClick();
                    TabP_Btn1.PerformClick();
                }
                else if(TabP.Name.ToString() == "KSWMS220")
                {
                    TabP_Pnl = TabP.Controls[0] as Panel;
                    TabP_Btn0 = TabP.Controls[1] as Button;
                    TabP_Btn1 = TabP.Controls[2] as Button;
                    TabP_Btn0.PerformClick();
                    TabP_Btn1.PerformClick();
                }
                else if (TabP.Name.ToString() == "KSWMS230")
                {
                    TabP_Pnl = TabP.Controls[0] as Panel;
                    TabP_Btn0 = TabP.Controls[1] as Button;
                    TabP_Btn1 = TabP.Controls[2] as Button;
                    TabP_Btn0.PerformClick();
                    TabP_Btn1.PerformClick();
                }
                else if (TabP.Name.ToString() == "KSWMS320")
                {
                    Btn_320();
                    TabP_Pnl = TabP.Controls[0] as Panel;
                    TabP_Btn0 = TabP.Controls[1] as Button;
                    TabP_Btn1 = TabP.Controls[2] as Button;
                    TabP_Btn0.PerformClick();
                    TabP_Btn1.PerformClick();
                }
                else if (TabP.Name.ToString() == "KSWMS410")
                {
                    Btn_410();
                    TabP_Pnl = TabP.Controls[0] as Panel;
                    TabP_Btn0 = TabP.Controls[1] as Button;
                    TabP_Btn1 = TabP.Controls[2] as Button;
                    TabP_Btn2 = TabP.Controls[3] as Button;
                    TabP_Btn0.PerformClick();
                    TabP_Btn1.PerformClick();
                }
                else if (TabP.Name.ToString() == "KSWMS420")
                {
                    Btn_420();
                    TabP_Pnl = TabP.Controls[0] as Panel;
                    TabP_Btn0 = TabP.Controls[1] as Button;
                    TabP_Btn1 = TabP.Controls[2] as Button;
                    TabP_Btn2 = TabP.Controls[3] as Button;
                    TabP_Btn0.PerformClick();
                    TabP_Btn1.PerformClick();
                }
                else if (TabP.Name.ToString() == "KSWMS710")
                {
                    Btn_710();
                    TabP_Pnl = TabP.Controls[0] as Panel;
                    TabP_Btn0 = TabP.Controls[1] as Button;
                    TabP_Btn1 = TabP.Controls[2] as Button;
                    TabP_Btn2 = TabP.Controls[3] as Button;
                    TabP_Btn0.PerformClick();
                    TabP_Btn1.PerformClick();
                }
                else
                {
                    TabP_Pnl = TabP.Controls[0] as Panel;
                    TabP_Btn0 = TabP.Controls[1] as Button;
                    TabP_Btn1 = TabP.Controls[2] as Button;
                    TabP_Btn0.PerformClick();
                    TabP_Btn1.PerformClick();
                }
                //MessageBox.Show(TabP.Controls[2].ToString());
            }
        }

        private void Btn_All_Colse()
        {
            toolStripButton1.Enabled = false;
            InqFunc.Enabled = false;
            NewFunc.Enabled = false;
            DelFunc.Enabled = false;
            SaveFunc.Enabled = false;
            ExcelFunc.Enabled = false;
            PrintFunc.Enabled = false;
            //CloseFunc.Enabled = false;
        }

        private void Btn_01()
        {
            toolStripButton1.Enabled = false;
            InqFunc.Enabled = false;
            NewFunc.Enabled = true;
            DelFunc.Enabled = false;
            SaveFunc.Enabled = false;
            ExcelFunc.Enabled = false;
            PrintFunc.Enabled = false;
            CloseFunc.Enabled = true;
        }
        private void Btn_320()
        {
            toolStripButton1.Enabled = false;
            InqFunc.Enabled = true;
            NewFunc.Enabled = false;
            DelFunc.Enabled = false;
            SaveFunc.Enabled = false;
            ExcelFunc.Enabled = false;
            PrintFunc.Enabled = false;
            CloseFunc.Enabled = true;
        }
        private void Btn_410()
        {
            toolStripButton1.Enabled = false;
            InqFunc.Enabled = true;
            NewFunc.Enabled = false;
            DelFunc.Enabled = false;
            SaveFunc.Enabled = false;
            ExcelFunc.Enabled = true;
            PrintFunc.Enabled = false;
            CloseFunc.Enabled = true;
        }

        private void Btn_420()
        {
            toolStripButton1.Enabled = false;
            InqFunc.Enabled = true;
            NewFunc.Enabled = false;
            DelFunc.Enabled = false;
            SaveFunc.Enabled = false;
            ExcelFunc.Enabled = true;
            PrintFunc.Enabled = false;
            CloseFunc.Enabled = true;
        }
        private void Btn_710()
        {
            toolStripButton1.Enabled = false;
            InqFunc.Enabled = true;
            NewFunc.Enabled = false;
            DelFunc.Enabled = false;
            SaveFunc.Enabled = true;
            ExcelFunc.Enabled = false;
            PrintFunc.Enabled = false;
            CloseFunc.Enabled = true;
        }

        public void AutoInPut_ORD_Btn()
        {
            toolStripButton1.Enabled = true;
            InqFunc.Enabled = true;
            NewFunc.Enabled = false;
            DelFunc.Enabled = false;
            SaveFunc.Enabled = false;
            ExcelFunc.Enabled = true;
            PrintFunc.Enabled = true;
        }
        public void AutoInPut_WRK_Btn()
        {
            toolStripButton1.Enabled = true;
            InqFunc.Enabled = true;
            NewFunc.Enabled = false;
            DelFunc.Enabled = false;
            SaveFunc.Enabled = false;
            ExcelFunc.Enabled = false;
            PrintFunc.Enabled = true;
        }
        public void AutoInPut_RSV_Btn()
        {
            toolStripButton1.Enabled = true;
            InqFunc.Enabled = true;
            NewFunc.Enabled = false;
            DelFunc.Enabled = false;
            SaveFunc.Enabled = false;
            ExcelFunc.Enabled = false;
            PrintFunc.Enabled = false;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            // tree node 펼치기
            for (int i = 0; i < treeView1.Nodes.Count; i++)
            {
                this.treeView1.Nodes[i].Expand();
                //this.treeView1.Nodes[i].Name.ToString();
            }
            //MessageBox.Show(this.treeView1.Nodes.Count.ToString());
        }

        private void CloseFunc_Click(object sender, EventArgs e)
        {
            // Removes the selected tab:  
            if (tabControl1.TabCount > 0)
            {
                tabControl1.TabPages.Remove(tabControl1.SelectedTab);
                Btn_All_Colse();
            }
                
            
            // Removes all the tabs:  
            //tabControl1.TabPages.Clear();
        }

        private void Main_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.OpenForms["LogIn"].Close();
        }
        
        
        private void treeView1_NodeMouseDoubleClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            Btn_All_Colse();

            if (e.Node.Name.ToString().Remove(0, 1) != "00")
            {
                // 사용자 사용 권한 체크
                if (u_ath.Substring(0, 1) == "X" && e.Node.Name.ToString() == "110")
                {
                    MessageBox.Show("110. 사용자 관리 화면 사용권한이 없습니다.", "사용권한", MessageBoxButtons.OK, (MessageBoxIcon)16);
                    return;
                }
                if (u_ath.Substring(16, 1) == "X" && e.Node.Name.ToString() == "210")
                {
                    MessageBox.Show("210. 자동 입고 화면 사용권한이 없습니다.", "사용권한", MessageBoxButtons.OK, (MessageBoxIcon)16);
                    return;
                }
                if (u_ath.Substring(17, 1) == "X" && e.Node.Name.ToString() == "220")
                {
                    MessageBox.Show("220. 수동 입고 화면 사용권한이 없습니다.", "사용권한", MessageBoxButtons.OK, (MessageBoxIcon)16);
                    return;
                }
                if (u_ath.Substring(18, 1) == "X" && e.Node.Name.ToString() == "230")
                {
                    MessageBox.Show("230. 바코드 확인 화면 사용권한이 없습니다.", "사용권한", MessageBoxButtons.OK, (MessageBoxIcon)16);
                    return;
                }
                if (u_ath.Substring(32, 1) == "X" && e.Node.Name.ToString() == "310")
                {
                    MessageBox.Show("310. 오더출고 화면 사용권한이 없습니다.", "사용권한", MessageBoxButtons.OK, (MessageBoxIcon)16);
                    return;
                }
                if (u_ath.Substring(33, 1) == "X" && e.Node.Name.ToString() == "320")
                {
                    MessageBox.Show("320. 수동출고 화면 사용권한이 없습니다.", "사용권한", MessageBoxButtons.OK, (MessageBoxIcon)16);
                    return;
                }
                if (u_ath.Substring(48, 1) == "X" && e.Node.Name.ToString() == "410")
                {
                    MessageBox.Show("410. LOCATION 조회 화면 사용권한이 없습니다.", "사용권한", MessageBoxButtons.OK, (MessageBoxIcon)16);
                    return;
                }
                if (u_ath.Substring(49, 1) == "X" && e.Node.Name.ToString() == "420")
                {
                    MessageBox.Show("420. 재고조회 화면 사용권한이 없습니다.", "사용권한", MessageBoxButtons.OK, (MessageBoxIcon)16);
                    return;
                }
                if (u_ath.Substring(50, 1) == "X" && e.Node.Name.ToString() == "430")
                {
                    MessageBox.Show("430. 창고사용현황 조회 화면 사용권한이 없습니다.", "사용권한", MessageBoxButtons.OK, (MessageBoxIcon)16);
                    return;
                }
                if (u_ath.Substring(51, 1) == "X" && e.Node.Name.ToString() == "440")
                {
                    MessageBox.Show("440. 적재율 조회(팝업) 화면 사용권한이 없습니다.", "사용권한", MessageBoxButtons.OK, (MessageBoxIcon)16);
                    return;
                }
                if (u_ath.Substring(52, 1) == "X" && e.Node.Name.ToString() == "450")
                {
                    MessageBox.Show("450. 작업실적 조회 화면 사용권한이 없습니다.", "사용권한", MessageBoxButtons.OK, (MessageBoxIcon)16);
                    return;
                }
                if (u_ath.Substring(56, 1) == "X" && e.Node.Name.ToString() == "490")
                {
                    MessageBox.Show("490. 에러기록조회 화면 사용권한이 없습니다.", "사용권한", MessageBoxButtons.OK, (MessageBoxIcon)16);
                    return;
                }
                if (u_ath.Substring(64, 1) == "X" && e.Node.Name.ToString() == "510")
                {
                    MessageBox.Show("510. 상태파악/에러처리 화면 사용권한이 없습니다.", "사용권한", MessageBoxButtons.OK, (MessageBoxIcon)16);
                    return;
                }
                if (u_ath.Substring(80, 1) == "X" && e.Node.Name.ToString() == "610")
                {
                    MessageBox.Show("610. LOCATION 재고수정 화면 사용권한이 없습니다.", "사용권한", MessageBoxButtons.OK, (MessageBoxIcon)16);
                    return;
                }
                if (u_ath.Substring(81, 1) == "X" && e.Node.Name.ToString() == "620")
                {
                    MessageBox.Show("620. 분포도 재고수정 화면 사용권한이 없습니다.", "사용권한", MessageBoxButtons.OK, (MessageBoxIcon)16);
                    return;
                }
                if (u_ath.Substring(96, 1) == "X" && e.Node.Name.ToString() == "710")
                {
                    MessageBox.Show("710. 설비 사용정의 화면 사용권한이 없습니다.", "사용권한", MessageBoxButtons.OK, (MessageBoxIcon)16);
                    return;
                }
                if (u_ath.Substring(97, 1) == "X" && e.Node.Name.ToString() == "720")
                {
                    MessageBox.Show("720. LOCATION 사용정의 화면 사용권한이 없습니다.", "사용권한", MessageBoxButtons.OK, (MessageBoxIcon)16);
                    return;
                }
                
                // NODE 클릭 처리
                if (tabControl1.TabCount > 0)
                {
                    for (int i = 0; i < tabControl1.TabCount; i++)
                    {
                        //this.tabControl1.SelectedIndex = i;
                        //if (tabControl1.SelectedTab.Name.ToString() == "KSWMS" + e.Node.Name.ToString())
                        if (tabControl1.TabPages[i].Name.ToString() == "KSWMS" + e.Node.Name.ToString())
                        {
                            tabchk = true;
                            //this.tabControl1.SelectedIndex = i;
                            C_tabindex = i;
                            //break;
                        }
                    }

                    if (tabchk)
                    {
                        tabchk = false;
                        this.tabControl1.SelectedIndex = C_tabindex;

                        SetTabP_Ctl();
                    }
                    else
                    {
                        TabPages_Add(e.Node.Name.ToString());
                        C_tabindex = tabControl1.TabCount - 1;
                        this.tabControl1.SelectedIndex = C_tabindex;

                        SetTabP_Ctl();
                    }
                }
                else
                {
                    TabPages_Add(e.Node.Name.ToString());
                    SetTabP_Ctl();
                }
            }

            if (e.Node.Name.ToString() == "440")
            {
                POPUP_랙적재율정보 PopupForm = new POPUP_랙적재율정보();
                PopupForm.ShowDialog();
            }

            SetTabP_Ctl();
        }

     
        private void Main_Resize(object sender, EventArgs e)
        {
            treeView1.Size = new Size(245, Convert.ToInt32(Size.Height.ToString()) - 150);
            panel3.Size = new Size(Convert.ToInt32(Size.Width.ToString()) - 270, Convert.ToInt32(Size.Height.ToString()) - 120);
            
            SetTabP_Ctl();
            
            //TabP_Pnl.Size = new Size(Convert.ToInt32(Size.Width.ToString()) - 270, Convert.ToInt32(Size.Height.ToString()) - 120);
            //dataGridView1.Size = new Size(Convert.ToInt32(Size.Width.ToString()) - 20, Convert.ToInt32(Size.Height.ToString()) - 240);
            //groupBox1.Size = new Size(Convert.ToInt32(Size.Width.ToString()) - 20, 90);
        }

        private void button3_Click(object sender, EventArgs e)
        {
            // tree node 닫기
            for (int i = 0; i < treeView1.Nodes.Count; i++)
            {
                this.treeView1.Nodes[i].Collapse();
            }
        }

        private static List<string> MainTreeNode_Para(TreeView tree)
        {
            List<string> Node_List = new List<string>();

            // Print each node recursively.  
            TreeNodeCollection nodes = tree.Nodes;

            foreach (TreeNode n in nodes)
            {   if(n.Name.ToString() != "900")
                {
                    for (int i = 0; i < n.Nodes.Count; i++)
                    {
                        //MessageBox.Show(n.Nodes[i].Text.ToString());
                        Node_List.Add(n.Nodes[i].Text.ToString());
                    }
                }
            }
            return Node_List;
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            if (TabP.Name.ToString() == "KSWMS110")
            {
            }
            else if (TabP.Name.ToString() == "KSWMS210")
            {
                TabP_Btn2.PerformClick();
            }
        }

        private void InqFunc_Click(object sender, EventArgs e)
        {
            if (TabP.Name.ToString() == "KSWMS110") TabP_Btn1.PerformClick();
            else if (TabP.Name.ToString() == "KSWMS210") TabP_Btn1.PerformClick();
            else if (TabP.Name.ToString() == "KSWMS320") TabP_Btn1.PerformClick();
            else if (TabP.Name.ToString() == "KSWMS420") TabP_Btn1.PerformClick();
            else if (TabP.Name.ToString() == "KSWMS710") TabP_Btn1.PerformClick();
            else TabP_Btn1.PerformClick();
        }

        private void NewFunc_Click(object sender, EventArgs e)
        {
            if (TabP.Name.ToString() == "KSWMS110")
            {
                TabP_Btn2.PerformClick();
            }
        }

        private void ExcelFunc_Click(object sender, EventArgs e)
        {
            if (TabP.Name.ToString() == "KSWMS410")
            {
                TabP_Btn2.PerformClick();
            }
            if (TabP.Name.ToString() == "KSWMS420")
            {
                TabP_Btn2.PerformClick();
            }
        }

        private void PrintFunc_Click(object sender, EventArgs e)
        {
            if (TabP.Name.ToString() == "KSWMS110")
            {
            }
            else if (TabP.Name.ToString() == "KSWMS210")
            {
                TabP_Btn3.PerformClick();
            }
        }

        private void SaveFunc_Click(object sender, EventArgs e)
        {
            if (TabP.Name.ToString() == "KSWMS710")
            {
                TabP_Btn2.PerformClick();
            }
        }
        
    }


    /*
        class Class1
       {
           /// <summary>
           /// 해당 응용 프로그램의 주 진입점입니다.
           /// </summary>
           [STAThread]
           static void Main(string[] args)
           {
               //
               // TODO: 여기에 응용 프로그램을 시작하는 코드를 추가합니다.
               //
               string tmp = "한글English";

               byte []bArray_ =Encoding.Default.GetBytes(tmp);
               Console.Out.WriteLine(Encoding.Default.EncodingName);
               Console.Out.WriteLine(Encoding.Default.GetString(bArray_));
               Console.Out.WriteLine(bArray_.Length);

               byte []u7Array_ = Encoding.Convert(Encoding.Default, Encoding.UTF7, bArray_);
               Console.Out.WriteLine(Encoding.UTF7.EncodingName);
               Console.Out.WriteLine(Encoding.UTF7.GetString(u7Array_));
               Console.Out.WriteLine(u7Array_.Length);

               byte []u8Array_ = Encoding.Convert(Encoding.Default, Encoding.UTF8, bArray_);
               Console.Out.WriteLine(Encoding.UTF8.EncodingName);
               Console.Out.WriteLine(Encoding.UTF8.GetString(u8Array_));
               Console.Out.WriteLine(u8Array_.Length);

               byte []uArray_ = Encoding.Convert(Encoding.Default, Encoding.Unicode, bArray_);
               Console.Out.WriteLine(Encoding.Unicode.EncodingName);
               Console.Out.WriteLine(Encoding.Unicode.GetString(uArray_));
               Console.Out.WriteLine(uArray_.Length);

               byte []aArray_ = Encoding.Convert(Encoding.Default, Encoding.ASCII, bArray_);
               Console.Out.WriteLine(Encoding.ASCII.EncodingName);
               Console.Out.WriteLine(Encoding.ASCII.GetString(aArray_));
               Console.Out.WriteLine(aArray_.Length);
           }
       }

      - 결과:
      한국어
      한글English
      11
      유니코드(UTF-7)
      한글English
      15
      유니코드(UTF-8)
      한글English
      13
      유니코드
      한글English
      18
      US-ASCII
      ??English
      9
     */

}
