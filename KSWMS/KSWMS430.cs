﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace KSWMS
{
    public partial class KSWMS430 : MyFormPage
    {
        private TabPage TabP;
        private DataTable[] d = new DataTable[5];
        private int COL_Count_1 = 51, COL_Count_2 = 51, COL_Count_3 = 51, COL_Count_4 = 51, ROW_Count = 18;
       

        public KSWMS430()
        {
            InitializeComponent();
            this.pnl = panel1;
            this.btn1 = button1;
            this.btn2 = button2;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            tabControl1.Size = new Size(Convert.ToInt32(panel1.Size.Width.ToString()), Convert.ToInt32(panel1.Size.Height.ToString()) - 50);

            //MessageBox.Show(panel1.Size.ToString());
            panel50.Size = new Size(Convert.ToInt32(panel1.Size.Width.ToString()) - 5, 25);
            panel2.Size = new Size(Convert.ToInt32(panel1.Size.Width.ToString()) - 5, 25);
            panel3.Size = new Size(Convert.ToInt32(panel1.Size.Width.ToString()) - 5, 25);
            panel4.Size = new Size(Convert.ToInt32(panel1.Size.Width.ToString()) - 5, 25);
            panel5.Size = new Size(Convert.ToInt32(panel1.Size.Width.ToString()) - 5, 25);
            panel6.Size = new Size(Convert.ToInt32(panel1.Size.Width.ToString()) - 5, 25);
            panel7.Size = new Size(Convert.ToInt32(panel1.Size.Width.ToString()) - 5, 25);
            panel8.Size = new Size(Convert.ToInt32(panel1.Size.Width.ToString()) - 5, 25);
            panel9.Size = new Size(Convert.ToInt32(panel1.Size.Width.ToString()) - 5, 25);
            panel10.Size = new Size(Convert.ToInt32(panel1.Size.Width.ToString()) - 5, 25);
            panel11.Size = new Size(Convert.ToInt32(panel1.Size.Width.ToString()) - 5, 25);
            panel12.Size = new Size(Convert.ToInt32(panel1.Size.Width.ToString()) - 5, 25);
            panel13.Size = new Size(Convert.ToInt32(panel1.Size.Width.ToString()) - 5, 25);
            panel14.Size = new Size(Convert.ToInt32(panel1.Size.Width.ToString()) - 5, 25);
            panel15.Size = new Size(Convert.ToInt32(panel1.Size.Width.ToString()) - 5, 25);
            panel16.Size = new Size(Convert.ToInt32(panel1.Size.Width.ToString()) - 5, 25);

            dataGridView1.Size = new Size(Convert.ToInt32(panel1.Size.Width.ToString()) - 8, Convert.ToInt32(panel1.Size.Height.ToString()) / 2 - 78);
            dataGridView3.Size = new Size(Convert.ToInt32(panel1.Size.Width.ToString()) - 8, Convert.ToInt32(panel1.Size.Height.ToString()) / 2 - 78);
            dataGridView5.Size = new Size(Convert.ToInt32(panel1.Size.Width.ToString()) - 8, Convert.ToInt32(panel1.Size.Height.ToString()) / 2 - 78);
            dataGridView7.Size = new Size(Convert.ToInt32(panel1.Size.Width.ToString()) - 8, Convert.ToInt32(panel1.Size.Height.ToString()) / 2 - 78);
            dataGridView9.Size = new Size(Convert.ToInt32(panel1.Size.Width.ToString()) - 8, Convert.ToInt32(panel1.Size.Height.ToString()) / 2 - 78);
            dataGridView11.Size = new Size(Convert.ToInt32(panel1.Size.Width.ToString()) - 8, Convert.ToInt32(panel1.Size.Height.ToString()) / 2 - 78);
            dataGridView13.Size = new Size(Convert.ToInt32(panel1.Size.Width.ToString()) - 8, Convert.ToInt32(panel1.Size.Height.ToString()) / 2 - 78);
            dataGridView15.Size = new Size(Convert.ToInt32(panel1.Size.Width.ToString()) - 8, Convert.ToInt32(panel1.Size.Height.ToString()) / 2 - 78);
           
            panel2.Location = new Point(0, (Convert.ToInt32(panel1.Size.Height.ToString()) / 2) - 52);
            panel4.Location = new Point(0, (Convert.ToInt32(panel1.Size.Height.ToString()) / 2) - 52);
            panel6.Location = new Point(0, (Convert.ToInt32(panel1.Size.Height.ToString()) / 2) - 52);
            panel8.Location = new Point(0, (Convert.ToInt32(panel1.Size.Height.ToString()) / 2) - 52);
            panel12.Location = new Point(0, (Convert.ToInt32(panel1.Size.Height.ToString()) / 2) - 52);
            panel14.Location = new Point(0, (Convert.ToInt32(panel1.Size.Height.ToString()) / 2) - 52);
            panel16.Location = new Point(0, (Convert.ToInt32(panel1.Size.Height.ToString()) / 2) - 52);
            panel10.Location = new Point(0, (Convert.ToInt32(panel1.Size.Height.ToString()) / 2) - 52);

            dataGridView2.Location = new Point(0, (Convert.ToInt32(panel1.Size.Height.ToString()) / 2) - 27);
            dataGridView4.Location = new Point(0, (Convert.ToInt32(panel1.Size.Height.ToString()) / 2) - 27);
            dataGridView6.Location = new Point(0, (Convert.ToInt32(panel1.Size.Height.ToString()) / 2) - 27);
            dataGridView8.Location = new Point(0, (Convert.ToInt32(panel1.Size.Height.ToString()) / 2) - 27);
            dataGridView12.Location = new Point(0, (Convert.ToInt32(panel1.Size.Height.ToString()) / 2) - 27);
            dataGridView14.Location = new Point(0, (Convert.ToInt32(panel1.Size.Height.ToString()) / 2) - 27);
            dataGridView16.Location = new Point(0, (Convert.ToInt32(panel1.Size.Height.ToString()) / 2) - 27);
            dataGridView10.Location = new Point(0, (Convert.ToInt32(panel1.Size.Height.ToString()) / 2) - 27);

            dataGridView2.Size = new Size(Convert.ToInt32(panel1.Size.Width.ToString()) - 8, Convert.ToInt32(panel1.Size.Height.ToString()) / 2 - 55);
            dataGridView4.Size = new Size(Convert.ToInt32(panel1.Size.Width.ToString()) - 8, Convert.ToInt32(panel1.Size.Height.ToString()) / 2 - 55);
            dataGridView6.Size = new Size(Convert.ToInt32(panel1.Size.Width.ToString()) - 8, Convert.ToInt32(panel1.Size.Height.ToString()) / 2 - 55);
            dataGridView8.Size = new Size(Convert.ToInt32(panel1.Size.Width.ToString()) - 8, Convert.ToInt32(panel1.Size.Height.ToString()) / 2 - 55);
            dataGridView10.Size = new Size(Convert.ToInt32(panel1.Size.Width.ToString()) - 8, Convert.ToInt32(panel1.Size.Height.ToString()) / 2 - 55);
            dataGridView12.Size = new Size(Convert.ToInt32(panel1.Size.Width.ToString()) - 8, Convert.ToInt32(panel1.Size.Height.ToString()) / 2 - 55);
            dataGridView14.Size = new Size(Convert.ToInt32(panel1.Size.Width.ToString()) - 8, Convert.ToInt32(panel1.Size.Height.ToString()) / 2 - 55);
            dataGridView16.Size = new Size(Convert.ToInt32(panel1.Size.Width.ToString()) - 8, Convert.ToInt32(panel1.Size.Height.ToString()) / 2 - 55);

        }
        
        private void button2_Click(object sender, EventArgs e)
        {
            //label1.Text = "430 폼";
            T1_dataGridView1_Main_Process();
            T1_dataGridView2_Main_Process();
        }

        private void tabControl1_MouseClick(object sender, MouseEventArgs e)
        {
            SetTabP_Ctl();
        }

        private void SetTabP_Ctl()
        {
            //T1_dataGridView1_Clear();
            //T2_dataGridView2_Clear();
            //T3_dataGridView4_Clear();
            //T3_dataGridView5_Clear();

            TabP = tabControl1.SelectedTab;
            if (TabP != null)
            {
                if (TabP.Name.ToString() == "tabPage1")
                {
                    T1_dataGridView1_Main_Process();
                    T1_dataGridView2_Main_Process();
                    //fmain.AutoInPut_ORD_Btn();
                }
                else if (TabP.Name.ToString() == "tabPage2")
                {
                    T2_dataGridView3_Main_Process();
                    T2_dataGridView4_Main_Process();
                    //fmain.AutoInPut_WRK_Btn();
                }
                else if (TabP.Name.ToString() == "tabPage3")
                {
                    T3_dataGridView5_Main_Process();
                    T3_dataGridView6_Main_Process();
                    //fmain.AutoInPut_RSV_Btn();
                }
                else if (TabP.Name.ToString() == "tabPage4")
                {
                    T4_dataGridView7_Main_Process();
                    T4_dataGridView8_Main_Process();
                    //fmain.AutoInPut_RSV_Btn();
                }
                else if (TabP.Name.ToString() == "tabPage5")
                {
                    T5_dataGridView9_Main_Process();
                    T5_dataGridView10_Main_Process();
                    //fmain.AutoInPut_RSV_Btn();
                }
                else if (TabP.Name.ToString() == "tabPage6")
                {
                    T6_dataGridView11_Main_Process();
                    T6_dataGridView12_Main_Process();
                    //fmain.AutoInPut_RSV_Btn();
                }
                else if (TabP.Name.ToString() == "tabPage7")
                {
                    T7_dataGridView13_Main_Process();
                    T7_dataGridView14_Main_Process();
                    //fmain.AutoInPut_RSV_Btn();
                }
                else if (TabP.Name.ToString() == "tabPage8")
                {
                    T8_dataGridView15_Main_Process();
                    T8_dataGridView16_Main_Process();
                    //fmain.AutoInPut_RSV_Btn();
                }
                //MessageBox.Show(TabP.Controls[2].ToString());
            }
        }


        // dataGridView1 Set ===========================================================================

        private void T1_dataGridView1_Main_Process()
        {
            Cursor.Current = Cursors.WaitCursor;

            List<string> Condition = new List<string>();
            Condition.Add("11");

            d[0] = CommonDB.R_datatbl(Qry.KSWMS430_001(Condition));
 
            dataGridView_Clear(dataGridView1);
            dataGridView_Set(dataGridView1, COL_Count_1);
           
            int j = 0;

            while (j < ROW_Count)
            {
                int n = dataGridView1.Rows.Add();

                dataGridView1.Rows[n].HeaderCell.Value = (18-n).ToString();
               
                for (int i = 0; i < COL_Count_1; i++)
                {
                    string dtqry;
                    if (i < 9)
                    {
                        dtqry = "LOC = " + "11" + "0" + (i+1).ToString();
                        if((18-n) < 10) dtqry += "0" + (18-n).ToString();
                        else dtqry += (18 - n).ToString();
                    }
                    else
                    {
                        dtqry = "LOC = " + "11" + (i + 1).ToString();
                        if ((18 - n) < 10) dtqry += "0" + (18 - n).ToString();
                        else dtqry += (18 - n).ToString();
                    }

                    DataRow[] checkrow = d[0].Select(dtqry);
                    DataRow temp = checkrow[0];

                    dataGridView_Write(temp, dataGridView1, n, i);
                }

                j++;
            }
            d[0].Clear();
        }

        
        // dataGridView2 Set ===========================================================================

        private void T1_dataGridView2_Main_Process()
        {
            Cursor.Current = Cursors.WaitCursor;

            List<string> Condition = new List<string>();
            Condition.Add("12");

            d[0] = CommonDB.R_datatbl(Qry.KSWMS430_001(Condition));

            dataGridView_Clear(dataGridView2);
            dataGridView_Set(dataGridView2, COL_Count_1);
           
            int j = 0;

            while (j < ROW_Count)
            {
                int n = dataGridView2.Rows.Add();

                dataGridView2.Rows[n].HeaderCell.Value = (18 - n).ToString();

                for (int i = 0; i < COL_Count_1; i++)
                {
                    string dtqry;
                    if (i < 9)
                    {
                        dtqry = "LOC = " + "12" + "0" + (i + 1).ToString();
                        if ((18 - n) < 10) dtqry += "0" + (18 - n).ToString();
                        else dtqry += (18 - n).ToString();
                    }
                    else
                    {
                        dtqry = "LOC = " + "12" + (i + 1).ToString();
                        if ((18 - n) < 10) dtqry += "0" + (18 - n).ToString();
                        else dtqry += (18 - n).ToString();
                    }

                    DataRow[] checkrow = d[0].Select(dtqry);
                    DataRow temp = checkrow[0];

                    dataGridView_Write(temp, dataGridView2, n, i);
                }

                j++;
            }
            d[0].Clear();
        }

        // dataGridView3 Set ===========================================================================

        private void T2_dataGridView3_Main_Process()
        {
            Cursor.Current = Cursors.WaitCursor;

            List<string> Condition = new List<string>();
            Condition.Add("13");

            d[0] = CommonDB.R_datatbl(Qry.KSWMS430_001(Condition));

            dataGridView_Clear(dataGridView3);
            dataGridView_Set(dataGridView3, COL_Count_1);

            int j = 0;

            while (j < ROW_Count)
            {
                int n = dataGridView3.Rows.Add();

                dataGridView3.Rows[n].HeaderCell.Value = (18 - n).ToString();

                for (int i = 0; i < COL_Count_1; i++)
                {
                    string dtqry;
                    if (i < 9)
                    {
                        dtqry = "LOC = " + "13" + "0" + (i + 1).ToString();
                        if ((18 - n) < 10) dtqry += "0" + (18 - n).ToString();
                        else dtqry += (18 - n).ToString();
                    }
                    else
                    {
                        dtqry = "LOC = " + "13" + (i + 1).ToString();
                        if ((18 - n) < 10) dtqry += "0" + (18 - n).ToString();
                        else dtqry += (18 - n).ToString();
                    }

                    DataRow[] checkrow = d[0].Select(dtqry);
                    DataRow temp = checkrow[0];

                    dataGridView_Write(temp, dataGridView3, n, i);
                }
                j++;
            }
            d[0].Clear();
        }

        // dataGridView4 Set ===========================================================================

        private void T2_dataGridView4_Main_Process()
        {
            Cursor.Current = Cursors.WaitCursor;

            List<string> Condition = new List<string>();
            Condition.Add("14");

            d[0] = CommonDB.R_datatbl(Qry.KSWMS430_001(Condition));

            dataGridView_Clear(dataGridView4);
            dataGridView_Set(dataGridView4, COL_Count_1);

            int j = 0;

            while (j < ROW_Count)
            {
                int n = dataGridView4.Rows.Add();

                dataGridView4.Rows[n].HeaderCell.Value = (18 - n).ToString();

                for (int i = 0; i < COL_Count_1; i++)
                {
                    string dtqry;
                    if (i < 9)
                    {
                        dtqry = "LOC = " + "14" + "0" + (i + 1).ToString();
                        if ((18 - n) < 10) dtqry += "0" + (18 - n).ToString();
                        else dtqry += (18 - n).ToString();
                    }
                    else
                    {
                        dtqry = "LOC = " + "14" + (i + 1).ToString();
                        if ((18 - n) < 10) dtqry += "0" + (18 - n).ToString();
                        else dtqry += (18 - n).ToString();
                    }

                    DataRow[] checkrow = d[0].Select(dtqry);
                    DataRow temp = checkrow[0];

                    dataGridView_Write(temp, dataGridView4, n, i);
                }

                j++;
            }

            d[0].Clear();
        }

        // dataGridView5 Set ===========================================================================

        private void T3_dataGridView5_Main_Process()
        {
            Cursor.Current = Cursors.WaitCursor;

            List<string> Condition = new List<string>();
            Condition.Add("21");

            d[0] = CommonDB.R_datatbl(Qry.KSWMS430_001(Condition));

            dataGridView_Clear(dataGridView5);
            dataGridView_Set(dataGridView5, COL_Count_2);

            int j = 0;

            while (j < ROW_Count)
            {
                int n = dataGridView5.Rows.Add();

                dataGridView5.Rows[n].HeaderCell.Value = (18 - n).ToString();

                for (int i = 0; i < COL_Count_2; i++)
                {
                    string dtqry;
                    if (i < 9)
                    {
                        dtqry = "LOC = " + "21" + "0" + (i + 1).ToString();
                        if ((18 - n) < 10) dtqry += "0" + (18 - n).ToString();
                        else dtqry += (18 - n).ToString();
                    }
                    else
                    {
                        dtqry = "LOC = " + "21" + (i + 1).ToString();
                        if ((18 - n) < 10) dtqry += "0" + (18 - n).ToString();
                        else dtqry += (18 - n).ToString();
                    }

                    if (dtqry == "215718") MessageBox.Show(dtqry);
                   
                    DataRow[] checkrow = d[0].Select(dtqry);
                    DataRow temp = checkrow[0];
                    dataGridView_Write(temp, dataGridView5, n, i);
                  
                }

                j++;
            }

            d[0].Clear();
        }

        // dataGridView6 Set ===========================================================================

        private void T3_dataGridView6_Main_Process()
        {
            Cursor.Current = Cursors.WaitCursor;

            List<string> Condition = new List<string>();
            Condition.Add("22");

            d[0] = CommonDB.R_datatbl(Qry.KSWMS430_001(Condition));

            dataGridView_Clear(dataGridView6);
            dataGridView_Set(dataGridView6, COL_Count_2);

            int j = 0;

            while (j < ROW_Count)
            {
                int n = dataGridView6.Rows.Add();

                dataGridView6.Rows[n].HeaderCell.Value = (18 - n).ToString();

                for (int i = 0; i < COL_Count_2; i++)
                {
                    string dtqry;
                    if (i < 9)
                    {
                        dtqry = "LOC = " + "22" + "0" + (i + 1).ToString();
                        if ((18 - n) < 10) dtqry += "0" + (18 - n).ToString();
                        else dtqry += (18 - n).ToString();
                    }
                    else
                    {
                        dtqry = "LOC = " + "22" + (i + 1).ToString();
                        if ((18 - n) < 10) dtqry += "0" + (18 - n).ToString();
                        else dtqry += (18 - n).ToString();
                    }

                    DataRow[] checkrow = d[0].Select(dtqry);
                    DataRow temp = checkrow[0];
                    dataGridView_Write(temp, dataGridView6, n, i);
                }

                j++;
            }
            d[0].Clear();
        }

        // dataGridView7 Set ===========================================================================

        private void T4_dataGridView7_Main_Process()
        {
            Cursor.Current = Cursors.WaitCursor;

            List<string> Condition = new List<string>();
            Condition.Add("23");

            d[0] = CommonDB.R_datatbl(Qry.KSWMS430_001(Condition));

            dataGridView_Clear(dataGridView7);
            dataGridView_Set(dataGridView7, COL_Count_2);

            int j = 0;

            while (j < ROW_Count)
            {
                int n = dataGridView7.Rows.Add();

                dataGridView7.Rows[n].HeaderCell.Value = (18 - n).ToString();

                for (int i = 0; i < COL_Count_2; i++)
                {
                    string dtqry;
                    if (i < 9)
                    {
                        dtqry = "LOC = " + "23" + "0" + (i + 1).ToString();
                        if ((18 - n) < 10) dtqry += "0" + (18 - n).ToString();
                        else dtqry += (18 - n).ToString();
                    }
                    else
                    {
                        dtqry = "LOC = " + "23" + (i + 1).ToString();
                        if ((18 - n) < 10) dtqry += "0" + (18 - n).ToString();
                        else dtqry += (18 - n).ToString();
                    }

                    DataRow[] checkrow = d[0].Select(dtqry);
                    DataRow temp = checkrow[0];

                    dataGridView_Write(temp, dataGridView7, n, i);
                }

                j++;
            }
        }

        // dataGridView8 Set ===========================================================================

        private void T4_dataGridView8_Main_Process()
        {
            Cursor.Current = Cursors.WaitCursor;

            List<string> Condition = new List<string>();
            Condition.Add("24");

            d[0] = CommonDB.R_datatbl(Qry.KSWMS430_001(Condition));

            dataGridView_Clear(dataGridView8);
            dataGridView_Set(dataGridView8, COL_Count_2);

            int j = 0;

            while (j < ROW_Count)
            {
                int n = dataGridView8.Rows.Add();

                dataGridView8.Rows[n].HeaderCell.Value = (18 - n).ToString();

                for (int i = 0; i < COL_Count_2; i++)
                {
                    string dtqry;
                    if (i < 9)
                    {
                        dtqry = "LOC = " + "24" + "0" + (i + 1).ToString();
                        if ((18 - n) < 10) dtqry += "0" + (18 - n).ToString();
                        else dtqry += (18 - n).ToString();
                    }
                    else
                    {
                        dtqry = "LOC = " + "24" + (i + 1).ToString();
                        if ((18 - n) < 10) dtqry += "0" + (18 - n).ToString();
                        else dtqry += (18 - n).ToString();
                    }

                    DataRow[] checkrow = d[0].Select(dtqry);
                    DataRow temp = checkrow[0];

                    dataGridView_Write(temp, dataGridView8, n, i);
                }

                j++;
            }
        }

        // dataGridView9 Set ===========================================================================

        private void T5_dataGridView9_Main_Process()
        {
            Cursor.Current = Cursors.WaitCursor;

            List<string> Condition = new List<string>();
            Condition.Add("31");

            d[0] = CommonDB.R_datatbl(Qry.KSWMS430_001(Condition));

            dataGridView_Clear(dataGridView9);
            dataGridView_Set(dataGridView9, COL_Count_3);

            int j = 0;

            while (j < ROW_Count)
            {
                int n = dataGridView9.Rows.Add();

                dataGridView9.Rows[n].HeaderCell.Value = (18 - n).ToString();

                for (int i = 0; i < COL_Count_3; i++)
                {
                    string dtqry;
                    if (i < 9)
                    {
                        dtqry = "LOC = " + "31" + "0" + (i + 1).ToString();
                        if ((18 - n) < 10) dtqry += "0" + (18 - n).ToString();
                        else dtqry += (18 - n).ToString();
                    }
                    else
                    {
                        dtqry = "LOC = " + "31" + (i + 1).ToString();
                        if ((18 - n) < 10) dtqry += "0" + (18 - n).ToString();
                        else dtqry += (18 - n).ToString();
                    }

                    DataRow[] checkrow = d[0].Select(dtqry);
                    DataRow temp = checkrow[0];

                    dataGridView_Write(temp, dataGridView9, n, i);
                }

                j++;
            }
        }

        // dataGridView10 Set ===========================================================================

        private void T5_dataGridView10_Main_Process()
        {
            Cursor.Current = Cursors.WaitCursor;

            List<string> Condition = new List<string>();
            Condition.Add("32");

            d[0] = CommonDB.R_datatbl(Qry.KSWMS430_001(Condition));

            dataGridView_Clear(dataGridView10);
            dataGridView_Set(dataGridView10, COL_Count_3);

            int j = 0;

            while (j < ROW_Count)
            {
                int n = dataGridView10.Rows.Add();

                dataGridView10.Rows[n].HeaderCell.Value = (18 - n).ToString();

                for (int i = 0; i < COL_Count_3; i++)
                {
                    string dtqry;
                    if (i < 9)
                    {
                        dtqry = "LOC = " + "32" + "0" + (i + 1).ToString();
                        if ((18 - n) < 10) dtqry += "0" + (18 - n).ToString();
                        else dtqry += (18 - n).ToString();
                    }
                    else
                    {
                        dtqry = "LOC = " + "32" + (i + 1).ToString();
                        if ((18 - n) < 10) dtqry += "0" + (18 - n).ToString();
                        else dtqry += (18 - n).ToString();
                    }

                    DataRow[] checkrow = d[0].Select(dtqry);
                    DataRow temp = checkrow[0];

                    dataGridView_Write(temp, dataGridView10, n, i);
                }

                j++;
            }
        }

        // dataGridView11 Set ===========================================================================

        private void T6_dataGridView11_Main_Process()
        {
            Cursor.Current = Cursors.WaitCursor;

            List<string> Condition = new List<string>();
            Condition.Add("33");

            d[0] = CommonDB.R_datatbl(Qry.KSWMS430_001(Condition));

            dataGridView_Clear(dataGridView11);
            dataGridView_Set(dataGridView11, COL_Count_3);

            int j = 0;

            while (j < ROW_Count)
            {
                int n = dataGridView11.Rows.Add();

                dataGridView11.Rows[n].HeaderCell.Value = (18 - n).ToString();

                for (int i = 0; i < COL_Count_3; i++)
                {
                    string dtqry;
                    if (i < 9)
                    {
                        dtqry = "LOC = " + "33" + "0" + (i + 1).ToString();
                        if ((18 - n) < 10) dtqry += "0" + (18 - n).ToString();
                        else dtqry += (18 - n).ToString();
                    }
                    else
                    {
                        dtqry = "LOC = " + "33" + (i + 1).ToString();
                        if ((18 - n) < 10) dtqry += "0" + (18 - n).ToString();
                        else dtqry += (18 - n).ToString();
                    }

                    DataRow[] checkrow = d[0].Select(dtqry);
                    DataRow temp = checkrow[0];

                    dataGridView_Write(temp, dataGridView11, n, i);
                }

                j++;
            }
        }

        // dataGridView12 Set ===========================================================================

        private void T6_dataGridView12_Main_Process()
        {
            Cursor.Current = Cursors.WaitCursor;

            List<string> Condition = new List<string>();
            Condition.Add("34");

            d[0] = CommonDB.R_datatbl(Qry.KSWMS430_001(Condition));

            dataGridView_Clear(dataGridView12);
            dataGridView_Set(dataGridView12, COL_Count_3);

            int j = 0;

            while (j < ROW_Count)
            {
                int n = dataGridView12.Rows.Add();

                dataGridView12.Rows[n].HeaderCell.Value = (18 - n).ToString();

                for (int i = 0; i < COL_Count_3; i++)
                {
                    string dtqry;
                    if (i < 9)
                    {
                        dtqry = "LOC = " + "34" + "0" + (i + 1).ToString();
                        if ((18 - n) < 10) dtqry += "0" + (18 - n).ToString();
                        else dtqry += (18 - n).ToString();
                    }
                    else
                    {
                        dtqry = "LOC = " + "34" + (i + 1).ToString();
                        if ((18 - n) < 10) dtqry += "0" + (18 - n).ToString();
                        else dtqry += (18 - n).ToString();
                    }

                    DataRow[] checkrow = d[0].Select(dtqry);
                    DataRow temp = checkrow[0];

                    dataGridView_Write(temp, dataGridView12, n, i);
                }

                j++;
            }
        }

        // dataGridView13 Set ===========================================================================

        private void T7_dataGridView13_Main_Process()
        {
            Cursor.Current = Cursors.WaitCursor;

            List<string> Condition = new List<string>();
            Condition.Add("41");

            d[0] = CommonDB.R_datatbl(Qry.KSWMS430_001(Condition));

            dataGridView_Clear(dataGridView13);
            dataGridView_Set(dataGridView13, COL_Count_4);

            int j = 0;

            while (j < ROW_Count)
            {
                int n = dataGridView13.Rows.Add();

                dataGridView13.Rows[n].HeaderCell.Value = (18 - n).ToString();

                for (int i = 0; i < COL_Count_4; i++)
                {
                    string dtqry;
                    if (i < 9)
                    {
                        dtqry = "LOC = " + "41" + "0" + (i + 1).ToString();
                        if ((18 - n) < 10) dtqry += "0" + (18 - n).ToString();
                        else dtqry += (18 - n).ToString();
                    }
                    else
                    {
                        dtqry = "LOC = " + "41" + (i + 1).ToString();
                        if ((18 - n) < 10) dtqry += "0" + (18 - n).ToString();
                        else dtqry += (18 - n).ToString();
                    }

                    DataRow[] checkrow = d[0].Select(dtqry);
                    DataRow temp = checkrow[0];

                    dataGridView_Write(temp, dataGridView13, n, i);
                }

                j++;
            }
        }

        // dataGridView14 Set ===========================================================================

        private void T7_dataGridView14_Main_Process()
        {
            Cursor.Current = Cursors.WaitCursor;

            List<string> Condition = new List<string>();
            Condition.Add("42");

            d[0] = CommonDB.R_datatbl(Qry.KSWMS430_001(Condition));

            dataGridView_Clear(dataGridView14);
            dataGridView_Set(dataGridView14, COL_Count_4);

            int j = 0;

            while (j < ROW_Count)
            {
                int n = dataGridView14.Rows.Add();

                dataGridView14.Rows[n].HeaderCell.Value = (18 - n).ToString();

                for (int i = 0; i < COL_Count_4; i++)
                {
                    string dtqry;
                    if (i < 9)
                    {
                        dtqry = "LOC = " + "42" + "0" + (i + 1).ToString();
                        if ((18 - n) < 10) dtqry += "0" + (18 - n).ToString();
                        else dtqry += (18 - n).ToString();
                    }
                    else
                    {
                        dtqry = "LOC = " + "42" + (i + 1).ToString();
                        if ((18 - n) < 10) dtqry += "0" + (18 - n).ToString();
                        else dtqry += (18 - n).ToString();
                    }

                    DataRow[] checkrow = d[0].Select(dtqry);
                    DataRow temp = checkrow[0];

                    dataGridView_Write(temp, dataGridView14, n, i);
                }

                j++;
            }
        }

        // dataGridView15 Set ===========================================================================

        private void T8_dataGridView15_Main_Process()
        {
            Cursor.Current = Cursors.WaitCursor;

            List<string> Condition = new List<string>();
            Condition.Add("43");

            d[0] = CommonDB.R_datatbl(Qry.KSWMS430_001(Condition));

            dataGridView_Clear(dataGridView15);
            dataGridView_Set(dataGridView15, COL_Count_4);

            int j = 0;

            while (j < ROW_Count)
            {
                int n = dataGridView15.Rows.Add();

                dataGridView15.Rows[n].HeaderCell.Value = (18 - n).ToString();

                for (int i = 0; i < COL_Count_4; i++)
                {
                    string dtqry;
                    if (i < 9)
                    {
                        dtqry = "LOC = " + "43" + "0" + (i + 1).ToString();
                        if ((18 - n) < 10) dtqry += "0" + (18 - n).ToString();
                        else dtqry += (18 - n).ToString();
                    }
                    else
                    {
                        dtqry = "LOC = " + "43" + (i + 1).ToString();
                        if ((18 - n) < 10) dtqry += "0" + (18 - n).ToString();
                        else dtqry += (18 - n).ToString();
                    }

                    DataRow[] checkrow = d[0].Select(dtqry);
                    DataRow temp = checkrow[0];

                    dataGridView_Write(temp, dataGridView15, n, i);
                }

                j++;
            }
        }


        // dataGridView16 Set ===========================================================================
        private void T8_dataGridView16_Main_Process()
        {
            Cursor.Current = Cursors.WaitCursor;

            List<string> Condition = new List<string>();
            Condition.Add("44");

            d[0] = CommonDB.R_datatbl(Qry.KSWMS430_001(Condition));

            dataGridView_Clear(dataGridView16);
            dataGridView_Set(dataGridView16, COL_Count_4);

            int j = 0;

            while (j < ROW_Count)
            {
                int n = dataGridView16.Rows.Add();

                dataGridView16.Rows[n].HeaderCell.Value = (18 - n).ToString();

                for (int i = 0; i < COL_Count_4; i++)
                {
                    string dtqry;
                    if (i < 9)
                    {
                        dtqry = "LOC = " + "44" + "0" + (i + 1).ToString();
                        if ((18 - n) < 10) dtqry += "0" + (18 - n).ToString();
                        else dtqry += (18 - n).ToString();
                    }
                    else
                    {
                        dtqry = "LOC = " + "44" + (i + 1).ToString();
                        if ((18 - n) < 10) dtqry += "0" + (18 - n).ToString();
                        else dtqry += (18 - n).ToString();
                    }

                    DataRow[] checkrow = d[0].Select(dtqry);
                    DataRow temp = checkrow[0];

                    dataGridView_Write(temp, dataGridView16, n, i);
                }

                j++;
            }
        }

        // Common ===========================================================================
        private void dataGridView_Clear(DataGridView dgv)
        {
            // 그리드 초기화
            dgv.DataSource = null;
            dgv.Columns.Clear();
        }

        private void dataGridView_Set(DataGridView dgv, int COL_Count)
        {
            //this.Controls.Add(dataGridView1);

            //dataGridView1.ColumnCount = d[0].Columns.Count;
            dgv.ColumnCount = COL_Count;

            dgv.Columns[0].Name = "01";
            dgv.Columns[1].Name = "02";
            dgv.Columns[2].Name = "03";
            dgv.Columns[3].Name = "04";
            dgv.Columns[4].Name = "05";
            dgv.Columns[5].Name = "06";
            dgv.Columns[6].Name = "07";
            dgv.Columns[7].Name = "08";
            dgv.Columns[8].Name = "09";
            dgv.Columns[9].Name = "10";
            dgv.Columns[10].Name = "11";
            dgv.Columns[11].Name = "12";
            dgv.Columns[12].Name = "13";
            dgv.Columns[13].Name = "14";
            dgv.Columns[14].Name = "15";
            dgv.Columns[15].Name = "16";
            dgv.Columns[16].Name = "17";
            dgv.Columns[17].Name = "18";
            dgv.Columns[18].Name = "19";
            dgv.Columns[19].Name = "20";
            dgv.Columns[20].Name = "21";
            dgv.Columns[21].Name = "22";
            dgv.Columns[22].Name = "23";
            dgv.Columns[23].Name = "24";
            dgv.Columns[24].Name = "25";
            dgv.Columns[25].Name = "26";
            dgv.Columns[26].Name = "27";
            dgv.Columns[27].Name = "28";
            dgv.Columns[28].Name = "29";
            dgv.Columns[29].Name = "30";
            dgv.Columns[30].Name = "31";
            dgv.Columns[31].Name = "32";
            dgv.Columns[32].Name = "33";
            dgv.Columns[33].Name = "34";
            dgv.Columns[34].Name = "35";
            dgv.Columns[35].Name = "36";
            dgv.Columns[36].Name = "37";
            dgv.Columns[37].Name = "38";
            dgv.Columns[38].Name = "39";
            dgv.Columns[39].Name = "40";
            dgv.Columns[40].Name = "41";
            dgv.Columns[41].Name = "42";
            dgv.Columns[42].Name = "43";
            dgv.Columns[43].Name = "44";
            dgv.Columns[44].Name = "45";
            dgv.Columns[45].Name = "46";
            dgv.Columns[46].Name = "47";
            dgv.Columns[47].Name = "48";
            dgv.Columns[48].Name = "49";
            dgv.Columns[49].Name = "50";


            dgv.Columns[50].Name = "51";
           /* if (dgv.Name.ToString() != "dataGridView16" && dgv.Name.ToString() != "dataGridView15" && dgv.Name.ToString() != "dataGridView14" && dgv.Name.ToString() != "dataGridView13")
            {
                dgv.Columns[50].Name = "51";
                dgv.Columns[51].Name = "52";
                dgv.Columns[52].Name = "53";

                if (dgv.Name.ToString() != "dataGridView12" && dgv.Name.ToString() != "dataGridView11" && dgv.Name.ToString() != "dataGridView10" && dgv.Name.ToString() != "dataGridView9")
                {
                    dgv.Columns[53].Name = "54";
                    dgv.Columns[54].Name = "55";
                    dgv.Columns[55].Name = "56";

                    if (dgv.Name.ToString() != "dataGridView8" && dgv.Name.ToString() != "dataGridView7" && dgv.Name.ToString() != "dataGridView6" && dgv.Name.ToString() != "dataGridView5")
                    {
                        dgv.Columns[56].Name = "57";
                        dgv.Columns[57].Name = "58";
                        dgv.Columns[58].Name = "59";
                    }
                }
            }*/
            
            //dataGridView1.Columns[24].Visible = false;

            //for (int i = 0; i <= 7; i++)
            //{
            //    dataGridView1.Columns[i].Frozen = true;
            //}

            //this.dataGridView1.Columns["암호"].Visible = false;
            //this.dataGridView1.Columns["권한"].Visible = false;

            // Columns AutoSize
            //dGVProcess.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells;

            // Head Cell_Columns Font Style = off
            dgv.EnableHeadersVisualStyles = false;
            // Head Cell_Columns HeightSizeMode = off
            dgv.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.DisableResizing;

            // Head Cell_Columns HeightSize 임의 지정
            dgv.ColumnHeadersHeight = 30;

            // Head Rows Control
            //dataGridView1.RowHeadersVisible = false;
            //dataGridView1.RowHeadersWidthSizeMode = DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            // RowHeadersWidth_Size 임의 지정
            dgv.RowHeadersWidth = 55;
            dgv.RowHeadersDefaultCellStyle.Font = new Font("맑은고딕", 10, FontStyle.Bold);
            dgv.RowHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            //for (int j = 0; j < d[0].Columns.Count; j++)
            for (int j = 0; j < COL_Count; j++)
            {
                dgv.Columns[j].HeaderCell.Style.Font = new Font("맑은고딕", 10, FontStyle.Bold);
                //dGVProcess.Columns[j].HeaderCell.Style.ForeColor = Color.Red;
                //dataGridView1.Columns[j].HeaderCell.Style.BackColor = Color.Yellow;

                // dGVState.Columns[j].HeaderCell.Size = new Size();

                // Head Cell_Columns Alignment
                //dataGridView1.Columns[j].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
                //if (j < 1) dataGridView1.Columns[j].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
                //else dataGridView1.Columns[j].HeaderCell.Style.Alignment = DataGridViewContentAlignment.BottomCenter;

                dgv.Columns[j].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;

                // Cell Font Style
                dgv.Columns[j].DefaultCellStyle.Font = new Font("맑은고딕", 10, FontStyle.Bold);
                //dGVProcess.Columns[j].DefaultCellStyle.Font = new Font("Tahoma", 18, FontStyle.Bold);

                // Cell Font Alignment
                //if (j == 6)
                //{
                //    dataGridView1.Columns[j].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                //}
                //else
                //{
                //    dataGridView1.Columns[j].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                //
                //}

                //  Cell Height 지정
                //  var dgv = new DataGridView();
                //dataGridView1.RowTemplate.Height = 30;


                // Head Cell_Columns Width Size 임의 지정
                DataGridViewColumn column1 = dgv.Columns[0]; column1.Width = 25;
                DataGridViewColumn column2 = dgv.Columns[1]; column2.Width = 25;
                DataGridViewColumn column3 = dgv.Columns[2]; column3.Width = 25;
                DataGridViewColumn column4 = dgv.Columns[3]; column4.Width = 25;
                DataGridViewColumn column5 = dgv.Columns[4]; column5.Width = 25;
                DataGridViewColumn column6 = dgv.Columns[5]; column6.Width = 25;
                DataGridViewColumn column7 = dgv.Columns[6]; column7.Width = 25;
                DataGridViewColumn column8 = dgv.Columns[7]; column8.Width = 25;
                DataGridViewColumn column9 = dgv.Columns[8]; column9.Width = 25;
                DataGridViewColumn column10 = dgv.Columns[9]; column10.Width = 25;
                DataGridViewColumn column11 = dgv.Columns[10]; column11.Width = 25;
                DataGridViewColumn column12 = dgv.Columns[11]; column12.Width = 25;
                DataGridViewColumn column13 = dgv.Columns[12]; column13.Width = 25;
                DataGridViewColumn column14 = dgv.Columns[13]; column14.Width = 25;
                DataGridViewColumn column15 = dgv.Columns[14]; column15.Width = 25;
                DataGridViewColumn column16 = dgv.Columns[15]; column16.Width = 25;
                DataGridViewColumn column17 = dgv.Columns[16]; column17.Width = 25;
                DataGridViewColumn column18 = dgv.Columns[17]; column18.Width = 25;
                DataGridViewColumn column19 = dgv.Columns[18]; column19.Width = 25;
                DataGridViewColumn column20 = dgv.Columns[19]; column20.Width = 25;
                DataGridViewColumn column21 = dgv.Columns[20]; column21.Width = 25;
                DataGridViewColumn column22 = dgv.Columns[21]; column22.Width = 25;
                DataGridViewColumn column23 = dgv.Columns[22]; column23.Width = 25;
                DataGridViewColumn column24 = dgv.Columns[23]; column24.Width = 25;
                DataGridViewColumn column25 = dgv.Columns[24]; column25.Width = 25;
                DataGridViewColumn column26 = dgv.Columns[25]; column26.Width = 25;
                DataGridViewColumn column27 = dgv.Columns[26]; column27.Width = 25;
                DataGridViewColumn column28 = dgv.Columns[27]; column28.Width = 25;
                DataGridViewColumn column29 = dgv.Columns[28]; column29.Width = 25;
                DataGridViewColumn column30 = dgv.Columns[29]; column30.Width = 25;
                DataGridViewColumn column31 = dgv.Columns[30]; column31.Width = 25;
                DataGridViewColumn column32 = dgv.Columns[31]; column32.Width = 25;
                DataGridViewColumn column33 = dgv.Columns[32]; column33.Width = 25;
                DataGridViewColumn column34 = dgv.Columns[33]; column34.Width = 25;
                DataGridViewColumn column35 = dgv.Columns[34]; column35.Width = 25;
                DataGridViewColumn column36 = dgv.Columns[35]; column36.Width = 25;
                DataGridViewColumn column37 = dgv.Columns[36]; column37.Width = 25;
                DataGridViewColumn column38 = dgv.Columns[37]; column38.Width = 25;
                DataGridViewColumn column39 = dgv.Columns[38]; column39.Width = 25;
                DataGridViewColumn column40 = dgv.Columns[39]; column40.Width = 25;
                DataGridViewColumn column41 = dgv.Columns[40]; column41.Width = 25;
                DataGridViewColumn column42 = dgv.Columns[41]; column42.Width = 25;
                DataGridViewColumn column43 = dgv.Columns[42]; column43.Width = 25;
                DataGridViewColumn column44 = dgv.Columns[43]; column44.Width = 25;
                DataGridViewColumn column45 = dgv.Columns[44]; column45.Width = 25;
                DataGridViewColumn column46 = dgv.Columns[45]; column46.Width = 25;
                DataGridViewColumn column47 = dgv.Columns[46]; column47.Width = 25;
                DataGridViewColumn column48 = dgv.Columns[47]; column48.Width = 25;
                DataGridViewColumn column49 = dgv.Columns[48]; column49.Width = 25;
                DataGridViewColumn column50 = dgv.Columns[49]; column50.Width = 25;

                DataGridViewColumn column51 = dgv.Columns[50]; column51.Width = 25;
                /*if (dgv.Name.ToString() != "dataGridView16" && dgv.Name.ToString() != "dataGridView15" && dgv.Name.ToString() != "dataGridView14" && dgv.Name.ToString() != "dataGridView13")
                {
                    DataGridViewColumn column51 = dgv.Columns[50]; column51.Width = 25;
                    DataGridViewColumn column52 = dgv.Columns[51]; column52.Width = 25;
                    DataGridViewColumn column53 = dgv.Columns[52]; column53.Width = 25;

                    if (dgv.Name.ToString() != "dataGridView12" && dgv.Name.ToString() != "dataGridView11" && dgv.Name.ToString() != "dataGridView10" && dgv.Name.ToString() != "dataGridView9")
                    {
                        DataGridViewColumn column54 = dgv.Columns[53]; column54.Width = 25;
                        DataGridViewColumn column55 = dgv.Columns[54]; column55.Width = 25;
                        DataGridViewColumn column56 = dgv.Columns[55]; column56.Width = 25;

                        if (dgv.Name.ToString() != "dataGridView8" && dgv.Name.ToString() != "dataGridView7" && dgv.Name.ToString() != "dataGridView6" && dgv.Name.ToString() != "dataGridView5")
                        {
                            DataGridViewColumn column57 = dgv.Columns[56]; column57.Width = 25;
                            DataGridViewColumn column58 = dgv.Columns[57]; column58.Width = 25;
                            DataGridViewColumn column59 = dgv.Columns[58]; column59.Width = 25;
                        }
                    }
                }*/
            }
        }
        
        private void dataGridView_Write(DataRow temp, DataGridView dgv, int n, int i)
        {
            if (temp["LUG"].ToString() == "1" && temp["USE"].ToString() == "O") dgv.Rows[n].Cells[i].Value = "P";
            else if (temp["USE"].ToString() == "X") dgv.Rows[n].Cells[i].Style.BackColor = Color.Red;
            else if (temp["LUG"].ToString() == "2") dgv.Rows[n].Cells[i].Value = "E";
            else dgv.Rows[n].Cells[i].Value = "";
        }
    }
}
