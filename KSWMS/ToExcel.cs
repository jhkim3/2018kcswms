﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using excel = Microsoft.Office.Interop.Excel;
using System.Reflection;
using System.Diagnostics;
using System.Windows.Forms;

namespace KSWMS
{
    class ToExcel
    {
        public static void WirteExcel(DataGridView list, string filename)
        {
            DateTime now = DateTime.Now;

            excel.Application xls;
            excel._Workbook wbook;
            excel._Worksheet wSheet;
            string[] Alpabat = new string[]{"A","B","C","D","E","F","G","H","I","J","K",
                                            "L","M","N","O","P","Q","R","S","T","U","V",
                                            "W","X","Y","Z","AA","AB","AC","AD","AE","AF",
                                            "AG","AH","AI","AJ","AK","AL","AM","AN","AO",
                                            "AP","AQ","AR","AS","AT","AU","AV","AW","AX",
                                            "AY","AZ"};
            List<string> col = new List<string>();
            try
            {
                string[] header = new string[list.Columns.Count];
                int Colcount = 0;
                foreach (DataGridViewColumn s in list.Columns)
                {
                    header[Colcount] = s.Name.ToString();
                    Colcount++;
                }
                string[,] item = new string[list.Rows.Count, list.Columns.Count];
                for (int i = 0; i < list.Rows.Count; i++)
                {
                    for (int j = 0; j < list.Columns.Count; j++)
                    {
                        item[i, j] = list.Rows[i].Cells[j].Value.ToString();
                    }
                }

                xls = new excel.Application();
                wbook = (excel._Workbook)(xls.Workbooks.Add(Missing.Value));
                wSheet = (excel._Worksheet)wbook.ActiveSheet;

                wSheet.get_Range("A1", Alpabat[list.Columns.Count - 1] + "1").Value2 = header;
                wSheet.get_Range("A2", Alpabat[list.Columns.Count - 1] + list.Rows.Count.ToString()).Value2 = item;
                xls.Visible = false;
                xls.UserControl = false;
                object TypMissing = Type.Missing;
                wSheet.Columns.AutoFit();

                //==========================================================================================
                var saveFileDialoge = new SaveFileDialog();
                saveFileDialoge.FileName = filename + "_" +
                                           now.Year.ToString() + "_" +
                                           now.Month.ToString() + "_" +
                                           now.Day.ToString();//+ now.ToString().Remove(10,11);
                // saveFileDialoge.FileName = "PartProcess";
                saveFileDialoge.DefaultExt = ".xlsx";
                if (saveFileDialoge.ShowDialog() == DialogResult.OK)
                {
                    //wbook.SaveAs(saveFileDialoge.FileName,  excel.XlFileFormat.xlWorkbookNormal, TypMissing, TypMissing, TypMissing, TypMissing, excel.XlSaveAsAccessMode.xlNoChange, TypMissing, TypMissing, TypMissing, TypMissing, TypMissing);
                    wbook.SaveAs(saveFileDialoge.FileName, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Microsoft.Office.Interop.Excel.XlSaveAsAccessMode.xlExclusive, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing);
                }

                //wbook.SaveAs("D:\tset.xls", excel.XlFileFormat.xlWorkbookNormal, TypMissing, TypMissing,
                //   TypMissing, TypMissing, excel.XlSaveAsAccessMode.xlNoChange,
                //   TypMissing, TypMissing, TypMissing, TypMissing, TypMissing);
                //wbook.Save();
                //===========================================================================================

                Process[] ExCel = Process.GetProcessesByName("EXCEL");
                if (ExCel.Length != 0)
                {
                    for (int j = 0; j < ExCel.Length; j++)
                    {
                        ExCel[j].Kill();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
