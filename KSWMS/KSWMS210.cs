﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Printing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace KSWMS
{
    public partial class KSWMS210 : MyFormPage
    {
        private Main fmain;
        private TabPage TabP;
        private DataTable[] d = new DataTable[5];
        private Rectangle dataGridView1_r1, dataGridView1_r2, dataGridView1_r3, dataGridView1_r4, dataGridView1_r5, dataGridView1_r6;
        private Rectangle dataGridView2_r1, dataGridView2_r2, dataGridView2_r3, dataGridView2_r4, dataGridView2_r5, dataGridView2_r6;
        private Rectangle dataGridView3_r1, dataGridView3_r2, dataGridView3_r3, dataGridView3_r4, dataGridView3_r5, dataGridView3_r6;
        private Rectangle dataGridView4_r1, dataGridView4_r2, dataGridView4_r3;
        private Rectangle dataGridView5_r1, dataGridView5_r2, dataGridView5_r3;

        private int[] dataGridView1_DoubleHead_width = new int[50];
        private int[] dataGridView2_DoubleHead_width = new int[50];
        private int[] dataGridView3_DoubleHead_width = new int[50];
        private int[] dataGridView4_DoubleHead_width = new int[50];
        private int[] dataGridView5_DoubleHead_width = new int[50];

        private string SEQ_WMS_NO, SEQ_RSV_NO;
        
        public KSWMS210(Main _main)
        {
            InitializeComponent();
            this.pnl = panel1;
            this.btn1 = button1;
            this.btn2 = button2;
            this.btn3 = button3;
            this.btn4 = button4;


            cmb_Set();
            fmain = _main;

            Common.setTextBoxEnter(textBox21, button20);
            Common.setTextBoxEnter(textBox22, button20);
        }
        
        private void tabControl1_MouseClick(object sender, MouseEventArgs e)
        {
            SetTabP_Ctl();
        }
        
        private void SetTabP_Ctl()
        {
            T1_dataGridView1_Clear();
            T2_dataGridView2_Clear();
            T3_dataGridView4_Clear();
            T3_dataGridView5_Clear();

            TabP = tabControl1.SelectedTab;
            if (TabP != null)
            {
                if (TabP.Name.ToString() == "tabPage1")
                {
                    
                    T1_dataGridView1_Main_Process();
                    fmain.AutoInPut_ORD_Btn();
                }
                else if (TabP.Name.ToString() == "tabPage2")
                {
                    T2_dataGridView2_Main_Process();
                    T2_dataGridView3_Main_Process();
                    fmain.AutoInPut_WRK_Btn();
                }
                else if (TabP.Name.ToString() == "tabPage3")
                {
                    T3_dataGridView4_Main_Process();
                    T3_dataGridView5_Main_Process();
                    fmain.AutoInPut_RSV_Btn();
                }
                //MessageBox.Show(TabP.Controls[2].ToString());
            }
        }

        private void Insert_After()
        {
            SetTabP_Ctl();

            //dataGridView1 포커스(FOCUS)위치 세팅 후 dataGridView1_CellDoubleClick
            if (dataGridView1.RowCount > 0)
            {
                dataGridView1.CurrentCell = dataGridView1.Rows[dataGridView1.RowCount - 1].Cells[1];
                dataGridView1_CellDoubleClick(this.dataGridView1, new DataGridViewCellEventArgs(this.dataGridView1.CurrentCell.ColumnIndex, this.dataGridView1.CurrentRow.Index));
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            dataGridView4.Size = new Size(Convert.ToInt32(panel1.Size.Width.ToString()) - 10, Convert.ToInt32(panel1.Size.Height.ToString())/2 - 75);
            dataGridView5.Location = new Point(0, (Convert.ToInt32(panel1.Size.Height.ToString())/ 2) + 20);
            dataGridView5.Size = new Size(Convert.ToInt32(panel1.Size.Width.ToString()) - 10, Convert.ToInt32(panel1.Size.Height.ToString()) / 2 - 55);
            button18.Location = new Point(550, Convert.ToInt32(panel1.Size.Height.ToString()) / 2 - 13);
            button19.Location = new Point(630, Convert.ToInt32(panel1.Size.Height.ToString()) / 2 - 13);
            //button19.Location = new Point(550, 270);
            //button20.Location = new Point(630, 270);
            //MessageBox.Show(panel1.Size.Width.ToString() +"     "+ panel1.Size.Height.ToString());

            dataGridView2.Size = new Size(Convert.ToInt32(panel1.Size.Width.ToString()) - 10, Convert.ToInt32(panel1.Size.Height.ToString()) / 2 - 65);
            panel3.Location = new Point(0, (Convert.ToInt32(panel1.Size.Height.ToString()) / 2) - 20);
            dataGridView3.Location = new Point(0, (Convert.ToInt32(panel1.Size.Height.ToString()) / 2) + 35);
            dataGridView3.Size = new Size(Convert.ToInt32(panel1.Size.Width.ToString()) - 10, Convert.ToInt32(panel1.Size.Height.ToString()) / 2 - 70);

            panel4.Location = new Point(240, (Convert.ToInt32(panel1.Size.Height.ToString()) / 2) - 75);

            SetTabP_Ctl();
            
        }
        private void button2_Click(object sender, EventArgs e)
        {
            SetTabP_Ctl();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            TabP = tabControl1.SelectedTab;
            if (TabP != null)
            {
                if (TabP.Name.ToString() == "tabPage1")
                {
                    textBox90.Text = "";
                    textBox91.Text = "";

                    textBox1.Text = "";
                    textBox2.Text = "";
                    textBox3.Text = "";
                    textBox4.Text = "";
                    textBox5.Text = "";
                    textBox6.Text = "";
                    textBox7.Text = "";
                    textBox8.Text = "";
                    textBox9.Text = "";
                    textBox10.Text = "";
                    textBox11.Text = "";
                    textBox12.Text = "";
                    textBox13.Text = "";
                    textBox14.Text = "";
                    textBox15.Text = "";
                    textBox16.Text = "";
                    textBox17.Text = "";
                    textBox18.Text = "";
                    comboBox1.SelectedIndex = 0;
                    comboBox2.SelectedIndex = 0;
                    dateTimePicker1.Value = DateTime.Now.Date;
                    dateTimePicker2.Value = DateTime.Now.Date;
                    dateTimePicker3.Value = DateTime.Now.Date;
                }
                else if (TabP.Name.ToString() == "tabPage2")
                {
                    dateTimePicker4.Value = DateTime.Now.Date;
                    dateTimePicker5.Value = DateTime.Now.Date;
                    comboBox3.SelectedIndex = 0;
                    checkBox1.Checked = false;
                    checkBox2.Checked = false;
                    textBox19.Text = "0";
                }
                else if (TabP.Name.ToString() == "tabPage3")
                {

                }

                SetTabP_Ctl();
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            TabP = tabControl1.SelectedTab;

            if (TabP != null)
            {
                if (TabP.Name.ToString() == "tabPage1")
                {
                    MessageBox.Show("오더내역 출력");
                }
                else if (TabP.Name.ToString() == "tabPage2")
                {
                    if (dataGridView3.RowCount <= 0) MessageBox.Show("출력할 작업이 없습니다. 확인하세요!", "확인!", MessageBoxButtons.OK, (MessageBoxIcon)64);
                    else
                    {
                        DialogResult dr = MessageBox.Show("바코드 출력을 하시겠습니까?",
                       "출력 확인", MessageBoxButtons.YesNo);
                        switch (dr)
                        {
                            case DialogResult.Yes:
                                
                                // BAR CODE PRINT ================================================================================
                                string prtcommand;
                                PrintDialog pd = new PrintDialog();
                                pd.PrinterSettings = new PrinterSettings();
                                if (DialogResult.OK == pd.ShowDialog(this))
                                {
                                     for (int c = 0; c < dataGridView3.RowCount; c++)
                                     {
                                         prtcommand = "#!A1#DC#IMNR99.99/35.00#HV80#PR2/2/#ERN/1//0#R0/0#T3#J10#YB8/0M/20/5///" +
                                             dataGridView3.Rows[c].Cells[17].Value.ToString() + "/" + Regex.Replace(dataGridView3.Rows[c].Cells[5].Value.ToString(), @"\D", "") +
                                             "#G#Q1#G#!P1";
                                         RawPrinterHelper.SendStringToPrinter(pd.PrinterSettings.PrinterName, prtcommand);
                                     }
                                    // UPDATE TABINORD TABINRSV 예약을 출력으로 변경====================================================
                                    Insert_TAB210();   // 진한 위치 이동, 프린터 출력 취소시에는 처리되지 않게 하기 위해
                                }
                                break;
                            case DialogResult.No:
                                break;
                        }
                    }
                }
                else if (TabP.Name.ToString() == "tabPage3")
                {

                }
            }
        }

        private void button20_Click(object sender, EventArgs e)
        {
            T2_dataGridView3_Main_Process();
        }

        private void button14_Click(object sender, EventArgs e)
        {
            if (textBox1.Text == "" || textBox2.Text == "" || textBox3.Text == "") MessageBox.Show("관리정보를 확인하세요!", "확인!", MessageBoxButtons.OK, (MessageBoxIcon)64);
            else if (textBox4.Text == "" || textBox5.Text == "" || textBox6.Text == "") MessageBox.Show("화주정보를 확인하세요!", "확인!", MessageBoxButtons.OK, (MessageBoxIcon)64);
            else if (textBox7.Text == "" || textBox8.Text == "" || textBox10.Text == "") MessageBox.Show("제품정보를 확인하세요!", "확인!", MessageBoxButtons.OK, (MessageBoxIcon)64);
            else if (textBox11.Text == "" || textBox12.Text == "") MessageBox.Show("선박정보를 확인하세요!", "확인!", MessageBoxButtons.OK, (MessageBoxIcon)64);
            else if (textBox13.Text == "") MessageBox.Show("총수량을 확인하세요!", "확인!", MessageBoxButtons.OK, (MessageBoxIcon)64);
            else if (textBox14.Text == "") MessageBox.Show("B/L NO. 를 확인하세요!", "확인!", MessageBoxButtons.OK, (MessageBoxIcon)64);
            else if (textBox15.Text == "") MessageBox.Show("화물관리를 확인하세요!", "확인!", MessageBoxButtons.OK, (MessageBoxIcon)64);
            //else if (textBox16.Text == "") MessageBox.Show("표시를 확인하세요!", "오류~~");
            else if (textBox91.Text == "예약" || textBox91.Text == "출력") MessageBox.Show("예약중이거나 출력된 자료는 생성/수정 하실수 없습니다.", "오류~~", MessageBoxButtons.OK, (MessageBoxIcon)16);
            else
            {
                List<string> Condition = new List<string>();

                Condition.Add(dateTimePicker1.Value.Year.ToString());                    //  0
                Condition.Add(dateTimePicker1.Value.Month.ToString());                   //  1
                Condition.Add(dateTimePicker1.Value.Day.ToString());                     //  2
                 
                Condition.Add(dateTimePicker2.Value.Year.ToString());                    //  3
                Condition.Add(dateTimePicker2.Value.Month.ToString());                   //  4
                Condition.Add(dateTimePicker2.Value.Day.ToString());                     //  5

                Condition.Add(textBox1.Text);                                            //  6
                Condition.Add(textBox2.Text);                                            //  7
                Condition.Add(textBox3.Text);                                            //  8
                Condition.Add(textBox4.Text);                                            //  9  
                Condition.Add(textBox5.Text);                                            //  10
                Condition.Add(textBox6.Text);                                            //  11
                Condition.Add(textBox7.Text);                                            //  12
                Condition.Add(textBox8.Text);                                            //  13
                Condition.Add(textBox9.Text);                                            //  14
                Condition.Add(textBox10.Text);                                           //  15
                Condition.Add(textBox11.Text);                                           //  16
                Condition.Add(textBox12.Text);                                           //  17
                Condition.Add(textBox13.Text);                                           //  18
                Condition.Add(textBox14.Text);                                           //  19
                Condition.Add(textBox15.Text);                                           //  20
                Condition.Add(textBox16.Text);                                           //  21
                Condition.Add(textBox17.Text);                                           //  22
                Condition.Add(textBox18.Text);                                           //  23
                Condition.Add(fmain.u_id);                                               //  24

                if (textBox90.Text != "")
                {
                    // UPDATE

                    Condition.Add(textBox90.Text);                                       //  25

                    d[0] = CommonDB.R_datatbl(Qry.KSWMS210_003(Condition));

                    if (d[0].Rows[0][0].ToString() == "1")
                    {
                        MessageBox.Show("WMS_NO: " + textBox90.Text + " 수정된 자료가 없습니다.", "확인!", MessageBoxButtons.OK, (MessageBoxIcon)64);
                    }
                    else
                    {
                        DialogResult dr = MessageBox.Show("WMS_NO: " + textBox90.Text + " 정보를 수정 하시겠습니까?.",
                              "오더생성여부", MessageBoxButtons.YesNo, (MessageBoxIcon)32);
                        switch (dr)
                        {
                            case DialogResult.Yes:
                                //MessageBox.Show(Qry.KSWMS210_004(Condition));
                                CommonDB.R_datatbl(Qry.KSWMS210_004(Condition));
                                button2.PerformClick();
                                break;
                            case DialogResult.No:
                                break;
                        }
                    }
                }
                else
                {
                    // INSERT

                    Creat_WMS_NO();
                    Condition.Add(SEQ_WMS_NO);                                       //  25
                    //MessageBox.Show(Condition[25]);

                    DialogResult dr = MessageBox.Show("WMS_NO: " + SEQ_WMS_NO + " 정보를 등록 하시겠습니까?",
                        "오더생성여부", MessageBoxButtons.YesNo, (MessageBoxIcon)32);
                    switch (dr)
                    {
                        case DialogResult.Yes:
                            //textBox20.Text = Qry.KSWMS210_005(Condition);
                            //MessageBox.Show(Qry.KSWMS210_005(Condition));
                            CommonDB.R_datatbl(Qry.KSWMS210_005(Condition));
                            Insert_After();

                            break;
                        case DialogResult.No:
                            break;
                    }
                }
            }
        }
        
        private void button15_Click(object sender, EventArgs e)
        {
            List<string> Condition = new List<string>();
            
            Condition.Add(dataGridView1.CurrentRow.Cells[0].Value.ToString());

            if (dataGridView1.CurrentRow.Cells[4].Value.ToString() == "대기") 
            {
                Condition.Add("0");
                DialogResult dr = MessageBox.Show("WMS_NO: " + dataGridView1.CurrentRow.Cells[0].Value.ToString() + " 의 정보를 삭제 하시겠습니까?",
                    "사용자관리", MessageBoxButtons.YesNo);
                switch (dr)
                {
                    case DialogResult.Yes:
                        d[0] = CommonDB.R_datatbl(Qry.KSWMS210_006(Condition));

                        textBox90.Text = "";
                        textBox91.Text = "";

                        textBox1.Text = "";
                        textBox2.Text = "";
                        textBox3.Text = "";
                        textBox4.Text = "";
                        textBox5.Text = "";
                        textBox6.Text = "";
                        textBox7.Text = "";
                        textBox8.Text = "";
                        textBox9.Text = "";
                        textBox10.Text = "";
                        textBox11.Text = "";
                        textBox12.Text = "";
                        textBox13.Text = "";
                        textBox14.Text = "";
                        textBox15.Text = "";
                        textBox16.Text = "";
                        textBox17.Text = "";
                        textBox18.Text = "";

                        button2.PerformClick();

                        break;
                    case DialogResult.No:
                        break;
                }
            }
            else if (dataGridView1.CurrentRow.Cells[4].Value.ToString() == "출력") MessageBox.Show("출력된 자료는 삭제 하실수 없습니다.", "오류~~", MessageBoxButtons.OK, (MessageBoxIcon)16);
            else MessageBox.Show("대기 또는 출력이 아닙니다.", "오류~~", MessageBoxButtons.OK, (MessageBoxIcon)16);
        }

        private void Creat_WMS_NO()
        {
            SEQ_WMS_NO = null;
            SEQ_WMS_NO = dateTimePicker1.Value.Year.ToString().Remove(0, 2);
            if (dateTimePicker1.Value.Month.ToString() == "10") SEQ_WMS_NO += "A";
            else if (dateTimePicker1.Value.Month.ToString() == "11") SEQ_WMS_NO += "B";
            else if (dateTimePicker1.Value.Month.ToString() == "12") SEQ_WMS_NO += "C";
            else SEQ_WMS_NO += dateTimePicker1.Value.Month.ToString();
            
            if (dateTimePicker1.Value.Day < 10)
            {
                SEQ_WMS_NO += "0";
                SEQ_WMS_NO += dateTimePicker1.Value.Day.ToString();
            }
            else  SEQ_WMS_NO += dateTimePicker1.Value.Day.ToString();

            List<string> Condition = new List<string>();

            Condition.Add(SEQ_WMS_NO);                    //  0

            d[0] = CommonDB.R_datatbl(Qry.KSWMS210_010(Condition));
            // MessageBox.Show(d[0].Rows[0][0].ToString());
            if (d[0].Rows[0][0].ToString() == "")
            {
                SEQ_WMS_NO += "0001";
            }
            else
            {
                SEQ_WMS_NO += string.Format("{0}", (Convert.ToInt32(d[0].Rows[0][0].ToString().Remove(0, 5)) + 1).ToString("0000"));
            }
        }

        private void Creat_RSV_NO()
        {
            SEQ_RSV_NO = null;
            SEQ_RSV_NO = dataGridView2.CurrentRow.Cells[0].Value.ToString().Remove(5, 4);
            List<string> Condition = new List<string>();

            Condition.Add(SEQ_RSV_NO);                    //  0

            d[0] = CommonDB.R_datatbl(Qry.KSWMS210_011(Condition));
            // MessageBox.Show(d[0].Rows[0][0].ToString());
            if (d[0].Rows[0][0].ToString() == "")
            {
                SEQ_RSV_NO += "0001";
            }
            else
            {
                SEQ_RSV_NO += string.Format("{0}", (Convert.ToInt32(d[0].Rows[0][0].ToString().Remove(0, 5)) + 1).ToString("0000"));
            }
        }

        private void button16_Click(object sender, EventArgs e)
        {
            //MessageBox.Show(Regex.Replace(dataGridView2.CurrentRow.Cells[9].Value.ToString(), @"\D", ""));
            if (dataGridView2.RowCount <= 0) MessageBox.Show("예약할 입고 작업이 없습니다. 확인하세요!", "확인!", MessageBoxButtons.OK, (MessageBoxIcon)64);
            else if (dataGridView2.CurrentRow.Cells[3].Value.ToString() == "" || dataGridView2.CurrentRow.Cells[3].Value.ToString() == "00000") MessageBox.Show("화주코드를 확인하세요!", "확인!", MessageBoxButtons.OK, (MessageBoxIcon)64);
            else if (dataGridView2.CurrentRow.Cells[19].Value.ToString() == "" || dataGridView2.CurrentRow.Cells[19].Value.ToString() == "00000") MessageBox.Show("관리코드를 확인하세요!", "확인!", MessageBoxButtons.OK, (MessageBoxIcon)64);
            else if (dataGridView2.CurrentRow.Cells[22].Value.ToString() == "" || dataGridView2.CurrentRow.Cells[22].Value.ToString() == "00000") MessageBox.Show("선박코드를 확인하세요!", "확인!", MessageBoxButtons.OK, (MessageBoxIcon)64);
            else if (dataGridView2.CurrentRow.Cells[9].Value.ToString() == "" || dataGridView2.CurrentRow.Cells[9].Value.ToString() == "0") MessageBox.Show("총수량을 확인하세요!", "확인!", MessageBoxButtons.OK, (MessageBoxIcon)64);
            else if (checkBox1.Checked == true) MessageBox.Show("선택발행 체크를 해지해 주세요!", "확인!", MessageBoxButtons.OK, (MessageBoxIcon)64);
            else if (dataGridView3.RowCount > 0) MessageBox.Show("예약작업이 진행중에 있습니다.", "오류~~", MessageBoxButtons.OK, (MessageBoxIcon)16);
            else if (textBox19.Text == "" || textBox19.Text == "0") MessageBox.Show("파레트 수량을 확인하세요.", "오류~~", MessageBoxButtons.OK, (MessageBoxIcon)16);
            else if (Convert.ToInt32(textBox19.Text) > Convert.ToInt32(Regex.Replace(dataGridView2.CurrentRow.Cells[9].Value.ToString(), @"\D", ""))) MessageBox.Show("파레트 수량이 총수량보다 큽니다.", "오류~~", MessageBoxButtons.OK, (MessageBoxIcon)16);
            else
            {
                //MessageBox.Show((Convert.ToInt32(Regex.Replace(dataGridView2.CurrentRow.Cells[9].Value.ToString(), @"\D", "")) % Convert.ToInt32(textBox19.Text)).ToString());
                
                DialogResult dr = MessageBox.Show("WMS_NO: " + dataGridView2.CurrentRow.Cells[0].Value.ToString() + " 입고 예약 하시겠습니까?",
                       "예약생성여부", MessageBoxButtons.YesNo, (MessageBoxIcon)32);
                switch (dr)
                {
                    case DialogResult.Yes:
                        
                        List<string> Condition = new List<string>();

                        // INSERT TABINRSV==========================================================================
                        Creat_RSV_NO();

                        int i = 0;
                        int k;

                        if((Convert.ToInt32(Regex.Replace(dataGridView2.CurrentRow.Cells[9].Value.ToString(), @"\D", "")) % Convert.ToInt32(textBox19.Text)) == 0) k = Convert.ToInt32(Regex.Replace(dataGridView2.CurrentRow.Cells[9].Value.ToString(), @"\D", "")) / Convert.ToInt32(textBox19.Text) - 1;
                        else k = Convert.ToInt32(Regex.Replace(dataGridView2.CurrentRow.Cells[9].Value.ToString(), @"\D", "")) / Convert.ToInt32(textBox19.Text);

                        // 20180718 진한 추가 
                        d[0] = CommonDB.R_datatbl(Qry.KSWMS210_012);
                        int emptyRackCount = Convert.ToInt32(d[0].Rows[0][0]) - k;
                        
                        if( Common.m_InputLimit > emptyRackCount )
                        {
                            double rate = Common.m_InputLimit / 147;
                            MessageBox.Show("창고 공간이 " + rate.ToString() + "% 남아 입고 작업을 진행 하지 못하였습니다.");
                            return;
                        }

                        while (i <= k)
                        {
                            Condition.Add(dataGridView2.CurrentRow.Cells[0].Value.ToString());                  // 0     
                            Condition.Add((i + 1).ToString());                                                  // 1                                                                                                         

                            if (i != (Convert.ToInt32(Regex.Replace(dataGridView2.CurrentRow.Cells[9].Value.ToString(), @"\D", "")) / Convert.ToInt32(textBox19.Text))) Condition.Add(textBox19.Text);       //  2
                            else Condition.Add((Convert.ToInt32(Regex.Replace(dataGridView2.CurrentRow.Cells[9].Value.ToString(), @"\D", "")) % Convert.ToInt32(textBox19.Text)).ToString());                //  2
                            
                            if (i == 0) Condition.Add(SEQ_RSV_NO);                                                                                                    //  3
                            else Condition.Add(SEQ_RSV_NO.Remove(5, 4) + string.Format("{0}", (Convert.ToInt32(SEQ_RSV_NO.Remove(0, 5)) + i).ToString("0000")));      //  3
                              
                            Condition.Add(fmain.u_id);                                                          // 4
                            
                            //textBox20.Text = Qry.KSWMS210_008(Condition);
                            CommonDB.R_datatbl(Qry.KSWMS210_008(Condition));
                        
                            //리스트 초기화
                            Condition.Clear();

                            i++;
                        }
                        
                        // UPDATE TABINORD 예약으로 변경==================================================================
                        Condition.Add(dataGridView2.CurrentRow.Cells[0].Value.ToString());                  // 0  
                        Condition.Add("1");                                                                 // 1
                        CommonDB.R_datatbl(Qry.KSWMS210_008_01(Condition));
                        
                        break;

                    case DialogResult.No:
                        break;
                }

                T2_dataGridView2_Main_Process();
                T2_dataGridView3_Main_Process();
            }
        }

        private void button17_Click(object sender, EventArgs e)
        {
            List<string> Condition = new List<string>();
            
            if (dataGridView3.RowCount <= 0) MessageBox.Show("예약 취소할 작업이 없습니다. 확인하세요!", "확인!", MessageBoxButtons.OK, (MessageBoxIcon)64);
            else if (dataGridView3.CurrentRow.Cells[3].Value.ToString() != "예약") MessageBox.Show("예약된 자료 아닙니다 확인하세요!.", "오류~~", MessageBoxButtons.OK, (MessageBoxIcon)16);
            else if (dataGridView3.CurrentRow.Cells[3].Value.ToString() == "예약")
            {
                Condition.Add(dataGridView3.CurrentRow.Cells[0].Value.ToString());
                Condition.Add("0");
                DialogResult dr = MessageBox.Show("WMS_NO: "  + dataGridView3.CurrentRow.Cells[0].Value.ToString()  + " 의 예약을 취소 하시겠습니까?",
                    "입고 예약 취소 확인", MessageBoxButtons.YesNo);
                switch (dr)
                {
                    case DialogResult.Yes:
                        CommonDB.R_datatbl(Qry.KSWMS210_008_02(Condition));

                        // UPDATE TABINORD 예약을 다시 대기로 변경==================================================================
                        Condition.Add(dataGridView3.CurrentRow.Cells[0].Value.ToString());                  // 0  
                        Condition.Add("0");                                                                 // 1
                        CommonDB.R_datatbl(Qry.KSWMS210_008_01(Condition));

                        textBox19.Text = "0";

                        break;
                    case DialogResult.No:
                        break;
                }
            }
            else MessageBox.Show("오류 입니다. 관리자에게 문의하세요.", "오류~~", MessageBoxButtons.OK, (MessageBoxIcon)16);
            
            T2_dataGridView2_Main_Process();
            T2_dataGridView3_Main_Process();
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox1.Checked == true) panel4.Visible = true;
            else panel4.Visible = false;

            button20.PerformClick();
        }
        
        private void textBox13_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!(Char.IsDigit(e.KeyChar)) && e.KeyChar != 8) e.Handled = true;
        }

        private void textBox19_KeyPress(object sender, KeyPressEventArgs e)
        {
            //숫자,백스페이스,마이너스,소숫점 만 입력받는다.
            //if (!(Char.IsDigit(e.KeyChar)) && e.KeyChar != 8 && e.KeyChar != 45 && e.KeyChar != 46) //8:백스페이스,45:마이너스,46:소수점
            //{
            //    e.Handled = true;
            //}

            if (!(Char.IsDigit(e.KeyChar)) && e.KeyChar != 8) e.Handled = true;
        }

        private void Insert_TAB210()
        {
            //MessageBox.Show(dataGridView3.RowCount.ToString() + "   " + dataGridView3.CurrentRow.Cells[0].Value.ToString());
            d[0] = CommonDB.R_datatbl(Qry.KSWMS210_012);
            if (checkBox1.Checked == false)
            {
                if (dataGridView3.RowCount > Convert.ToInt32(d[0].Rows[0][0])) MessageBox.Show("창고 LOCATION이 부족합니다.", "확인!", MessageBoxButtons.OK, (MessageBoxIcon)64);
                else
                {
                    // UPDATE TABINORD TABINRSV 예약을 출력으로 변경==================================================================

                    List<string> Condition = new List<string>();
                    Condition.Add(fmain.u_id);                                                          // 0
                    Condition.Add(dataGridView3.CurrentRow.Cells[0].Value.ToString());                  // 1  
                    CommonDB.R_datatbl(Qry.KSWMS210_013(Condition));
                    CommonDB.R_datatbl(Qry.KSWMS210_014(Condition));
                    //textBox19.Text = "0";
                }
            }
            else
            {
                List<string> Condition = new List<string>();
                              
                for(int i = 0; i < dataGridView3.RowCount; i++)
                {
                    //MessageBox.Show(dataGridView3.Rows[i].Cells[17].Value.ToString()+"     "+ Regex.Replace(dataGridView3.Rows[i].Cells[3].Value.ToString(), @"\D", ""));
                    Condition.Add(fmain.u_id);                                                                                                        // 0
                    Condition.Add(dataGridView3.Rows[i].Cells[17].Value.ToString());                                                                  // 1
                    Condition.Add(Regex.Replace(dataGridView3.Rows[i].Cells[3].Value.ToString(), @"\D", ""));                                         // 2     
                    Condition.Add((Convert.ToInt32(Regex.Replace(dataGridView3.Rows[i].Cells[3].Value.ToString(), @"\D", "")) + 1).ToString());       // 3                                                                                                         
                    
                    CommonDB.R_datatbl(Qry.KSWMS210_015(Condition));
                    
                    //리스트 초기화
                    Condition.Clear();
                }

            }

            T2_dataGridView2_Main_Process();
            T2_dataGridView3_Main_Process();
        }

        private void T1_dataGridView1_Main_Process()
        {
            Cursor.Current = Cursors.WaitCursor;


            List<string> Condition = new List<string>();
            
            Condition.Add(dateTimePicker3.Value.Year.ToString());
            Condition.Add(dateTimePicker3.Value.Month.ToString());
            Condition.Add(dateTimePicker3.Value.Day.ToString());

            //  "A-전체", "0-대기", "1-예약", "2-출력"
            if (comboBox1.SelectedIndex == 1)      Condition.Add("0");
            else if(comboBox1.SelectedIndex == 2)  Condition.Add("1");
            else if(comboBox1.SelectedIndex == 3)  Condition.Add("2");
            else                                   Condition.Add("A");

            //  "A-전체", "11-고려오더", "12-생성오더", "13-수동오더"
            if (comboBox2.SelectedIndex == 1)      Condition.Add("11");
            else if (comboBox2.SelectedIndex == 2) Condition.Add("12");
            else if (comboBox2.SelectedIndex == 3) Condition.Add("13");
            else                                   Condition.Add("A");

            d[0] = CommonDB.R_datatbl(Qry.KSWMS210_001(Condition));
            
            T1_dataGridView1_Clear();
            dataGridView1Set();
            //MessageBox.Show(dataGridView1.Columns.Clear.ToString());

            int j = 0;

            while (j < d[0].Rows.Count)
            {
                int n = dataGridView1.Rows.Add();

                for (int i = 0; i <= d[0].Columns.Count - 1; i++)
                {
                    dataGridView1.Rows[n].Cells[i].Value = d[0].Rows[j][i].ToString();

                    //if (i == 0)
                    //{
                    //    dataGridView1.Rows[n].Cells[i].Value = (j + 1).ToString();
                    //}
                    //else if (i == 18)
                    //{
                    //    if (d[0].Rows[j][14].ToString() != "")
                    //    {
                    //        dataGridView1.Rows[n].Cells[i].Value = "0";
                    //    }
                    //    else
                    //    {
                    //        dataGridView1.Rows[n].Cells[i].Value = d[0].Rows[j][i - 1].ToString();
                    //    }
                    //}
                    //else
                    //{
                    //    dataGridView1.Rows[n].Cells[i].Value = d[0].Rows[j][i - 1].ToString();
                    //}

                    /*
                    if (dataGridView1.Rows[n].Cells[i].Value.Equals("EST"))
                    {
                        dataGridView1.Rows[n].DefaultCellStyle.BackColor = Color.Red;
                        dataGridView1.Rows[n].DefaultCellStyle.ForeColor = Color.White;
                    }
                    */
                }

                j++;
            }

            label18.Text = j.ToString();
            //statusBar1.Panels[0].Text = "전체 " + j.ToString() + " 건이 검색 되었습니다.";
        }

        
        private void T1_dataGridView1_Clear()
        {
            // 그리드 초기화
            dataGridView1.DataSource = null;
            dataGridView1.Columns.Clear();
        }

        private void dataGridView1Set()
        {
            //this.Controls.Add(dataGridView1);

            dataGridView1.ColumnCount = d[0].Columns.Count;
           
            dataGridView1.Columns[0].Name = "WMS NO";
            dataGridView1.Columns[1].Name = "최초재고";
            dataGridView1.Columns[2].Name = "입고일";
            dataGridView1.Columns[3].Name = "구역";
            dataGridView1.Columns[4].Name = "상태";
            dataGridView1.Columns[5].Name = "유형";
            dataGridView1.Columns[6].Name = "총수량";
            dataGridView1.Columns[7].Name = "B/L NO";
            dataGridView1.Columns[8].Name = "코드";
            dataGridView1.Columns[9].Name = "상호";
            dataGridView1.Columns[10].Name = "담당";
            dataGridView1.Columns[11].Name = "코드";
            dataGridView1.Columns[12].Name = "제품명";
            dataGridView1.Columns[13].Name = "규격";
            dataGridView1.Columns[14].Name = "단위";
            dataGridView1.Columns[15].Name = "표시";
            dataGridView1.Columns[16].Name = "비고";
            dataGridView1.Columns[17].Name = "화물관리번호";
            dataGridView1.Columns[18].Name = "차량";
            dataGridView1.Columns[19].Name = "코드";
            dataGridView1.Columns[20].Name = "상호";
            dataGridView1.Columns[21].Name = "담당";
            dataGridView1.Columns[22].Name = "코드";
            dataGridView1.Columns[23].Name = "선박명";
            dataGridView1.Columns[24].Name = "통관일";

            dataGridView1.Columns[24].Visible = false;

            for (int i = 0; i <= 7 ; i++)
            {
                dataGridView1.Columns[i].Frozen = true;
            }
              
            //this.dataGridView1.Columns["암호"].Visible = false;
            //this.dataGridView1.Columns["권한"].Visible = false;

            // Columns AutoSize
            //dGVProcess.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells;

            // Head Cell_Columns Font Style = off
            dataGridView1.EnableHeadersVisualStyles = false;
            // Head Cell_Columns HeightSizeMode = off
            dataGridView1.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.DisableResizing;

            // Head Cell_Columns HeightSize 임의 지정
            dataGridView1.ColumnHeadersHeight = 60;

            // Head Rows Control
            //dataGridView1.RowHeadersVisible = false;
            //dataGridView1.RowHeadersWidthSizeMode = DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            // RowHeadersWidth_Size 임의 지정
            dataGridView1.RowHeadersWidth = 20;
            
         

            for (int j = 0; j < d[0].Columns.Count; j++)
            {
                dataGridView1.Columns[j].HeaderCell.Style.Font = new Font("맑은고딕", 10, FontStyle.Bold);
                //dGVProcess.Columns[j].HeaderCell.Style.ForeColor = Color.Red;
                //dataGridView1.Columns[j].HeaderCell.Style.BackColor = Color.Yellow;

                // dGVState.Columns[j].HeaderCell.Size = new Size();

                // Head Cell_Columns Alignment
                //dataGridView1.Columns[j].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
                //if (j < 1) dataGridView1.Columns[j].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
                //else dataGridView1.Columns[j].HeaderCell.Style.Alignment = DataGridViewContentAlignment.BottomCenter;

                dataGridView1.Columns[j].HeaderCell.Style.Alignment = DataGridViewContentAlignment.BottomCenter;

                // Cell Font Style
                //dGVProcess.Columns[j].DefaultCellStyle.Font = new Font("Tahoma", 18, FontStyle.Bold);

                // Cell Font Alignment
                if (j == 6)
                {
                    dataGridView1.Columns[j].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                }
                //else
                //{
                //    dataGridView1.Columns[j].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                //
                //}

                //  Cell Height 지정
                //  var dgv = new DataGridView();
                //dataGridView1.RowTemplate.Height = 30;


                // Head Cell_Columns Width Size 임의 지정
                DataGridViewColumn column1 = dataGridView1.Columns[0];
                column1.Width = 100;
                DataGridViewColumn column2 = dataGridView1.Columns[1];
                column2.Width = 100;
                DataGridViewColumn column3 = dataGridView1.Columns[2];
                column3.Width = 100;
                DataGridViewColumn column4 = dataGridView1.Columns[3];
                column4.Width = 60;
                DataGridViewColumn column5 = dataGridView1.Columns[4];
                column5.Width = 60;
                DataGridViewColumn column6 = dataGridView1.Columns[5];
                column6.Width = 80;
                DataGridViewColumn column7 = dataGridView1.Columns[6];
                column7.Width = 70;
                DataGridViewColumn column8 = dataGridView1.Columns[7];
                column8.Width = 150;
                DataGridViewColumn column9 = dataGridView1.Columns[8];
                column9.Width = 60;
                DataGridViewColumn column10 = dataGridView1.Columns[9];
                column10.Width = 180;
                DataGridViewColumn column11 = dataGridView1.Columns[10];
                column11.Width = 60;
                DataGridViewColumn column12 = dataGridView1.Columns[11];
                column12.Width = 60;
                DataGridViewColumn column13 = dataGridView1.Columns[12];
                column13.Width = 200;
                DataGridViewColumn column14 = dataGridView1.Columns[13];
                column14.Width = 60;
                DataGridViewColumn column15 = dataGridView1.Columns[14];
                column15.Width = 60;
                DataGridViewColumn column16 = dataGridView1.Columns[15];
                column16.Width = 60;
                DataGridViewColumn column17 = dataGridView1.Columns[16];
                column17.Width = 150;
                DataGridViewColumn column18 = dataGridView1.Columns[17];
                column18.Width = 150;
                DataGridViewColumn column19 = dataGridView1.Columns[18];
                column19.Width = 80;
                DataGridViewColumn column20 = dataGridView1.Columns[19];
                column20.Width = 60;
                DataGridViewColumn column21 = dataGridView1.Columns[20];
                column21.Width = 180;
                DataGridViewColumn column22 = dataGridView1.Columns[21];
                column22.Width = 60;
                DataGridViewColumn column23 = dataGridView1.Columns[22];
                column23.Width = 60;
                DataGridViewColumn column24 = dataGridView1.Columns[23];
                column24.Width = 120;

                dataGridView1_r1 = dataGridView1.GetCellDisplayRectangle(0, -1, false);
                dataGridView1_DoubleHead_width[0] = dataGridView1.GetCellDisplayRectangle(1, -1, false).Width;
                dataGridView1_DoubleHead_width[1] = dataGridView1.GetCellDisplayRectangle(2, -1, false).Width;
                dataGridView1_DoubleHead_width[2] = dataGridView1.GetCellDisplayRectangle(3, -1, false).Width;
                dataGridView1_DoubleHead_width[3] = dataGridView1.GetCellDisplayRectangle(4, -1, false).Width;
                dataGridView1_DoubleHead_width[4] = dataGridView1.GetCellDisplayRectangle(5, -1, false).Width;
                dataGridView1_DoubleHead_width[5] = dataGridView1.GetCellDisplayRectangle(6, -1, false).Width;
                dataGridView1_DoubleHead_width[6] = dataGridView1.GetCellDisplayRectangle(7, -1, false).Width;


                dataGridView1_r2 = dataGridView1.GetCellDisplayRectangle(8, -1, false);
                dataGridView1_DoubleHead_width[10] = dataGridView1.GetCellDisplayRectangle(9, -1, false).Width;
                dataGridView1_DoubleHead_width[11] = dataGridView1.GetCellDisplayRectangle(10, -1, false).Width;

                dataGridView1_r3 = dataGridView1.GetCellDisplayRectangle(11, -1, false);
                dataGridView1_DoubleHead_width[20] = dataGridView1.GetCellDisplayRectangle(12, -1, false).Width;
                dataGridView1_DoubleHead_width[21] = dataGridView1.GetCellDisplayRectangle(13, -1, false).Width;
                dataGridView1_DoubleHead_width[22] = dataGridView1.GetCellDisplayRectangle(14, -1, false).Width;

                dataGridView1_r4 = dataGridView1.GetCellDisplayRectangle(15, -1, false);
                dataGridView1_DoubleHead_width[23] = dataGridView1.GetCellDisplayRectangle(16, -1, false).Width;
                dataGridView1_DoubleHead_width[24] = dataGridView1.GetCellDisplayRectangle(17, -1, false).Width;
                dataGridView1_DoubleHead_width[25] = dataGridView1.GetCellDisplayRectangle(18, -1, false).Width;

                dataGridView1_r5 = dataGridView1.GetCellDisplayRectangle(19, -1, false);
                dataGridView1_DoubleHead_width[26] = dataGridView1.GetCellDisplayRectangle(20, -1, false).Width;
                dataGridView1_DoubleHead_width[27] = dataGridView1.GetCellDisplayRectangle(21, -1, false).Width;

                dataGridView1_r6 = dataGridView1.GetCellDisplayRectangle(22, -1, false);
                dataGridView1_DoubleHead_width[28] = dataGridView1.GetCellDisplayRectangle(23, -1, false).Width;

            }


        }

        private void dataGridView1_CellPainting(object sender, DataGridViewCellPaintingEventArgs e)
        {
            if (e.RowIndex == -1 && e.ColumnIndex > -1)

            {
                Rectangle r = e.CellBounds;

                r.Y += e.CellBounds.Height / 2;

                r.Height = e.CellBounds.Height / 2;

                e.PaintBackground(r, true);

                e.PaintContent(r);

                e.Handled = true;
            }
        }

        private void dataGridView1_ColumnWidthChanged(object sender, DataGridViewColumnEventArgs e)
        {
            //DataGridView gv = (DataGridView)sender;

            Rectangle rtHeader = dataGridView1.DisplayRectangle;

            rtHeader.Height = dataGridView1.ColumnHeadersHeight / 2;

            dataGridView1.Invalidate(rtHeader);
        }

        private void dataGridView1_Paint(object sender, PaintEventArgs e)
        {
            //DataGridView dataGridView1 = (DataGridView)sender;

            string[] strHeaders = { "작  업", "화  주", "제  품", "작업정보", "관  리", "선  박" };

            StringFormat format = new StringFormat();

            format.Alignment = StringAlignment.Center;

            format.LineAlignment = StringAlignment.Center;


            // Category Painting

            {
                dataGridView1_r1.X += 1;

                dataGridView1_r1.Y += 2;

                dataGridView1_r1.Width = dataGridView1_r1.Width + dataGridView1_DoubleHead_width[0] + 
                                         dataGridView1_DoubleHead_width[1] + dataGridView1_DoubleHead_width[2] + 
                                         dataGridView1_DoubleHead_width[3] + dataGridView1_DoubleHead_width[4] + 
                                         dataGridView1_DoubleHead_width[5] + dataGridView1_DoubleHead_width[6] - 2;

                dataGridView1_r1.Height = (dataGridView1_r1.Height / 2) - 2;

                e.Graphics.DrawRectangle(new Pen(dataGridView1.BackgroundColor), dataGridView1_r1);

                e.Graphics.FillRectangle(new SolidBrush(dataGridView1.ColumnHeadersDefaultCellStyle.BackColor),
                //e.Graphics.FillRectangle(new SolidBrush(Color.Yellow),

                dataGridView1_r1);


                e.Graphics.DrawString(strHeaders[0],

                    //dataGridView1.ColumnHeadersDefaultCellStyle.Font,
                    new Font("맑은고딕", 9, FontStyle.Bold),

                    new SolidBrush(dataGridView1.ColumnHeadersDefaultCellStyle.ForeColor),

                    dataGridView1_r1,

                    format);

            }

            // Projection Painting

            {
                dataGridView1_r2.X += 1;

                dataGridView1_r2.Y += 2;

                dataGridView1_r2.Width = dataGridView1_r2.Width + dataGridView1_DoubleHead_width[10] + 
                                         dataGridView1_DoubleHead_width[11] - 2;

                dataGridView1_r2.Height = (dataGridView1_r2.Height / 2) - 2;

                e.Graphics.DrawRectangle(new Pen(dataGridView1.BackgroundColor), dataGridView1_r2);

                e.Graphics.FillRectangle(new SolidBrush(dataGridView1.ColumnHeadersDefaultCellStyle.BackColor),
                //e.Graphics.FillRectangle(new SolidBrush(Color.Yellow),

                dataGridView1_r2);


                e.Graphics.DrawString(strHeaders[1],

                    //dataGridView1.ColumnHeadersDefaultCellStyle.Font,
                    new Font("맑은고딕", 9, FontStyle.Bold),

                    new SolidBrush(dataGridView1.ColumnHeadersDefaultCellStyle.ForeColor),

                    dataGridView1_r2,

                    format);
            }

            // 제품

            {
                dataGridView1_r3.X += 1;

                dataGridView1_r3.Y += 2;

                dataGridView1_r3.Width = dataGridView1_r3.Width + dataGridView1_DoubleHead_width[20] + dataGridView1_DoubleHead_width[21] + 
                                      dataGridView1_DoubleHead_width[22] - 2;

                dataGridView1_r3.Height = (dataGridView1_r3.Height / 2) - 2;

                e.Graphics.DrawRectangle(new Pen(dataGridView1.BackgroundColor), dataGridView1_r3);

                e.Graphics.FillRectangle(new SolidBrush(dataGridView1.ColumnHeadersDefaultCellStyle.BackColor),
                //e.Graphics.FillRectangle(new SolidBrush(Color.Yellow),

                dataGridView1_r3);


                e.Graphics.DrawString(strHeaders[2],

                    //dataGridView1.ColumnHeadersDefaultCellStyle.Font,
                    new Font("맑은고딕", 9, FontStyle.Bold),

                    new SolidBrush(dataGridView1.ColumnHeadersDefaultCellStyle.ForeColor),

                    dataGridView1_r3,

                    format);
            }

            // 작업정보

            {
                dataGridView1_r4.X += 1;

                dataGridView1_r4.Y += 2;

                dataGridView1_r4.Width = dataGridView1_r4.Width + dataGridView1_DoubleHead_width[23] + dataGridView1_DoubleHead_width[24] +
                                         dataGridView1_DoubleHead_width[25] - 2;

                dataGridView1_r4.Height = (dataGridView1_r4.Height / 2) - 2;

                e.Graphics.DrawRectangle(new Pen(dataGridView1.BackgroundColor), dataGridView1_r4);

                e.Graphics.FillRectangle(new SolidBrush(dataGridView1.ColumnHeadersDefaultCellStyle.BackColor),
                //e.Graphics.FillRectangle(new SolidBrush(Color.Yellow),

                dataGridView1_r4);


                e.Graphics.DrawString(strHeaders[3],

                    //dataGridView1.ColumnHeadersDefaultCellStyle.Font,
                    new Font("맑은고딕", 9, FontStyle.Bold),

                    new SolidBrush(dataGridView1.ColumnHeadersDefaultCellStyle.ForeColor),

                    dataGridView1_r4,

                    format);
            }

            // 관리

            {
                dataGridView1_r5.X += 1;

                dataGridView1_r5.Y += 2;

                dataGridView1_r5.Width = dataGridView1_r5.Width + dataGridView1_DoubleHead_width[26] + dataGridView1_DoubleHead_width[27] +
                                      dataGridView1_DoubleHead_width[28] - 2;

                dataGridView1_r5.Height = (dataGridView1_r5.Height / 2) - 2;

                e.Graphics.DrawRectangle(new Pen(dataGridView1.BackgroundColor), dataGridView1_r5);

                e.Graphics.FillRectangle(new SolidBrush(dataGridView1.ColumnHeadersDefaultCellStyle.BackColor),
                //e.Graphics.FillRectangle(new SolidBrush(Color.Yellow),

                dataGridView1_r5);


                e.Graphics.DrawString(strHeaders[4],

                    //dataGridView1.ColumnHeadersDefaultCellStyle.Font,
                    new Font("맑은고딕", 9, FontStyle.Bold),

                    new SolidBrush(dataGridView1.ColumnHeadersDefaultCellStyle.ForeColor),

                    dataGridView1_r5,

                    format);
            }

            // 선박

            {
                dataGridView1_r6.X += 1;

                dataGridView1_r6.Y += 2;

                dataGridView1_r6.Width = dataGridView1_r6.Width + dataGridView1_DoubleHead_width[28] - 2;

                dataGridView1_r6.Height = (dataGridView1_r6.Height / 2) - 2;

                e.Graphics.DrawRectangle(new Pen(dataGridView1.BackgroundColor), dataGridView1_r6);

                e.Graphics.FillRectangle(new SolidBrush(dataGridView1.ColumnHeadersDefaultCellStyle.BackColor),
                //e.Graphics.FillRectangle(new SolidBrush(Color.Yellow),

                dataGridView1_r6);


                e.Graphics.DrawString(strHeaders[5],

                    //dataGridView1.ColumnHeadersDefaultCellStyle.Font,
                    new Font("맑은고딕", 9, FontStyle.Bold),

                    new SolidBrush(dataGridView1.ColumnHeadersDefaultCellStyle.ForeColor),

                    dataGridView1_r6,

                    format);
            }
        }

        private void dataGridView1_Scroll(object sender, ScrollEventArgs e)
        {
            dataGridView1Set();

            //DataGridView gv = (DataGridView)sender;

            Rectangle rtHeader = dataGridView1.DisplayRectangle;

            rtHeader.Height = dataGridView1.ColumnHeadersHeight / 2;

            dataGridView1.Invalidate(rtHeader);
        }
        
        private void dataGridView1_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex < 0 || e.ColumnIndex != 0)
            {
                return;
            }
            
            dateTimePicker1.Value = Convert.ToDateTime(dataGridView1.Rows[e.RowIndex].Cells[2].Value);

            if (Regex.IsMatch(dataGridView1.Rows[e.RowIndex].Cells[24].FormattedValue.ToString(), @"^(19|20)\d{2}/(0[1-9]|1[012])/(0[1-9]|[12][0-9]|3[0-1])$")) dateTimePicker2.Value = Convert.ToDateTime(dataGridView1.Rows[e.RowIndex].Cells[24].Value);
            else
            {
                MessageBox.Show("통관일 없음!", "확인!", MessageBoxButtons.OK, (MessageBoxIcon)48);
                dateTimePicker2.Value = Convert.ToDateTime(dataGridView1.Rows[e.RowIndex].Cells[2].Value);
            }


            textBox90.Text = dataGridView1.Rows[e.RowIndex].Cells[0].FormattedValue.ToString();
            textBox91.Text = dataGridView1.Rows[e.RowIndex].Cells[4].FormattedValue.ToString();

            textBox1.Text = dataGridView1.Rows[e.RowIndex].Cells[19].FormattedValue.ToString();
            textBox2.Text = dataGridView1.Rows[e.RowIndex].Cells[20].FormattedValue.ToString();
            textBox3.Text = dataGridView1.Rows[e.RowIndex].Cells[21].FormattedValue.ToString();

            textBox4.Text = dataGridView1.Rows[e.RowIndex].Cells[8].FormattedValue.ToString();
            textBox5.Text = dataGridView1.Rows[e.RowIndex].Cells[9].FormattedValue.ToString();
            textBox6.Text = dataGridView1.Rows[e.RowIndex].Cells[10].FormattedValue.ToString();

            textBox7.Text = dataGridView1.Rows[e.RowIndex].Cells[11].FormattedValue.ToString();
            textBox8.Text = dataGridView1.Rows[e.RowIndex].Cells[12].FormattedValue.ToString();
            textBox9.Text = dataGridView1.Rows[e.RowIndex].Cells[13].FormattedValue.ToString();
            textBox10.Text = dataGridView1.Rows[e.RowIndex].Cells[14].FormattedValue.ToString();

            textBox11.Text = dataGridView1.Rows[e.RowIndex].Cells[22].FormattedValue.ToString();
            textBox12.Text = dataGridView1.Rows[e.RowIndex].Cells[23].FormattedValue.ToString();

            textBox13.Text = dataGridView1.Rows[e.RowIndex].Cells[6].FormattedValue.ToString();
            textBox14.Text = dataGridView1.Rows[e.RowIndex].Cells[7].FormattedValue.ToString();
            textBox15.Text = dataGridView1.Rows[e.RowIndex].Cells[17].FormattedValue.ToString();
            textBox16.Text = dataGridView1.Rows[e.RowIndex].Cells[15].FormattedValue.ToString();
            textBox17.Text = dataGridView1.Rows[e.RowIndex].Cells[18].FormattedValue.ToString();

            textBox18.Text = dataGridView1.Rows[e.RowIndex].Cells[16].FormattedValue.ToString();
        }



        // dataGridView2 Set ===========================================================================

        private void T2_dataGridView2_Main_Process()
        {
            Cursor.Current = Cursors.WaitCursor;
            
            List<string> Condition = new List<string>();

            Condition.Add(dateTimePicker4.Value.Year.ToString());
            Condition.Add(dateTimePicker4.Value.Month.ToString());
            Condition.Add(dateTimePicker4.Value.Day.ToString());

            //  "A-전체", "0-대기", "1-예약", "2-출력"
            //if (comboBox1.SelectedIndex == 1) Condition.Add("0");
            //else if (comboBox1.SelectedIndex == 2) Condition.Add("1");
            //else if (comboBox1.SelectedIndex == 3) Condition.Add("2");
            //else Condition.Add("A");

            //  "A-전체", "11-고려오더", "12-생성오더", "13-수동오더"
            if (comboBox3.SelectedIndex == 1) Condition.Add("11");
            else if (comboBox3.SelectedIndex == 2) Condition.Add("12");
            else if (comboBox3.SelectedIndex == 3) Condition.Add("13");
            else Condition.Add("A");
            
            d[1] = CommonDB.R_datatbl(Qry.KSWMS210_002(Condition));

            T2_dataGridView2_Clear();
            dataGridView2Set();

            int j = 0;
            
            while (j < d[1].Rows.Count)
            {
                int n = dataGridView2.Rows.Add();
               
                for (int i = 0; i <= d[1].Columns.Count - 1; i++)
                {
                    dataGridView2.Rows[j].Cells[i].Value = d[1].Rows[j][i].ToString();

                    //if (i == 0)
                    //{
                    //    dataGridView1.Rows[n].Cells[i].Value = (j + 1).ToString();
                    //}
                    //else if (i == 18)
                    //{
                    //    if (d[1].Rows[j][14].ToString() != "")
                    //    {
                    //        dataGridView1.Rows[n].Cells[i].Value = "0";
                    //    }
                    //    else
                    //    {
                    //        dataGridView1.Rows[n].Cells[i].Value = d[1].Rows[j][i - 1].ToString();
                    //    }
                    //}
                    //else
                    //{
                    //    dataGridView1.Rows[n].Cells[i].Value = d[1].Rows[j][i - 1].ToString();
                    //}

                    /*
                    if (dataGridView1.Rows[n].Cells[i].Value.Equals("EST"))
                    {
                        dataGridView1.Rows[n].DefaultCellStyle.BackColor = Color.Red;
                        dataGridView1.Rows[n].DefaultCellStyle.ForeColor = Color.White;
                    }
                    */
                }

                j++;
            }

            label19.Text = j.ToString();
            //statusBar1.Panels[0].Text = "전체 " + j.ToString() + " 건이 검색 되었습니다.";

        }
        private void T2_dataGridView2_Clear()
        {
            // 그리드 초기화
            dataGridView2.DataSource = null;
            dataGridView2.Columns.Clear();
        }

        private void dataGridView2Set()
        {
            //this.Controls.Add(dataGridView2);
            dataGridView2.ColumnCount = d[1].Columns.Count;
           
            dataGridView2.Columns[0].Name = "WMS NO";
            dataGridView2.Columns[1].Name = "입고일";
            dataGridView2.Columns[2].Name = "B/L NO";
            dataGridView2.Columns[3].Name = "코드";
            dataGridView2.Columns[4].Name = "상호";
            dataGridView2.Columns[5].Name = "담당";
            dataGridView2.Columns[6].Name = "코드";
            dataGridView2.Columns[7].Name = "제품명";
            dataGridView2.Columns[8].Name = "규격";
            dataGridView2.Columns[9].Name = "총수량";
            dataGridView2.Columns[10].Name = "단위";
            dataGridView2.Columns[11].Name = "표시";
            dataGridView2.Columns[12].Name = "비고";
            dataGridView2.Columns[13].Name = "화물관리번호";
            dataGridView2.Columns[14].Name = "최초재고";
            dataGridView2.Columns[15].Name = "구역";
            dataGridView2.Columns[16].Name = "상태";
            dataGridView2.Columns[17].Name = "유형";
            dataGridView2.Columns[18].Name = "차량";
            dataGridView2.Columns[19].Name = "코드";
            dataGridView2.Columns[20].Name = "상호";
            dataGridView2.Columns[21].Name = "담당";
            dataGridView2.Columns[22].Name = "코드";
            dataGridView2.Columns[23].Name = "선박명";

            for (int i = 0; i <= 2; i++)
            {
                dataGridView2.Columns[i].Frozen = true;
            }

            //this.dataGridView2.Columns["암호"].Visible = false;
            //this.dataGridView2.Columns["권한"].Visible = false;

            // Columns AutoSize
            //dGVProcess.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells;

            // Head Cell_Columns Font Style = off
            dataGridView2.EnableHeadersVisualStyles = false;
            // Head Cell_Columns HeightSizeMode = off
            dataGridView2.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.DisableResizing;

            // Head Cell_Columns HeightSize 임의 지정
            dataGridView2.ColumnHeadersHeight = 60;

            // Head Rows Control
            //dataGridView2.RowHeadersVisible = false;  // Head Rows Hide
            //dataGridView2.RowHeadersWidthSizeMode = DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            // RowHeadersWidth_Size 임의 지정
            dataGridView2.RowHeadersWidth = 20;

            //for (int j = 0; j < d[1].Columns.Count + 1; j++)
            for (int j = 0; j < d[1].Columns.Count; j++)
            {
                dataGridView2.Columns[j].HeaderCell.Style.Font = new Font("맑은고딕", 10, FontStyle.Bold);
                // dGVProcess.Columns[j].HeaderCell.Style.ForeColor = Color.Red;
                //dataGridView2.Columns[j].HeaderCell.Style.BackColor = Color.Yellow;

                // dGVState.Columns[j].HeaderCell.Size = new Size();

                // Head Cell_Columns Alignment
                //dataGridView2.Columns[j].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
                //if (j < 1) dataGridView2.Columns[j].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
                //else dataGridView2.Columns[j].HeaderCell.Style.Alignment = DataGridViewContentAlignment.BottomCenter;

                dataGridView2.Columns[j].HeaderCell.Style.Alignment = DataGridViewContentAlignment.BottomCenter;

                // Cell Font Style
                //dGVProcess.Columns[j].DefaultCellStyle.Font = new Font("Tahoma", 18, FontStyle.Bold);

                // Cell Font Alignment
                if (j == 0)
                {

                }
                else if (j == 9)
                {
                    dataGridView2.Columns[j].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                }
                //else
                //{
                //    dataGridView2.Columns[j].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                //
                //}

                //  Cell Height 지정
                //  var dgv = new DataGridView();
                //dataGridView2.RowTemplate.Height = 30;


                // Head Cell_Columns Width Size 임의 지정
                DataGridViewColumn column1 = dataGridView2.Columns[0];
                column1.Width = 100;
                DataGridViewColumn column2 = dataGridView2.Columns[1];
                column2.Width = 100;
                DataGridViewColumn column3 = dataGridView2.Columns[2];
                column3.Width = 150;
                DataGridViewColumn column4 = dataGridView2.Columns[3];
                column4.Width = 60;
                DataGridViewColumn column5 = dataGridView2.Columns[4];
                column5.Width = 180;
                DataGridViewColumn column6 = dataGridView2.Columns[5];
                column6.Width = 60;
                DataGridViewColumn column7 = dataGridView2.Columns[6];
                column7.Width = 60;
                DataGridViewColumn column8 = dataGridView2.Columns[7];
                column8.Width = 200;
                DataGridViewColumn column9 = dataGridView2.Columns[8];
                column9.Width = 60;
                DataGridViewColumn column10 = dataGridView2.Columns[9];
                column10.Width = 70;
                DataGridViewColumn column11 = dataGridView2.Columns[10];
                column11.Width = 60;
                DataGridViewColumn column12 = dataGridView2.Columns[11];
                column12.Width = 60;
                DataGridViewColumn column13 = dataGridView2.Columns[12];
                column13.Width = 150;
                DataGridViewColumn column14 = dataGridView2.Columns[13];
                column14.Width = 150;
                DataGridViewColumn column15 = dataGridView2.Columns[14];
                column15.Width = 100;
                DataGridViewColumn column16 = dataGridView2.Columns[15];
                column16.Width = 60;
                DataGridViewColumn column17 = dataGridView2.Columns[16];
                column17.Width = 60;
                DataGridViewColumn column18 = dataGridView2.Columns[17];
                column18.Width = 80;
                DataGridViewColumn column19 = dataGridView2.Columns[18];
                column19.Width = 80;
                DataGridViewColumn column20 = dataGridView2.Columns[19];
                column20.Width = 60;
                DataGridViewColumn column21 = dataGridView2.Columns[20];
                column21.Width = 180;
                DataGridViewColumn column22 = dataGridView2.Columns[21];
                column22.Width = 60;
                DataGridViewColumn column23 = dataGridView2.Columns[22];
                column23.Width = 60;
                DataGridViewColumn column24 = dataGridView2.Columns[23];
                column24.Width = 120;
            }

            dataGridView2_r1 = dataGridView2.GetCellDisplayRectangle(0, -1, false);
            dataGridView2_DoubleHead_width[0] = dataGridView2.GetCellDisplayRectangle(1, -1, false).Width;
            dataGridView2_DoubleHead_width[1] = dataGridView2.GetCellDisplayRectangle(2, -1, false).Width;
           
            dataGridView2_r2 = dataGridView2.GetCellDisplayRectangle(3, -1, false);
            dataGridView2_DoubleHead_width[2] = dataGridView2.GetCellDisplayRectangle(4, -1, false).Width;
            dataGridView2_DoubleHead_width[3] = dataGridView2.GetCellDisplayRectangle(5, -1, false).Width;

            dataGridView2_r3 = dataGridView2.GetCellDisplayRectangle(6, -1, false);
            dataGridView2_DoubleHead_width[4] = dataGridView2.GetCellDisplayRectangle(7, -1, false).Width;
            dataGridView2_DoubleHead_width[5] = dataGridView2.GetCellDisplayRectangle(8, -1, false).Width;
            dataGridView2_DoubleHead_width[6] = dataGridView2.GetCellDisplayRectangle(9, -1, false).Width;
            dataGridView2_DoubleHead_width[7] = dataGridView2.GetCellDisplayRectangle(10, -1, false).Width;

            dataGridView2_r4 = dataGridView2.GetCellDisplayRectangle(11, -1, false);
            dataGridView2_DoubleHead_width[8] = dataGridView2.GetCellDisplayRectangle(12, -1, false).Width;
            dataGridView2_DoubleHead_width[9] = dataGridView2.GetCellDisplayRectangle(13, -1, false).Width;
            dataGridView2_DoubleHead_width[10] = dataGridView2.GetCellDisplayRectangle(14, -1, false).Width;
            dataGridView2_DoubleHead_width[11] = dataGridView2.GetCellDisplayRectangle(15, -1, false).Width;
            dataGridView2_DoubleHead_width[12] = dataGridView2.GetCellDisplayRectangle(16, -1, false).Width;
            dataGridView2_DoubleHead_width[13] = dataGridView2.GetCellDisplayRectangle(17, -1, false).Width;
            dataGridView2_DoubleHead_width[14] = dataGridView2.GetCellDisplayRectangle(18, -1, false).Width;

            dataGridView2_r5 = dataGridView2.GetCellDisplayRectangle(19, -1, false);
            dataGridView2_DoubleHead_width[15] = dataGridView2.GetCellDisplayRectangle(20, -1, false).Width;
            dataGridView2_DoubleHead_width[16] = dataGridView2.GetCellDisplayRectangle(21, -1, false).Width;

            dataGridView2_r6 = dataGridView2.GetCellDisplayRectangle(22, -1, false);
            dataGridView2_DoubleHead_width[17] = dataGridView2.GetCellDisplayRectangle(23, -1, false).Width;
        }

   
        private void dataGridView2_CellPainting(object sender, DataGridViewCellPaintingEventArgs e)
        {

            // DoubleHead ============================================================================

            if (e.RowIndex == -1 && e.ColumnIndex > -1)

            {
                Rectangle r = e.CellBounds;

                r.Y += e.CellBounds.Height / 2;

                r.Height = e.CellBounds.Height / 2;

                e.PaintBackground(r, true);

                e.PaintContent(r);

                e.Handled = true;
            }
        }

        private void dataGridView2_ColumnWidthChanged(object sender, DataGridViewColumnEventArgs e)
        {
            DataGridView gv = (DataGridView)sender;

            Rectangle rtHeader = gv.DisplayRectangle;

            rtHeader.Height = gv.ColumnHeadersHeight / 2;

            gv.Invalidate(rtHeader);
        }

        private void dataGridView2_Paint(object sender, PaintEventArgs e)
        {
            DataGridView gv = (DataGridView)sender;

            string[] strHeaders = { "작  업", "화  주", "제  품", "작  업", "관  리", "선  박" };

            StringFormat format = new StringFormat();

            format.Alignment = StringAlignment.Center;

            format.LineAlignment = StringAlignment.Center;


            // Category Painting

            {
                dataGridView2_r1.X += 1;

                dataGridView2_r1.Y += 2;

                dataGridView2_r1.Width = dataGridView2_r1.Width + dataGridView2_DoubleHead_width[0] +
                                         dataGridView2_DoubleHead_width[1] - 2;

                dataGridView2_r1.Height = (dataGridView2_r1.Height / 2) - 2;

                e.Graphics.DrawRectangle(new Pen(gv.BackgroundColor), dataGridView2_r1);

                e.Graphics.FillRectangle(new SolidBrush(gv.ColumnHeadersDefaultCellStyle.BackColor),
                //e.Graphics.FillRectangle(new SolidBrush(Color.Yellow),

                dataGridView2_r1);


                e.Graphics.DrawString(strHeaders[0],

                    //gv.ColumnHeadersDefaultCellStyle.Font,
                    new Font("맑은고딕", 9, FontStyle.Bold),

                    new SolidBrush(gv.ColumnHeadersDefaultCellStyle.ForeColor),

                    dataGridView2_r1,

                    format);

            }

            // Projection Painting

            {
                dataGridView2_r2.X += 1;

                dataGridView2_r2.Y += 2;

                dataGridView2_r2.Width = dataGridView2_r2.Width + dataGridView2_DoubleHead_width[2] +
                                         dataGridView2_DoubleHead_width[3] - 2;

                dataGridView2_r2.Height = (dataGridView2_r2.Height / 2) - 2;

                e.Graphics.DrawRectangle(new Pen(gv.BackgroundColor), dataGridView2_r2);

                e.Graphics.FillRectangle(new SolidBrush(gv.ColumnHeadersDefaultCellStyle.BackColor),
                //e.Graphics.FillRectangle(new SolidBrush(Color.Yellow),

                dataGridView2_r2);


                e.Graphics.DrawString(strHeaders[1],

                    //gv.ColumnHeadersDefaultCellStyle.Font,
                    new Font("맑은고딕", 9, FontStyle.Bold),

                    new SolidBrush(gv.ColumnHeadersDefaultCellStyle.ForeColor),

                    dataGridView2_r2,

                    format);
            }

            // 제품

            {
                dataGridView2_r3.X += 1;

                dataGridView2_r3.Y += 2;

                dataGridView2_r3.Width = dataGridView2_r3.Width + dataGridView2_DoubleHead_width[4] +
                                         dataGridView2_DoubleHead_width[5] + dataGridView2_DoubleHead_width[6] +
                                         dataGridView2_DoubleHead_width[7] - 2;

                dataGridView2_r3.Height = (dataGridView2_r3.Height / 2) - 2;

                e.Graphics.DrawRectangle(new Pen(gv.BackgroundColor), dataGridView2_r3);

                e.Graphics.FillRectangle(new SolidBrush(gv.ColumnHeadersDefaultCellStyle.BackColor),
                //e.Graphics.FillRectangle(new SolidBrush(Color.Yellow),

                dataGridView2_r3);


                e.Graphics.DrawString(strHeaders[2],

                    //gv.ColumnHeadersDefaultCellStyle.Font,
                    new Font("맑은고딕", 9, FontStyle.Bold),

                    new SolidBrush(gv.ColumnHeadersDefaultCellStyle.ForeColor),

                    dataGridView2_r3,

                    format);
            }

            // 작업

            {
                dataGridView2_r4.X += 1;

                dataGridView2_r4.Y += 2;

                dataGridView2_r4.Width = dataGridView2_r4.Width + dataGridView2_DoubleHead_width[8] +
                                         dataGridView2_DoubleHead_width[9] + dataGridView2_DoubleHead_width[10] +
                                         dataGridView2_DoubleHead_width[11] + dataGridView2_DoubleHead_width[12] +
                                         dataGridView2_DoubleHead_width[13] + dataGridView2_DoubleHead_width[14] - 2;

                dataGridView2_r4.Height = (dataGridView2_r4.Height / 2) - 2;

                e.Graphics.DrawRectangle(new Pen(gv.BackgroundColor), dataGridView2_r4);

                e.Graphics.FillRectangle(new SolidBrush(gv.ColumnHeadersDefaultCellStyle.BackColor),
                //e.Graphics.FillRectangle(new SolidBrush(Color.Yellow),

                dataGridView2_r4);


                e.Graphics.DrawString(strHeaders[3],

                    //gv.ColumnHeadersDefaultCellStyle.Font,
                    new Font("맑은고딕", 9, FontStyle.Bold),

                    new SolidBrush(gv.ColumnHeadersDefaultCellStyle.ForeColor),

                    dataGridView2_r4,

                    format);
            }

            // 관리
            {
                dataGridView2_r5.X += 1;

                dataGridView2_r5.Y += 2;

                dataGridView2_r5.Width = dataGridView2_r5.Width + dataGridView2_DoubleHead_width[15] +
                                         dataGridView2_DoubleHead_width[16] - 2;

                dataGridView2_r5.Height = (dataGridView2_r5.Height / 2) - 2;

                e.Graphics.DrawRectangle(new Pen(gv.BackgroundColor), dataGridView2_r5);

                e.Graphics.FillRectangle(new SolidBrush(gv.ColumnHeadersDefaultCellStyle.BackColor),
                //e.Graphics.FillRectangle(new SolidBrush(Color.Yellow),

                dataGridView2_r5);


                e.Graphics.DrawString(strHeaders[4],

                    //gv.ColumnHeadersDefaultCellStyle.Font,
                    new Font("맑은고딕", 9, FontStyle.Bold),

                    new SolidBrush(gv.ColumnHeadersDefaultCellStyle.ForeColor),

                    dataGridView2_r5,

                    format);
            }

            // 선박
            {
                dataGridView2_r6.X += 1;

                dataGridView2_r6.Y += 2;

                dataGridView2_r6.Width = dataGridView2_r6.Width + dataGridView2_DoubleHead_width[17] - 2;

                dataGridView2_r6.Height = (dataGridView2_r6.Height / 2) - 2;

                e.Graphics.DrawRectangle(new Pen(gv.BackgroundColor), dataGridView2_r6);

                e.Graphics.FillRectangle(new SolidBrush(gv.ColumnHeadersDefaultCellStyle.BackColor),
                //e.Graphics.FillRectangle(new SolidBrush(Color.Yellow),

                dataGridView2_r6);


                e.Graphics.DrawString(strHeaders[5],

                    //gv.ColumnHeadersDefaultCellStyle.Font,
                    new Font("맑은고딕", 9, FontStyle.Bold),

                    new SolidBrush(gv.ColumnHeadersDefaultCellStyle.ForeColor),

                    dataGridView2_r6,

                    format);
            }
        }

        private void dataGridView2_Scroll(object sender, ScrollEventArgs e)
        {
            dataGridView2Set();

            //DataGridView gv = (DataGridView)sender;

            Rectangle rtHeader = dataGridView2.DisplayRectangle;

            rtHeader.Height = dataGridView2.ColumnHeadersHeight / 2;

            dataGridView2.Invalidate(rtHeader);

        }

        // dataGridView3 Set ===========================================================================

        private void dateTimePicker5_ValueChanged(object sender, EventArgs e)
        {
            button20.PerformClick();
        }

       

        private void T2_dataGridView3_Main_Process()
        {
            Cursor.Current = Cursors.WaitCursor;
            
            List<string> Condition = new List<string>();

            Condition.Add(dateTimePicker5.Value.Year.ToString());
            Condition.Add(dateTimePicker5.Value.Month.ToString());
            Condition.Add(dateTimePicker5.Value.Day.ToString());
            //  " 선택발행 체크 여부"
            if (checkBox1.Checked == false) Condition.Add("0");
            else Condition.Add("X");
            Condition.Add(textBox21.Text);
            Condition.Add(textBox22.Text);
            
            d[2] = CommonDB.R_datatbl(Qry.KSWMS210_007(Condition));

            T2_dataGridView3_Clear();
            dataGridView3Set();

            int j = 0;

            while (j < d[2].Rows.Count)
            {
                int n = dataGridView3.Rows.Add();

                for (int i = 0; i <= d[2].Columns.Count - 1; i++)
                {
                    dataGridView3.Rows[j].Cells[i].Value = d[2].Rows[j][i].ToString();

                    //if (i == 0)
                    //{
                    //    dataGridView1.Rows[n].Cells[i].Value = (j + 1).ToString();
                    //}
                    //else if (i == 18)
                    //{
                    //    if (d[2].Rows[j][14].ToString() != "")
                    //    {
                    //        dataGridView1.Rows[n].Cells[i].Value = "0";
                    //    }
                    //    else
                    //    {
                    //        dataGridView1.Rows[n].Cells[i].Value = d[2].Rows[j][i - 1].ToString();
                    //    }
                    //}
                    //else
                    //{
                    //    dataGridView1.Rows[n].Cells[i].Value = d[2].Rows[j][i - 1].ToString();
                    //}

                    /*
                    if (dataGridView1.Rows[n].Cells[i].Value.Equals("EST"))
                    {
                        dataGridView1.Rows[n].DefaultCellStyle.BackColor = Color.Red;
                        dataGridView1.Rows[n].DefaultCellStyle.ForeColor = Color.White;
                    }
                    */
                }

                j++;
            }

            label23.Text = j.ToString();
            //statusBar1.Panels[0].Text = "전체 " + j.ToString() + " 건이 검색 되었습니다.";

        }
        private void T2_dataGridView3_Clear()
        {
            // 그리드 초기화
            dataGridView3.DataSource = null;
            dataGridView3.Columns.Clear();
        }

        private void dataGridView3Set()
        {
            //this.Controls.Add(dataGridView3);
            dataGridView3.ColumnCount = d[2].Columns.Count;

            dataGridView3.Columns[0].Name = "WMS NO";
            dataGridView3.Columns[1].Name = "순번";
            dataGridView3.Columns[2].Name = "입고일";
            dataGridView3.Columns[3].Name = "출력";
            dataGridView3.Columns[4].Name = "유형";
            dataGridView3.Columns[5].Name = "수량";
            dataGridView3.Columns[6].Name = "총수량";
            dataGridView3.Columns[7].Name = "B/L NO";
            dataGridView3.Columns[8].Name = "코드";
            dataGridView3.Columns[9].Name = "상호";
            dataGridView3.Columns[10].Name = "담당";
            dataGridView3.Columns[11].Name = "코드";
            dataGridView3.Columns[12].Name = "제품명";
            dataGridView3.Columns[13].Name = "규격";
            dataGridView3.Columns[14].Name = "단위";
            dataGridView3.Columns[15].Name = "표시";
            dataGridView3.Columns[16].Name = "비고";
            dataGridView3.Columns[17].Name = "바코드";
            dataGridView3.Columns[18].Name = "화물관리번호";
            dataGridView3.Columns[19].Name = "대표코드";
            dataGridView3.Columns[20].Name = "구역";
            dataGridView3.Columns[21].Name = "최초재고";
            dataGridView3.Columns[22].Name = "차량";
            dataGridView3.Columns[23].Name = "코드";
            dataGridView3.Columns[24].Name = "상호";
            dataGridView3.Columns[25].Name = "담당";
            dataGridView3.Columns[26].Name = "코드";
            dataGridView3.Columns[27].Name = "선박명";

            for (int i = 0; i <= 7; i++)
            {
                dataGridView3.Columns[i].Frozen = true;
            }
            //this.dataGridView3.Columns["암호"].Visible = false;
            //this.dataGridView3.Columns["권한"].Visible = false;

            // Columns AutoSize
            //dGVProcess.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells;

            // Head Cell_Columns Font Style = off
            dataGridView3.EnableHeadersVisualStyles = false;
            // Head Cell_Columns HeightSizeMode = off
            dataGridView3.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.DisableResizing;

            // Head Cell_Columns HeightSize 임의 지정
            dataGridView3.ColumnHeadersHeight = 60;

            // Head Rows Control
            //dataGridView3.RowHeadersVisible = false;  // Head Rows Hide
            //dataGridView3.RowHeadersWidthSizeMode = DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            // RowHeadersWidth_Size 임의 지정
            dataGridView3.RowHeadersWidth = 20;

            //for (int j = 0; j < d[2].Columns.Count + 1; j++)
            for (int j = 0; j < d[2].Columns.Count; j++)
            {
                dataGridView3.Columns[j].HeaderCell.Style.Font = new Font("맑은고딕", 10, FontStyle.Bold);
                // dGVProcess.Columns[j].HeaderCell.Style.ForeColor = Color.Red;
                //dataGridView3.Columns[j].HeaderCell.Style.BackColor = Color.Yellow;

                // dGVState.Columns[j].HeaderCell.Size = new Size();

                // Head Cell_Columns Alignment
                //dataGridView3.Columns[j].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
                //if (j < 1) dataGridView3.Columns[j].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
                //else dataGridView3.Columns[j].HeaderCell.Style.Alignment = DataGridViewContentAlignment.BottomCenter;

                dataGridView3.Columns[j].HeaderCell.Style.Alignment = DataGridViewContentAlignment.BottomCenter;

                // Cell Font Style
                //dGVProcess.Columns[j].DefaultCellStyle.Font = new Font("Tahoma", 18, FontStyle.Bold);

                // Cell Font Alignment
                if (j == 0)
                {

                }
                else if (j == 5 || j == 6)
                {
                    dataGridView3.Columns[j].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                }
                //else
                //{
                //    dataGridView3.Columns[j].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                //
                //}

                //  Cell Height 지정
                //  var dgv = new DataGridView();
                //dataGridView3.RowTemplate.Height = 30;


                // Head Cell_Columns Width Size 임의 지정
                DataGridViewColumn column1 = dataGridView3.Columns[0];
                column1.Width = 100;
                DataGridViewColumn column2 = dataGridView3.Columns[1];
                column2.Width = 60;
                DataGridViewColumn column3 = dataGridView3.Columns[2];
                column3.Width = 100;
                DataGridViewColumn column4 = dataGridView3.Columns[3];
                column4.Width = 60;
                DataGridViewColumn column5 = dataGridView3.Columns[4];
                column5.Width = 100;
                DataGridViewColumn column6 = dataGridView3.Columns[5];
                column6.Width = 60;
                DataGridViewColumn column7 = dataGridView3.Columns[6];
                column7.Width = 70;
                DataGridViewColumn column8 = dataGridView3.Columns[7];
                column8.Width = 200;
                DataGridViewColumn column9 = dataGridView3.Columns[8];
                column9.Width = 60;
                DataGridViewColumn column10 = dataGridView3.Columns[9];
                column10.Width = 150;
                DataGridViewColumn column11 = dataGridView3.Columns[10];
                column11.Width = 60;
                DataGridViewColumn column12 = dataGridView3.Columns[11];
                column12.Width = 60;
                DataGridViewColumn column13 = dataGridView3.Columns[12];
                column13.Width = 150;
                DataGridViewColumn column14 = dataGridView3.Columns[13];
                column14.Width = 60;
                DataGridViewColumn column15 = dataGridView3.Columns[14];
                column15.Width = 60;
                DataGridViewColumn column16 = dataGridView3.Columns[15];
                column16.Width = 150;
                DataGridViewColumn column17 = dataGridView3.Columns[16];
                column17.Width = 150;
                DataGridViewColumn column18 = dataGridView3.Columns[17];
                column18.Width = 100;
                DataGridViewColumn column19 = dataGridView3.Columns[18];
                column19.Width = 150;
                DataGridViewColumn column20 = dataGridView3.Columns[19];
                column20.Width = 180;
                DataGridViewColumn column21 = dataGridView3.Columns[20];
                column21.Width = 60;
                DataGridViewColumn column22 = dataGridView3.Columns[21];
                column22.Width = 100;
                DataGridViewColumn column23 = dataGridView3.Columns[22];
                column23.Width = 100;
                DataGridViewColumn column24 = dataGridView3.Columns[23];
                column24.Width = 60;
                DataGridViewColumn column25 = dataGridView3.Columns[24];
                column25.Width = 120;
                DataGridViewColumn column26 = dataGridView3.Columns[25];
                column26.Width = 60;
                DataGridViewColumn column27 = dataGridView3.Columns[26];
                column27.Width = 60;
                DataGridViewColumn column28 = dataGridView3.Columns[27];
                column28.Width = 120;
            }

            dataGridView3_r1 = dataGridView3.GetCellDisplayRectangle(0, -1, false);
            dataGridView3_DoubleHead_width[0] = dataGridView3.GetCellDisplayRectangle(1, -1, false).Width;
            dataGridView3_DoubleHead_width[1] = dataGridView3.GetCellDisplayRectangle(2, -1, false).Width;
            dataGridView3_DoubleHead_width[2] = dataGridView3.GetCellDisplayRectangle(3, -1, false).Width;
            dataGridView3_DoubleHead_width[3] = dataGridView3.GetCellDisplayRectangle(4, -1, false).Width;
            dataGridView3_DoubleHead_width[4] = dataGridView3.GetCellDisplayRectangle(5, -1, false).Width;
            dataGridView3_DoubleHead_width[5] = dataGridView3.GetCellDisplayRectangle(6, -1, false).Width;
            dataGridView3_DoubleHead_width[6] = dataGridView3.GetCellDisplayRectangle(7, -1, false).Width;

            dataGridView3_r2 = dataGridView3.GetCellDisplayRectangle(8, -1, false);
            dataGridView3_DoubleHead_width[7] = dataGridView3.GetCellDisplayRectangle(9, -1, false).Width;
            dataGridView3_DoubleHead_width[8] = dataGridView3.GetCellDisplayRectangle(10, -1, false).Width;

            dataGridView3_r3 = dataGridView3.GetCellDisplayRectangle(11, -1, false);
            dataGridView3_DoubleHead_width[9] = dataGridView3.GetCellDisplayRectangle(12, -1, false).Width;
            dataGridView3_DoubleHead_width[10] = dataGridView3.GetCellDisplayRectangle(13, -1, false).Width;
            dataGridView3_DoubleHead_width[11] = dataGridView3.GetCellDisplayRectangle(14, -1, false).Width;

            dataGridView3_r4 = dataGridView3.GetCellDisplayRectangle(15, -1, false);
            dataGridView3_DoubleHead_width[12] = dataGridView3.GetCellDisplayRectangle(16, -1, false).Width;
            dataGridView3_DoubleHead_width[13] = dataGridView3.GetCellDisplayRectangle(17, -1, false).Width;
            dataGridView3_DoubleHead_width[14] = dataGridView3.GetCellDisplayRectangle(18, -1, false).Width;
            dataGridView3_DoubleHead_width[15] = dataGridView3.GetCellDisplayRectangle(19, -1, false).Width;
            dataGridView3_DoubleHead_width[16] = dataGridView3.GetCellDisplayRectangle(20, -1, false).Width;
            dataGridView3_DoubleHead_width[17] = dataGridView3.GetCellDisplayRectangle(21, -1, false).Width;
            dataGridView3_DoubleHead_width[18] = dataGridView3.GetCellDisplayRectangle(22, -1, false).Width;

            dataGridView3_r5 = dataGridView3.GetCellDisplayRectangle(23, -1, false);
            dataGridView3_DoubleHead_width[19] = dataGridView3.GetCellDisplayRectangle(24, -1, false).Width;
            dataGridView3_DoubleHead_width[20] = dataGridView3.GetCellDisplayRectangle(25, -1, false).Width;

            dataGridView3_r6 = dataGridView3.GetCellDisplayRectangle(26, -1, false);
            dataGridView3_DoubleHead_width[21] = dataGridView3.GetCellDisplayRectangle(27, -1, false).Width;
        }


        private void dataGridView3_CellPainting(object sender, DataGridViewCellPaintingEventArgs e)
        {

            // DoubleHead ============================================================================

            if (e.RowIndex == -1 && e.ColumnIndex > -1)

            {
                Rectangle r = e.CellBounds;

                r.Y += e.CellBounds.Height / 2;

                r.Height = e.CellBounds.Height / 2;

                e.PaintBackground(r, true);

                e.PaintContent(r);

                e.Handled = true;
            }
        }

        private void dataGridView3_ColumnWidthChanged(object sender, DataGridViewColumnEventArgs e)
        {
            DataGridView gv = (DataGridView)sender;

            Rectangle rtHeader = gv.DisplayRectangle;

            rtHeader.Height = gv.ColumnHeadersHeight / 2;

            gv.Invalidate(rtHeader);
        }

        private void dataGridView3_Paint(object sender, PaintEventArgs e)
        {
            DataGridView gv = (DataGridView)sender;

            string[] strHeaders = { "오  더", "화  주", "제  품", "작업정보", "관  리", "선  박" };

            StringFormat format = new StringFormat();

            format.Alignment = StringAlignment.Center;

            format.LineAlignment = StringAlignment.Center;


            // Category Painting

            {
                dataGridView3_r1.X += 1;

                dataGridView3_r1.Y += 2;

                dataGridView3_r1.Width = dataGridView3_r1.Width + dataGridView3_DoubleHead_width[0] +
                                         dataGridView3_DoubleHead_width[1] + dataGridView3_DoubleHead_width[2] +
                                         dataGridView3_DoubleHead_width[3] + dataGridView3_DoubleHead_width[4] +
                                         dataGridView3_DoubleHead_width[5] + dataGridView3_DoubleHead_width[6] - 2;

                dataGridView3_r1.Height = (dataGridView3_r1.Height / 2) - 2;

                e.Graphics.DrawRectangle(new Pen(gv.BackgroundColor), dataGridView3_r1);

                e.Graphics.FillRectangle(new SolidBrush(gv.ColumnHeadersDefaultCellStyle.BackColor),
                //e.Graphics.FillRectangle(new SolidBrush(Color.Yellow),

                dataGridView3_r1);


                e.Graphics.DrawString(strHeaders[0],

                    //gv.ColumnHeadersDefaultCellStyle.Font,
                    new Font("맑은고딕", 9, FontStyle.Bold),

                    new SolidBrush(gv.ColumnHeadersDefaultCellStyle.ForeColor),

                    dataGridView3_r1,

                    format);

            }

            // Projection Painting

            {
                dataGridView3_r2.X += 1;

                dataGridView3_r2.Y += 2;

                dataGridView3_r2.Width = dataGridView3_r2.Width + dataGridView3_DoubleHead_width[7] +
                                         dataGridView3_DoubleHead_width[8] - 2;

                dataGridView3_r2.Height = (dataGridView3_r2.Height / 2) - 2;

                e.Graphics.DrawRectangle(new Pen(gv.BackgroundColor), dataGridView3_r2);

                e.Graphics.FillRectangle(new SolidBrush(gv.ColumnHeadersDefaultCellStyle.BackColor),
                //e.Graphics.FillRectangle(new SolidBrush(Color.Yellow),

                dataGridView3_r2);


                e.Graphics.DrawString(strHeaders[1],

                    //gv.ColumnHeadersDefaultCellStyle.Font,
                    new Font("맑은고딕", 9, FontStyle.Bold),

                    new SolidBrush(gv.ColumnHeadersDefaultCellStyle.ForeColor),

                    dataGridView3_r2,

                    format);
            }

            // 제품

            {
                dataGridView3_r3.X += 1;

                dataGridView3_r3.Y += 2;

                dataGridView3_r3.Width = dataGridView3_r3.Width + dataGridView3_DoubleHead_width[9] +
                                         dataGridView3_DoubleHead_width[10] + dataGridView3_DoubleHead_width[11] - 2;

                dataGridView3_r3.Height = (dataGridView3_r3.Height / 2) - 2;

                e.Graphics.DrawRectangle(new Pen(gv.BackgroundColor), dataGridView3_r3);

                e.Graphics.FillRectangle(new SolidBrush(gv.ColumnHeadersDefaultCellStyle.BackColor),
                //e.Graphics.FillRectangle(new SolidBrush(Color.Yellow),

                dataGridView3_r3);


                e.Graphics.DrawString(strHeaders[2],

                    //gv.ColumnHeadersDefaultCellStyle.Font,
                    new Font("맑은고딕", 9, FontStyle.Bold),

                    new SolidBrush(gv.ColumnHeadersDefaultCellStyle.ForeColor),

                    dataGridView3_r3,

                    format);
            }

            // 작업

            {
                dataGridView3_r4.X += 1;

                dataGridView3_r4.Y += 2;

                dataGridView3_r4.Width = dataGridView3_r4.Width + dataGridView3_DoubleHead_width[12] +
                                         dataGridView3_DoubleHead_width[13] + dataGridView3_DoubleHead_width[14] +
                                         dataGridView3_DoubleHead_width[15] + dataGridView3_DoubleHead_width[16] +
                                         dataGridView3_DoubleHead_width[17] + dataGridView3_DoubleHead_width[18] - 2;

                dataGridView3_r4.Height = (dataGridView3_r4.Height / 2) - 2;

                e.Graphics.DrawRectangle(new Pen(gv.BackgroundColor), dataGridView3_r4);

                e.Graphics.FillRectangle(new SolidBrush(gv.ColumnHeadersDefaultCellStyle.BackColor),
                //e.Graphics.FillRectangle(new SolidBrush(Color.Yellow),

                dataGridView3_r4);


                e.Graphics.DrawString(strHeaders[3],

                    //gv.ColumnHeadersDefaultCellStyle.Font,
                    new Font("맑은고딕", 9, FontStyle.Bold),

                    new SolidBrush(gv.ColumnHeadersDefaultCellStyle.ForeColor),

                    dataGridView3_r4,

                    format);
            }

            // 관리
            {
                dataGridView3_r5.X += 1;

                dataGridView3_r5.Y += 2;

                dataGridView3_r5.Width = dataGridView3_r5.Width + dataGridView3_DoubleHead_width[19] +
                                         dataGridView3_DoubleHead_width[20] - 2;

                dataGridView3_r5.Height = (dataGridView3_r5.Height / 2) - 2;

                e.Graphics.DrawRectangle(new Pen(gv.BackgroundColor), dataGridView3_r5);

                e.Graphics.FillRectangle(new SolidBrush(gv.ColumnHeadersDefaultCellStyle.BackColor),
                //e.Graphics.FillRectangle(new SolidBrush(Color.Yellow),

                dataGridView3_r5);


                e.Graphics.DrawString(strHeaders[4],

                    //gv.ColumnHeadersDefaultCellStyle.Font,
                    new Font("맑은고딕", 9, FontStyle.Bold),

                    new SolidBrush(gv.ColumnHeadersDefaultCellStyle.ForeColor),

                    dataGridView3_r5,

                    format);
            }

            // 선박
            {
                dataGridView3_r6.X += 1;

                dataGridView3_r6.Y += 2;

                dataGridView3_r6.Width = dataGridView3_r6.Width + dataGridView3_DoubleHead_width[21] - 2;

                dataGridView3_r6.Height = (dataGridView3_r6.Height / 2) - 2;

                e.Graphics.DrawRectangle(new Pen(gv.BackgroundColor), dataGridView3_r6);

                e.Graphics.FillRectangle(new SolidBrush(gv.ColumnHeadersDefaultCellStyle.BackColor),
                //e.Graphics.FillRectangle(new SolidBrush(Color.Yellow),

                dataGridView3_r6);


                e.Graphics.DrawString(strHeaders[5],

                    //gv.ColumnHeadersDefaultCellStyle.Font,
                    new Font("맑은고딕", 9, FontStyle.Bold),

                    new SolidBrush(gv.ColumnHeadersDefaultCellStyle.ForeColor),

                    dataGridView3_r6,

                    format);
            }
        }

        private void dataGridView3_Scroll(object sender, ScrollEventArgs e)
        {
            dataGridView3Set();

            //DataGridView gv = (DataGridView)sender;

            Rectangle rtHeader = dataGridView3.DisplayRectangle;

            rtHeader.Height = dataGridView3.ColumnHeadersHeight / 2;

            dataGridView3.Invalidate(rtHeader);

        }
        // dataGridView4 Set ===========================================================================

        private void T3_dataGridView4_Main_Process()
        {
            Cursor.Current = Cursors.WaitCursor;
            d[3] = CommonDB.R_datatbl(Qry.KSWMS210_009);
       
            T3_dataGridView4_Clear();
            dataGridView4Set();
       
            int j = 0;
            
            while (j < d[3].Rows.Count)
            {
                int n = dataGridView4.Rows.Add();
            
                for (int i = 1; i <= d[3].Columns.Count; i++)
                {
                    dataGridView4.Rows[j].Cells[i].Value = d[3].Rows[j][i - 1].ToString();
            
                    //if (i == 0)
                    //{
                    //    dataGridView1.Rows[n].Cells[i].Value = (j + 1).ToString();
                    //}
                    //else if (i == 18)
                    //{
                    //    if (d[3].Rows[j][14].ToString() != "")
                    //    {
                    //        dataGridView1.Rows[n].Cells[i].Value = "0";
                    //    }
                    //    else
                    //    {
                    //        dataGridView1.Rows[n].Cells[i].Value = d[3].Rows[j][i - 1].ToString();
                    //    }
                    //}
                    //else
                    //{
                    //    dataGridView1.Rows[n].Cells[i].Value = d[3].Rows[j][i - 1].ToString();
                    //}
            
                    /*
                    if (dataGridView1.Rows[n].Cells[i].Value.Equals("EST"))
                    {
                        dataGridView1.Rows[n].DefaultCellStyle.BackColor = Color.Red;
                        dataGridView1.Rows[n].DefaultCellStyle.ForeColor = Color.White;
                    }
                    */
                }
            
                j++;
            }
       
       
            //statusBar1.Panels[0].Text = "전체 " + j.ToString() + " 건이 검색 되었습니다.";
       
        }
        private void T3_dataGridView4_Clear()
        {
            // 그리드 초기화
            dataGridView4.DataSource = null;
            dataGridView4.Columns.Clear();
        }
       
        private void dataGridView4Set()
        {
            DataGridViewCheckBoxColumn checkColumn = new DataGridViewCheckBoxColumn();
            //checkColumn.Name = "X";
            //checkColumn.HeaderText = "X";
            checkColumn.Width = 30;
            //checkColumn.ReadOnly = false;
            //checkColumn.FillWeight = 10;
            //if the datagridview is resized (on form resize) the checkbox won't take up too much; value is relative to the other columns' fill values
            dataGridView4.Columns.Add(checkColumn);


            //this.Controls.Add(dataGridView4);
            dataGridView4.ColumnCount = d[3].Columns.Count + 1;
            
            //dataGridView4.Columns[0].Name = "WMS NO";
            dataGridView4.Columns[1].Name = "WMS NO";
            dataGridView4.Columns[2].Name = "최초재고";
            dataGridView4.Columns[3].Name = "입고일";
            dataGridView4.Columns[4].Name = "구역";
            dataGridView4.Columns[5].Name = "상태";
            dataGridView4.Columns[6].Name = "유형";
            dataGridView4.Columns[7].Name = "총수량";
            dataGridView4.Columns[8].Name = "B/L NO";
            dataGridView4.Columns[9].Name = "코드";
            dataGridView4.Columns[10].Name = "상호";
            dataGridView4.Columns[11].Name = "담당";
            dataGridView4.Columns[12].Name = "코드";
            dataGridView4.Columns[13].Name = "제품명";
            dataGridView4.Columns[14].Name = "규격";
            dataGridView4.Columns[15].Name = "단위";
            dataGridView4.Columns[16].Name = "표시";
        
            //this.dataGridView4.Columns["암호"].Visible = false;
            //this.dataGridView4.Columns["권한"].Visible = false;
       
            // Columns AutoSize
            //dGVProcess.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells;
       
            // Head Cell_Columns Font Style = off
            dataGridView4.EnableHeadersVisualStyles = false;
            // Head Cell_Columns HeightSizeMode = off
            dataGridView4.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
       
            // Head Cell_Columns HeightSize 임의 지정
            dataGridView4.ColumnHeadersHeight = 60;
       
            // Head Rows Control
            dataGridView4.RowHeadersVisible = false;  // Head Rows Hide
            //dataGridView4.RowHeadersWidthSizeMode = DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            // RowHeadersWidth_Size 임의 지정
            dataGridView4.RowHeadersWidth = 20;

            //for (int j = 0; j < d[3].Columns.Count + 1; j++)
            for (int j = 0; j < d[3].Columns.Count; j++)
            {
                dataGridView4.Columns[j].HeaderCell.Style.Font = new Font("맑은고딕", 10, FontStyle.Bold);
                // dGVProcess.Columns[j].HeaderCell.Style.ForeColor = Color.Red;
                //dataGridView4.Columns[j].HeaderCell.Style.BackColor = Color.Yellow;
       
                // dGVState.Columns[j].HeaderCell.Size = new Size();
       
                // Head Cell_Columns Alignment
                //dataGridView4.Columns[j].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
                //if (j < 1) dataGridView4.Columns[j].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
                //else dataGridView4.Columns[j].HeaderCell.Style.Alignment = DataGridViewContentAlignment.BottomCenter;
       
                dataGridView4.Columns[j].HeaderCell.Style.Alignment = DataGridViewContentAlignment.BottomCenter;
       
                // Cell Font Style
                //dGVProcess.Columns[j].DefaultCellStyle.Font = new Font("Tahoma", 18, FontStyle.Bold);
       
                // Cell Font Alignment
                if (j == 0)
                {
       
                }
                else if (j == 2)
                {
                    dataGridView4.Columns[j].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                }
                //else
                //{
                //    dataGridView4.Columns[j].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                //
                //}
       
                //  Cell Height 지정
                //  var dgv = new DataGridView();
                //dataGridView4.RowTemplate.Height = 30;
       
       
                // Head Cell_Columns Width Size 임의 지정
                DataGridViewColumn column1 = dataGridView4.Columns[0];
                column1.Width = 40;
                DataGridViewColumn column2 = dataGridView4.Columns[1];
                column2.Width = 100;
                DataGridViewColumn column3 = dataGridView4.Columns[2];
                column3.Width = 150;
                DataGridViewColumn column4 = dataGridView4.Columns[3];
                column4.Width = 60;
                DataGridViewColumn column5 = dataGridView4.Columns[4];
                column5.Width = 60;
                DataGridViewColumn column6 = dataGridView4.Columns[5];
                column6.Width = 60;
                DataGridViewColumn column7 = dataGridView4.Columns[6];
                column7.Width = 90;
                DataGridViewColumn column8 = dataGridView4.Columns[7];
                column8.Width = 150;
                DataGridViewColumn column9 = dataGridView4.Columns[8];
                column9.Width = 60;
                DataGridViewColumn column10 = dataGridView4.Columns[9];
                column10.Width = 180;
                DataGridViewColumn column11 = dataGridView4.Columns[10];
                column11.Width = 60;
                DataGridViewColumn column12 = dataGridView4.Columns[11];
                column12.Width = 60;
                DataGridViewColumn column13 = dataGridView4.Columns[12];
                column13.Width = 120;
                DataGridViewColumn column14 = dataGridView4.Columns[13];
                column14.Width = 60;
                DataGridViewColumn column15 = dataGridView4.Columns[14];
                column15.Width = 60;
                DataGridViewColumn column16 = dataGridView4.Columns[15];
                column16.Width = 60;
            }

            dataGridView4_r1 = dataGridView4.GetCellDisplayRectangle(1, -1, false);
            dataGridView4_DoubleHead_width[0] = dataGridView4.GetCellDisplayRectangle(2, -1, false).Width;
            dataGridView4_DoubleHead_width[1] = dataGridView4.GetCellDisplayRectangle(3, -1, false).Width;
            dataGridView4_DoubleHead_width[2] = dataGridView4.GetCellDisplayRectangle(4, -1, false).Width;
            dataGridView4_DoubleHead_width[3] = dataGridView4.GetCellDisplayRectangle(5, -1, false).Width;
            dataGridView4_DoubleHead_width[4] = dataGridView4.GetCellDisplayRectangle(6, -1, false).Width;
            dataGridView4_DoubleHead_width[5] = dataGridView4.GetCellDisplayRectangle(7, -1, false).Width;


            dataGridView4_r2 = dataGridView4.GetCellDisplayRectangle(8, -1, false);
            dataGridView4_DoubleHead_width[10] = dataGridView4.GetCellDisplayRectangle(9, -1, false).Width;
            dataGridView4_DoubleHead_width[11] = dataGridView4.GetCellDisplayRectangle(10, -1, false).Width;

            dataGridView4_r3 = dataGridView4.GetCellDisplayRectangle(11, -1, false);
            dataGridView4_DoubleHead_width[20] = dataGridView4.GetCellDisplayRectangle(12, -1, false).Width;
            dataGridView4_DoubleHead_width[21] = dataGridView4.GetCellDisplayRectangle(13, -1, false).Width;
            dataGridView4_DoubleHead_width[22] = dataGridView4.GetCellDisplayRectangle(14, -1, false).Width;
        }

       

        private void dataGridView4_gvSheetListCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            foreach (DataGridViewRow r in dataGridView4.Rows)
            {
                //r.Cells["X"].Value = ((CheckBox)sender).Checked;

                r.Cells[0].Value = ((CheckBox)sender).Checked;
            }
        }

      

        private void dataGridView4_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if(e.RowIndex < 0 || e.ColumnIndex != 0)
            {
                return;
            }
            
            if(dataGridView4.Rows[e.RowIndex].Cells[0].Value != null)
            {
                if (dataGridView4.Rows[e.RowIndex].Cells[0].Value.Equals(false))
                {
                    //option 1
                    (dataGridView4.CurrentRow.Cells[e.ColumnIndex] as DataGridViewCheckBoxCell).Value = true;
                }
                else
                {
                    //option 1
                    (dataGridView4.CurrentRow.Cells[e.ColumnIndex] as DataGridViewCheckBoxCell).Value = false;
                }
            }
            else
            {
                (dataGridView4.CurrentRow.Cells[e.ColumnIndex] as DataGridViewCheckBoxCell).Value = true;
            }
            
            //MessageBox.Show(dataGridView4.Rows[e.RowIndex].Cells[0].FormattedValue.ToString());
            //option 2
            //DataGridViewCheckBoxCell cbc = (dataGridView4.CurrentRow.Cells[e.ColumnIndex] as DataGridViewCheckBoxCell);
            //cbc.Value = true;
            //option 3
            //dataGridView4.CurrentCell.Value = true;
            
        }


        private void dataGridView4_CellPainting(object sender, DataGridViewCellPaintingEventArgs e)
        {
            // Head Check BOX ============================================================================
            //if (e.ColumnIndex == 0 && e.RowIndex == -1)
            // {
            // e.PaintBackground(e.ClipBounds, false);
            //
            // Point pt = e.CellBounds.Location;  // where you want the bitmap in the cell
            //
            // int nChkBoxWidth = 15;
            // int nChkBoxHeight = 15;
            // int offsetx = (e.CellBounds.Width - nChkBoxWidth) / 2;
            // int offsety = (e.CellBounds.Height - nChkBoxHeight) / 2;
            //
            // pt.X += offsetx;
            // pt.Y += offsety;
            //
            // CheckBox cb = new CheckBox();
            // cb.Size = new Size(nChkBoxWidth, nChkBoxHeight);
            // cb.Location = pt;
            // cb.CheckedChanged += new EventHandler(dataGridView4_gvSheetListCheckBox_CheckedChanged);
            //
            // ((DataGridView)sender).Controls.Add(cb);
            //
            // e.Handled = true;
            // }

            // DoubleHead ============================================================================

            if (e.RowIndex == -1 && e.ColumnIndex > -1)

            {
                Rectangle r = e.CellBounds;

                r.Y += e.CellBounds.Height / 2;

                r.Height = e.CellBounds.Height / 2;

                e.PaintBackground(r, true);

                e.PaintContent(r);

                e.Handled = true;
            }
        }

        private void dataGridView4_ColumnWidthChanged(object sender, DataGridViewColumnEventArgs e)
        {
            DataGridView gv = (DataGridView)sender;

            Rectangle rtHeader = gv.DisplayRectangle;

            rtHeader.Height = gv.ColumnHeadersHeight / 2;

            gv.Invalidate(rtHeader);
        }

        private void dataGridView4_Paint(object sender, PaintEventArgs e)
        {
            DataGridView gv = (DataGridView)sender;

            string[] strHeaders = { "작  업", "화  주", "제  품" };

            StringFormat format = new StringFormat();

            format.Alignment = StringAlignment.Center;

            format.LineAlignment = StringAlignment.Center;


            // Category Painting

            {
                dataGridView4_r1.X += 1;

                dataGridView4_r1.Y += 2;

                dataGridView4_r1.Width = dataGridView4_r1.Width + dataGridView4_DoubleHead_width[0] +
                                         dataGridView4_DoubleHead_width[1] + dataGridView4_DoubleHead_width[2] +
                                         dataGridView4_DoubleHead_width[3] + dataGridView4_DoubleHead_width[4] +
                                         dataGridView4_DoubleHead_width[5] - 2;

                dataGridView4_r1.Height = (dataGridView4_r1.Height / 2) - 2;

                e.Graphics.DrawRectangle(new Pen(gv.BackgroundColor), dataGridView4_r1);

                e.Graphics.FillRectangle(new SolidBrush(gv.ColumnHeadersDefaultCellStyle.BackColor),
                //e.Graphics.FillRectangle(new SolidBrush(Color.Yellow),

                dataGridView4_r1);


                e.Graphics.DrawString(strHeaders[0],

                    //gv.ColumnHeadersDefaultCellStyle.Font,
                    new Font("맑은고딕", 9, FontStyle.Bold),

                    new SolidBrush(gv.ColumnHeadersDefaultCellStyle.ForeColor),

                    dataGridView4_r1,

                    format);

            }

            // Projection Painting

            {
                dataGridView4_r2.X += 1;

                dataGridView4_r2.Y += 2;

                dataGridView4_r2.Width = dataGridView4_r2.Width + dataGridView4_DoubleHead_width[10] + 
                                         dataGridView4_DoubleHead_width[11] - 2;

                dataGridView4_r2.Height = (dataGridView4_r2.Height / 2) - 2;

                e.Graphics.DrawRectangle(new Pen(gv.BackgroundColor), dataGridView4_r2);

                e.Graphics.FillRectangle(new SolidBrush(gv.ColumnHeadersDefaultCellStyle.BackColor),
                //e.Graphics.FillRectangle(new SolidBrush(Color.Yellow),

                dataGridView4_r2);


                e.Graphics.DrawString(strHeaders[1],

                    //gv.ColumnHeadersDefaultCellStyle.Font,
                    new Font("맑은고딕", 9, FontStyle.Bold),

                    new SolidBrush(gv.ColumnHeadersDefaultCellStyle.ForeColor),

                    dataGridView4_r2,

                    format);
            }

            // GROUP HEAD 매출율

            {
                dataGridView4_r3.X += 1;

                dataGridView4_r3.Y += 2;

                dataGridView4_r3.Width = dataGridView4_r3.Width + dataGridView4_DoubleHead_width[20] +
                                         dataGridView4_DoubleHead_width[21] + dataGridView4_DoubleHead_width[22] - 2;

                dataGridView4_r3.Height = (dataGridView4_r3.Height / 2) - 2;

                e.Graphics.DrawRectangle(new Pen(gv.BackgroundColor), dataGridView4_r3);

                e.Graphics.FillRectangle(new SolidBrush(gv.ColumnHeadersDefaultCellStyle.BackColor),
                //e.Graphics.FillRectangle(new SolidBrush(Color.Yellow),

                dataGridView4_r3);


                e.Graphics.DrawString(strHeaders[2],

                    //gv.ColumnHeadersDefaultCellStyle.Font,
                    new Font("맑은고딕", 9, FontStyle.Bold),

                    new SolidBrush(gv.ColumnHeadersDefaultCellStyle.ForeColor),

                    dataGridView4_r3,

                    format);
            }
            
        }

        private void dataGridView4_Scroll(object sender, ScrollEventArgs e)
        {
            dataGridView4Set();

            //DataGridView gv = (DataGridView)sender;

            Rectangle rtHeader = dataGridView4.DisplayRectangle;

            rtHeader.Height = dataGridView4.ColumnHeadersHeight / 2;

            dataGridView4.Invalidate(rtHeader);
            
        }

       

        // dataGridView5 Set ===========================================================================

        private void T3_dataGridView5_Main_Process()
        {
            Cursor.Current = Cursors.WaitCursor;
            d[4] = CommonDB.R_datatbl(Qry.KSWMS210_025);

            T3_dataGridView5_Clear();
            dataGridView5Set();

            int j = 0;

            while (j < d[4].Rows.Count)
            {
                int n = dataGridView5.Rows.Add();

                for (int i = 1; i <= d[4].Columns.Count; i++)
                {
                    dataGridView5.Rows[n].Cells[i].Value = d[4].Rows[j][i - 1].ToString();

                    //if (i == 0)
                    //{
                    //    dataGridView5.Rows[n].Cells[i].Value = (j + 1).ToString();
                    //}
                    //else if (i == 18)
                    //{
                    //    if (d[4].Rows[j][14].ToString() != "")
                    //    {
                    //        dataGridView5.Rows[n].Cells[i].Value = "0";
                    //    }
                    //    else
                    //    {
                    //        dataGridView5.Rows[n].Cells[i].Value = d[4].Rows[j][i - 1].ToString();
                    //    }
                    //}
                    //else
                    //{
                    //    dataGridView5.Rows[n].Cells[i].Value = d[4].Rows[j][i - 1].ToString();
                    //}

                    /*
                    if (dataGridView5.Rows[n].Cells[i].Value.Equals("EST"))
                    {
                        dataGridView5.Rows[n].DefaultCellStyle.BackColor = Color.Red;
                        dataGridView5.Rows[n].DefaultCellStyle.ForeColor = Color.White;
                    }
                    */
                }

                j++;
            }


            //statusBar1.Panels[0].Text = "전체 " + j.ToString() + " 건이 검색 되었습니다.";
        }

        private void dataGridView5_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex < 0 || e.ColumnIndex != 0)
            {
                return;
            }

            if (dataGridView5.Rows[e.RowIndex].Cells[0].Value != null)
            {
                if (dataGridView5.Rows[e.RowIndex].Cells[0].Value.Equals(false))
                {
                    //option 1
                    (dataGridView5.CurrentRow.Cells[e.ColumnIndex] as DataGridViewCheckBoxCell).Value = true;
                }
                else
                {
                    //option 1
                    (dataGridView5.CurrentRow.Cells[e.ColumnIndex] as DataGridViewCheckBoxCell).Value = false;
                }
            }
            else
            {
                (dataGridView5.CurrentRow.Cells[e.ColumnIndex] as DataGridViewCheckBoxCell).Value = true;
            }

            //MessageBox.Show(dataGridView4.Rows[e.RowIndex].Cells[0].FormattedValue.ToString());
            //option 2
            //DataGridViewCheckBoxCell cbc = (dataGridView4.CurrentRow.Cells[e.ColumnIndex] as DataGridViewCheckBoxCell);
            //cbc.Value = true;
            //option 3
            //dataGridView4.CurrentCell.Value = true;
        }

        private void T3_dataGridView5_Clear()
        {
            // 그리드 초기화
            dataGridView1.DataSource = null;
            dataGridView1.Columns.Clear();

            // 그리드 초기화
            dataGridView5.DataSource = null;
            dataGridView5.Columns.Clear();
        }

        private void button10_Click(object sender, EventArgs e)
        {
            POPUP_거래처정보 PopupForm = new POPUP_거래처정보();
            PopupForm.ShowDialog();

            textBox1.Text = PopupForm.Passvalue_거래처코드;
            textBox2.Text = PopupForm.Passvalue_거래처명;
            textBox3.Text = PopupForm.Passvalue_담당자;
        }

        private void button11_Click(object sender, EventArgs e)
        {
            POPUP_거래처정보 PopupForm = new POPUP_거래처정보();
            PopupForm.ShowDialog();

            textBox4.Text = PopupForm.Passvalue_거래처코드;
            textBox5.Text = PopupForm.Passvalue_거래처명;
            textBox6.Text = PopupForm.Passvalue_담당자;
        }

        private void button12_Click(object sender, EventArgs e)
        {
            POPUP_제품정보 PopupForm = new POPUP_제품정보();
            PopupForm.ShowDialog();

            textBox7.Text = PopupForm.Passvalue_제품코드;
            textBox8.Text = PopupForm.Passvalue_제품명;
            textBox9.Text = PopupForm.Passvalue_규격;
            textBox10.Text = PopupForm.Passvalue_단위;
        }

        private void button13_Click(object sender, EventArgs e)
        {
            POPUP_선박정보 PopupForm = new POPUP_선박정보();
            PopupForm.ShowDialog();
            
            textBox11.Text = PopupForm.Passvalue_선박코드;
            textBox12.Text = PopupForm.Passvalue_선박명;
        }

        private void dataGridView5Set()
        {
            DataGridViewCheckBoxColumn checkColumn = new DataGridViewCheckBoxColumn();
            //checkColumn.Name = "X";
            //checkColumn.HeaderText = "X";
            checkColumn.Width = 30;
            //checkColumn.ReadOnly = false;
            //checkColumn.FillWeight = 10;
            //if the datagridview is resized (on form resize) the checkbox won't take up too much; value is relative to the other columns' fill values
            dataGridView5.Columns.Add(checkColumn);


            //this.Controls.Add(dataGridView5);

            dataGridView5.ColumnCount = d[4].Columns.Count + 1;

            //dataGridView5.Columns[0].Name = "WMS NO";
            dataGridView5.Columns[1].Name = "작업일시";
            dataGridView5.Columns[2].Name = "입고일자";
            dataGridView5.Columns[3].Name = "B/L NO";
            dataGridView5.Columns[4].Name = "코드";
            dataGridView5.Columns[5].Name = "상호";
            dataGridView5.Columns[6].Name = "담당";
            dataGridView5.Columns[7].Name = "코드";
            dataGridView5.Columns[8].Name = "제품명";
            dataGridView5.Columns[9].Name = "규격";
            dataGridView5.Columns[10].Name = "수량";
            dataGridView5.Columns[11].Name = "단위";
            dataGridView5.Columns[12].Name = "전송";
            dataGridView5.Columns[13].Name = "표시";
            dataGridView5.Columns[14].Name = "비고";
            dataGridView5.Columns[15].Name = "WMS NO";
            dataGridView5.Columns[16].Name = "최초재고";
            dataGridView5.Columns[17].Name = "화물관리";


            //this.dataGridView5.Columns["암호"].Visible = false;
            //this.dataGridView5.Columns["권한"].Visible = false;

            // Columns AutoSize
            //dGVProcess.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells;

            // Head Cell_Columns Font Style = off
            dataGridView5.EnableHeadersVisualStyles = false;
            // Head Cell_Columns HeightSizeMode = off
            dataGridView5.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
           
            // Head Cell_Columns HeightSize 임의 지정
            dataGridView5.ColumnHeadersHeight = 60;

            // Head Rows Control
            dataGridView5.RowHeadersVisible = false;
            //dataGridView5.RowHeadersWidthSizeMode = DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            // RowHeadersWidth_Size 임의 지정
            dataGridView5.RowHeadersWidth = 20;



            for (int j = 0; j < d[4].Columns.Count; j++)
            {
                dataGridView5.Columns[j].HeaderCell.Style.Font = new Font("맑은고딕", 10, FontStyle.Bold);
                // dGVProcess.Columns[j].HeaderCell.Style.ForeColor = Color.Red;
                //dataGridView5.Columns[j].HeaderCell.Style.BackColor = Color.Yellow;

                // dGVState.Columns[j].HeaderCell.Size = new Size();

                // Head Cell_Columns Alignment
                //dataGridView5.Columns[j].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
                //if (j < 1) dataGridView5.Columns[j].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
                //else dataGridView5.Columns[j].HeaderCell.Style.Alignment = DataGridViewContentAlignment.BottomCenter;

                dataGridView5.Columns[j].HeaderCell.Style.Alignment = DataGridViewContentAlignment.BottomCenter;

                // Cell Font Style
                //dGVProcess.Columns[j].DefaultCellStyle.Font = new Font("Tahoma", 18, FontStyle.Bold);

                // Cell Font Alignment
                if (j == 2)
                {
                    dataGridView5.Columns[j].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                }
                //else
                //{
                //    dataGridView5.Columns[j].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                //
                //}

                //  Cell Height 지정
                //  var dgv = new DataGridView();
                //dataGridView5.RowTemplate.Height = 30;


                // Head Cell_Columns Width Size 임의 지정
                DataGridViewColumn column1 = dataGridView5.Columns[0];
                column1.Width = 40;
                DataGridViewColumn column2 = dataGridView5.Columns[1];
                column2.Width = 150;
                DataGridViewColumn column3 = dataGridView5.Columns[2];
                column3.Width = 100;
                DataGridViewColumn column4 = dataGridView5.Columns[3];
                column4.Width = 150;
                DataGridViewColumn column5 = dataGridView5.Columns[4];
                column5.Width = 60;
                DataGridViewColumn column6 = dataGridView5.Columns[5];
                column6.Width = 150;
                DataGridViewColumn column7 = dataGridView5.Columns[6];
                column7.Width = 60;
                DataGridViewColumn column8 = dataGridView5.Columns[7];
                column8.Width = 60;
                DataGridViewColumn column9 = dataGridView5.Columns[8];
                column9.Width = 150;
                DataGridViewColumn column10 = dataGridView5.Columns[9];
                column10.Width = 80;
                DataGridViewColumn column11 = dataGridView5.Columns[10];
                column11.Width = 70;
                DataGridViewColumn column12 = dataGridView5.Columns[11];
                column12.Width = 70;
                DataGridViewColumn column13 = dataGridView5.Columns[12];
                column13.Width = 70;
                DataGridViewColumn column14 = dataGridView5.Columns[13];
                column14.Width = 100;
                DataGridViewColumn column15 = dataGridView5.Columns[14];
                column15.Width = 150;
                DataGridViewColumn column16 = dataGridView5.Columns[15];
                column16.Width = 100;
                DataGridViewColumn column17 = dataGridView5.Columns[16];
                column17.Width = 100;
                DataGridViewColumn column18 = dataGridView5.Columns[17];
                column18.Width = 150;
            }

            dataGridView5_r1 = dataGridView5.GetCellDisplayRectangle(1, -1, false);
            dataGridView5_DoubleHead_width[0] = dataGridView5.GetCellDisplayRectangle(2, -1, false).Width;
            dataGridView5_DoubleHead_width[1] = dataGridView5.GetCellDisplayRectangle(3, -1, false).Width;
            dataGridView5_DoubleHead_width[2] = dataGridView5.GetCellDisplayRectangle(4, -1, false).Width;
            dataGridView5_DoubleHead_width[3] = dataGridView5.GetCellDisplayRectangle(5, -1, false).Width;
            dataGridView5_DoubleHead_width[4] = dataGridView5.GetCellDisplayRectangle(6, -1, false).Width;
            dataGridView5_DoubleHead_width[5] = dataGridView5.GetCellDisplayRectangle(7, -1, false).Width;

            dataGridView5_r2 = dataGridView5.GetCellDisplayRectangle(8, -1, false);
            dataGridView5_DoubleHead_width[10] = dataGridView5.GetCellDisplayRectangle(9, -1, false).Width;
            dataGridView5_DoubleHead_width[11] = dataGridView5.GetCellDisplayRectangle(10, -1, false).Width;

            dataGridView5_r3 = dataGridView5.GetCellDisplayRectangle(11, -1, false);
            dataGridView5_DoubleHead_width[20] = dataGridView5.GetCellDisplayRectangle(12, -1, false).Width;
            dataGridView5_DoubleHead_width[21] = dataGridView5.GetCellDisplayRectangle(13, -1, false).Width;
            dataGridView5_DoubleHead_width[22] = dataGridView5.GetCellDisplayRectangle(14, -1, false).Width;
            

        }
        private void dataGridView5_gvSheetListCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            foreach (DataGridViewRow r in dataGridView5.Rows)
            {
                //r.Cells["X"].Value = ((CheckBox)sender).Checked;

                r.Cells[0].Value = ((CheckBox)sender).Checked;
            }
        }
        private void dataGridView5_CellPainting(object sender, DataGridViewCellPaintingEventArgs e)
        {
            // Head Check BOX ============================================================================
            //if (e.ColumnIndex == 0 && e.RowIndex == -1)
            //{
            //    e.PaintBackground(e.ClipBounds, false);
            //
            //    Point pt = e.CellBounds.Location;  // where you want the bitmap in the cell
            //
            //    int nChkBoxWidth = 15;
            //    int nChkBoxHeight = 15;
            //    int offsetx = (e.CellBounds.Width - nChkBoxWidth) / 2;
            //    int offsety = (e.CellBounds.Height - nChkBoxHeight) / 2;
            //
            //    pt.X += offsetx;
            //    pt.Y += offsety;
            //
            //    CheckBox cb = new CheckBox();
            //    cb.Size = new Size(nChkBoxWidth, nChkBoxHeight);
            //    cb.Location = pt;
            //    cb.CheckedChanged += new EventHandler(dataGridView5_gvSheetListCheckBox_CheckedChanged);
            //
            //    ((DataGridView)sender).Controls.Add(cb);
            //
            //    e.Handled = true;
            //}

            // DoubleHead ============================================================================
            if (e.RowIndex == -1 && e.ColumnIndex > -1)
          
            {
                Rectangle G5_r = e.CellBounds;
          
                G5_r.Y += e.CellBounds.Height / 2;
          
                G5_r.Height = e.CellBounds.Height / 2;
          
                e.PaintBackground(G5_r, true);
          
                e.PaintContent(G5_r);
                
                e.Handled = true;
            }
        }

        private void dataGridView5_ColumnWidthChanged(object sender, DataGridViewColumnEventArgs e)
        {
            DataGridView gv = (DataGridView)sender;
          
            Rectangle rtHeader = gv.DisplayRectangle;
          
            rtHeader.Height = gv.ColumnHeadersHeight / 2;
          
            gv.Invalidate(rtHeader);
        }

        private void dataGridView5_Scroll(object sender, ScrollEventArgs e)
        {
            dataGridView5Set();

            //DataGridView gv = (DataGridView)sender;

            Rectangle rtHeader = dataGridView5.DisplayRectangle;
          
            rtHeader.Height = dataGridView5.ColumnHeadersHeight / 2;

            dataGridView5.Invalidate(rtHeader);
        }

        private void dataGridView5_Paint(object sender, PaintEventArgs e)
        {
            DataGridView gv = (DataGridView)sender;
           
            string[] strHeaders = { "작  업", "화  주", "제  품" };
           
            StringFormat format = new StringFormat();
           
            format.Alignment = StringAlignment.Center;
           
            format.LineAlignment = StringAlignment.Center;
           
           
            // Category Painting
           
            {
                dataGridView5_r1.X += 1;

                dataGridView5_r1.Y += 2;

                dataGridView5_r1.Width = dataGridView5_r1.Width + dataGridView5_DoubleHead_width[0] +
                                         dataGridView5_DoubleHead_width[1] + dataGridView5_DoubleHead_width[2] +
                                         dataGridView5_DoubleHead_width[3] + dataGridView5_DoubleHead_width[4] +
                                         dataGridView5_DoubleHead_width[5] + dataGridView5_DoubleHead_width[6] - 2;

                dataGridView5_r1.Height = (dataGridView5_r1.Height / 2) - 2;
           
                e.Graphics.DrawRectangle(new Pen(gv.BackgroundColor), dataGridView5_r1);
           
                e.Graphics.FillRectangle(new SolidBrush(gv.ColumnHeadersDefaultCellStyle.BackColor),
                //e.Graphics.FillRectangle(new SolidBrush(Color.Yellow),

                dataGridView5_r1);
           
           
                e.Graphics.DrawString(strHeaders[0],
           
                    //gv.ColumnHeadersDefaultCellStyle.Font,
                    new Font("맑은고딕", 9, FontStyle.Bold),
           
                    new SolidBrush(gv.ColumnHeadersDefaultCellStyle.ForeColor),

                    dataGridView5_r1,
           
                    format);
           
            }
           
            // Projection Painting
           
            {
                dataGridView5_r2.X += 1;
           
                dataGridView5_r2.Y += 2;

                dataGridView5_r2.Width = dataGridView5_r2.Width + dataGridView5_DoubleHead_width[10] +
                                         dataGridView5_DoubleHead_width[11] - 2;
           
                dataGridView5_r2.Height = (dataGridView5_r2.Height / 2) - 2;
           
                e.Graphics.DrawRectangle(new Pen(gv.BackgroundColor), dataGridView5_r2);
           
                e.Graphics.FillRectangle(new SolidBrush(gv.ColumnHeadersDefaultCellStyle.BackColor),
                //e.Graphics.FillRectangle(new SolidBrush(Color.Yellow),
           
                dataGridView5_r2);
           
           
                e.Graphics.DrawString(strHeaders[1],
           
                    //gv.ColumnHeadersDefaultCellStyle.Font,
                    new Font("맑은고딕", 9, FontStyle.Bold),
           
                    new SolidBrush(gv.ColumnHeadersDefaultCellStyle.ForeColor),
           
                    dataGridView5_r2,
           
                    format);
            }
           
            // GROUP HEAD 매출율
           
            {
                dataGridView5_r3.X += 1;
           
                dataGridView5_r3.Y += 2;
           
                dataGridView5_r3.Width = dataGridView5_r3.Width + dataGridView5_DoubleHead_width[20] +
                                         dataGridView5_DoubleHead_width[21] + dataGridView5_DoubleHead_width[22] - 2;
           
                dataGridView5_r3.Height = (dataGridView5_r3.Height / 2) - 2;
           
                e.Graphics.DrawRectangle(new Pen(gv.BackgroundColor), dataGridView5_r3);
           
                e.Graphics.FillRectangle(new SolidBrush(gv.ColumnHeadersDefaultCellStyle.BackColor),
                //e.Graphics.FillRectangle(new SolidBrush(Color.Yellow),
           
                dataGridView5_r3);
           
           
                e.Graphics.DrawString(strHeaders[2],
           
                    //gv.ColumnHeadersDefaultCellStyle.Font,
                    new Font("맑은고딕", 9, FontStyle.Bold),
           
                    new SolidBrush(gv.ColumnHeadersDefaultCellStyle.ForeColor),
           
                    dataGridView5_r3,
           
                    format);
            }
        }


        private void cmb_Set()
        {
            string[] vlaue_data01 = { "A-전체", "0-대기", "1-예약", "2-출력" };
            comboBox1.Items.AddRange(vlaue_data01);
            comboBox1.SelectedIndex = 0;

            string[] vlaue_data02 = { "A-전체", "11-고려오더", "12-생성오더", "13-수동오더" };
            comboBox2.Items.AddRange(vlaue_data02);
            comboBox2.SelectedIndex = 0;

            string[] vlaue_data03 = { "A-전체", "11-고려오더", "12-생성오더", "13-수동오더" };
            comboBox3.Items.AddRange(vlaue_data03);
            comboBox3.SelectedIndex = 0;

            //dateTimePicker1.Value = new DateTime(under3mon.Year, under3mon.Month, 1, 0, 0, 0);
            //dateTimePicker1.Value.ToString(now.AddDays(-7).ToString());
            //dateTimePicker2.Value = new DateTime(now.Year, now.Month, now.Day);
        }
        
    }
    
    
}
