﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace KSWMS
{
    public partial class KSWMS450 : MyFormPage
    {
        public KSWMS450()
        {
            InitializeComponent();
            this.pnl = panel1;
            this.btn1 = button1;
            this.btn2 = button2;
        }


        private void button1_Click(object sender, EventArgs e)
        {
            dataGridView1.Size = new Size(Convert.ToInt32(panel1.Size.Width.ToString()) - 5, Convert.ToInt32(panel1.Size.Height.ToString()) - 140);
            //panel3.Location = new Point(0, (Convert.ToInt32(panel1.Size.Height.ToString()) / 2) + 32);
            //dataGridView2.Location = new Point(0, (Convert.ToInt32(panel1.Size.Height.ToString()) / 2) + 80);
            //dataGridView2.Size = new Size(Convert.ToInt32(panel1.Size.Width.ToString()) - 5, Convert.ToInt32(panel1.Size.Height.ToString()) / 2 - 85);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            //label1.Text = "210 폼";
        }

        private void button10_Click(object sender, EventArgs e)
        {
            POPUP_거래처정보 PopupForm = new POPUP_거래처정보();
            PopupForm.ShowDialog();

            textBox1.Text = PopupForm.Passvalue_거래처코드;
        }

        private void button11_Click(object sender, EventArgs e)
        {
            POPUP_제품정보 PopupForm = new POPUP_제품정보();
            PopupForm.ShowDialog();

            textBox2.Text = PopupForm.Passvalue_제품코드;
        }

    }
}
