﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace KSWMS
{
    public partial class KSWMS710 : MyFormPage
    {
        private DataTable d1 = new DataTable();

        public KSWMS710(string u_name)
        {
            InitializeComponent();
            this.pnl = panel1;
            this.btn1 = button1;
            this.btn2 = button2;
            this.btn3 = button3;
           
            cmb_Set();

            if (u_name == "관리자")
            {             
                this.textBox1.Text = Common.m_InputLimit.ToString();
                this.panel5.Visible = true;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //MessageBox.Show(panel1.Size.ToString());
            panel2.Size = new Size(Convert.ToInt32(panel1.Size.Width.ToString()) - 5, 25);
            panel3.Size = new Size(Convert.ToInt32(panel1.Size.Width.ToString()) - 5, 25);
            panel4.Size = new Size(Convert.ToInt32(panel1.Size.Width.ToString()) - 5, 25);
            //dataGridView1.Size = new Size(Convert.ToInt32(panel1.Size.Width.ToString()) - 30, Convert.ToInt32(panel1.Size.Height.ToString()) - 100);
        }
        
        private void button2_Click(object sender, EventArgs e)
        {
            Main_Process();
        }
        private void button3_Click(object sender, EventArgs e)
        {
            List<string> Condition = new List<string>();


            DialogResult dr = MessageBox.Show("설비 사용정의를 저장하시겠습니까?.",
                              "설비 사용정의", MessageBoxButtons.YesNo, (MessageBoxIcon)32);
            switch (dr)
            {
                case DialogResult.Yes:

                    Condition.Add("110");
                    Condition.Add(State_Save(i110));
                    CommonDB.R_datatbl(Qry.KSWMS710_002(Condition));
                    Condition.Clear();

                    Condition.Add("120");
                    Condition.Add(State_Save(i120));
                    CommonDB.R_datatbl(Qry.KSWMS710_002(Condition));
                    Condition.Clear();

                    Condition.Add("130");
                    Condition.Add(State_Save(i130));
                    CommonDB.R_datatbl(Qry.KSWMS710_002(Condition));
                    Condition.Clear();

                    Condition.Add("140");
                    Condition.Add(State_Save(i140));
                    CommonDB.R_datatbl(Qry.KSWMS710_002(Condition));
                    Condition.Clear();

                    Condition.Add("210");
                    Condition.Add(State_Save(o210));
                    CommonDB.R_datatbl(Qry.KSWMS710_002(Condition));
                    Condition.Clear();

                    Condition.Add("220");
                    Condition.Add(State_Save(o220));
                    CommonDB.R_datatbl(Qry.KSWMS710_002(Condition));
                    Condition.Clear();

                    Condition.Add("230");
                    Condition.Add(State_Save(o230));
                    CommonDB.R_datatbl(Qry.KSWMS710_002(Condition));
                    Condition.Clear();

                    Condition.Add("240");
                    Condition.Add(State_Save(o240));
                    CommonDB.R_datatbl(Qry.KSWMS710_002(Condition));
                    Condition.Clear();

                    Condition.Add("310");
                    Condition.Add(State_Save(io310));
                    Condition.Add(IO_Save(st310));
                    Condition.Add(Stk(comboBox1));
                    CommonDB.R_datatbl(Qry.KSWMS710_002(Condition));
                    Condition.Clear();

                    Condition.Add("320");
                    Condition.Add(State_Save(io320));
                    Condition.Add(IO_Save(st320));
                    Condition.Add(Stk(comboBox2));
                    CommonDB.R_datatbl(Qry.KSWMS710_002(Condition));
                    Condition.Clear();

                    Condition.Add("330");
                    Condition.Add(State_Save(io330));
                    Condition.Add(IO_Save(st330));
                    Condition.Add(Stk(comboBox3));
                    CommonDB.R_datatbl(Qry.KSWMS710_002(Condition));
                    Condition.Clear();

                    Condition.Add("340");
                    Condition.Add(State_Save(io340));
                    Condition.Add(IO_Save(st340));
                    Condition.Add(Stk(comboBox4));
                    CommonDB.R_datatbl(Qry.KSWMS710_002(Condition));
                    Condition.Clear();

                    break;
                case DialogResult.No:
                    break;
            }
        }


        private void cmb_Set()
        {
            string[] vlaue_data01 = { "0-전체", "1-1호기", "2-2호기", "3-3호기"};
            comboBox1.Items.AddRange(vlaue_data01);
            comboBox1.SelectedIndex = 0;

            string[] vlaue_data02 = { "0-전체", "1-1호기", "2-2호기", "3-3호기", "4-4호기" };
            comboBox2.Items.AddRange(vlaue_data02);
            comboBox2.SelectedIndex = 0;

            string[] vlaue_data03 = { "0-전체", "1-1호기", "2-2호기", "3-3호기", "4-4호기" };
            comboBox3.Items.AddRange(vlaue_data03);
            comboBox3.SelectedIndex = 0;

            string[] vlaue_data04 = { "0-전체", "2-2호기", "3-3호기", "4-4호기" };
            comboBox4.Items.AddRange(vlaue_data04);
            comboBox4.SelectedIndex = 0;
        }

        private void buttonCheck_Click(object sender, EventArgs e)
        {
            Button CheckButton = (Button) sender;

            if (CheckButton.Text == "사용")
            {
                CheckButton.Text = "금지";
                CheckButton.BackColor = Color.Red;
            } 
            else
            {
                CheckButton.Text = "사용";
                CheckButton.BackColor = Color.Blue;
            }
        }
        public static String State_Save(Button btn)
        {
            string result;

            if (btn.Text == "사용") result = "1";
            else result = "0";
            
            return result;
        }

        public static String IO_Save(Button btn)
        {
            string result;

            if (btn.Text == "입고") result = "I";
            else result = "O";

            return result;
        }

        public static String Stk(ComboBox cmb)
        {
            string result;

            result = cmb.Text.Substring(0, 1);

            return result;
        }

        private void Main_Process()
        {
            d1 = CommonDB.R_datatbl(Qry.KSWMS710_001);
            int j = 0;

            while (j < d1.Rows.Count)
            {
                if (d1.Rows[j][0].ToString() == "110") useBtn_Chk(d1, i110, j);
                else if (d1.Rows[j][0].ToString() == "120") useBtn_Chk(d1, i120, j);
                else if (d1.Rows[j][0].ToString() == "130") useBtn_Chk(d1, i130, j);
                else if (d1.Rows[j][0].ToString() == "140") useBtn_Chk(d1, i140, j);
                else if (d1.Rows[j][0].ToString() == "210") useBtn_Chk(d1, o210, j);
                else if (d1.Rows[j][0].ToString() == "220") useBtn_Chk(d1, o220, j);
                else if (d1.Rows[j][0].ToString() == "230") useBtn_Chk(d1, o230, j);
                else if (d1.Rows[j][0].ToString() == "240") useBtn_Chk(d1, o240, j);
                else if (d1.Rows[j][0].ToString() == "310")
                {
                    useBtn_Chk(d1, io310, j);
                    stBtn_Chk(d1, st310, j);
                }
                else if (d1.Rows[j][0].ToString() == "320")
                {
                    useBtn_Chk(d1, io320, j);
                    stBtn_Chk(d1, st320, j);
                }
                else if (d1.Rows[j][0].ToString() == "330")
                {
                    useBtn_Chk(d1, io330, j);
                    stBtn_Chk(d1, st330, j);
                }
                else if (d1.Rows[j][0].ToString() == "340")
                {
                    useBtn_Chk(d1, io340, j);
                    stBtn_Chk(d1, st340, j);
                }

                j++;
            }
        }

        private void useBtn_Chk(DataTable dt, Button btn, int j)
        {
            if (dt.Rows[j][1].ToString() == "1")
            {
                btn.Text = "사용";
                btn.BackColor = Color.Blue;
            }
            else
            {
                btn.Text = "금지";
                btn.BackColor = Color.Red;
            }
        }

        private void stBtn_Chk(DataTable dt, Button btn, int j)
        {
            if (dt.Rows[j][2].ToString() == "I")
            {
                btn.Text = "입고";
                btn.BackColor = Color.WhiteSmoke;
            }
            else
            {
                btn.Text = "출고";
                btn.BackColor = Color.LawnGreen;
            }
        }

        private void btnInputLimit_Click(object sender, EventArgs e)
        {
            int val = 100;
            string txbox = this.textBox1.Text;
            if (int.TryParse(txbox, out val))
            {
                Common.m_InputLimit = val;
            }
        }
    }
}
