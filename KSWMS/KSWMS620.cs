﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace KSWMS
{
    public partial class KSWMS620 : MyFormPage
    {
        public KSWMS620()
        {
            InitializeComponent();
            this.pnl = panel1;
            this.btn1 = button1;
            this.btn2 = button2;

        }

        private void button1_Click(object sender, EventArgs e)
        {
            tabControl1.Size = new Size(Convert.ToInt32(panel1.Size.Width.ToString()), Convert.ToInt32(panel1.Size.Height.ToString()) - 50);

            //MessageBox.Show(panel1.Size.ToString());
            panel50.Size = new Size(Convert.ToInt32(panel1.Size.Width.ToString()) - 5, 25);
            panel2.Size = new Size(Convert.ToInt32(panel1.Size.Width.ToString()) - 5, 25);
            panel3.Size = new Size(Convert.ToInt32(panel1.Size.Width.ToString()) - 5, 25);
            panel4.Size = new Size(Convert.ToInt32(panel1.Size.Width.ToString()) - 5, 25);
            panel5.Size = new Size(Convert.ToInt32(panel1.Size.Width.ToString()) - 5, 25);
            panel6.Size = new Size(Convert.ToInt32(panel1.Size.Width.ToString()) - 5, 25);
            panel7.Size = new Size(Convert.ToInt32(panel1.Size.Width.ToString()) - 5, 25);
            panel8.Size = new Size(Convert.ToInt32(panel1.Size.Width.ToString()) - 5, 25);
            panel9.Size = new Size(Convert.ToInt32(panel1.Size.Width.ToString()) - 5, 25);
            panel10.Size = new Size(Convert.ToInt32(panel1.Size.Width.ToString()) - 5, 25);
            panel11.Size = new Size(Convert.ToInt32(panel1.Size.Width.ToString()) - 5, 25);
            panel12.Size = new Size(Convert.ToInt32(panel1.Size.Width.ToString()) - 5, 25);
            panel13.Size = new Size(Convert.ToInt32(panel1.Size.Width.ToString()) - 5, 25);
            panel14.Size = new Size(Convert.ToInt32(panel1.Size.Width.ToString()) - 5, 25);
            panel15.Size = new Size(Convert.ToInt32(panel1.Size.Width.ToString()) - 5, 25);
            panel16.Size = new Size(Convert.ToInt32(panel1.Size.Width.ToString()) - 5, 25);

            dataGridView1.Size = new Size(Convert.ToInt32(panel1.Size.Width.ToString()) - 8, Convert.ToInt32(panel1.Size.Height.ToString()) / 2 - 78);
            dataGridView3.Size = new Size(Convert.ToInt32(panel1.Size.Width.ToString()) - 8, Convert.ToInt32(panel1.Size.Height.ToString()) / 2 - 78);
            dataGridView5.Size = new Size(Convert.ToInt32(panel1.Size.Width.ToString()) - 8, Convert.ToInt32(panel1.Size.Height.ToString()) / 2 - 78);
            dataGridView7.Size = new Size(Convert.ToInt32(panel1.Size.Width.ToString()) - 8, Convert.ToInt32(panel1.Size.Height.ToString()) / 2 - 78);
            dataGridView9.Size = new Size(Convert.ToInt32(panel1.Size.Width.ToString()) - 8, Convert.ToInt32(panel1.Size.Height.ToString()) / 2 - 78);
            dataGridView11.Size = new Size(Convert.ToInt32(panel1.Size.Width.ToString()) - 8, Convert.ToInt32(panel1.Size.Height.ToString()) / 2 - 78);
            dataGridView13.Size = new Size(Convert.ToInt32(panel1.Size.Width.ToString()) - 8, Convert.ToInt32(panel1.Size.Height.ToString()) / 2 - 78);
            dataGridView15.Size = new Size(Convert.ToInt32(panel1.Size.Width.ToString()) - 8, Convert.ToInt32(panel1.Size.Height.ToString()) / 2 - 78);
           
            panel2.Location = new Point(0, (Convert.ToInt32(panel1.Size.Height.ToString()) / 2) - 52);
            panel4.Location = new Point(0, (Convert.ToInt32(panel1.Size.Height.ToString()) / 2) - 52);
            panel6.Location = new Point(0, (Convert.ToInt32(panel1.Size.Height.ToString()) / 2) - 52);
            panel8.Location = new Point(0, (Convert.ToInt32(panel1.Size.Height.ToString()) / 2) - 52);
            panel12.Location = new Point(0, (Convert.ToInt32(panel1.Size.Height.ToString()) / 2) - 52);
            panel14.Location = new Point(0, (Convert.ToInt32(panel1.Size.Height.ToString()) / 2) - 52);
            panel16.Location = new Point(0, (Convert.ToInt32(panel1.Size.Height.ToString()) / 2) - 52);
            panel10.Location = new Point(0, (Convert.ToInt32(panel1.Size.Height.ToString()) / 2) - 52);

            dataGridView2.Location = new Point(0, (Convert.ToInt32(panel1.Size.Height.ToString()) / 2) - 27);
            dataGridView4.Location = new Point(0, (Convert.ToInt32(panel1.Size.Height.ToString()) / 2) - 27);
            dataGridView6.Location = new Point(0, (Convert.ToInt32(panel1.Size.Height.ToString()) / 2) - 27);
            dataGridView8.Location = new Point(0, (Convert.ToInt32(panel1.Size.Height.ToString()) / 2) - 27);
            dataGridView12.Location = new Point(0, (Convert.ToInt32(panel1.Size.Height.ToString()) / 2) - 27);
            dataGridView14.Location = new Point(0, (Convert.ToInt32(panel1.Size.Height.ToString()) / 2) - 27);
            dataGridView16.Location = new Point(0, (Convert.ToInt32(panel1.Size.Height.ToString()) / 2) - 27);
            dataGridView10.Location = new Point(0, (Convert.ToInt32(panel1.Size.Height.ToString()) / 2) - 27);

            dataGridView2.Size = new Size(Convert.ToInt32(panel1.Size.Width.ToString()) - 8, Convert.ToInt32(panel1.Size.Height.ToString()) / 2 - 55);
            dataGridView4.Size = new Size(Convert.ToInt32(panel1.Size.Width.ToString()) - 8, Convert.ToInt32(panel1.Size.Height.ToString()) / 2 - 55);
            dataGridView6.Size = new Size(Convert.ToInt32(panel1.Size.Width.ToString()) - 8, Convert.ToInt32(panel1.Size.Height.ToString()) / 2 - 55);
            dataGridView8.Size = new Size(Convert.ToInt32(panel1.Size.Width.ToString()) - 8, Convert.ToInt32(panel1.Size.Height.ToString()) / 2 - 55);
            dataGridView10.Size = new Size(Convert.ToInt32(panel1.Size.Width.ToString()) - 8, Convert.ToInt32(panel1.Size.Height.ToString()) / 2 - 55);
            dataGridView12.Size = new Size(Convert.ToInt32(panel1.Size.Width.ToString()) - 8, Convert.ToInt32(panel1.Size.Height.ToString()) / 2 - 55);
            dataGridView14.Size = new Size(Convert.ToInt32(panel1.Size.Width.ToString()) - 8, Convert.ToInt32(panel1.Size.Height.ToString()) / 2 - 55);
            dataGridView16.Size = new Size(Convert.ToInt32(panel1.Size.Width.ToString()) - 8, Convert.ToInt32(panel1.Size.Height.ToString()) / 2 - 55);

        }
        
        private void button2_Click(object sender, EventArgs e)
        {
            //label1.Text = "620 폼";
        }
    }
}
