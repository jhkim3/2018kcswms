﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace KSWMS
{
    public partial class KSWMS410 : MyFormPage
    {
        DataTable d1 = new DataTable();

        public KSWMS410()
        {
            InitializeComponent();
            this.pnl = panel1;
            this.btn1 = button1;
            this.btn2 = button2;
            this.btn3 = button3;

            cmb_Set();
        }

        private void Main_Process()
        {
            Cursor.Current = Cursors.WaitCursor;

            List<string> Condition = new List<string>();
            if (checkBox1.Checked == true) Condition.Add("Y");              // 0
            else Condition.Add("N");
            Condition.Add(dateTimePicker1.Value.ToShortDateString());       // 1
            Condition.Add(dateTimePicker2.Value.ToShortDateString());       // 2

            if (checkBox2.Checked == true) Condition.Add("Y");              // 3
            else Condition.Add("N");
            string loc;
            loc = numericUpDown1.Value.ToString() + numericUpDown2.Value.ToString();
            if (numericUpDown3.Value < 10) loc += "0" + numericUpDown3.Value.ToString();
            else loc += numericUpDown3.Value.ToString();
            if (numericUpDown4.Value < 10) loc += "0" + numericUpDown4.Value.ToString();
            else loc += numericUpDown4.Value.ToString();
            Condition.Add(loc);                                               // 4
            Condition.Add(textBox1.Text);                                     // 5
            Condition.Add(textBox2.Text);                                     // 6
            Condition.Add(textBox3.Text);                                     // 7
            Condition.Add(textBox4.Text);                                     // 8
            Condition.Add(comboBox1.SelectedItem.ToString().Substring(0, 1)); // 9
            Condition.Add(comboBox2.SelectedItem.ToString().Substring(0, 1)); // 10
            Condition.Add(comboBox3.SelectedItem.ToString().Substring(0, 1)); // 11

            d1 = CommonDB.R_datatbl(Qry.KSWMS410_001(Condition));

            Clear();
            dataGridView1Set();

            int j = 0;

            while (j < d1.Rows.Count)
            {
                int n = dataGridView1.Rows.Add();

                for (int i = 0; i < d1.Columns.Count; i++)
                {
                    dataGridView1.Rows[n].Cells[i].Value = d1.Rows[j][i].ToString();
                }

                j++;
            }

            textBox5.Text = j.ToString();
        }

        private void Clear()
        {
            // 그리드 초기화
            dataGridView1.DataSource = null;
            dataGridView1.Columns.Clear();
        }

        private void dataGridView1Set()
        {
            //this.Controls.Add(dataGridView1);

            dataGridView1.ColumnCount = d1.Columns.Count;

            dataGridView1.Columns[0].Name = "Location";
            dataGridView1.Columns[1].Name = "상태";
            dataGridView1.Columns[2].Name = "재고";
            dataGridView1.Columns[3].Name = "사용";
            dataGridView1.Columns[4].Name = "입고유형";
            dataGridView1.Columns[5].Name = "구역";
            dataGridView1.Columns[6].Name = "창고입고";
            dataGridView1.Columns[7].Name = "입고일자";
            dataGridView1.Columns[8].Name = "B/L NO";
            dataGridView1.Columns[9].Name = "바코드";
            dataGridView1.Columns[10].Name = "코드";
            dataGridView1.Columns[11].Name = "상호";
            dataGridView1.Columns[12].Name = "담당";
            dataGridView1.Columns[13].Name = "코드";
            dataGridView1.Columns[14].Name = "제품명";
            dataGridView1.Columns[15].Name = "규격";
            dataGridView1.Columns[16].Name = "수량";
            dataGridView1.Columns[17].Name = "단위";
            dataGridView1.Columns[18].Name = "표시";
            dataGridView1.Columns[19].Name = "비고";
            dataGridView1.Columns[20].Name = "오더량";
            dataGridView1.Columns[21].Name = "화물관리";
            dataGridView1.Columns[22].Name = "통관일자";
            dataGridView1.Columns[23].Name = "WMS NO";
            dataGridView1.Columns[24].Name = "순번";
            dataGridView1.Columns[25].Name = "최초재고";
            dataGridView1.Columns[26].Name = "대표코드";
            dataGridView1.Columns[27].Name = "코드";
            dataGridView1.Columns[28].Name = "상호";
            dataGridView1.Columns[29].Name = "담당";
            dataGridView1.Columns[30].Name = "코드";
            dataGridView1.Columns[31].Name = "선박명";

            this.dataGridView1.Columns[0].Frozen = true;
            //this.dataGridView1.Columns[1].Visible = false;

            // Columns AutoSize
            //dGVProcess.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells;

            // Head Cell_Columns Font Style = off
            dataGridView1.EnableHeadersVisualStyles = false;
            // Head Cell_Columns HeightSizeMode = off
            dataGridView1.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.DisableResizing;

            // Head Cell_Columns Width Size 임의 지정
            DataGridViewColumn column1 = dataGridView1.Columns[0];
            column1.Width = 100;
            DataGridViewColumn column2 = dataGridView1.Columns[1];
            column2.Width = 70;
            DataGridViewColumn column3 = dataGridView1.Columns[2];
            column3.Width = 70;
            DataGridViewColumn column4 = dataGridView1.Columns[3];
            column4.Width = 70;
            DataGridViewColumn column5 = dataGridView1.Columns[4];
            column5.Width = 100;
            DataGridViewColumn column6 = dataGridView1.Columns[5];
            column6.Width = 70;
            DataGridViewColumn column7 = dataGridView1.Columns[6];
            column7.Width = 100;
            DataGridViewColumn column8 = dataGridView1.Columns[7];
            column8.Width = 100;
            DataGridViewColumn column9 = dataGridView1.Columns[8];
            column9.Width = 150;
            DataGridViewColumn column10 = dataGridView1.Columns[9];
            column10.Width = 100;
            DataGridViewColumn column11 = dataGridView1.Columns[10];
            column11.Width = 70;
            DataGridViewColumn column12 = dataGridView1.Columns[11];
            column12.Width = 120;
            DataGridViewColumn column13 = dataGridView1.Columns[12];
            column13.Width = 70;
            DataGridViewColumn column14 = dataGridView1.Columns[13];
            column14.Width = 70;
            DataGridViewColumn column15 = dataGridView1.Columns[14];
            column15.Width = 150;
            DataGridViewColumn column16 = dataGridView1.Columns[15];
            column16.Width = 70;
            DataGridViewColumn column17 = dataGridView1.Columns[16];
            column17.Width = 70;
            DataGridViewColumn column18 = dataGridView1.Columns[17];
            column18.Width = 70;
            DataGridViewColumn column19 = dataGridView1.Columns[18]; //표시
            column19.Width = 80;
            DataGridViewColumn column20 = dataGridView1.Columns[19];
            column20.Width = 100;
            DataGridViewColumn column21 = dataGridView1.Columns[20];
            column21.Width = 70;
            DataGridViewColumn column22 = dataGridView1.Columns[21];
            column22.Width = 150;
            DataGridViewColumn column23 = dataGridView1.Columns[22];
            column23.Width = 100;
            DataGridViewColumn column24 = dataGridView1.Columns[23];
            column24.Width = 100;
            DataGridViewColumn column25 = dataGridView1.Columns[24];
            column25.Width = 80;
            DataGridViewColumn column26 = dataGridView1.Columns[25];
            column26.Width = 100;
            DataGridViewColumn column27 = dataGridView1.Columns[26];
            column27.Width = 250;
            DataGridViewColumn column28 = dataGridView1.Columns[27];
            column28.Width = 70;
            DataGridViewColumn column29 = dataGridView1.Columns[28];
            column29.Width = 100;
            DataGridViewColumn column30 = dataGridView1.Columns[29];
            column30.Width = 70;
            DataGridViewColumn column31 = dataGridView1.Columns[30];
            column31.Width = 70;
            DataGridViewColumn column32 = dataGridView1.Columns[31];
            column32.Width = 100;

            // Head Cell_Columns HeightSize 임의 지정
            dataGridView1.ColumnHeadersHeight = 60;

            for (int j = 0; j < d1.Columns.Count; j++)
            {
                dataGridView1.Columns[j].HeaderCell.Style.Font = new Font("맑은고딕", 10, FontStyle.Bold);
                // dGVProcess.Columns[j].HeaderCell.Style.ForeColor = Color.Red;
                //dataGridView1.Columns[j].HeaderCell.Style.BackColor = Color.Yellow;

                // dGVState.Columns[j].HeaderCell.Size = new Size();

                // Head Cell_Columns Alignment
                dataGridView1.Columns[j].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;

                // Cell Font Style
                //dGVProcess.Columns[j].DefaultCellStyle.Font = new Font("Tahoma", 18, FontStyle.Bold);
                if (j < 1) dataGridView1.Columns[j].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
                else dataGridView1.Columns[j].HeaderCell.Style.Alignment = DataGridViewContentAlignment.BottomCenter;

                // Cell Font Alignment
                if (j == 2)
                {
                    dataGridView1.Columns[j].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                }
                //else
                //{
                //    dataGridView1.Columns[j].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                //
                //}

                //  Cell Height 지정
                //  var dgv = new DataGridView();
                //dataGridView1.RowTemplate.Height = 30;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //MessageBox.Show(panel1.Size.ToString());
            panel2.Size = new Size(Convert.ToInt32(panel1.Size.Width.ToString()) - 5, 25);
            dataGridView1.Size = new Size(Convert.ToInt32(panel1.Size.Width.ToString()) - 5, Convert.ToInt32(panel1.Size.Height.ToString()) - 145);
            //panel3.Location = new Point((Convert.ToInt32(panel1.Size.Width.ToString()) / 2) + 15, 0);
            //panel4.Location = new Point((Convert.ToInt32(panel1.Size.Width.ToString()) / 2) + 15, 26);
            //dataGridView2.Location = new Point((Convert.ToInt32(panel1.Size.Width.ToString()) / 2) + 15, 126);
            //panel3.Size = new Size((Convert.ToInt32(panel1.Size.Width.ToString()) / 2) - 20, 25);
            //dataGridView2.Size = new Size((Convert.ToInt32(panel1.Size.Width.ToString()) / 2) - 20, Convert.ToInt32(panel1.Size.Height.ToString()) - 126);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Main_Process();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            ToExcel.WirteExcel(dataGridView1, "LOCATION 조회");
        }
        
        private void dataGridView1_CellPainting(object sender, DataGridViewCellPaintingEventArgs e)
        {
            if (e.RowIndex == -1 && e.ColumnIndex > -1)

            {
                Rectangle r = e.CellBounds;

                r.Y += e.CellBounds.Height / 2;

                r.Height = e.CellBounds.Height / 2;

                e.PaintBackground(r, true);

                e.PaintContent(r);

                e.Handled = true;
            }
        }

        private void dataGridView1_ColumnWidthChanged(object sender, DataGridViewColumnEventArgs e)
        {
            DataGridView gv = (DataGridView)sender;

            Rectangle rtHeader = gv.DisplayRectangle;

            rtHeader.Height = gv.ColumnHeadersHeight / 2;

            gv.Invalidate(rtHeader);
        }

        private void dataGridView1_Paint(object sender, PaintEventArgs e)
        {
            DataGridView gv = (DataGridView)sender;

            string[] strHeaders = { "랙정보", "오더정보", "화주", "제품", "오더정보", "관리", "선박" };

            StringFormat format = new StringFormat();

            format.Alignment = StringAlignment.Center;

            format.LineAlignment = StringAlignment.Center;
            
            // Category Painting

            {
                Rectangle r1 = gv.GetCellDisplayRectangle(1, -1, false);
                int width1 = gv.GetCellDisplayRectangle(2, -1, false).Width;
                int width2 = gv.GetCellDisplayRectangle(3, -1, false).Width;
                int width3 = gv.GetCellDisplayRectangle(4, -1, false).Width;
                int width4 = gv.GetCellDisplayRectangle(5, -1, false).Width;
                int width5 = gv.GetCellDisplayRectangle(6, -1, false).Width;

                r1.X += 1;

                r1.Y += 2;

                r1.Width = r1.Width + width1 + width2 + width3 + width4 + width5 - 2;

                r1.Height = (r1.Height / 2) - 2;

                e.Graphics.DrawRectangle(new Pen(gv.BackgroundColor), r1);

                e.Graphics.FillRectangle(new SolidBrush(gv.ColumnHeadersDefaultCellStyle.BackColor),
                //e.Graphics.FillRectangle(new SolidBrush(Color.Yellow),

                r1);


                e.Graphics.DrawString(strHeaders[0],

                    //gv.ColumnHeadersDefaultCellStyle.Font,
                    new Font("맑은고딕", 9, FontStyle.Bold),

                    new SolidBrush(gv.ColumnHeadersDefaultCellStyle.ForeColor),

                    r1,

                    format);
            }

            // Projection Painting

            {
                Rectangle r2 = gv.GetCellDisplayRectangle(7, -1, false);
                int width1 = gv.GetCellDisplayRectangle(8, -1, false).Width;
                int width2 = gv.GetCellDisplayRectangle(9, -1, false).Width;

                r2.X += 1;

                r2.Y += 2;

                r2.Width = r2.Width + width1 + width2 - 2;

                r2.Height = (r2.Height / 2) - 2;

                e.Graphics.DrawRectangle(new Pen(gv.BackgroundColor), r2);

                e.Graphics.FillRectangle(new SolidBrush(gv.ColumnHeadersDefaultCellStyle.BackColor), 
                //e.Graphics.FillRectangle(new SolidBrush(Color.Yellow),

                r2);


                e.Graphics.DrawString(strHeaders[1],

                    //gv.ColumnHeadersDefaultCellStyle.Font,
                    new Font("맑은고딕", 9, FontStyle.Bold),

                    new SolidBrush(gv.ColumnHeadersDefaultCellStyle.ForeColor),

                    r2,

                    format);
            }
            
            {
                Rectangle r3 = gv.GetCellDisplayRectangle(10, -1, false);
                int width1 = gv.GetCellDisplayRectangle(11, -1, false).Width;
                int width2 = gv.GetCellDisplayRectangle(12, -1, false).Width;

                r3.X += 1;

                r3.Y += 2;

                r3.Width = r3.Width + width1 + width2 - 2;

                r3.Height = (r3.Height / 2) - 2;

                e.Graphics.DrawRectangle(new Pen(gv.BackgroundColor), r3);

                e.Graphics.FillRectangle(new SolidBrush(gv.ColumnHeadersDefaultCellStyle.BackColor), 
                //e.Graphics.FillRectangle(new SolidBrush(Color.Yellow),

                r3);
                
                e.Graphics.DrawString(strHeaders[2],

                    //gv.ColumnHeadersDefaultCellStyle.Font,
                    new Font("맑은고딕", 9, FontStyle.Bold),

                    new SolidBrush(gv.ColumnHeadersDefaultCellStyle.ForeColor),

                    r3,

                    format);
            }

            {
                Rectangle r4 = gv.GetCellDisplayRectangle(13, -1, false);
                int width1 = gv.GetCellDisplayRectangle(14, -1, false).Width;
                int width2 = gv.GetCellDisplayRectangle(15, -1, false).Width;
                int width3 = gv.GetCellDisplayRectangle(16, -1, false).Width;
                int width4 = gv.GetCellDisplayRectangle(17, -1, false).Width;

                r4.X += 1;

                r4.Y += 2;

                r4.Width = r4.Width + width1 + width2 + width3 + width4 - 2;

                r4.Height = (r4.Height / 2) - 2;

                e.Graphics.DrawRectangle(new Pen(gv.BackgroundColor), r4);

                e.Graphics.FillRectangle(new SolidBrush(gv.ColumnHeadersDefaultCellStyle.BackColor),
                //e.Graphics.FillRectangle(new SolidBrush(Color.Yellow),

                r4);

                e.Graphics.DrawString(strHeaders[3],

                    //gv.ColumnHeadersDefaultCellStyle.Font,
                    new Font("맑은고딕", 9, FontStyle.Bold),

                    new SolidBrush(gv.ColumnHeadersDefaultCellStyle.ForeColor),

                    r4,

                    format);
            }

            {
                Rectangle r5 = gv.GetCellDisplayRectangle(18, -1, false);
                int width1 = gv.GetCellDisplayRectangle(19, -1, false).Width;
                int width2 = gv.GetCellDisplayRectangle(20, -1, false).Width;
                int width3 = gv.GetCellDisplayRectangle(21, -1, false).Width;
                int width4 = gv.GetCellDisplayRectangle(22, -1, false).Width;
                int width5 = gv.GetCellDisplayRectangle(23, -1, false).Width;
                int width6 = gv.GetCellDisplayRectangle(24, -1, false).Width;
                int width7 = gv.GetCellDisplayRectangle(25, -1, false).Width;
                int width8 = gv.GetCellDisplayRectangle(26, -1, false).Width;

                r5.X += 1;

                r5.Y += 2;

                r5.Width = r5.Width + width1 + width2 + width3 + width4 + width5 + width6 + width7 + width8 - 2;

                r5.Height = (r5.Height / 2) - 2;

                e.Graphics.DrawRectangle(new Pen(gv.BackgroundColor), r5);

                e.Graphics.FillRectangle(new SolidBrush(gv.ColumnHeadersDefaultCellStyle.BackColor),
                //e.Graphics.FillRectangle(new SolidBrush(Color.Yellow),

                r5);

                e.Graphics.DrawString(strHeaders[4],

                    //gv.ColumnHeadersDefaultCellStyle.Font,
                    new Font("맑은고딕", 9, FontStyle.Bold),

                    new SolidBrush(gv.ColumnHeadersDefaultCellStyle.ForeColor),

                    r5,

                    format);
            }

            {
                Rectangle r6 = gv.GetCellDisplayRectangle(27, -1, false);
                int width1 = gv.GetCellDisplayRectangle(28, -1, false).Width;
                int width2 = gv.GetCellDisplayRectangle(29, -1, false).Width;
               
                r6.X += 1;

                r6.Y += 2;

                r6.Width = r6.Width + width1 + width2 - 2;

                r6.Height = (r6.Height / 2) - 2;

                e.Graphics.DrawRectangle(new Pen(gv.BackgroundColor), r6);

                e.Graphics.FillRectangle(new SolidBrush(gv.ColumnHeadersDefaultCellStyle.BackColor),
                //e.Graphics.FillRectangle(new SolidBrush(Color.Yellow),

                r6);

                e.Graphics.DrawString(strHeaders[5],

                    //gv.ColumnHeadersDefaultCellStyle.Font,
                    new Font("맑은고딕", 9, FontStyle.Bold),

                    new SolidBrush(gv.ColumnHeadersDefaultCellStyle.ForeColor),

                    r6,

                    format);
            }

            {
                Rectangle r7 = gv.GetCellDisplayRectangle(30, -1, false);
                int width1 = gv.GetCellDisplayRectangle(31, -1, false).Width;

                r7.X += 1;

                r7.Y += 2;

                r7.Width = r7.Width + width1 - 2;

                r7.Height = (r7.Height / 2) - 2;

                e.Graphics.DrawRectangle(new Pen(gv.BackgroundColor), r7);

                e.Graphics.FillRectangle(new SolidBrush(gv.ColumnHeadersDefaultCellStyle.BackColor),
                //e.Graphics.FillRectangle(new SolidBrush(Color.Yellow),

                r7);

                e.Graphics.DrawString(strHeaders[6],

                    //gv.ColumnHeadersDefaultCellStyle.Font,
                    new Font("맑은고딕", 9, FontStyle.Bold),

                    new SolidBrush(gv.ColumnHeadersDefaultCellStyle.ForeColor),

                    r7,

                    format);
            }
        }

        private void dataGridView1_Scroll(object sender, ScrollEventArgs e)
        {
            DataGridView gv = (DataGridView)sender;

            Rectangle rtHeader = gv.DisplayRectangle;

            rtHeader.Height = gv.ColumnHeadersHeight / 2;

            gv.Invalidate(rtHeader);
        }


        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox1.Checked == true)
            {
                dateTimePicker1.Enabled = false;
                dateTimePicker2.Enabled = false;
            }
            else
            {
                dateTimePicker1.Enabled = true;
                dateTimePicker2.Enabled = true;
            }
        }
        private void checkBox2_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox2.Checked == true)
            {
                numericUpDown1.Enabled = false;
                numericUpDown2.Enabled = false;
                numericUpDown3.Enabled = false;
                numericUpDown4.Enabled = false;
            }
            else
            {
                numericUpDown1.Enabled = true;
                numericUpDown2.Enabled = true;
                numericUpDown3.Enabled = true;
                numericUpDown4.Enabled = true;
            }
        }

        private void button10_Click(object sender, EventArgs e)
        {
            POPUP_거래처정보 PopupForm = new POPUP_거래처정보();
            PopupForm.ShowDialog();

            textBox1.Text = PopupForm.Passvalue_거래처코드;
        }

        private void button11_Click(object sender, EventArgs e)
        {
            POPUP_제품정보 PopupForm = new POPUP_제품정보();
            PopupForm.ShowDialog();

            textBox2.Text = PopupForm.Passvalue_제품코드;
        }

        private void cmb_Set()
        {
            string[] vlaue_data01 = { "A-전체", "0-공랙", "1-재고", "2-공PLT" };
            comboBox1.Items.AddRange(vlaue_data01);
            comboBox1.SelectedIndex = 0;

            string[] vlaue_data02 = { "A-전체", "O-사용", "X-금지" };
            comboBox2.Items.AddRange(vlaue_data02);
            comboBox2.SelectedIndex = 0;

            string[] vlaue_data03 = { "A-전체", "0-정상", "1-입고중", "2-출고중", "3-예약중", "4-수정중", "5-공출고", "6-이중입고", "7-재고확인", "9-사용금지", "R-출고예약", "X-재고이상" };
            comboBox3.Items.AddRange(vlaue_data03);
            comboBox3.SelectedIndex = 0;

            //dateTimePicker1.Value = new DateTime(under3mon.Year, under3mon.Month, 1, 0, 0, 0);
            //dateTimePicker1.Value.ToString(now.AddDays(-7).ToString());
            //dateTimePicker2.Value = new DateTime(now.Year, now.Month, now.Day);
        }

       
    }
}
