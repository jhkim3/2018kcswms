﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace KSWMS
{
    public partial class POPUP_선박정보 : Form
    {
        DataTable d1 = new DataTable();
        private string[] POPUP_선박정보_Value = new string[5];
        public string Passvalue_선박코드
        {
            get { return this.POPUP_선박정보_Value[0]; }
            set { this.POPUP_선박정보_Value[0] = value; }
        }
        public string Passvalue_선박명
        {
            get { return this.POPUP_선박정보_Value[1]; }
            set { this.POPUP_선박정보_Value[1] = value; }
        }

        public POPUP_선박정보()
        {
            InitializeComponent();
            Common.setTextBoxEnter(textBox1, button2);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            textBox1.Text = "";
            textBox2.Text = "";
            button2.PerformClick();
        }
        
        private void button2_Click(object sender, EventArgs e)
        {
            Main_Process();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Application.OpenForms["POPUP_선박정보"].Close();
        }

        private void Main_Process()
        {
            Cursor.Current = Cursors.WaitCursor;

            List<string> Condition = new List<string>();
            
            Condition.Add(textBox1.Text);
         
            d1 = CommonDB.R_datatbl(Qry.POPUP_선박정보_001(Condition));

            Clear();
            dataGridView1Set();

            int j = 0;

            while (j < d1.Rows.Count)
            {
                int n = dataGridView1.Rows.Add();

                for (int i = 0; i <= d1.Columns.Count - 1; i++)
                {
                    dataGridView1.Rows[n].Cells[i].Value = d1.Rows[j][i].ToString();

                    //if (i == 0)
                    //{
                    //    dataGridView1.Rows[n].Cells[i].Value = (j + 1).ToString();
                    //}
                    //else if (i == 18)
                    //{
                    //    if (d1.Rows[j][14].ToString() != "")
                    //    {
                    //        dataGridView1.Rows[n].Cells[i].Value = "0";
                    //    }
                    //    else
                    //    {
                    //        dataGridView1.Rows[n].Cells[i].Value = d1.Rows[j][i - 1].ToString();
                    //    }
                    //}
                    //else
                    //{
                    //    dataGridView1.Rows[n].Cells[i].Value = d1.Rows[j][i - 1].ToString();
                    //}

                    /*
                    if (dataGridView1.Rows[n].Cells[i].Value.Equals("EST"))
                    {
                        dataGridView1.Rows[n].DefaultCellStyle.BackColor = Color.Red;
                        dataGridView1.Rows[n].DefaultCellStyle.ForeColor = Color.White;
                    }
                    */
                }

                j++;
            }


            //statusBar1.Panels[0].Text = "전체 " + j.ToString() + " 건이 검색 되었습니다.";
        }

        private void Clear()
        {
            // 그리드 초기화
            dataGridView1.DataSource = null;
            dataGridView1.Columns.Clear();
        }
        
        private void dataGridView1Set()
        {
            //this.Controls.Add(dataGridView1);

            dataGridView1.ColumnCount = d1.Columns.Count;

            dataGridView1.Columns[0].Name = "코드";
            dataGridView1.Columns[1].Name = "선박명";
            dataGridView1.Columns[2].Name = "비고";
            //dataGridView1.Columns[3].Name = "단위";
            //dataGridView1.Columns[4].Name = "구역";
            //dataGridView1.Columns[5].Name = "비고";
            //dataGridView1.Columns[6].Name = "매입금액";
            //dataGridView1.Columns[7].Name = "이윤율(건수)";
            //dataGridView1.Columns[8].Name = "이윤율(품목)";
            //dataGridView1.Columns[7].Name = "이윤율(금액)";

            //this.dataGridView1.Columns["암호"].Visible = false;
            //this.dataGridView1.Columns["권한"].Visible = false;

            // Columns AutoSize
            //dGVProcess.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells;

            // Head Cell_Columns Font Style = off
            dataGridView1.EnableHeadersVisualStyles = false;
            // Head Cell_Columns HeightSizeMode = off
            dataGridView1.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.DisableResizing;

            // Head Cell_Columns Width Size 임의 지정
            DataGridViewColumn column1 = dataGridView1.Columns[0];
            column1.Width = 60;

            DataGridViewColumn column2 = dataGridView1.Columns[1];
            column2.Width = 150;

            DataGridViewColumn column3 = dataGridView1.Columns[2];
            column3.Width = 200;

            //DataGridViewColumn column4 = dataGridView1.Columns[3];
            //column4.Width = 60;
            //
            //DataGridViewColumn column5 = dataGridView1.Columns[4];
            //column5.Width = 60;
            //
            //DataGridViewColumn column6 = dataGridView1.Columns[5];
            //column6.Width = 200;
            
            //DataGridViewColumn column7 = dataGridView1.Columns[6];
            //column7.Width = 90;
            //
            //DataGridViewColumn column8 = dataGridView1.Columns[7];
            //column8.Width = 150;

            //DataGridViewColumn column9 = dataGridView1.Columns[8];
            //column9.Width = 50;

            //DataGridViewColumn column10 = dataGridView1.Columns[9];
            //column10.Width = 50;



            // Head Cell_Columns HeightSize 임의 지정
            dataGridView1.ColumnHeadersHeight = 30;

            for (int j = 0; j < d1.Columns.Count; j++)
            {
                dataGridView1.Columns[j].HeaderCell.Style.Font = new Font("맑은고딕", 10, FontStyle.Bold);
                // dGVProcess.Columns[j].HeaderCell.Style.ForeColor = Color.Red;
                //dataGridView1.Columns[j].HeaderCell.Style.BackColor = Color.Yellow;

                // dGVState.Columns[j].HeaderCell.Size = new Size();

                // Head Cell_Columns Alignment
                dataGridView1.Columns[j].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;

                // Cell Font Style
                //dGVProcess.Columns[j].DefaultCellStyle.Font = new Font("Tahoma", 18, FontStyle.Bold);

                // Cell Font Alignment
                //if (j == 2)
                //{
                //    dataGridView1.Columns[j].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                //}
                //else
                //{
                //    dataGridView1.Columns[j].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                //
                //}

                //  Cell Height 지정
                //  var dgv = new DataGridView();
                //dataGridView1.RowTemplate.Height = 30;

            }
        }

        private void POPUP_관리정보_M_Load(object sender, EventArgs e)
        {
            Main_Process();
        }

        private void dataGridView1_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            Passvalue_선박코드 = dataGridView1.Rows[e.RowIndex].Cells[0].FormattedValue.ToString();
            Passvalue_선박명 = dataGridView1.Rows[e.RowIndex].Cells[1].FormattedValue.ToString();
            this.Hide();
        }
    }
}
