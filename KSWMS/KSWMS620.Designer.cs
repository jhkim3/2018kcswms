﻿namespace KSWMS
{
    partial class KSWMS620
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.button62 = new System.Windows.Forms.Button();
            this.label62 = new System.Windows.Forms.Label();
            this.button61 = new System.Windows.Forms.Button();
            this.label61 = new System.Windows.Forms.Label();
            this.button60 = new System.Windows.Forms.Button();
            this.label60 = new System.Windows.Forms.Label();
            this.button59 = new System.Windows.Forms.Button();
            this.label59 = new System.Windows.Forms.Label();
            this.button58 = new System.Windows.Forms.Button();
            this.label58 = new System.Windows.Forms.Label();
            this.button57 = new System.Windows.Forms.Button();
            this.label57 = new System.Windows.Forms.Label();
            this.button56 = new System.Windows.Forms.Button();
            this.label56 = new System.Windows.Forms.Label();
            this.button55 = new System.Windows.Forms.Button();
            this.label55 = new System.Windows.Forms.Label();
            this.button54 = new System.Windows.Forms.Button();
            this.label54 = new System.Windows.Forms.Label();
            this.button53 = new System.Windows.Forms.Button();
            this.label53 = new System.Windows.Forms.Label();
            this.button52 = new System.Windows.Forms.Button();
            this.label52 = new System.Windows.Forms.Label();
            this.button51 = new System.Windows.Forms.Button();
            this.label51 = new System.Windows.Forms.Label();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.dataGridView2 = new System.Windows.Forms.DataGridView();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.panel50 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.dataGridView4 = new System.Windows.Forms.DataGridView();
            this.panel4 = new System.Windows.Forms.Panel();
            this.label4 = new System.Windows.Forms.Label();
            this.dataGridView3 = new System.Windows.Forms.DataGridView();
            this.panel3 = new System.Windows.Forms.Panel();
            this.label3 = new System.Windows.Forms.Label();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.dataGridView6 = new System.Windows.Forms.DataGridView();
            this.panel6 = new System.Windows.Forms.Panel();
            this.label6 = new System.Windows.Forms.Label();
            this.dataGridView5 = new System.Windows.Forms.DataGridView();
            this.panel5 = new System.Windows.Forms.Panel();
            this.label5 = new System.Windows.Forms.Label();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.dataGridView8 = new System.Windows.Forms.DataGridView();
            this.panel8 = new System.Windows.Forms.Panel();
            this.label8 = new System.Windows.Forms.Label();
            this.dataGridView7 = new System.Windows.Forms.DataGridView();
            this.panel7 = new System.Windows.Forms.Panel();
            this.label7 = new System.Windows.Forms.Label();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.dataGridView10 = new System.Windows.Forms.DataGridView();
            this.panel10 = new System.Windows.Forms.Panel();
            this.label10 = new System.Windows.Forms.Label();
            this.dataGridView9 = new System.Windows.Forms.DataGridView();
            this.panel9 = new System.Windows.Forms.Panel();
            this.label9 = new System.Windows.Forms.Label();
            this.tabPage6 = new System.Windows.Forms.TabPage();
            this.dataGridView12 = new System.Windows.Forms.DataGridView();
            this.panel12 = new System.Windows.Forms.Panel();
            this.label12 = new System.Windows.Forms.Label();
            this.dataGridView11 = new System.Windows.Forms.DataGridView();
            this.panel11 = new System.Windows.Forms.Panel();
            this.label11 = new System.Windows.Forms.Label();
            this.tabPage7 = new System.Windows.Forms.TabPage();
            this.dataGridView14 = new System.Windows.Forms.DataGridView();
            this.panel14 = new System.Windows.Forms.Panel();
            this.label14 = new System.Windows.Forms.Label();
            this.dataGridView13 = new System.Windows.Forms.DataGridView();
            this.panel13 = new System.Windows.Forms.Panel();
            this.label13 = new System.Windows.Forms.Label();
            this.tabPage8 = new System.Windows.Forms.TabPage();
            this.dataGridView16 = new System.Windows.Forms.DataGridView();
            this.panel16 = new System.Windows.Forms.Panel();
            this.label16 = new System.Windows.Forms.Label();
            this.dataGridView15 = new System.Windows.Forms.DataGridView();
            this.panel15 = new System.Windows.Forms.Panel();
            this.label15 = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).BeginInit();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.panel50.SuspendLayout();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView4)).BeginInit();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView3)).BeginInit();
            this.panel3.SuspendLayout();
            this.tabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView6)).BeginInit();
            this.panel6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView5)).BeginInit();
            this.panel5.SuspendLayout();
            this.tabPage4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView8)).BeginInit();
            this.panel8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView7)).BeginInit();
            this.panel7.SuspendLayout();
            this.tabPage5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView10)).BeginInit();
            this.panel10.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView9)).BeginInit();
            this.panel9.SuspendLayout();
            this.tabPage6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView12)).BeginInit();
            this.panel12.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView11)).BeginInit();
            this.panel11.SuspendLayout();
            this.tabPage7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView14)).BeginInit();
            this.panel14.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView13)).BeginInit();
            this.panel13.SuspendLayout();
            this.tabPage8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView16)).BeginInit();
            this.panel16.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView15)).BeginInit();
            this.panel15.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.groupBox1);
            this.panel1.Controls.Add(this.tabControl1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(969, 566);
            this.panel1.TabIndex = 1;
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.button62);
            this.groupBox1.Controls.Add(this.label62);
            this.groupBox1.Controls.Add(this.button61);
            this.groupBox1.Controls.Add(this.label61);
            this.groupBox1.Controls.Add(this.button60);
            this.groupBox1.Controls.Add(this.label60);
            this.groupBox1.Controls.Add(this.button59);
            this.groupBox1.Controls.Add(this.label59);
            this.groupBox1.Controls.Add(this.button58);
            this.groupBox1.Controls.Add(this.label58);
            this.groupBox1.Controls.Add(this.button57);
            this.groupBox1.Controls.Add(this.label57);
            this.groupBox1.Controls.Add(this.button56);
            this.groupBox1.Controls.Add(this.label56);
            this.groupBox1.Controls.Add(this.button55);
            this.groupBox1.Controls.Add(this.label55);
            this.groupBox1.Controls.Add(this.button54);
            this.groupBox1.Controls.Add(this.label54);
            this.groupBox1.Controls.Add(this.button53);
            this.groupBox1.Controls.Add(this.label53);
            this.groupBox1.Controls.Add(this.button52);
            this.groupBox1.Controls.Add(this.label52);
            this.groupBox1.Controls.Add(this.button51);
            this.groupBox1.Controls.Add(this.label51);
            this.groupBox1.Location = new System.Drawing.Point(0, 519);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(965, 45);
            this.groupBox1.TabIndex = 11;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "랙상태 범례";
            // 
            // button62
            // 
            this.button62.BackColor = System.Drawing.Color.Black;
            this.button62.ForeColor = System.Drawing.SystemColors.ControlText;
            this.button62.Location = new System.Drawing.Point(922, 15);
            this.button62.Name = "button62";
            this.button62.Size = new System.Drawing.Size(25, 25);
            this.button62.TabIndex = 23;
            this.button62.UseVisualStyleBackColor = false;
            // 
            // label62
            // 
            this.label62.AutoSize = true;
            this.label62.Font = new System.Drawing.Font("Gulim", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label62.Location = new System.Drawing.Point(943, 20);
            this.label62.Name = "label62";
            this.label62.Size = new System.Drawing.Size(59, 16);
            this.label62.TabIndex = 22;
            this.label62.Text = "미설치";
            // 
            // button61
            // 
            this.button61.BackColor = System.Drawing.Color.Red;
            this.button61.ForeColor = System.Drawing.SystemColors.ControlText;
            this.button61.Location = new System.Drawing.Point(847, 15);
            this.button61.Name = "button61";
            this.button61.Size = new System.Drawing.Size(25, 25);
            this.button61.TabIndex = 21;
            this.button61.UseVisualStyleBackColor = false;
            // 
            // label61
            // 
            this.label61.AutoSize = true;
            this.label61.Font = new System.Drawing.Font("Gulim", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label61.Location = new System.Drawing.Point(868, 20);
            this.label61.Name = "label61";
            this.label61.Size = new System.Drawing.Size(59, 16);
            this.label61.TabIndex = 20;
            this.label61.Text = "금지랙";
            // 
            // button60
            // 
            this.button60.BackColor = System.Drawing.Color.Orange;
            this.button60.ForeColor = System.Drawing.SystemColors.ControlText;
            this.button60.Location = new System.Drawing.Point(753, 15);
            this.button60.Name = "button60";
            this.button60.Size = new System.Drawing.Size(25, 25);
            this.button60.TabIndex = 19;
            this.button60.UseVisualStyleBackColor = false;
            // 
            // label60
            // 
            this.label60.AutoSize = true;
            this.label60.Font = new System.Drawing.Font("Gulim", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label60.Location = new System.Drawing.Point(775, 20);
            this.label60.Name = "label60";
            this.label60.Size = new System.Drawing.Size(76, 16);
            this.label60.TabIndex = 18;
            this.label60.Text = "재고이상";
            // 
            // button59
            // 
            this.button59.BackColor = System.Drawing.Color.GreenYellow;
            this.button59.ForeColor = System.Drawing.SystemColors.ControlText;
            this.button59.Location = new System.Drawing.Point(658, 15);
            this.button59.Name = "button59";
            this.button59.Size = new System.Drawing.Size(25, 25);
            this.button59.TabIndex = 17;
            this.button59.UseVisualStyleBackColor = false;
            // 
            // label59
            // 
            this.label59.AutoSize = true;
            this.label59.Font = new System.Drawing.Font("Gulim", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label59.Location = new System.Drawing.Point(681, 20);
            this.label59.Name = "label59";
            this.label59.Size = new System.Drawing.Size(76, 16);
            this.label59.TabIndex = 16;
            this.label59.Text = "재고확인";
            // 
            // button58
            // 
            this.button58.BackColor = System.Drawing.Color.Violet;
            this.button58.ForeColor = System.Drawing.SystemColors.ControlText;
            this.button58.Location = new System.Drawing.Point(582, 15);
            this.button58.Name = "button58";
            this.button58.Size = new System.Drawing.Size(25, 25);
            this.button58.TabIndex = 15;
            this.button58.UseVisualStyleBackColor = false;
            // 
            // label58
            // 
            this.label58.AutoSize = true;
            this.label58.Font = new System.Drawing.Font("Gulim", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label58.Location = new System.Drawing.Point(602, 20);
            this.label58.Name = "label58";
            this.label58.Size = new System.Drawing.Size(59, 16);
            this.label58.TabIndex = 14;
            this.label58.Text = "공출고";
            // 
            // button57
            // 
            this.button57.BackColor = System.Drawing.Color.Purple;
            this.button57.ForeColor = System.Drawing.SystemColors.ControlText;
            this.button57.Location = new System.Drawing.Point(489, 15);
            this.button57.Name = "button57";
            this.button57.Size = new System.Drawing.Size(25, 25);
            this.button57.TabIndex = 13;
            this.button57.UseVisualStyleBackColor = false;
            // 
            // label57
            // 
            this.label57.AutoSize = true;
            this.label57.Font = new System.Drawing.Font("Gulim", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label57.Location = new System.Drawing.Point(510, 20);
            this.label57.Name = "label57";
            this.label57.Size = new System.Drawing.Size(76, 16);
            this.label57.TabIndex = 12;
            this.label57.Text = "이중입고";
            // 
            // button56
            // 
            this.button56.BackColor = System.Drawing.Color.DarkKhaki;
            this.button56.ForeColor = System.Drawing.SystemColors.ControlText;
            this.button56.Location = new System.Drawing.Point(413, 15);
            this.button56.Name = "button56";
            this.button56.Size = new System.Drawing.Size(25, 25);
            this.button56.TabIndex = 11;
            this.button56.UseVisualStyleBackColor = false;
            // 
            // label56
            // 
            this.label56.AutoSize = true;
            this.label56.Font = new System.Drawing.Font("Gulim", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label56.Location = new System.Drawing.Point(434, 20);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(59, 16);
            this.label56.TabIndex = 10;
            this.label56.Text = "출고중";
            // 
            // button55
            // 
            this.button55.BackColor = System.Drawing.Color.Blue;
            this.button55.ForeColor = System.Drawing.SystemColors.ControlText;
            this.button55.Location = new System.Drawing.Point(338, 15);
            this.button55.Name = "button55";
            this.button55.Size = new System.Drawing.Size(25, 25);
            this.button55.TabIndex = 9;
            this.button55.UseVisualStyleBackColor = false;
            // 
            // label55
            // 
            this.label55.AutoSize = true;
            this.label55.Font = new System.Drawing.Font("Gulim", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label55.Location = new System.Drawing.Point(359, 20);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(59, 16);
            this.label55.TabIndex = 8;
            this.label55.Text = "입고중";
            // 
            // button54
            // 
            this.button54.BackColor = System.Drawing.Color.Gold;
            this.button54.ForeColor = System.Drawing.SystemColors.ControlText;
            this.button54.Location = new System.Drawing.Point(246, 15);
            this.button54.Name = "button54";
            this.button54.Size = new System.Drawing.Size(25, 25);
            this.button54.TabIndex = 7;
            this.button54.UseVisualStyleBackColor = false;
            // 
            // label54
            // 
            this.label54.AutoSize = true;
            this.label54.Font = new System.Drawing.Font("Gulim", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label54.Location = new System.Drawing.Point(267, 20);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(76, 16);
            this.label54.TabIndex = 6;
            this.label54.Text = "출고예약";
            // 
            // button53
            // 
            this.button53.BackColor = System.Drawing.Color.DeepSkyBlue;
            this.button53.ForeColor = System.Drawing.SystemColors.ControlText;
            this.button53.Location = new System.Drawing.Point(153, 15);
            this.button53.Name = "button53";
            this.button53.Size = new System.Drawing.Size(25, 25);
            this.button53.TabIndex = 5;
            this.button53.UseVisualStyleBackColor = false;
            // 
            // label53
            // 
            this.label53.AutoSize = true;
            this.label53.Font = new System.Drawing.Font("Gulim", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label53.Location = new System.Drawing.Point(174, 20);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(76, 16);
            this.label53.TabIndex = 4;
            this.label53.Text = "입고예약";
            // 
            // button52
            // 
            this.button52.BackColor = System.Drawing.Color.Silver;
            this.button52.ForeColor = System.Drawing.SystemColors.ControlText;
            this.button52.Location = new System.Drawing.Point(74, 15);
            this.button52.Name = "button52";
            this.button52.Size = new System.Drawing.Size(25, 25);
            this.button52.TabIndex = 3;
            this.button52.UseVisualStyleBackColor = false;
            // 
            // label52
            // 
            this.label52.AutoSize = true;
            this.label52.Font = new System.Drawing.Font("Gulim", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label52.Location = new System.Drawing.Point(95, 20);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(59, 16);
            this.label52.TabIndex = 2;
            this.label52.Text = "수정중";
            // 
            // button51
            // 
            this.button51.BackColor = System.Drawing.Color.White;
            this.button51.ForeColor = System.Drawing.SystemColors.ControlText;
            this.button51.Location = new System.Drawing.Point(10, 15);
            this.button51.Name = "button51";
            this.button51.Size = new System.Drawing.Size(25, 25);
            this.button51.TabIndex = 1;
            this.button51.UseVisualStyleBackColor = false;
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.Font = new System.Drawing.Font("Gulim", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label51.Location = new System.Drawing.Point(35, 20);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(42, 16);
            this.label51.TabIndex = 0;
            this.label51.Text = "정상";
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.Controls.Add(this.tabPage5);
            this.tabControl1.Controls.Add(this.tabPage6);
            this.tabControl1.Controls.Add(this.tabPage7);
            this.tabControl1.Controls.Add(this.tabPage8);
            this.tabControl1.Font = new System.Drawing.Font("Gulim", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.tabControl1.ItemSize = new System.Drawing.Size(200, 25);
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(969, 516);
            this.tabControl1.SizeMode = System.Windows.Forms.TabSizeMode.Fixed;
            this.tabControl1.TabIndex = 5;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.dataGridView2);
            this.tabPage1.Controls.Add(this.panel2);
            this.tabPage1.Controls.Add(this.dataGridView1);
            this.tabPage1.Controls.Add(this.panel50);
            this.tabPage1.Font = new System.Drawing.Font("Gulim", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.tabPage1.Location = new System.Drawing.Point(4, 29);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(961, 483);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "1호기[1열, 2열]";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // dataGridView2
            // 
            this.dataGridView2.AllowUserToAddRows = false;
            this.dataGridView2.AllowUserToDeleteRows = false;
            this.dataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView2.Location = new System.Drawing.Point(0, 265);
            this.dataGridView2.Name = "dataGridView2";
            this.dataGridView2.ReadOnly = true;
            this.dataGridView2.RowTemplate.Height = 23;
            this.dataGridView2.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView2.Size = new System.Drawing.Size(960, 215);
            this.dataGridView2.TabIndex = 562;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.panel2.Controls.Add(this.label2);
            this.panel2.Location = new System.Drawing.Point(0, 240);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(960, 25);
            this.panel2.TabIndex = 561;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Gulim", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label2.ForeColor = System.Drawing.Color.Navy;
            this.label2.Location = new System.Drawing.Point(30, 5);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(119, 16);
            this.label2.TabIndex = 21;
            this.label2.Text = "1호기 [ 2 열 ]";
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(0, 25);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.RowTemplate.Height = 23;
            this.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView1.Size = new System.Drawing.Size(960, 215);
            this.dataGridView1.TabIndex = 560;
            // 
            // panel50
            // 
            this.panel50.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.panel50.Controls.Add(this.label1);
            this.panel50.Location = new System.Drawing.Point(0, 0);
            this.panel50.Name = "panel50";
            this.panel50.Size = new System.Drawing.Size(960, 25);
            this.panel50.TabIndex = 559;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Gulim", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label1.ForeColor = System.Drawing.Color.Navy;
            this.label1.Location = new System.Drawing.Point(30, 5);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(657, 16);
            this.label1.TabIndex = 21;
            this.label1.Text = "1호기 [ 1 열 ]    ▶ 랙재고 범례 [ P : 제품(Product), E : 공파레트(Empty pallet) ]";
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.dataGridView4);
            this.tabPage2.Controls.Add(this.panel4);
            this.tabPage2.Controls.Add(this.dataGridView3);
            this.tabPage2.Controls.Add(this.panel3);
            this.tabPage2.Font = new System.Drawing.Font("Gulim", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.tabPage2.Location = new System.Drawing.Point(4, 29);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(961, 483);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "1호기[3열, 4열]";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // dataGridView4
            // 
            this.dataGridView4.AllowUserToAddRows = false;
            this.dataGridView4.AllowUserToDeleteRows = false;
            this.dataGridView4.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView4.Location = new System.Drawing.Point(0, 265);
            this.dataGridView4.Name = "dataGridView4";
            this.dataGridView4.ReadOnly = true;
            this.dataGridView4.RowTemplate.Height = 23;
            this.dataGridView4.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView4.Size = new System.Drawing.Size(960, 215);
            this.dataGridView4.TabIndex = 565;
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.panel4.Controls.Add(this.label4);
            this.panel4.Location = new System.Drawing.Point(0, 240);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(960, 25);
            this.panel4.TabIndex = 564;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Gulim", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label4.ForeColor = System.Drawing.Color.Navy;
            this.label4.Location = new System.Drawing.Point(30, 5);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(120, 16);
            this.label4.TabIndex = 21;
            this.label4.Text = "1호기 [ 4 열 ]";
            // 
            // dataGridView3
            // 
            this.dataGridView3.AllowUserToAddRows = false;
            this.dataGridView3.AllowUserToDeleteRows = false;
            this.dataGridView3.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView3.Location = new System.Drawing.Point(0, 25);
            this.dataGridView3.Name = "dataGridView3";
            this.dataGridView3.ReadOnly = true;
            this.dataGridView3.RowTemplate.Height = 23;
            this.dataGridView3.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView3.Size = new System.Drawing.Size(960, 215);
            this.dataGridView3.TabIndex = 563;
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.panel3.Controls.Add(this.label3);
            this.panel3.Location = new System.Drawing.Point(0, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(960, 25);
            this.panel3.TabIndex = 560;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Gulim", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label3.ForeColor = System.Drawing.Color.Navy;
            this.label3.Location = new System.Drawing.Point(30, 5);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(657, 16);
            this.label3.TabIndex = 21;
            this.label3.Text = "1호기 [ 3 열 ]    ▶ 랙재고 범례 [ P : 제품(Product), E : 공파레트(Empty pallet) ]";
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.dataGridView6);
            this.tabPage3.Controls.Add(this.panel6);
            this.tabPage3.Controls.Add(this.dataGridView5);
            this.tabPage3.Controls.Add(this.panel5);
            this.tabPage3.Font = new System.Drawing.Font("Gulim", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.tabPage3.Location = new System.Drawing.Point(4, 29);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(961, 483);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "2호기[1열, 2열]";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // dataGridView6
            // 
            this.dataGridView6.AllowUserToAddRows = false;
            this.dataGridView6.AllowUserToDeleteRows = false;
            this.dataGridView6.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView6.Location = new System.Drawing.Point(0, 265);
            this.dataGridView6.Name = "dataGridView6";
            this.dataGridView6.ReadOnly = true;
            this.dataGridView6.RowTemplate.Height = 23;
            this.dataGridView6.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView6.Size = new System.Drawing.Size(960, 215);
            this.dataGridView6.TabIndex = 566;
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.panel6.Controls.Add(this.label6);
            this.panel6.Location = new System.Drawing.Point(0, 240);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(960, 25);
            this.panel6.TabIndex = 565;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Gulim", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label6.ForeColor = System.Drawing.Color.Navy;
            this.label6.Location = new System.Drawing.Point(30, 5);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(119, 16);
            this.label6.TabIndex = 21;
            this.label6.Text = "2호기 [ 2 열 ]";
            // 
            // dataGridView5
            // 
            this.dataGridView5.AllowUserToAddRows = false;
            this.dataGridView5.AllowUserToDeleteRows = false;
            this.dataGridView5.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView5.Location = new System.Drawing.Point(0, 25);
            this.dataGridView5.Name = "dataGridView5";
            this.dataGridView5.ReadOnly = true;
            this.dataGridView5.RowTemplate.Height = 23;
            this.dataGridView5.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView5.Size = new System.Drawing.Size(960, 215);
            this.dataGridView5.TabIndex = 564;
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.panel5.Controls.Add(this.label5);
            this.panel5.Location = new System.Drawing.Point(0, 0);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(960, 25);
            this.panel5.TabIndex = 563;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Gulim", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label5.ForeColor = System.Drawing.Color.Navy;
            this.label5.Location = new System.Drawing.Point(30, 5);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(657, 16);
            this.label5.TabIndex = 21;
            this.label5.Text = "2호기 [ 1 열 ]    ▶ 랙재고 범례 [ P : 제품(Product), E : 공파레트(Empty pallet) ]";
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.dataGridView8);
            this.tabPage4.Controls.Add(this.panel8);
            this.tabPage4.Controls.Add(this.dataGridView7);
            this.tabPage4.Controls.Add(this.panel7);
            this.tabPage4.Location = new System.Drawing.Point(4, 29);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Size = new System.Drawing.Size(961, 483);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "2호기[3열, 4열]";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // dataGridView8
            // 
            this.dataGridView8.AllowUserToAddRows = false;
            this.dataGridView8.AllowUserToDeleteRows = false;
            this.dataGridView8.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView8.Location = new System.Drawing.Point(0, 265);
            this.dataGridView8.Name = "dataGridView8";
            this.dataGridView8.ReadOnly = true;
            this.dataGridView8.RowTemplate.Height = 23;
            this.dataGridView8.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView8.Size = new System.Drawing.Size(960, 215);
            this.dataGridView8.TabIndex = 569;
            // 
            // panel8
            // 
            this.panel8.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.panel8.Controls.Add(this.label8);
            this.panel8.Location = new System.Drawing.Point(0, 240);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(960, 25);
            this.panel8.TabIndex = 568;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Gulim", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label8.ForeColor = System.Drawing.Color.Navy;
            this.label8.Location = new System.Drawing.Point(30, 5);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(120, 16);
            this.label8.TabIndex = 21;
            this.label8.Text = "2호기 [ 4 열 ]";
            // 
            // dataGridView7
            // 
            this.dataGridView7.AllowUserToAddRows = false;
            this.dataGridView7.AllowUserToDeleteRows = false;
            this.dataGridView7.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView7.Location = new System.Drawing.Point(0, 25);
            this.dataGridView7.Name = "dataGridView7";
            this.dataGridView7.ReadOnly = true;
            this.dataGridView7.RowTemplate.Height = 23;
            this.dataGridView7.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView7.Size = new System.Drawing.Size(960, 215);
            this.dataGridView7.TabIndex = 567;
            // 
            // panel7
            // 
            this.panel7.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.panel7.Controls.Add(this.label7);
            this.panel7.Location = new System.Drawing.Point(0, 0);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(960, 25);
            this.panel7.TabIndex = 566;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Gulim", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label7.ForeColor = System.Drawing.Color.Navy;
            this.label7.Location = new System.Drawing.Point(30, 5);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(657, 16);
            this.label7.TabIndex = 21;
            this.label7.Text = "2호기 [ 3 열 ]    ▶ 랙재고 범례 [ P : 제품(Product), E : 공파레트(Empty pallet) ]";
            // 
            // tabPage5
            // 
            this.tabPage5.Controls.Add(this.dataGridView10);
            this.tabPage5.Controls.Add(this.panel10);
            this.tabPage5.Controls.Add(this.dataGridView9);
            this.tabPage5.Controls.Add(this.panel9);
            this.tabPage5.Location = new System.Drawing.Point(4, 29);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Size = new System.Drawing.Size(961, 483);
            this.tabPage5.TabIndex = 4;
            this.tabPage5.Text = "3호기[1열, 2열]";
            this.tabPage5.UseVisualStyleBackColor = true;
            // 
            // dataGridView10
            // 
            this.dataGridView10.AllowUserToAddRows = false;
            this.dataGridView10.AllowUserToDeleteRows = false;
            this.dataGridView10.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView10.Location = new System.Drawing.Point(0, 265);
            this.dataGridView10.Name = "dataGridView10";
            this.dataGridView10.ReadOnly = true;
            this.dataGridView10.RowTemplate.Height = 23;
            this.dataGridView10.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView10.Size = new System.Drawing.Size(960, 215);
            this.dataGridView10.TabIndex = 570;
            // 
            // panel10
            // 
            this.panel10.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.panel10.Controls.Add(this.label10);
            this.panel10.Location = new System.Drawing.Point(0, 240);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(960, 25);
            this.panel10.TabIndex = 569;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Gulim", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label10.ForeColor = System.Drawing.Color.Navy;
            this.label10.Location = new System.Drawing.Point(30, 5);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(119, 16);
            this.label10.TabIndex = 21;
            this.label10.Text = "3호기 [ 2 열 ]";
            // 
            // dataGridView9
            // 
            this.dataGridView9.AllowUserToAddRows = false;
            this.dataGridView9.AllowUserToDeleteRows = false;
            this.dataGridView9.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView9.Location = new System.Drawing.Point(0, 25);
            this.dataGridView9.Name = "dataGridView9";
            this.dataGridView9.ReadOnly = true;
            this.dataGridView9.RowTemplate.Height = 23;
            this.dataGridView9.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView9.Size = new System.Drawing.Size(960, 215);
            this.dataGridView9.TabIndex = 568;
            // 
            // panel9
            // 
            this.panel9.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.panel9.Controls.Add(this.label9);
            this.panel9.Location = new System.Drawing.Point(0, 0);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(960, 25);
            this.panel9.TabIndex = 567;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Gulim", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label9.ForeColor = System.Drawing.Color.Navy;
            this.label9.Location = new System.Drawing.Point(30, 5);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(657, 16);
            this.label9.TabIndex = 21;
            this.label9.Text = "3호기 [ 1 열 ]    ▶ 랙재고 범례 [ P : 제품(Product), E : 공파레트(Empty pallet) ]";
            // 
            // tabPage6
            // 
            this.tabPage6.Controls.Add(this.dataGridView12);
            this.tabPage6.Controls.Add(this.panel12);
            this.tabPage6.Controls.Add(this.dataGridView11);
            this.tabPage6.Controls.Add(this.panel11);
            this.tabPage6.Location = new System.Drawing.Point(4, 29);
            this.tabPage6.Name = "tabPage6";
            this.tabPage6.Size = new System.Drawing.Size(961, 483);
            this.tabPage6.TabIndex = 5;
            this.tabPage6.Text = "3호기[3열, 4열]";
            this.tabPage6.UseVisualStyleBackColor = true;
            // 
            // dataGridView12
            // 
            this.dataGridView12.AllowUserToAddRows = false;
            this.dataGridView12.AllowUserToDeleteRows = false;
            this.dataGridView12.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView12.Location = new System.Drawing.Point(0, 265);
            this.dataGridView12.Name = "dataGridView12";
            this.dataGridView12.ReadOnly = true;
            this.dataGridView12.RowTemplate.Height = 23;
            this.dataGridView12.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView12.Size = new System.Drawing.Size(960, 215);
            this.dataGridView12.TabIndex = 569;
            // 
            // panel12
            // 
            this.panel12.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.panel12.Controls.Add(this.label12);
            this.panel12.Location = new System.Drawing.Point(0, 240);
            this.panel12.Name = "panel12";
            this.panel12.Size = new System.Drawing.Size(960, 25);
            this.panel12.TabIndex = 568;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Gulim", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label12.ForeColor = System.Drawing.Color.Navy;
            this.label12.Location = new System.Drawing.Point(30, 5);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(120, 16);
            this.label12.TabIndex = 21;
            this.label12.Text = "3호기 [ 4 열 ]";
            // 
            // dataGridView11
            // 
            this.dataGridView11.AllowUserToAddRows = false;
            this.dataGridView11.AllowUserToDeleteRows = false;
            this.dataGridView11.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView11.Location = new System.Drawing.Point(0, 25);
            this.dataGridView11.Name = "dataGridView11";
            this.dataGridView11.ReadOnly = true;
            this.dataGridView11.RowTemplate.Height = 23;
            this.dataGridView11.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView11.Size = new System.Drawing.Size(960, 215);
            this.dataGridView11.TabIndex = 567;
            // 
            // panel11
            // 
            this.panel11.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.panel11.Controls.Add(this.label11);
            this.panel11.Location = new System.Drawing.Point(0, 0);
            this.panel11.Name = "panel11";
            this.panel11.Size = new System.Drawing.Size(960, 25);
            this.panel11.TabIndex = 566;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Gulim", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label11.ForeColor = System.Drawing.Color.Navy;
            this.label11.Location = new System.Drawing.Point(30, 5);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(657, 16);
            this.label11.TabIndex = 21;
            this.label11.Text = "3호기 [ 3 열 ]    ▶ 랙재고 범례 [ P : 제품(Product), E : 공파레트(Empty pallet) ]";
            // 
            // tabPage7
            // 
            this.tabPage7.Controls.Add(this.dataGridView14);
            this.tabPage7.Controls.Add(this.panel14);
            this.tabPage7.Controls.Add(this.dataGridView13);
            this.tabPage7.Controls.Add(this.panel13);
            this.tabPage7.Location = new System.Drawing.Point(4, 29);
            this.tabPage7.Name = "tabPage7";
            this.tabPage7.Size = new System.Drawing.Size(961, 483);
            this.tabPage7.TabIndex = 6;
            this.tabPage7.Text = "4호기[1열, 2열]";
            this.tabPage7.UseVisualStyleBackColor = true;
            // 
            // dataGridView14
            // 
            this.dataGridView14.AllowUserToAddRows = false;
            this.dataGridView14.AllowUserToDeleteRows = false;
            this.dataGridView14.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView14.Location = new System.Drawing.Point(0, 265);
            this.dataGridView14.Name = "dataGridView14";
            this.dataGridView14.ReadOnly = true;
            this.dataGridView14.RowTemplate.Height = 23;
            this.dataGridView14.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView14.Size = new System.Drawing.Size(960, 215);
            this.dataGridView14.TabIndex = 574;
            // 
            // panel14
            // 
            this.panel14.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.panel14.Controls.Add(this.label14);
            this.panel14.Location = new System.Drawing.Point(0, 240);
            this.panel14.Name = "panel14";
            this.panel14.Size = new System.Drawing.Size(960, 25);
            this.panel14.TabIndex = 573;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Gulim", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label14.ForeColor = System.Drawing.Color.Navy;
            this.label14.Location = new System.Drawing.Point(30, 5);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(120, 16);
            this.label14.TabIndex = 21;
            this.label14.Text = "4호기 [ 2 열 ]";
            // 
            // dataGridView13
            // 
            this.dataGridView13.AllowUserToAddRows = false;
            this.dataGridView13.AllowUserToDeleteRows = false;
            this.dataGridView13.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView13.Location = new System.Drawing.Point(0, 25);
            this.dataGridView13.Name = "dataGridView13";
            this.dataGridView13.ReadOnly = true;
            this.dataGridView13.RowTemplate.Height = 23;
            this.dataGridView13.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView13.Size = new System.Drawing.Size(960, 215);
            this.dataGridView13.TabIndex = 572;
            // 
            // panel13
            // 
            this.panel13.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.panel13.Controls.Add(this.label13);
            this.panel13.Location = new System.Drawing.Point(0, 0);
            this.panel13.Name = "panel13";
            this.panel13.Size = new System.Drawing.Size(960, 25);
            this.panel13.TabIndex = 571;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Gulim", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label13.ForeColor = System.Drawing.Color.Navy;
            this.label13.Location = new System.Drawing.Point(30, 5);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(658, 16);
            this.label13.TabIndex = 21;
            this.label13.Text = "4호기 [ 1 열 ]    ▶ 랙재고 범례 [ P : 제품(Product), E : 공파레트(Empty pallet) ]";
            // 
            // tabPage8
            // 
            this.tabPage8.Controls.Add(this.dataGridView16);
            this.tabPage8.Controls.Add(this.panel16);
            this.tabPage8.Controls.Add(this.dataGridView15);
            this.tabPage8.Controls.Add(this.panel15);
            this.tabPage8.Location = new System.Drawing.Point(4, 29);
            this.tabPage8.Name = "tabPage8";
            this.tabPage8.Size = new System.Drawing.Size(961, 483);
            this.tabPage8.TabIndex = 7;
            this.tabPage8.Text = "4호기[3열, 4열]";
            this.tabPage8.UseVisualStyleBackColor = true;
            // 
            // dataGridView16
            // 
            this.dataGridView16.AllowUserToAddRows = false;
            this.dataGridView16.AllowUserToDeleteRows = false;
            this.dataGridView16.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView16.Location = new System.Drawing.Point(0, 265);
            this.dataGridView16.Name = "dataGridView16";
            this.dataGridView16.ReadOnly = true;
            this.dataGridView16.RowTemplate.Height = 23;
            this.dataGridView16.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView16.Size = new System.Drawing.Size(960, 215);
            this.dataGridView16.TabIndex = 569;
            // 
            // panel16
            // 
            this.panel16.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.panel16.Controls.Add(this.label16);
            this.panel16.Location = new System.Drawing.Point(0, 240);
            this.panel16.Name = "panel16";
            this.panel16.Size = new System.Drawing.Size(960, 25);
            this.panel16.TabIndex = 568;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Gulim", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label16.ForeColor = System.Drawing.Color.Navy;
            this.label16.Location = new System.Drawing.Point(30, 5);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(121, 16);
            this.label16.TabIndex = 21;
            this.label16.Text = "4호기 [ 4 열 ]";
            // 
            // dataGridView15
            // 
            this.dataGridView15.AllowUserToAddRows = false;
            this.dataGridView15.AllowUserToDeleteRows = false;
            this.dataGridView15.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView15.Location = new System.Drawing.Point(0, 25);
            this.dataGridView15.Name = "dataGridView15";
            this.dataGridView15.ReadOnly = true;
            this.dataGridView15.RowTemplate.Height = 23;
            this.dataGridView15.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView15.Size = new System.Drawing.Size(960, 215);
            this.dataGridView15.TabIndex = 567;
            // 
            // panel15
            // 
            this.panel15.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.panel15.Controls.Add(this.label15);
            this.panel15.Location = new System.Drawing.Point(0, 0);
            this.panel15.Name = "panel15";
            this.panel15.Size = new System.Drawing.Size(960, 25);
            this.panel15.TabIndex = 566;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Gulim", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label15.ForeColor = System.Drawing.Color.Navy;
            this.label15.Location = new System.Drawing.Point(30, 5);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(658, 16);
            this.label15.TabIndex = 21;
            this.label15.Text = "4호기 [ 3 열 ]    ▶ 랙재고 범례 [ P : 제품(Product), E : 공파레트(Empty pallet) ]";
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(514, 272);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 8;
            this.button2.Text = "button2";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(439, 272);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 6;
            this.button1.Text = "button1";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // KSWMS620
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(969, 566);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.panel1);
            this.Name = "KSWMS620";
            this.Text = "620. 분포도 재고수정";
            this.panel1.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.panel50.ResumeLayout(false);
            this.panel50.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView4)).EndInit();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView3)).EndInit();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.tabPage3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView6)).EndInit();
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView5)).EndInit();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.tabPage4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView8)).EndInit();
            this.panel8.ResumeLayout(false);
            this.panel8.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView7)).EndInit();
            this.panel7.ResumeLayout(false);
            this.panel7.PerformLayout();
            this.tabPage5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView10)).EndInit();
            this.panel10.ResumeLayout(false);
            this.panel10.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView9)).EndInit();
            this.panel9.ResumeLayout(false);
            this.panel9.PerformLayout();
            this.tabPage6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView12)).EndInit();
            this.panel12.ResumeLayout(false);
            this.panel12.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView11)).EndInit();
            this.panel11.ResumeLayout(false);
            this.panel11.PerformLayout();
            this.tabPage7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView14)).EndInit();
            this.panel14.ResumeLayout(false);
            this.panel14.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView13)).EndInit();
            this.panel13.ResumeLayout(false);
            this.panel13.PerformLayout();
            this.tabPage8.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView16)).EndInit();
            this.panel16.ResumeLayout(false);
            this.panel16.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView15)).EndInit();
            this.panel15.ResumeLayout(false);
            this.panel15.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.TabPage tabPage5;
        private System.Windows.Forms.TabPage tabPage6;
        private System.Windows.Forms.TabPage tabPage7;
        private System.Windows.Forms.TabPage tabPage8;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Panel panel50;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView dataGridView2;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.DataGridView dataGridView4;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.DataGridView dataGridView3;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DataGridView dataGridView6;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.DataGridView dataGridView5;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.DataGridView dataGridView8;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.DataGridView dataGridView7;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.DataGridView dataGridView10;
        private System.Windows.Forms.Panel panel10;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.DataGridView dataGridView9;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.DataGridView dataGridView12;
        private System.Windows.Forms.Panel panel12;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.DataGridView dataGridView11;
        private System.Windows.Forms.Panel panel11;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.DataGridView dataGridView16;
        private System.Windows.Forms.Panel panel16;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.DataGridView dataGridView15;
        private System.Windows.Forms.Panel panel15;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.DataGridView dataGridView14;
        private System.Windows.Forms.Panel panel14;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.DataGridView dataGridView13;
        private System.Windows.Forms.Panel panel13;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button button53;
        private System.Windows.Forms.Label label53;
        private System.Windows.Forms.Button button52;
        private System.Windows.Forms.Label label52;
        private System.Windows.Forms.Button button51;
        private System.Windows.Forms.Label label51;
        private System.Windows.Forms.Button button54;
        private System.Windows.Forms.Label label54;
        private System.Windows.Forms.Button button59;
        private System.Windows.Forms.Label label59;
        private System.Windows.Forms.Button button58;
        private System.Windows.Forms.Label label58;
        private System.Windows.Forms.Button button57;
        private System.Windows.Forms.Label label57;
        private System.Windows.Forms.Button button56;
        private System.Windows.Forms.Label label56;
        private System.Windows.Forms.Button button55;
        private System.Windows.Forms.Label label55;
        private System.Windows.Forms.Button button60;
        private System.Windows.Forms.Label label60;
        private System.Windows.Forms.Button button61;
        private System.Windows.Forms.Label label61;
        private System.Windows.Forms.Button button62;
        private System.Windows.Forms.Label label62;
    }
}