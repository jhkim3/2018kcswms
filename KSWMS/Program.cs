﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace KSWMS
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
           
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            //List<string> Node_List = new List<string>();
            //Application.Run(new KSWMS110(Node_List));
            //Application.Run(new KSWMS210());
            //Application.Run(new Main());
            Application.Run(new LogIn());
            //Application.Run(new POPUP_제품정보());
        }
    }

}
