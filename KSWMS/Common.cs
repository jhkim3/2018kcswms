﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace KSWMS
{
    class Common
    {

        public static int m_InputLimit = 147;

        public static void setDateTimePickerEnter(DateTimePicker dTP, Button btn)
        {
            dTP.KeyPress += new System.Windows.Forms.KeyPressEventHandler(delegate (object sender, KeyPressEventArgs e)
            {
                if (e.KeyChar == 13)
                {
                    e.Handled = true;
                    btn.PerformClick();
                }
            });
        }

        public static void setTextBoxEnter(TextBox txt, Button btn)
        {
            txt.KeyPress += new System.Windows.Forms.KeyPressEventHandler(delegate (object sender, KeyPressEventArgs e)
            {
                if (e.KeyChar == 13)
                {
                    e.Handled = true;
                    btn.PerformClick();
                }
            });
        }

        public static void setComboBoxEnter(ComboBox cmb, Button btn)
        {
            cmb.KeyPress += new System.Windows.Forms.KeyPressEventHandler(delegate (object sender, KeyPressEventArgs e)
            {
                if (e.KeyChar == 13)
                {
                    e.Handled = true;
                    btn.PerformClick();
                }
            });
        }

        public static void set_toolStripButton_DateTimePickerEnter(DateTimePicker dTP, ToolStripButton btn)
        {
            dTP.KeyPress += new System.Windows.Forms.KeyPressEventHandler(delegate (object sender, KeyPressEventArgs e)
            {
                if (e.KeyChar == 13)
                {
                    e.Handled = true;
                    btn.PerformClick();
                }
            });
        }

        public static void set_toolStripButton_TextBoxEnter(TextBox txt, ToolStripButton btn)
        {
            txt.KeyPress += new System.Windows.Forms.KeyPressEventHandler(delegate (object sender, KeyPressEventArgs e)
            {
                if (e.KeyChar == 13)
                {
                    e.Handled = true;
                    btn.PerformClick();
                }
            });
        }

        public static void set_toolStripButton_ComboBoxEnter(ComboBox cmb, ToolStripButton btn)
        {
            cmb.KeyPress += new System.Windows.Forms.KeyPressEventHandler(delegate (object sender, KeyPressEventArgs e)
            {
                if (e.KeyChar == 13)
                {
                    e.Handled = true;
                    btn.PerformClick();
                }
            });
        }



    }
}
