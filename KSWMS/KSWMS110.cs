﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace KSWMS
{
    public partial class KSWMS110 : MyFormPage
    {
        DataTable d1 = new DataTable();
        string UserChk;

        public KSWMS110(List<string> treenode)
        {
            InitializeComponent();
            this.pnl = panel1;
            this.btn1 = button1;
            this.btn2 = button2;
            this.btn3 = button3;          

            TreeNode_dataGridView2(treenode);
        }

        private void TreeNode_dataGridView2(List<string> treenode)
        {
            // 그리드 초기화
            dataGridView2.DataSource = null;
            dataGridView2.Columns.Clear();

            dataGridView2.ColumnCount = 2;

            dataGridView2.Columns[0].Name = "프로그램";
            dataGridView2.Columns[1].Name = "권한";

            // Head Cell_Columns Font Style = off
            dataGridView2.EnableHeadersVisualStyles = false;
            // Head Cell_Columns HeightSizeMode = off
            dataGridView2.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.DisableResizing;

            // Head Cell_Columns Width Size 임의 지정
            DataGridViewColumn column1 = dataGridView2.Columns[0];
            column1.Width = 220;
            DataGridViewColumn column2 = dataGridView2.Columns[1];
            column2.Width = 80;

            // Head Cell_Columns HeightSize 임의 지정
            dataGridView2.ColumnHeadersHeight = 30;

            for (int j = 0; j < dataGridView2.ColumnCount; j++)
            {
                dataGridView2.Columns[j].HeaderCell.Style.Font = new Font("맑은고딕", 10, FontStyle.Bold);
                //dataGridView1.Columns[j].HeaderCell.Style.BackColor = Color.Yellow;

                // dGVState.Columns[j].HeaderCell.Size = new Size();

                // Head Cell_Columns Alignment
                dataGridView2.Columns[j].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;

                // Cell Font Style
                //dGVProcess.Columns[j].DefaultCellStyle.Font = new Font("Tahoma", 18, FontStyle.Bold);

                // Cell Font Alignment
                if (j == 1)
                {
                    dataGridView2.Columns[j].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                }
                //else
                //{
                //    dataGridView1.Columns[j].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                //
                //}

                //  Cell Height 지정
                //  var dgv = new DataGridView();
                //dataGridView1.RowTemplate.Height = 30;
            }

            int g = 0;
            while (g < treenode.Count)
            {
                int n = dataGridView2.Rows.Add();
                dataGridView2.Rows[n].Cells[0].Value = treenode[g];
                g++;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //MessageBox.Show(panel1.Size.ToString());
            panel2.Size = new Size((Convert.ToInt32(panel1.Size.Width.ToString()) / 2) + 13, 25);
            dataGridView1.Size = new Size((Convert.ToInt32(panel1.Size.Width.ToString()) / 2) + 13, Convert.ToInt32(panel1.Size.Height.ToString()) - 30);
            panel3.Location = new Point((Convert.ToInt32(panel1.Size.Width.ToString()) / 2) + 15, 0);
            panel4.Location = new Point((Convert.ToInt32(panel1.Size.Width.ToString()) / 2) + 15, 26);
            dataGridView2.Location = new Point((Convert.ToInt32(panel1.Size.Width.ToString()) / 2) + 15, 126);
            panel3.Size = new Size((Convert.ToInt32(panel1.Size.Width.ToString()) / 2) - 20, 25);
            dataGridView2.Size = new Size((Convert.ToInt32(panel1.Size.Width.ToString()) / 2) - 20, Convert.ToInt32(panel1.Size.Height.ToString()) - 126);
        }

        private void button2_Click(object sender, EventArgs e)  //FORM LOAD
        {
            Main_Process();

            if (dataGridView1.Rows.Count > 0)
            {
                textBox1.Text = dataGridView1.Rows[0].Cells[0].FormattedValue.ToString();
                textBox2.Text = dataGridView1.Rows[0].Cells[3].FormattedValue.ToString();
                textBox3.Text = dataGridView1.Rows[0].Cells[1].FormattedValue.ToString();

                int ct = 0;
                foreach (char c in dataGridView1.Rows[0].Cells[4].FormattedValue.ToString())
                {
                    ct++;
                    if (textBox1.Text != "")
                    {
                        dataGridView2_ATH_GBN_ck(ct, c);
                    }
                }

                for (int j = 0; j < dataGridView2.Rows.Count; j++)
                {
                    if (dataGridView2.Rows[j].Cells[1].FormattedValue.ToString() == "쓰기") dataGridView2.Rows[j].Cells[1].Style.BackColor = Color.Green;
                    else dataGridView2.Rows[j].Cells[1].Style.BackColor = Color.Red;
                }
            }

            button4.Enabled = false;
            button5.Enabled = true;
            button6.Enabled = true;
            button7.Enabled = false;
            textBox1.ReadOnly = true;
            textBox2.ReadOnly = false;
            textBox3.ReadOnly = false;

            dataGridView2.CurrentCell = dataGridView2.Rows[0].Cells[0];
        }

        private void button3_Click(object sender, EventArgs e)
        {
            textBox1.Text = "";
            textBox2.Text = "";
            textBox3.Text = "";
            for (int j = 0; j < dataGridView2.Rows.Count; j++)
            {
                dataGridView2.Rows[j].Cells[1].Value = "쓰기";
                dataGridView2.Rows[j].Cells[1].Style.BackColor = Color.Green;
            }

            button4.Enabled = true;
            button5.Enabled = false;
            button6.Enabled = false;
            button7.Enabled = true;
            textBox1.ReadOnly = false;
            textBox2.ReadOnly = false;
            textBox3.ReadOnly = false;
        }

        private void button4_Click(object sender, EventArgs e)
        {
            if (textBox1.Text == "") MessageBox.Show("사용자 ID를 입력하세요.");
            else if (textBox2.Text == "") MessageBox.Show("패스워드를 입력하세요.");
            else if (textBox3.Text == "") MessageBox.Show("사용자 이름을 입력하세요.");
            else
            {
                List<string> Condition = new List<string>();

                Condition.Add(textBox1.Text);
                d1 = CommonDB.R_datatbl(Qry.KSWMS110_002(Condition));
                
                if (d1.Rows[0][0].ToString() == "1")
                {
                    MessageBox.Show("이미 ID: " + textBox1.Text + " 사용자가 등록되어 있습니다..");
                }
                else
                {
                    Condition.Add(textBox2.Text);
                    Condition.Add(textBox3.Text);
                    CreatUserChk();
                    Condition.Add(UserChk);

                    DialogResult dr = MessageBox.Show("ID: " + textBox1.Text + " 정보를 등록 하시겠습니까?.",
                          "사용자관리", MessageBoxButtons.YesNo);
                    switch (dr)
                    {
                        case DialogResult.Yes:
                            CommonDB.R_datatbl(Qry.KSWMS110_004(Condition));
                            button2.PerformClick();
                            break;
                        case DialogResult.No:
                            break;
                    }
                }
            }
        }

        private void button5_Click(object sender, EventArgs e)
        {
            List<string> Condition = new List<string>();

            CreatUserChk();

            Condition.Add(textBox1.Text);
            Condition.Add(textBox2.Text);
            Condition.Add(textBox3.Text);
            Condition.Add(UserChk);
            //MessageBox.Show(Condition[2].ToString());
            d1 = CommonDB.R_datatbl(Qry.KSWMS110_003(Condition));

            if (d1.Rows[0][0].ToString() == "1")
            {
                MessageBox.Show("수정된 자료가 없습니다.");
            }
            else
            {
                DialogResult dr = MessageBox.Show("ID: " + textBox1.Text + " 의 정보를 수정하시겠습니까?",
                      "사용자관리", MessageBoxButtons.YesNo);
                switch (dr)
                {
                    case DialogResult.Yes:
                        CommonDB.R_datatbl(Qry.KSWMS110_005(Condition));
                        break;
                    case DialogResult.No:
                        break;
                }
            }

            button2.PerformClick();
        }

        private void button6_Click(object sender, EventArgs e)
        {
            List<string> Condition = new List<string>();

            CreatUserChk();

            Condition.Add(textBox1.Text);
            Condition.Add(textBox2.Text);
            Condition.Add(textBox3.Text);
            Condition.Add(UserChk);

            DialogResult dr = MessageBox.Show("ID: " + textBox1.Text + " 의 정보를 삭제 하시겠습니까?",
                     "사용자관리", MessageBoxButtons.YesNo);
            switch (dr)
            {
                case DialogResult.Yes:
                    d1 = CommonDB.R_datatbl(Qry.KSWMS110_006(Condition));
                    break;
                case DialogResult.No:
                    break;
            }

            button2.PerformClick();
        }

        private void button7_Click(object sender, EventArgs e)
        {
            button2.PerformClick();
        }

        private void Main_Process()
        {
            Cursor.Current = Cursors.WaitCursor;
            d1 = CommonDB.R_datatbl(Qry.KSWMS110_001);
            
            Clear();
            dataGridView1Set();

            int j = 0;
            
            while (j < d1.Rows.Count)
            {
                int n = dataGridView1.Rows.Add();
                
                for (int i = 0; i <= d1.Columns.Count-1; i++)
                {
                    dataGridView1.Rows[n].Cells[i].Value = d1.Rows[j][i].ToString();
            
                    //if (i == 0)
                    //{
                    //    dataGridView1.Rows[n].Cells[i].Value = (j + 1).ToString();
                    //}
                    //else if (i == 18)
                    //{
                    //    if (d1.Rows[j][14].ToString() != "")
                    //    {
                    //        dataGridView1.Rows[n].Cells[i].Value = "0";
                    //    }
                    //    else
                    //    {
                    //        dataGridView1.Rows[n].Cells[i].Value = d1.Rows[j][i - 1].ToString();
                    //    }
                    //}
                    //else
                    //{
                    //    dataGridView1.Rows[n].Cells[i].Value = d1.Rows[j][i - 1].ToString();
                    //}
            
                    /*
                    if (dataGridView1.Rows[n].Cells[i].Value.Equals("EST"))
                    {
                        dataGridView1.Rows[n].DefaultCellStyle.BackColor = Color.Red;
                        dataGridView1.Rows[n].DefaultCellStyle.ForeColor = Color.White;
                    }
                    */
                }
            
                j++;
            }

            
            //statusBar1.Panels[0].Text = "전체 " + j.ToString() + " 건이 검색 되었습니다.";
        }

        private void Clear()
        {
            // 그리드 초기화
            dataGridView1.DataSource = null;
            dataGridView1.Columns.Clear();
        }

        private void CLEAR_dataGridView2()
        {
            for (int j = 0; j < dataGridView2.Rows.Count; j++)
            {
                dataGridView2.Rows[j].Cells[1].Value = "";
                dataGridView2.Rows[j].Cells[1].Style.BackColor = Color.White;
            }
        }

        private void dataGridView1Set()
        {
            //this.Controls.Add(dataGridView1);

            dataGridView1.ColumnCount = d1.Columns.Count;

            dataGridView1.Columns[0].Name = "사용자ID";
            dataGridView1.Columns[1].Name = "사용자명";
            dataGridView1.Columns[2].Name = "변경일자";
            dataGridView1.Columns[3].Name = "암호";
            dataGridView1.Columns[4].Name = "권한";
            //dataGridView1.Columns[5].Name = "매입품목";
            //dataGridView1.Columns[6].Name = "매입금액";
            //dataGridView1.Columns[7].Name = "이윤율(건수)";
            //dataGridView1.Columns[8].Name = "이윤율(품목)";
            //dataGridView1.Columns[7].Name = "이윤율(금액)";

            this.dataGridView1.Columns["암호"].Visible = false;
            this.dataGridView1.Columns["권한"].Visible = false;

            // Columns AutoSize
            //dGVProcess.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells;

            // Head Cell_Columns Font Style = off
            dataGridView1.EnableHeadersVisualStyles = false;
            // Head Cell_Columns HeightSizeMode = off
            dataGridView1.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.DisableResizing;

            // Head Cell_Columns Width Size 임의 지정
            DataGridViewColumn column1 = dataGridView1.Columns[0];
            column1.Width = 120;

            DataGridViewColumn column2 = dataGridView1.Columns[1];
            column2.Width = 200;

            DataGridViewColumn column3 = dataGridView1.Columns[2];
            column3.Width = 135;

            DataGridViewColumn column4 = dataGridView1.Columns[3];
            column4.Width = 60;
            
            DataGridViewColumn column5 = dataGridView1.Columns[4];
            column5.Width = 1300;
            
            //DataGridViewColumn column6 = dataGridView1.Columns[5];
            //column6.Width = 70;
            //
            //DataGridViewColumn column7 = dataGridView1.Columns[6];
            //column7.Width = 90;
            //
            //DataGridViewColumn column8 = dataGridView1.Columns[7];
            //column8.Width = 150;

            //DataGridViewColumn column9 = dataGridView1.Columns[8];
            //column9.Width = 50;

            //DataGridViewColumn column10 = dataGridView1.Columns[9];
            //column10.Width = 50;



            // Head Cell_Columns HeightSize 임의 지정
            dataGridView1.ColumnHeadersHeight = 30;

            for (int j = 0; j < d1.Columns.Count; j++)
            {
                dataGridView1.Columns[j].HeaderCell.Style.Font = new Font("맑은고딕", 10, FontStyle.Bold);
                // dGVProcess.Columns[j].HeaderCell.Style.ForeColor = Color.Red;
                //dataGridView1.Columns[j].HeaderCell.Style.BackColor = Color.Yellow;

                // dGVState.Columns[j].HeaderCell.Size = new Size();

                // Head Cell_Columns Alignment
                dataGridView1.Columns[j].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;

                // Cell Font Style
                //dGVProcess.Columns[j].DefaultCellStyle.Font = new Font("Tahoma", 18, FontStyle.Bold);

                // Cell Font Alignment
                if (j == 2)
                {
                    dataGridView1.Columns[j].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                }
                //else
                //{
                //    dataGridView1.Columns[j].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                //
                //}

                //  Cell Height 지정
                //  var dgv = new DataGridView();
                //dataGridView1.RowTemplate.Height = 30;

            }
        }

       
        private void dataGridView2_ATH_GBN_ck(int count, char c)
        {
            if (count == 1)
            {
                if (c.ToString() == "W") dataGridView2.Rows[0].Cells[1].Value = "쓰기";
                else dataGridView2.Rows[0].Cells[1].Value = "금지";
            }
            else if (count == 17)
            {
                if (c.ToString() == "W") dataGridView2.Rows[1].Cells[1].Value = "쓰기";
                else dataGridView2.Rows[1].Cells[1].Value = "금지";
            }
            else if (count == 18)
            {
                if (c.ToString() == "W") dataGridView2.Rows[2].Cells[1].Value = "쓰기";
                else dataGridView2.Rows[2].Cells[1].Value = "금지";
            }
            else if (count == 19)
            {
                if (c.ToString() == "W") dataGridView2.Rows[3].Cells[1].Value = "쓰기";
                else dataGridView2.Rows[3].Cells[1].Value = "금지";
            }
            else if (count == 33)
            {
                if (c.ToString() == "W") dataGridView2.Rows[4].Cells[1].Value = "쓰기";
                else dataGridView2.Rows[4].Cells[1].Value = "금지";
            }
            else if (count == 34)
            {
                if (c.ToString() == "W") dataGridView2.Rows[5].Cells[1].Value = "쓰기";
                else dataGridView2.Rows[5].Cells[1].Value = "금지";
            }
            else if (count == 49)
            {
                if (c.ToString() == "W") dataGridView2.Rows[6].Cells[1].Value = "쓰기";
                else dataGridView2.Rows[6].Cells[1].Value = "금지";
            }
            else if (count == 50)
            {
                if (c.ToString() == "W") dataGridView2.Rows[7].Cells[1].Value = "쓰기";
                else dataGridView2.Rows[7].Cells[1].Value = "금지";
            }
            else if (count == 51)
            {
                if (c.ToString() == "W") dataGridView2.Rows[8].Cells[1].Value = "쓰기";
                else dataGridView2.Rows[8].Cells[1].Value = "금지";
            }
            else if (count == 52)
            {
                if (c.ToString() == "W") dataGridView2.Rows[9].Cells[1].Value = "쓰기";
                else dataGridView2.Rows[9].Cells[1].Value = "금지";
            }
            else if (count == 53)
            {
                if (c.ToString() == "W") dataGridView2.Rows[10].Cells[1].Value = "쓰기";
                else dataGridView2.Rows[10].Cells[1].Value = "금지";
            }
            else if (count == 57)
            {
                if (c.ToString() == "W") dataGridView2.Rows[11].Cells[1].Value = "쓰기";
                else dataGridView2.Rows[11].Cells[1].Value = "금지";
            }
            else if (count == 65)
            {
                if (c.ToString() == "W") dataGridView2.Rows[12].Cells[1].Value = "쓰기";
                else dataGridView2.Rows[12].Cells[1].Value = "금지";
            }
            else if (count == 81)
            {
                if (c.ToString() == "W") dataGridView2.Rows[13].Cells[1].Value = "쓰기";
                else dataGridView2.Rows[13].Cells[1].Value = "금지";
            }
            else if (count == 82)
            {
                if (c.ToString() == "W") dataGridView2.Rows[14].Cells[1].Value = "쓰기";
                else dataGridView2.Rows[14].Cells[1].Value = "금지";
            }
            else if (count == 97)
            {
                if (c.ToString() == "W") dataGridView2.Rows[15].Cells[1].Value = "쓰기";
                else dataGridView2.Rows[15].Cells[1].Value = "금지";
            }
            else if (count == 98)
            {
                if (c.ToString() == "W") dataGridView2.Rows[16].Cells[1].Value = "쓰기";
                else dataGridView2.Rows[16].Cells[1].Value = "금지";
            }
            
        }

        public void CreatUserChk()
        {
            UserChk = null;

            for (int j = 1; j <= 160; j++)
            {
                if (j == 1)
                {
                    if (dataGridView2.Rows[0].Cells[1].Value.ToString() == "쓰기") UserChk += "W";
                    else UserChk += "X";
                }
                else if (j == 17)
                {
                    if (dataGridView2.Rows[1].Cells[1].Value.ToString() == "쓰기") UserChk += "W";
                    else UserChk += "X";
                }
                else if (j == 18)
                {
                    if (dataGridView2.Rows[2].Cells[1].Value.ToString() == "쓰기") UserChk += "W";
                    else UserChk += "X";
                }
                else if (j == 19)
                {
                    if (dataGridView2.Rows[3].Cells[1].Value.ToString() == "쓰기") UserChk += "W";
                    else UserChk += "X";
                }
                else if (j == 33)
                {
                    if (dataGridView2.Rows[4].Cells[1].Value.ToString() == "쓰기") UserChk += "W";
                    else UserChk += "X";
                }
                else if (j == 34)
                {
                    if (dataGridView2.Rows[5].Cells[1].Value.ToString() == "쓰기") UserChk += "W";
                    else UserChk += "X";
                }
                else if (j == 49)
                {
                    if (dataGridView2.Rows[6].Cells[1].Value.ToString() == "쓰기") UserChk += "W";
                    else UserChk += "X";
                }
                else if (j == 50)
                {
                    if (dataGridView2.Rows[7].Cells[1].Value.ToString() == "쓰기") UserChk += "W";
                    else UserChk += "X";
                }
                else if (j == 51)
                {
                    if (dataGridView2.Rows[8].Cells[1].Value.ToString() == "쓰기") UserChk += "W";
                    else UserChk += "X";
                }
                else if (j == 52)
                {
                    if (dataGridView2.Rows[9].Cells[1].Value.ToString() == "쓰기") UserChk += "W";
                    else UserChk += "X";
                }
                else if (j == 53)
                {
                    if (dataGridView2.Rows[10].Cells[1].Value.ToString() == "쓰기") UserChk += "W";
                    else UserChk += "X";
                }
                else if (j == 57)
                {
                    if (dataGridView2.Rows[11].Cells[1].Value.ToString() == "쓰기") UserChk += "W";
                    else UserChk += "X";
                }
                else if (j == 65)
                {
                    if (dataGridView2.Rows[12].Cells[1].Value.ToString() == "쓰기") UserChk += "W";
                    else UserChk += "X";
                }
                else if (j == 81)
                {
                    if (dataGridView2.Rows[13].Cells[1].Value.ToString() == "쓰기") UserChk += "W";
                    else UserChk += "X";
                }
                else if (j == 82)
                {
                    if (dataGridView2.Rows[14].Cells[1].Value.ToString() == "쓰기") UserChk += "W";
                    else UserChk += "X";
                }
                else if (j == 97)
                {
                    if (dataGridView2.Rows[15].Cells[1].Value.ToString() == "쓰기") UserChk += "W";
                    else UserChk += "X";
                }
                else if (j == 98)
                {
                    if (dataGridView2.Rows[16].Cells[1].Value.ToString() == "쓰기") UserChk += "W";
                    else UserChk += "X";
                }
                else UserChk += "X";
            }
        }

        private void dataGridView1_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if(button4.Enabled == true)
            {
                return;
            }

            if (e.RowIndex >= 0 && e.ColumnIndex >= 0)
            {
                textBox1.Text = dataGridView1.Rows[e.RowIndex].Cells[0].FormattedValue.ToString();
                textBox2.Text = dataGridView1.Rows[e.RowIndex].Cells[3].FormattedValue.ToString();
                textBox3.Text = dataGridView1.Rows[e.RowIndex].Cells[1].FormattedValue.ToString();

                int ct = 0;
                foreach (char c in dataGridView1.Rows[e.RowIndex].Cells[4].FormattedValue.ToString())
                {
                    ct++;
                    if (textBox1.Text != "")
                    {
                        dataGridView2_ATH_GBN_ck(ct, c);
                    }
                }

                for (int j = 0; j < dataGridView2.Rows.Count; j++)
                {
                    if (dataGridView2.Rows[j].Cells[1].FormattedValue.ToString() == "쓰기") dataGridView2.Rows[j].Cells[1].Style.BackColor = Color.Green;
                    else dataGridView2.Rows[j].Cells[1].Style.BackColor = Color.Red;
                }
            }

        }

       

        private void dataGridView2_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0 && e.ColumnIndex == 1)
            {
                if (dataGridView2.Rows[e.RowIndex].Cells[1].FormattedValue.ToString() == "쓰기")
                {
                    dataGridView2.Rows[e.RowIndex].Cells[e.ColumnIndex].Value = "금지";
                    dataGridView2.Rows[e.RowIndex].Cells[1].Style.BackColor = Color.Red;
                }
                else
                {
                    dataGridView2.Rows[e.RowIndex].Cells[1].Value = "쓰기";
                    dataGridView2.Rows[e.RowIndex].Cells[1].Style.BackColor = Color.Green;
                }
            }
        }

    }
}
