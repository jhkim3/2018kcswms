﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace KSWMS
{
    public partial class KSWMS310 : MyFormPage
    {
        private TabPage TabP;
        private DataTable d1 = new DataTable();
        private Rectangle dataGridView1_r1, dataGridView1_r2, dataGridView1_r3;
        private Rectangle dataGridView4_r1, dataGridView4_r2, dataGridView4_r3;
        private Rectangle dataGridView5_r1, dataGridView5_r2, dataGridView5_r3;

        private int[] dataGridView1_DoubleHead_width = new int[30];
        private int[] dataGridView4_DoubleHead_width = new int[30];
        private int[] dataGridView5_DoubleHead_width = new int[30];

        public KSWMS310()
        {
            InitializeComponent();
            this.pnl = panel1;
            this.btn1 = button1;
            this.btn2 = button2;
        }

        private void tabControl1_MouseClick(object sender, MouseEventArgs e)
        {
            SetTabP_Ctl();
        }

        private void SetTabP_Ctl()
        {
            T1_dataGridView1_Clear();
            TabP = tabControl1.SelectedTab;
            if (TabP != null)
            {
                if (TabP.Name.ToString() == "tabPage1")
                {
                    T1_dataGridView1_Main_Process();
                }
                else if (TabP.Name.ToString() == "tabPage2")
                {
                    
                }
                else if (TabP.Name.ToString() == "tabPage3")
                {
                    T3_dataGridView4_Main_Process();
                    T3_dataGridView5_Main_Process();
                }

                //MessageBox.Show(TabP.Controls[2].ToString());
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            dataGridView4.Size = new Size(Convert.ToInt32(panel1.Size.Width.ToString()) - 10, Convert.ToInt32(panel1.Size.Height.ToString())/2 - 75);
            dataGridView5.Location = new Point(0, (Convert.ToInt32(panel1.Size.Height.ToString())/ 2) + 20);
            dataGridView5.Size = new Size(Convert.ToInt32(panel1.Size.Width.ToString()) - 10, Convert.ToInt32(panel1.Size.Height.ToString()) / 2 - 55);
            button18.Location = new Point(550, Convert.ToInt32(panel1.Size.Height.ToString()) / 2 - 13);
            button19.Location = new Point(630, Convert.ToInt32(panel1.Size.Height.ToString()) / 2 - 13);
            //button19.Location = new Point(550, 270);
            //button20.Location = new Point(630, 270);
            //MessageBox.Show(panel1.Size.Width.ToString() +"     "+ panel1.Size.Height.ToString());

            dataGridView2.Size = new Size(Convert.ToInt32(panel1.Size.Width.ToString()) - 10, Convert.ToInt32(panel1.Size.Height.ToString()) / 2 - 65);
            panel3.Location = new Point(0, (Convert.ToInt32(panel1.Size.Height.ToString()) / 2) - 20);
            dataGridView3.Location = new Point(0, (Convert.ToInt32(panel1.Size.Height.ToString()) / 2) + 25);
            dataGridView3.Size = new Size(Convert.ToInt32(panel1.Size.Width.ToString()) - 10, Convert.ToInt32(panel1.Size.Height.ToString()) / 2 - 60);
        }
        private void button2_Click(object sender, EventArgs e)
        {
            T1_dataGridView1_Main_Process();
        }


        private void T1_dataGridView1_Main_Process()
        {
            Cursor.Current = Cursors.WaitCursor;
            d1 = CommonDB.R_datatbl(Qry.KSWMS210_009);
            
            T1_dataGridView1_Clear();
            dataGridView1Set();
            //MessageBox.Show(dataGridView1.Columns.Clear.ToString());

            int j = 0;

            while (j < d1.Rows.Count)
            {
                int n = dataGridView1.Rows.Add();

                for (int i = 0; i <= d1.Columns.Count - 1; i++)
                {
                    dataGridView1.Rows[n].Cells[i].Value = d1.Rows[j][i].ToString();

                    //if (i == 0)
                    //{
                    //    dataGridView1.Rows[n].Cells[i].Value = (j + 1).ToString();
                    //}
                    //else if (i == 18)
                    //{
                    //    if (d1.Rows[j][14].ToString() != "")
                    //    {
                    //        dataGridView1.Rows[n].Cells[i].Value = "0";
                    //    }
                    //    else
                    //    {
                    //        dataGridView1.Rows[n].Cells[i].Value = d1.Rows[j][i - 1].ToString();
                    //    }
                    //}
                    //else
                    //{
                    //    dataGridView1.Rows[n].Cells[i].Value = d1.Rows[j][i - 1].ToString();
                    //}

                    /*
                    if (dataGridView1.Rows[n].Cells[i].Value.Equals("EST"))
                    {
                        dataGridView1.Rows[n].DefaultCellStyle.BackColor = Color.Red;
                        dataGridView1.Rows[n].DefaultCellStyle.ForeColor = Color.White;
                    }
                    */
                }

                j++;
            }


            //statusBar1.Panels[0].Text = "전체 " + j.ToString() + " 건이 검색 되었습니다.";
        }

        
        private void T1_dataGridView1_Clear()
        {
            // 그리드 초기화
            dataGridView1.DataSource = null;
            dataGridView1.Columns.Clear();
          
        }

        private void dataGridView1Set()
        {
            //this.Controls.Add(dataGridView1);

            dataGridView1.ColumnCount = d1.Columns.Count;
           
            dataGridView1.Columns[0].Name = "WMS NO";
            dataGridView1.Columns[1].Name = "최초재고";
            dataGridView1.Columns[2].Name = "입고일";
            dataGridView1.Columns[3].Name = "구역";
            dataGridView1.Columns[4].Name = "상태";
            dataGridView1.Columns[5].Name = "유형";
            dataGridView1.Columns[6].Name = "총수량";
            dataGridView1.Columns[7].Name = "B/L NO";
            dataGridView1.Columns[8].Name = "코드";
            dataGridView1.Columns[9].Name = "상호";
            dataGridView1.Columns[10].Name = "담당";
            dataGridView1.Columns[11].Name = "코드";
            dataGridView1.Columns[12].Name = "제품명";
            dataGridView1.Columns[13].Name = "규격";
            dataGridView1.Columns[14].Name = "단위";
            dataGridView1.Columns[15].Name = "표시";

            //this.dataGridView1.Columns["암호"].Visible = false;
            //this.dataGridView1.Columns["권한"].Visible = false;

            // Columns AutoSize
            //dGVProcess.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells;

            // Head Cell_Columns Font Style = off
            dataGridView1.EnableHeadersVisualStyles = false;
            // Head Cell_Columns HeightSizeMode = off
            dataGridView1.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.DisableResizing;

            // Head Cell_Columns HeightSize 임의 지정
            dataGridView1.ColumnHeadersHeight = 60;

            // Head Rows Control
            //dataGridView1.RowHeadersVisible = false;
            //dataGridView1.RowHeadersWidthSizeMode = DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            // RowHeadersWidth_Size 임의 지정
            dataGridView1.RowHeadersWidth = 20;
            
         

            for (int j = 0; j < d1.Columns.Count; j++)
            {
                dataGridView1.Columns[j].HeaderCell.Style.Font = new Font("맑은고딕", 10, FontStyle.Bold);
                // dGVProcess.Columns[j].HeaderCell.Style.ForeColor = Color.Red;
                //dataGridView1.Columns[j].HeaderCell.Style.BackColor = Color.Yellow;

                // dGVState.Columns[j].HeaderCell.Size = new Size();

                // Head Cell_Columns Alignment
                //dataGridView1.Columns[j].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
                //if (j < 1) dataGridView1.Columns[j].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
                //else dataGridView1.Columns[j].HeaderCell.Style.Alignment = DataGridViewContentAlignment.BottomCenter;

                dataGridView1.Columns[j].HeaderCell.Style.Alignment = DataGridViewContentAlignment.BottomCenter;

                // Cell Font Style
                //dGVProcess.Columns[j].DefaultCellStyle.Font = new Font("Tahoma", 18, FontStyle.Bold);

                // Cell Font Alignment
                if (j == 2)
                {
                    dataGridView1.Columns[j].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                }
                //else
                //{
                //    dataGridView1.Columns[j].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                //
                //}

                //  Cell Height 지정
                //  var dgv = new DataGridView();
                //dataGridView1.RowTemplate.Height = 30;


                // Head Cell_Columns Width Size 임의 지정
                DataGridViewColumn column1 = dataGridView1.Columns[0];
                column1.Width = 100;
                DataGridViewColumn column2 = dataGridView1.Columns[1];
                column2.Width = 100;
                DataGridViewColumn column3 = dataGridView1.Columns[2];
                column3.Width = 150;
                DataGridViewColumn column4 = dataGridView1.Columns[3];
                column4.Width = 60;
                DataGridViewColumn column5 = dataGridView1.Columns[4];
                column5.Width = 60;
                DataGridViewColumn column6 = dataGridView1.Columns[5];
                column6.Width = 60;
                DataGridViewColumn column7 = dataGridView1.Columns[6];
                column7.Width = 90;
                DataGridViewColumn column8 = dataGridView1.Columns[7];
                column8.Width = 150;
                DataGridViewColumn column9 = dataGridView1.Columns[8];
                column9.Width = 60;
                DataGridViewColumn column10 = dataGridView1.Columns[9];
                column10.Width = 180;
                DataGridViewColumn column11 = dataGridView1.Columns[10];
                column11.Width = 60;
                DataGridViewColumn column12 = dataGridView1.Columns[11];
                column12.Width = 60;
                DataGridViewColumn column13 = dataGridView1.Columns[12];
                column13.Width = 120;
                DataGridViewColumn column14 = dataGridView1.Columns[13];
                column14.Width = 60;
                DataGridViewColumn column15 = dataGridView1.Columns[14];
                column15.Width = 60;

                dataGridView1_r1 = dataGridView1.GetCellDisplayRectangle(0, -1, false);
                dataGridView1_DoubleHead_width[0] = dataGridView1.GetCellDisplayRectangle(1, -1, false).Width;
                dataGridView1_DoubleHead_width[1] = dataGridView1.GetCellDisplayRectangle(2, -1, false).Width;
                dataGridView1_DoubleHead_width[2] = dataGridView1.GetCellDisplayRectangle(3, -1, false).Width;
                dataGridView1_DoubleHead_width[3] = dataGridView1.GetCellDisplayRectangle(4, -1, false).Width;
                dataGridView1_DoubleHead_width[4] = dataGridView1.GetCellDisplayRectangle(5, -1, false).Width;
                dataGridView1_DoubleHead_width[5] = dataGridView1.GetCellDisplayRectangle(6, -1, false).Width;
                dataGridView1_DoubleHead_width[6] = dataGridView1.GetCellDisplayRectangle(7, -1, false).Width;


                dataGridView1_r2 = dataGridView1.GetCellDisplayRectangle(8, -1, false);
                dataGridView1_DoubleHead_width[10] = dataGridView1.GetCellDisplayRectangle(9, -1, false).Width;
                dataGridView1_DoubleHead_width[11] = dataGridView1.GetCellDisplayRectangle(10, -1, false).Width;

                dataGridView1_r3 = dataGridView1.GetCellDisplayRectangle(11, -1, false);

                dataGridView1_DoubleHead_width[20] = dataGridView1.GetCellDisplayRectangle(12, -1, false).Width;
                dataGridView1_DoubleHead_width[21] = dataGridView1.GetCellDisplayRectangle(13, -1, false).Width;
                dataGridView1_DoubleHead_width[22] = dataGridView1.GetCellDisplayRectangle(14, -1, false).Width;

            }


        }

        private void dataGridView1_CellPainting(object sender, DataGridViewCellPaintingEventArgs e)
        {
            if (e.RowIndex == -1 && e.ColumnIndex > -1)

            {
                Rectangle r = e.CellBounds;

                r.Y += e.CellBounds.Height / 2;

                r.Height = e.CellBounds.Height / 2;

                e.PaintBackground(r, true);

                e.PaintContent(r);

                e.Handled = true;
            }
        }

        private void dataGridView1_ColumnWidthChanged(object sender, DataGridViewColumnEventArgs e)
        {
            DataGridView gv = (DataGridView)sender;

            Rectangle rtHeader = gv.DisplayRectangle;

            rtHeader.Height = gv.ColumnHeadersHeight / 2;

            gv.Invalidate(rtHeader);
        }

        private void dataGridView1_Paint(object sender, PaintEventArgs e)
        {
            DataGridView gv = (DataGridView)sender;

            string[] strHeaders = { "작  업", "화  주", "제  품" };

            StringFormat format = new StringFormat();

            format.Alignment = StringAlignment.Center;

            format.LineAlignment = StringAlignment.Center;


            // Category Painting

            {
                dataGridView1_r1.X += 1;

                dataGridView1_r1.Y += 2;

                dataGridView1_r1.Width = dataGridView1_r1.Width + dataGridView1_DoubleHead_width[0] + 
                                         dataGridView1_DoubleHead_width[1] + dataGridView1_DoubleHead_width[2] + 
                                         dataGridView1_DoubleHead_width[3] + dataGridView1_DoubleHead_width[4] + 
                                         dataGridView1_DoubleHead_width[5] + dataGridView1_DoubleHead_width[6] - 2;

                dataGridView1_r1.Height = (dataGridView1_r1.Height / 2) - 2;

                e.Graphics.DrawRectangle(new Pen(gv.BackgroundColor), dataGridView1_r1);

                e.Graphics.FillRectangle(new SolidBrush(gv.ColumnHeadersDefaultCellStyle.BackColor),
                //e.Graphics.FillRectangle(new SolidBrush(Color.Yellow),

                dataGridView1_r1);


                e.Graphics.DrawString(strHeaders[0],

                    //gv.ColumnHeadersDefaultCellStyle.Font,
                    new Font("맑은고딕", 9, FontStyle.Bold),

                    new SolidBrush(gv.ColumnHeadersDefaultCellStyle.ForeColor),

                    dataGridView1_r1,

                    format);

            }

            // Projection Painting

            {
                dataGridView1_r2.X += 1;

                dataGridView1_r2.Y += 2;

                dataGridView1_r2.Width = dataGridView1_r2.Width + dataGridView1_DoubleHead_width[10] + dataGridView1_DoubleHead_width[11] - 2;

                dataGridView1_r2.Height = (dataGridView1_r2.Height / 2) - 2;

                e.Graphics.DrawRectangle(new Pen(gv.BackgroundColor), dataGridView1_r2);

                e.Graphics.FillRectangle(new SolidBrush(gv.ColumnHeadersDefaultCellStyle.BackColor),
                //e.Graphics.FillRectangle(new SolidBrush(Color.Yellow),

                dataGridView1_r2);


                e.Graphics.DrawString(strHeaders[1],

                    //gv.ColumnHeadersDefaultCellStyle.Font,
                    new Font("맑은고딕", 9, FontStyle.Bold),

                    new SolidBrush(gv.ColumnHeadersDefaultCellStyle.ForeColor),

                    dataGridView1_r2,

                    format);
            }

            // GROUP HEAD 매출율

            {
                dataGridView1_r3.X += 1;

                dataGridView1_r3.Y += 2;

                dataGridView1_r3.Width = dataGridView1_r3.Width + dataGridView1_DoubleHead_width[20] + dataGridView1_DoubleHead_width[21] + 
                                      dataGridView1_DoubleHead_width[22] - 2;

                dataGridView1_r3.Height = (dataGridView1_r3.Height / 2) - 2;

                e.Graphics.DrawRectangle(new Pen(gv.BackgroundColor), dataGridView1_r3);

                e.Graphics.FillRectangle(new SolidBrush(gv.ColumnHeadersDefaultCellStyle.BackColor),
                //e.Graphics.FillRectangle(new SolidBrush(Color.Yellow),

                dataGridView1_r3);


                e.Graphics.DrawString(strHeaders[2],

                    //gv.ColumnHeadersDefaultCellStyle.Font,
                    new Font("맑은고딕", 9, FontStyle.Bold),

                    new SolidBrush(gv.ColumnHeadersDefaultCellStyle.ForeColor),

                    dataGridView1_r3,

                    format);
            }
        }

        private void dataGridView1_Scroll(object sender, ScrollEventArgs e)
        {
            DataGridView gv = (DataGridView)sender;

            Rectangle rtHeader = gv.DisplayRectangle;

            rtHeader.Height = gv.ColumnHeadersHeight / 2;

            gv.Invalidate(rtHeader);
        }


        // dataGridView4 Set ===========================================================================

        private void T3_dataGridView4_Main_Process()
        {
            Cursor.Current = Cursors.WaitCursor;
            d1 = CommonDB.R_datatbl(Qry.KSWMS210_009);
       
            T3_dataGridView4_Clear();
            dataGridView4Set();
       
            int j = 0;
            
            while (j < d1.Rows.Count)
            {
                int n = dataGridView4.Rows.Add();
            
                for (int i = 1; i <= d1.Columns.Count; i++)
                {
                    dataGridView4.Rows[j].Cells[i].Value = d1.Rows[j][i - 1].ToString();
            
                    //if (i == 0)
                    //{
                    //    dataGridView1.Rows[n].Cells[i].Value = (j + 1).ToString();
                    //}
                    //else if (i == 18)
                    //{
                    //    if (d1.Rows[j][14].ToString() != "")
                    //    {
                    //        dataGridView1.Rows[n].Cells[i].Value = "0";
                    //    }
                    //    else
                    //    {
                    //        dataGridView1.Rows[n].Cells[i].Value = d1.Rows[j][i - 1].ToString();
                    //    }
                    //}
                    //else
                    //{
                    //    dataGridView1.Rows[n].Cells[i].Value = d1.Rows[j][i - 1].ToString();
                    //}
            
                    /*
                    if (dataGridView1.Rows[n].Cells[i].Value.Equals("EST"))
                    {
                        dataGridView1.Rows[n].DefaultCellStyle.BackColor = Color.Red;
                        dataGridView1.Rows[n].DefaultCellStyle.ForeColor = Color.White;
                    }
                    */
                }
            
                j++;
            }
       
       
            //statusBar1.Panels[0].Text = "전체 " + j.ToString() + " 건이 검색 되었습니다.";
       
        }
        private void T3_dataGridView4_Clear()
        {
            // 그리드 초기화
            dataGridView4.DataSource = null;
            dataGridView4.Columns.Clear();
        }
       
        private void dataGridView4Set()
        {
            DataGridViewCheckBoxColumn checkColumn = new DataGridViewCheckBoxColumn();
            //checkColumn.Name = "X";
            //checkColumn.HeaderText = "X";
            checkColumn.Width = 30;
            //checkColumn.ReadOnly = false;
            //checkColumn.FillWeight = 10;
            //if the datagridview is resized (on form resize) the checkbox won't take up too much; value is relative to the other columns' fill values
            dataGridView4.Columns.Add(checkColumn);


            //this.Controls.Add(dataGridView4);
            dataGridView4.ColumnCount = d1.Columns.Count + 1;
            
            //dataGridView4.Columns[0].Name = "WMS NO";
            dataGridView4.Columns[1].Name = "WMS NO";
            dataGridView4.Columns[2].Name = "최초재고";
            dataGridView4.Columns[3].Name = "입고일";
            dataGridView4.Columns[4].Name = "구역";
            dataGridView4.Columns[5].Name = "상태";
            dataGridView4.Columns[6].Name = "유형";
            dataGridView4.Columns[7].Name = "총수량";
            dataGridView4.Columns[8].Name = "B/L NO";
            dataGridView4.Columns[9].Name = "코드";
            dataGridView4.Columns[10].Name = "상호";
            dataGridView4.Columns[11].Name = "담당";
            dataGridView4.Columns[12].Name = "코드";
            dataGridView4.Columns[13].Name = "제품명";
            dataGridView4.Columns[14].Name = "규격";
            dataGridView4.Columns[15].Name = "단위";
            dataGridView4.Columns[16].Name = "표시";
        
            //this.dataGridView4.Columns["암호"].Visible = false;
            //this.dataGridView4.Columns["권한"].Visible = false;
       
            // Columns AutoSize
            //dGVProcess.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells;
       
            // Head Cell_Columns Font Style = off
            dataGridView4.EnableHeadersVisualStyles = false;
            // Head Cell_Columns HeightSizeMode = off
            dataGridView4.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
       
            // Head Cell_Columns HeightSize 임의 지정
            dataGridView4.ColumnHeadersHeight = 60;
       
            // Head Rows Control
            dataGridView4.RowHeadersVisible = false;  // Head Rows Hide
            //dataGridView4.RowHeadersWidthSizeMode = DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            // RowHeadersWidth_Size 임의 지정
            dataGridView4.RowHeadersWidth = 20;

            //for (int j = 0; j < d1.Columns.Count + 1; j++)
            for (int j = 0; j < 16 ; j++)
            {
                dataGridView4.Columns[j].HeaderCell.Style.Font = new Font("맑은고딕", 10, FontStyle.Bold);
                // dGVProcess.Columns[j].HeaderCell.Style.ForeColor = Color.Red;
                //dataGridView4.Columns[j].HeaderCell.Style.BackColor = Color.Yellow;
       
                // dGVState.Columns[j].HeaderCell.Size = new Size();
       
                // Head Cell_Columns Alignment
                //dataGridView4.Columns[j].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
                //if (j < 1) dataGridView4.Columns[j].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
                //else dataGridView4.Columns[j].HeaderCell.Style.Alignment = DataGridViewContentAlignment.BottomCenter;
       
                dataGridView4.Columns[j].HeaderCell.Style.Alignment = DataGridViewContentAlignment.BottomCenter;
       
                // Cell Font Style
                //dGVProcess.Columns[j].DefaultCellStyle.Font = new Font("Tahoma", 18, FontStyle.Bold);
       
                // Cell Font Alignment
                if (j == 0)
                {
       
                }
                else if (j == 2)
                {
                    dataGridView4.Columns[j].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                }
                //else
                //{
                //    dataGridView4.Columns[j].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                //
                //}
       
                //  Cell Height 지정
                //  var dgv = new DataGridView();
                //dataGridView4.RowTemplate.Height = 30;
       
       
                // Head Cell_Columns Width Size 임의 지정
                DataGridViewColumn column1 = dataGridView4.Columns[0];
                column1.Width = 40;
                DataGridViewColumn column2 = dataGridView4.Columns[1];
                column2.Width = 100;
                DataGridViewColumn column3 = dataGridView4.Columns[2];
                column3.Width = 150;
                DataGridViewColumn column4 = dataGridView4.Columns[3];
                column4.Width = 60;
                DataGridViewColumn column5 = dataGridView4.Columns[4];
                column5.Width = 60;
                DataGridViewColumn column6 = dataGridView4.Columns[5];
                column6.Width = 60;
                DataGridViewColumn column7 = dataGridView4.Columns[6];
                column7.Width = 90;
                DataGridViewColumn column8 = dataGridView4.Columns[7];
                column8.Width = 150;
                DataGridViewColumn column9 = dataGridView4.Columns[8];
                column9.Width = 60;
                DataGridViewColumn column10 = dataGridView4.Columns[9];
                column10.Width = 180;
                DataGridViewColumn column11 = dataGridView4.Columns[10];
                column11.Width = 60;
                DataGridViewColumn column12 = dataGridView4.Columns[11];
                column12.Width = 60;
                DataGridViewColumn column13 = dataGridView4.Columns[12];
                column13.Width = 120;
                DataGridViewColumn column14 = dataGridView4.Columns[13];
                column14.Width = 60;
                DataGridViewColumn column15 = dataGridView4.Columns[14];
                column15.Width = 60;
                DataGridViewColumn column16 = dataGridView4.Columns[15];
                column16.Width = 60;
            }

            dataGridView4_r1 = dataGridView4.GetCellDisplayRectangle(1, -1, false);
            dataGridView4_DoubleHead_width[0] = dataGridView4.GetCellDisplayRectangle(2, -1, false).Width;
            dataGridView4_DoubleHead_width[1] = dataGridView4.GetCellDisplayRectangle(3, -1, false).Width;
            dataGridView4_DoubleHead_width[2] = dataGridView4.GetCellDisplayRectangle(4, -1, false).Width;
            dataGridView4_DoubleHead_width[3] = dataGridView4.GetCellDisplayRectangle(5, -1, false).Width;
            dataGridView4_DoubleHead_width[4] = dataGridView4.GetCellDisplayRectangle(6, -1, false).Width;
            dataGridView4_DoubleHead_width[5] = dataGridView4.GetCellDisplayRectangle(7, -1, false).Width;


            dataGridView4_r2 = dataGridView4.GetCellDisplayRectangle(8, -1, false);
            dataGridView4_DoubleHead_width[10] = dataGridView4.GetCellDisplayRectangle(9, -1, false).Width;
            dataGridView4_DoubleHead_width[11] = dataGridView4.GetCellDisplayRectangle(10, -1, false).Width;

            dataGridView4_r3 = dataGridView4.GetCellDisplayRectangle(11, -1, false);

            dataGridView4_DoubleHead_width[20] = dataGridView4.GetCellDisplayRectangle(12, -1, false).Width;
            dataGridView4_DoubleHead_width[21] = dataGridView4.GetCellDisplayRectangle(13, -1, false).Width;
            dataGridView4_DoubleHead_width[22] = dataGridView4.GetCellDisplayRectangle(14, -1, false).Width;
        }

        private void dataGridView4_gvSheetListCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            foreach (DataGridViewRow r in dataGridView4.Rows)
            {
                //r.Cells["X"].Value = ((CheckBox)sender).Checked;

                r.Cells[0].Value = ((CheckBox)sender).Checked;
            }
        }

        
        private void dataGridView4_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if(e.RowIndex < 0 || e.ColumnIndex != 0)
            {
                return;
            }
            
            if(dataGridView4.Rows[e.RowIndex].Cells[0].Value != null)
            {
                if (dataGridView4.Rows[e.RowIndex].Cells[0].Value.Equals(false))
                {
                    //option 1
                    (dataGridView4.CurrentRow.Cells[e.ColumnIndex] as DataGridViewCheckBoxCell).Value = true;
                }
                else
                {
                    //option 1
                    (dataGridView4.CurrentRow.Cells[e.ColumnIndex] as DataGridViewCheckBoxCell).Value = false;
                }
            }
            else
            {
                (dataGridView4.CurrentRow.Cells[e.ColumnIndex] as DataGridViewCheckBoxCell).Value = true;
            }
            
            //MessageBox.Show(dataGridView4.Rows[e.RowIndex].Cells[0].FormattedValue.ToString());
            //option 2
            //DataGridViewCheckBoxCell cbc = (dataGridView4.CurrentRow.Cells[e.ColumnIndex] as DataGridViewCheckBoxCell);
            //cbc.Value = true;
            //option 3
            //dataGridView4.CurrentCell.Value = true;
            
        }


        private void dataGridView4_CellPainting(object sender, DataGridViewCellPaintingEventArgs e)
        {
            // Head Check BOX ============================================================================
            if (e.ColumnIndex == 0 && e.RowIndex == -1)
             {
             e.PaintBackground(e.ClipBounds, false);
            
             Point pt = e.CellBounds.Location;  // where you want the bitmap in the cell
            
             int nChkBoxWidth = 15;
             int nChkBoxHeight = 15;
             int offsetx = (e.CellBounds.Width - nChkBoxWidth) / 2;
             int offsety = (e.CellBounds.Height - nChkBoxHeight) / 2;
            
             pt.X += offsetx;
             pt.Y += offsety;
            
             CheckBox cb = new CheckBox();
             cb.Size = new Size(nChkBoxWidth, nChkBoxHeight);
             cb.Location = pt;
             cb.CheckedChanged += new EventHandler(dataGridView4_gvSheetListCheckBox_CheckedChanged);
            
             ((DataGridView)sender).Controls.Add(cb);
            
             e.Handled = true;
             }

            // DoubleHead ============================================================================

            if (e.RowIndex == -1 && e.ColumnIndex > -1)

            {
                Rectangle r = e.CellBounds;

                r.Y += e.CellBounds.Height / 2;

                r.Height = e.CellBounds.Height / 2;

                e.PaintBackground(r, true);

                e.PaintContent(r);

                e.Handled = true;
            }
        }

        private void dataGridView4_ColumnWidthChanged(object sender, DataGridViewColumnEventArgs e)
        {
            DataGridView gv = (DataGridView)sender;

            Rectangle rtHeader = gv.DisplayRectangle;

            rtHeader.Height = gv.ColumnHeadersHeight / 2;

            gv.Invalidate(rtHeader);
        }

        private void dataGridView4_Paint(object sender, PaintEventArgs e)
        {
            DataGridView gv = (DataGridView)sender;

            string[] strHeaders = { "작  업", "화  주", "제  품" };

            StringFormat format = new StringFormat();

            format.Alignment = StringAlignment.Center;

            format.LineAlignment = StringAlignment.Center;


            // Category Painting

            {
                dataGridView4_r1.X += 1;

                dataGridView4_r1.Y += 2;

                dataGridView4_r1.Width = dataGridView4_r1.Width + dataGridView4_DoubleHead_width[0] +
                                         dataGridView4_DoubleHead_width[1] + dataGridView4_DoubleHead_width[2] +
                                         dataGridView4_DoubleHead_width[3] + dataGridView4_DoubleHead_width[4] +
                                         dataGridView4_DoubleHead_width[5] - 2;

                dataGridView4_r1.Height = (dataGridView4_r1.Height / 2) - 2;

                e.Graphics.DrawRectangle(new Pen(gv.BackgroundColor), dataGridView4_r1);

                e.Graphics.FillRectangle(new SolidBrush(gv.ColumnHeadersDefaultCellStyle.BackColor),
                //e.Graphics.FillRectangle(new SolidBrush(Color.Yellow),

                dataGridView4_r1);


                e.Graphics.DrawString(strHeaders[0],

                    //gv.ColumnHeadersDefaultCellStyle.Font,
                    new Font("맑은고딕", 9, FontStyle.Bold),

                    new SolidBrush(gv.ColumnHeadersDefaultCellStyle.ForeColor),

                    dataGridView4_r1,

                    format);

            }

            // Projection Painting

            {
                dataGridView4_r2.X += 1;

                dataGridView4_r2.Y += 2;

                dataGridView4_r2.Width = dataGridView4_r2.Width + dataGridView4_DoubleHead_width[10] + 
                                         dataGridView4_DoubleHead_width[11] - 2;

                dataGridView4_r2.Height = (dataGridView4_r2.Height / 2) - 2;

                e.Graphics.DrawRectangle(new Pen(gv.BackgroundColor), dataGridView4_r2);

                e.Graphics.FillRectangle(new SolidBrush(gv.ColumnHeadersDefaultCellStyle.BackColor),
                //e.Graphics.FillRectangle(new SolidBrush(Color.Yellow),

                dataGridView4_r2);


                e.Graphics.DrawString(strHeaders[1],

                    //gv.ColumnHeadersDefaultCellStyle.Font,
                    new Font("맑은고딕", 9, FontStyle.Bold),

                    new SolidBrush(gv.ColumnHeadersDefaultCellStyle.ForeColor),

                    dataGridView4_r2,

                    format);
            }

            // GROUP HEAD 매출율

            {
                dataGridView4_r3.X += 1;

                dataGridView4_r3.Y += 2;

                dataGridView4_r3.Width = dataGridView4_r3.Width + dataGridView4_DoubleHead_width[20] +
                                         dataGridView4_DoubleHead_width[21] + dataGridView4_DoubleHead_width[22] - 2;

                dataGridView4_r3.Height = (dataGridView4_r3.Height / 2) - 2;

                e.Graphics.DrawRectangle(new Pen(gv.BackgroundColor), dataGridView4_r3);

                e.Graphics.FillRectangle(new SolidBrush(gv.ColumnHeadersDefaultCellStyle.BackColor),
                //e.Graphics.FillRectangle(new SolidBrush(Color.Yellow),

                dataGridView4_r3);


                e.Graphics.DrawString(strHeaders[2],

                    //gv.ColumnHeadersDefaultCellStyle.Font,
                    new Font("맑은고딕", 9, FontStyle.Bold),

                    new SolidBrush(gv.ColumnHeadersDefaultCellStyle.ForeColor),

                    dataGridView4_r3,

                    format);
            }
            
        }

        private void dataGridView4_Scroll(object sender, ScrollEventArgs e)
        {
            DataGridView gv = (DataGridView)sender;

            Rectangle rtHeader = gv.DisplayRectangle;

            rtHeader.Height = gv.ColumnHeadersHeight / 2;

            gv.Invalidate(rtHeader);
        }

        
        // dataGridView5 Set ===========================================================================

        private void T3_dataGridView5_Main_Process()
        {
            Cursor.Current = Cursors.WaitCursor;
            d1 = CommonDB.R_datatbl(Qry.KSWMS210_009);

            T3_dataGridView5_Clear();
            dataGridView5Set();

            int j = 0;

            while (j < d1.Rows.Count)
            {
                int n = dataGridView5.Rows.Add();

                for (int i = 1; i <= d1.Columns.Count; i++)
                {
                    dataGridView5.Rows[n].Cells[i].Value = d1.Rows[j][i - 1].ToString();

                    //if (i == 0)
                    //{
                    //    dataGridView5.Rows[n].Cells[i].Value = (j + 1).ToString();
                    //}
                    //else if (i == 18)
                    //{
                    //    if (d1.Rows[j][14].ToString() != "")
                    //    {
                    //        dataGridView5.Rows[n].Cells[i].Value = "0";
                    //    }
                    //    else
                    //    {
                    //        dataGridView5.Rows[n].Cells[i].Value = d1.Rows[j][i - 1].ToString();
                    //    }
                    //}
                    //else
                    //{
                    //    dataGridView5.Rows[n].Cells[i].Value = d1.Rows[j][i - 1].ToString();
                    //}

                    /*
                    if (dataGridView5.Rows[n].Cells[i].Value.Equals("EST"))
                    {
                        dataGridView5.Rows[n].DefaultCellStyle.BackColor = Color.Red;
                        dataGridView5.Rows[n].DefaultCellStyle.ForeColor = Color.White;
                    }
                    */
                }

                j++;
            }


            //statusBar1.Panels[0].Text = "전체 " + j.ToString() + " 건이 검색 되었습니다.";
        }

        private void dataGridView5_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex < 0 || e.ColumnIndex != 0)
            {
                return;
            }

            if (dataGridView5.Rows[e.RowIndex].Cells[0].Value != null)
            {
                if (dataGridView5.Rows[e.RowIndex].Cells[0].Value.Equals(false))
                {
                    //option 1
                    (dataGridView5.CurrentRow.Cells[e.ColumnIndex] as DataGridViewCheckBoxCell).Value = true;
                }
                else
                {
                    //option 1
                    (dataGridView5.CurrentRow.Cells[e.ColumnIndex] as DataGridViewCheckBoxCell).Value = false;
                }
            }
            else
            {
                (dataGridView5.CurrentRow.Cells[e.ColumnIndex] as DataGridViewCheckBoxCell).Value = true;
            }

            //MessageBox.Show(dataGridView4.Rows[e.RowIndex].Cells[0].FormattedValue.ToString());
            //option 2
            //DataGridViewCheckBoxCell cbc = (dataGridView4.CurrentRow.Cells[e.ColumnIndex] as DataGridViewCheckBoxCell);
            //cbc.Value = true;
            //option 3
            //dataGridView4.CurrentCell.Value = true;
        }

        private void T3_dataGridView5_Clear()
        {
            // 그리드 초기화
            dataGridView1.DataSource = null;
            dataGridView1.Columns.Clear();

            // 그리드 초기화
            dataGridView5.DataSource = null;
            dataGridView5.Columns.Clear();
        }

        private void dataGridView5Set()
        {
            DataGridViewCheckBoxColumn checkColumn = new DataGridViewCheckBoxColumn();
            //checkColumn.Name = "X";
            //checkColumn.HeaderText = "X";
            checkColumn.Width = 30;
            //checkColumn.ReadOnly = false;
            //checkColumn.FillWeight = 10;
            //if the datagridview is resized (on form resize) the checkbox won't take up too much; value is relative to the other columns' fill values
            dataGridView5.Columns.Add(checkColumn);


            //this.Controls.Add(dataGridView5);

            dataGridView5.ColumnCount = d1.Columns.Count + 1;

            //dataGridView5.Columns[0].Name = "WMS NO";
            dataGridView5.Columns[1].Name = "WMS NO";
            dataGridView5.Columns[2].Name = "최초재고";
            dataGridView5.Columns[3].Name = "입고일";
            dataGridView5.Columns[4].Name = "구역";
            dataGridView5.Columns[5].Name = "상태";
            dataGridView5.Columns[6].Name = "유형";
            dataGridView5.Columns[7].Name = "총수량";
            dataGridView5.Columns[8].Name = "B/L NO";
            dataGridView5.Columns[9].Name = "코드";
            dataGridView5.Columns[10].Name = "상호";
            dataGridView5.Columns[11].Name = "담당";
            dataGridView5.Columns[12].Name = "코드";
            dataGridView5.Columns[13].Name = "제품명";
            dataGridView5.Columns[14].Name = "규격";
            dataGridView5.Columns[15].Name = "단위";
            dataGridView5.Columns[16].Name = "표시";

            //this.dataGridView5.Columns["암호"].Visible = false;
            //this.dataGridView5.Columns["권한"].Visible = false;

            // Columns AutoSize
            //dGVProcess.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells;

            // Head Cell_Columns Font Style = off
            dataGridView5.EnableHeadersVisualStyles = false;
            // Head Cell_Columns HeightSizeMode = off
            dataGridView5.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
           
            // Head Cell_Columns HeightSize 임의 지정
            dataGridView5.ColumnHeadersHeight = 60;

            // Head Rows Control
            dataGridView5.RowHeadersVisible = false;
            //dataGridView5.RowHeadersWidthSizeMode = DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            // RowHeadersWidth_Size 임의 지정
            dataGridView5.RowHeadersWidth = 20;



            for (int j = 0; j < d1.Columns.Count; j++)
            {
                dataGridView5.Columns[j].HeaderCell.Style.Font = new Font("맑은고딕", 10, FontStyle.Bold);
                // dGVProcess.Columns[j].HeaderCell.Style.ForeColor = Color.Red;
                //dataGridView5.Columns[j].HeaderCell.Style.BackColor = Color.Yellow;

                // dGVState.Columns[j].HeaderCell.Size = new Size();

                // Head Cell_Columns Alignment
                //dataGridView5.Columns[j].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
                //if (j < 1) dataGridView5.Columns[j].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
                //else dataGridView5.Columns[j].HeaderCell.Style.Alignment = DataGridViewContentAlignment.BottomCenter;

                dataGridView5.Columns[j].HeaderCell.Style.Alignment = DataGridViewContentAlignment.BottomCenter;

                // Cell Font Style
                //dGVProcess.Columns[j].DefaultCellStyle.Font = new Font("Tahoma", 18, FontStyle.Bold);

                // Cell Font Alignment
                if (j == 2)
                {
                    dataGridView5.Columns[j].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                }
                //else
                //{
                //    dataGridView5.Columns[j].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                //
                //}

                //  Cell Height 지정
                //  var dgv = new DataGridView();
                //dataGridView5.RowTemplate.Height = 30;


                // Head Cell_Columns Width Size 임의 지정
                DataGridViewColumn column1 = dataGridView5.Columns[0];
                column1.Width = 40;
                DataGridViewColumn column2 = dataGridView5.Columns[1];
                column2.Width = 100;
                DataGridViewColumn column3 = dataGridView5.Columns[2];
                column3.Width = 150;
                DataGridViewColumn column4 = dataGridView5.Columns[3];
                column4.Width = 60;
                DataGridViewColumn column5 = dataGridView5.Columns[4];
                column5.Width = 60;
                DataGridViewColumn column6 = dataGridView5.Columns[5];
                column6.Width = 60;
                DataGridViewColumn column7 = dataGridView5.Columns[6];
                column7.Width = 90;
                DataGridViewColumn column8 = dataGridView5.Columns[7];
                column8.Width = 150;
                DataGridViewColumn column9 = dataGridView5.Columns[8];
                column9.Width = 60;
                DataGridViewColumn column10 = dataGridView5.Columns[9];
                column10.Width = 180;
                DataGridViewColumn column11 = dataGridView5.Columns[10];
                column11.Width = 60;
                DataGridViewColumn column12 = dataGridView5.Columns[11];
                column12.Width = 60;
                DataGridViewColumn column13 = dataGridView5.Columns[12];
                column13.Width = 120;
                DataGridViewColumn column14 = dataGridView5.Columns[13];
                column14.Width = 60;
                DataGridViewColumn column15 = dataGridView5.Columns[14];
                column15.Width = 60;
            }

            dataGridView5_r1 = dataGridView5.GetCellDisplayRectangle(1, -1, false);
            dataGridView5_DoubleHead_width[0] = dataGridView5.GetCellDisplayRectangle(2, -1, false).Width;
            dataGridView5_DoubleHead_width[1] = dataGridView5.GetCellDisplayRectangle(3, -1, false).Width;
            dataGridView5_DoubleHead_width[2] = dataGridView5.GetCellDisplayRectangle(4, -1, false).Width;
            dataGridView5_DoubleHead_width[3] = dataGridView5.GetCellDisplayRectangle(5, -1, false).Width;
            dataGridView5_DoubleHead_width[4] = dataGridView5.GetCellDisplayRectangle(6, -1, false).Width;
            dataGridView5_DoubleHead_width[5] = dataGridView5.GetCellDisplayRectangle(7, -1, false).Width;

            dataGridView5_r2 = dataGridView5.GetCellDisplayRectangle(8, -1, false);
            dataGridView5_DoubleHead_width[10] = dataGridView5.GetCellDisplayRectangle(9, -1, false).Width;
            dataGridView5_DoubleHead_width[11] = dataGridView5.GetCellDisplayRectangle(10, -1, false).Width;

            dataGridView5_r3 = dataGridView5.GetCellDisplayRectangle(11, -1, false);
            dataGridView5_DoubleHead_width[20] = dataGridView5.GetCellDisplayRectangle(12, -1, false).Width;
            dataGridView5_DoubleHead_width[21] = dataGridView5.GetCellDisplayRectangle(13, -1, false).Width;
            dataGridView5_DoubleHead_width[22] = dataGridView5.GetCellDisplayRectangle(14, -1, false).Width;
            
        }
        private void dataGridView5_gvSheetListCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            foreach (DataGridViewRow r in dataGridView5.Rows)
            {
                //r.Cells["X"].Value = ((CheckBox)sender).Checked;

                r.Cells[0].Value = ((CheckBox)sender).Checked;
            }
        }
        private void dataGridView5_CellPainting(object sender, DataGridViewCellPaintingEventArgs e)
        {
            // Head Check BOX ============================================================================
            if (e.ColumnIndex == 0 && e.RowIndex == -1)
            {
                e.PaintBackground(e.ClipBounds, false);

                Point pt = e.CellBounds.Location;  // where you want the bitmap in the cell

                int nChkBoxWidth = 15;
                int nChkBoxHeight = 15;
                int offsetx = (e.CellBounds.Width - nChkBoxWidth) / 2;
                int offsety = (e.CellBounds.Height - nChkBoxHeight) / 2;

                pt.X += offsetx;
                pt.Y += offsety;

                CheckBox cb = new CheckBox();
                cb.Size = new Size(nChkBoxWidth, nChkBoxHeight);
                cb.Location = pt;
                cb.CheckedChanged += new EventHandler(dataGridView5_gvSheetListCheckBox_CheckedChanged);

                ((DataGridView)sender).Controls.Add(cb);

                e.Handled = true;
            }

            // DoubleHead ============================================================================
            if (e.RowIndex == -1 && e.ColumnIndex > -1)
          
            {
                Rectangle G5_r = e.CellBounds;
          
                G5_r.Y += e.CellBounds.Height / 2;
          
                G5_r.Height = e.CellBounds.Height / 2;
          
                e.PaintBackground(G5_r, true);
          
                e.PaintContent(G5_r);
          
                e.Handled = true;
            }
        }

        private void dataGridView5_ColumnWidthChanged(object sender, DataGridViewColumnEventArgs e)
        {
            DataGridView gv = (DataGridView)sender;
          
            Rectangle rtHeader = gv.DisplayRectangle;
          
            rtHeader.Height = gv.ColumnHeadersHeight / 2;
          
            gv.Invalidate(rtHeader);
        }

        private void dataGridView5_Scroll(object sender, ScrollEventArgs e)
        {
            DataGridView gv = (DataGridView)sender;
          
            Rectangle rtHeader = gv.DisplayRectangle;
          
            rtHeader.Height = gv.ColumnHeadersHeight / 2;
          
            gv.Invalidate(rtHeader);
        }

        private void dataGridView5_Paint(object sender, PaintEventArgs e)
        {
            DataGridView gv = (DataGridView)sender;
           
            string[] strHeaders = { "작  업", "화  주", "제  품" };
           
            StringFormat format = new StringFormat();
           
            format.Alignment = StringAlignment.Center;
           
            format.LineAlignment = StringAlignment.Center;
           
           
            // Category Painting
           
            {
                dataGridView5_r1.X += 1;

                dataGridView5_r1.Y += 2;

                dataGridView5_r1.Width = dataGridView5_r1.Width + dataGridView5_DoubleHead_width[0] +
                                         dataGridView5_DoubleHead_width[1] + dataGridView5_DoubleHead_width[2] +
                                         dataGridView5_DoubleHead_width[3] + dataGridView5_DoubleHead_width[4] +
                                         dataGridView5_DoubleHead_width[5] + dataGridView5_DoubleHead_width[6] - 2;

                dataGridView5_r1.Height = (dataGridView5_r1.Height / 2) - 2;
           
                e.Graphics.DrawRectangle(new Pen(gv.BackgroundColor), dataGridView5_r1);
           
                e.Graphics.FillRectangle(new SolidBrush(gv.ColumnHeadersDefaultCellStyle.BackColor),
                //e.Graphics.FillRectangle(new SolidBrush(Color.Yellow),

                dataGridView5_r1);
           
           
                e.Graphics.DrawString(strHeaders[0],
           
                    //gv.ColumnHeadersDefaultCellStyle.Font,
                    new Font("맑은고딕", 9, FontStyle.Bold),
           
                    new SolidBrush(gv.ColumnHeadersDefaultCellStyle.ForeColor),

                    dataGridView5_r1,
           
                    format);
           
            }
           
            // Projection Painting
           
            {
                dataGridView5_r2.X += 1;
           
                dataGridView5_r2.Y += 2;

                dataGridView5_r2.Width = dataGridView5_r2.Width + dataGridView5_DoubleHead_width[10] +
                                         dataGridView5_DoubleHead_width[11] - 2;
           
                dataGridView5_r2.Height = (dataGridView5_r2.Height / 2) - 2;
           
                e.Graphics.DrawRectangle(new Pen(gv.BackgroundColor), dataGridView5_r2);
           
                e.Graphics.FillRectangle(new SolidBrush(gv.ColumnHeadersDefaultCellStyle.BackColor),
                //e.Graphics.FillRectangle(new SolidBrush(Color.Yellow),
           
                dataGridView5_r2);
           
           
                e.Graphics.DrawString(strHeaders[1],
           
                    //gv.ColumnHeadersDefaultCellStyle.Font,
                    new Font("맑은고딕", 9, FontStyle.Bold),
           
                    new SolidBrush(gv.ColumnHeadersDefaultCellStyle.ForeColor),
           
                    dataGridView5_r2,
           
                    format);
            }
           
            // GROUP HEAD 매출율
           
            {
                dataGridView5_r3.X += 1;
           
                dataGridView5_r3.Y += 2;
           
                dataGridView5_r3.Width = dataGridView5_r3.Width + dataGridView5_DoubleHead_width[20] +
                                         dataGridView5_DoubleHead_width[21] + dataGridView5_DoubleHead_width[22] - 2;
           
                dataGridView5_r3.Height = (dataGridView5_r3.Height / 2) - 2;
           
                e.Graphics.DrawRectangle(new Pen(gv.BackgroundColor), dataGridView5_r3);
           
                e.Graphics.FillRectangle(new SolidBrush(gv.ColumnHeadersDefaultCellStyle.BackColor),
                //e.Graphics.FillRectangle(new SolidBrush(Color.Yellow),
           
                dataGridView5_r3);
           
           
                e.Graphics.DrawString(strHeaders[2],
           
                    //gv.ColumnHeadersDefaultCellStyle.Font,
                    new Font("맑은고딕", 9, FontStyle.Bold),
           
                    new SolidBrush(gv.ColumnHeadersDefaultCellStyle.ForeColor),
           
                    dataGridView5_r3,
           
                    format);
            }
        }
    }
}
