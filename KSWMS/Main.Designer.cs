﻿namespace KSWMS
{
    partial class Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.TreeNode treeNode1 = new System.Windows.Forms.TreeNode("110. 사용자 관리");
            System.Windows.Forms.TreeNode treeNode2 = new System.Windows.Forms.TreeNode("100. 코드관리", new System.Windows.Forms.TreeNode[] {
            treeNode1});
            System.Windows.Forms.TreeNode treeNode3 = new System.Windows.Forms.TreeNode("210. 자동입고");
            System.Windows.Forms.TreeNode treeNode4 = new System.Windows.Forms.TreeNode("220. 수동입고");
            System.Windows.Forms.TreeNode treeNode5 = new System.Windows.Forms.TreeNode("230. 바코드확인");
            System.Windows.Forms.TreeNode treeNode6 = new System.Windows.Forms.TreeNode("200. 입고관리", new System.Windows.Forms.TreeNode[] {
            treeNode3,
            treeNode4,
            treeNode5});
            System.Windows.Forms.TreeNode treeNode7 = new System.Windows.Forms.TreeNode("310. 오더출고");
            System.Windows.Forms.TreeNode treeNode8 = new System.Windows.Forms.TreeNode("320. 수동출고");
            System.Windows.Forms.TreeNode treeNode9 = new System.Windows.Forms.TreeNode("300. 출고관리", new System.Windows.Forms.TreeNode[] {
            treeNode7,
            treeNode8});
            System.Windows.Forms.TreeNode treeNode10 = new System.Windows.Forms.TreeNode("410. LOCATION 조회");
            System.Windows.Forms.TreeNode treeNode11 = new System.Windows.Forms.TreeNode("420. 재고조회");
            System.Windows.Forms.TreeNode treeNode12 = new System.Windows.Forms.TreeNode("430. 창고사용현황 조회");
            System.Windows.Forms.TreeNode treeNode13 = new System.Windows.Forms.TreeNode("440. 적재율 조회(팝업)");
            System.Windows.Forms.TreeNode treeNode14 = new System.Windows.Forms.TreeNode("450. 작업실적 조회");
            System.Windows.Forms.TreeNode treeNode15 = new System.Windows.Forms.TreeNode("490. 에러기록조회");
            System.Windows.Forms.TreeNode treeNode16 = new System.Windows.Forms.TreeNode("400. 조회관리", new System.Windows.Forms.TreeNode[] {
            treeNode10,
            treeNode11,
            treeNode12,
            treeNode13,
            treeNode14,
            treeNode15});
            System.Windows.Forms.TreeNode treeNode17 = new System.Windows.Forms.TreeNode("510. 상태파악/에러처리");
            System.Windows.Forms.TreeNode treeNode18 = new System.Windows.Forms.TreeNode("500. 상태파악/에러처리", new System.Windows.Forms.TreeNode[] {
            treeNode17});
            System.Windows.Forms.TreeNode treeNode19 = new System.Windows.Forms.TreeNode("610. LOCATION 재고수정");
            System.Windows.Forms.TreeNode treeNode20 = new System.Windows.Forms.TreeNode("620. 분포도 재고수정");
            System.Windows.Forms.TreeNode treeNode21 = new System.Windows.Forms.TreeNode("600. 재고수정", new System.Windows.Forms.TreeNode[] {
            treeNode19,
            treeNode20});
            System.Windows.Forms.TreeNode treeNode22 = new System.Windows.Forms.TreeNode("710. 설비 사용정의");
            System.Windows.Forms.TreeNode treeNode23 = new System.Windows.Forms.TreeNode("720. LOCATION 사용정의");
            System.Windows.Forms.TreeNode treeNode24 = new System.Windows.Forms.TreeNode("700. 사용정의", new System.Windows.Forms.TreeNode[] {
            treeNode22,
            treeNode23});
            System.Windows.Forms.TreeNode treeNode25 = new System.Windows.Forms.TreeNode("990. 로그인/로그아웃");
            System.Windows.Forms.TreeNode treeNode26 = new System.Windows.Forms.TreeNode("900. 기타", new System.Windows.Forms.TreeNode[] {
            treeNode25});
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Main));
            this.panel3 = new System.Windows.Forms.Panel();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.button1 = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.button3 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.statusBarPanel1 = new System.Windows.Forms.StatusBarPanel();
            this.statusBar1 = new System.Windows.Forms.StatusBar();
            this.statusBarPanel2 = new System.Windows.Forms.StatusBarPanel();
            this.statusBarPanel3 = new System.Windows.Forms.StatusBarPanel();
            this.statusBarPanel4 = new System.Windows.Forms.StatusBarPanel();
            this.statusBarPanel5 = new System.Windows.Forms.StatusBarPanel();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.lblLogo = new System.Windows.Forms.ToolStripLabel();
            this.tsl1 = new System.Windows.Forms.ToolStripSeparator();
            this.tlbMain = new System.Windows.Forms.ToolStrip();
            this.toolStripButton1 = new System.Windows.Forms.ToolStripButton();
            this.InqFunc = new System.Windows.Forms.ToolStripButton();
            this.NewFunc = new System.Windows.Forms.ToolStripButton();
            this.DelFunc = new System.Windows.Forms.ToolStripButton();
            this.SaveFunc = new System.Windows.Forms.ToolStripButton();
            this.ExcelFunc = new System.Windows.Forms.ToolStripButton();
            this.PrintFunc = new System.Windows.Forms.ToolStripButton();
            this.CloseFunc = new System.Windows.Forms.ToolStripButton();
            this.ExitFunc = new System.Windows.Forms.ToolStripButton();
            this.treeView1 = new System.Windows.Forms.TreeView();
            this.panel3.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.statusBarPanel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.statusBarPanel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.statusBarPanel3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.statusBarPanel4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.statusBarPanel5)).BeginInit();
            this.tlbMain.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.tabControl1);
            this.panel3.Location = new System.Drawing.Point(250, 50);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(980, 600);
            this.panel3.TabIndex = 99;
            // 
            // tabControl1
            // 
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Font = new System.Drawing.Font("Gulim", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.tabControl1.ItemSize = new System.Drawing.Size(108, 18);
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(980, 600);
            this.tabControl1.SizeMode = System.Windows.Forms.TabSizeMode.Fixed;
            this.tabControl1.TabIndex = 0;
            this.tabControl1.MouseClick += new System.Windows.Forms.MouseEventHandler(this.tabControl1_MouseClick);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(200, 1);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(30, 30);
            this.button1.TabIndex = 95;
            this.button1.Text = "button1";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Visible = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.button3);
            this.panel2.Controls.Add(this.button1);
            this.panel2.Controls.Add(this.button2);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Location = new System.Drawing.Point(1, 50);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(245, 30);
            this.panel2.TabIndex = 98;
            // 
            // button3
            // 
            this.button3.Image = global::KSWMS.Properties.Resources.접기_30;
            this.button3.Location = new System.Drawing.Point(150, 1);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(30, 30);
            this.button3.TabIndex = 96;
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button2
            // 
            this.button2.Image = global::KSWMS.Properties.Resources.펼치기_30;
            this.button2.Location = new System.Drawing.Point(120, 1);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(30, 30);
            this.button2.TabIndex = 1;
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // label2
            // 
            this.label2.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label2.Font = new System.Drawing.Font("Gulim", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label2.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label2.Location = new System.Drawing.Point(1, 5);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(84, 21);
            this.label2.TabIndex = 0;
            this.label2.Text = "KSWMS";
            // 
            // statusBarPanel1
            // 
            this.statusBarPanel1.Name = "statusBarPanel1";
            this.statusBarPanel1.Text = "Message";
            this.statusBarPanel1.Width = 200;
            // 
            // statusBar1
            // 
            this.statusBar1.Font = new System.Drawing.Font("Gulim", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.statusBar1.Location = new System.Drawing.Point(0, 655);
            this.statusBar1.Name = "statusBar1";
            this.statusBar1.Panels.AddRange(new System.Windows.Forms.StatusBarPanel[] {
            this.statusBarPanel1,
            this.statusBarPanel2,
            this.statusBarPanel3,
            this.statusBarPanel4,
            this.statusBarPanel5});
            this.statusBar1.ShowPanels = true;
            this.statusBar1.Size = new System.Drawing.Size(1234, 30);
            this.statusBar1.TabIndex = 95;
            this.statusBar1.Text = "statusBar1";
            // 
            // statusBarPanel2
            // 
            this.statusBarPanel2.Alignment = System.Windows.Forms.HorizontalAlignment.Center;
            this.statusBarPanel2.AutoSize = System.Windows.Forms.StatusBarPanelAutoSize.Spring;
            this.statusBarPanel2.Name = "statusBarPanel2";
            this.statusBarPanel2.Width = 860;
            // 
            // statusBarPanel3
            // 
            this.statusBarPanel3.Alignment = System.Windows.Forms.HorizontalAlignment.Center;
            this.statusBarPanel3.AutoSize = System.Windows.Forms.StatusBarPanelAutoSize.Contents;
            this.statusBarPanel3.Name = "statusBarPanel3";
            this.statusBarPanel3.Text = "ID";
            this.statusBarPanel3.Width = 29;
            // 
            // statusBarPanel4
            // 
            this.statusBarPanel4.Alignment = System.Windows.Forms.HorizontalAlignment.Center;
            this.statusBarPanel4.AutoSize = System.Windows.Forms.StatusBarPanelAutoSize.Contents;
            this.statusBarPanel4.Name = "statusBarPanel4";
            this.statusBarPanel4.Text = "Name";
            this.statusBarPanel4.Width = 52;
            // 
            // statusBarPanel5
            // 
            this.statusBarPanel5.Alignment = System.Windows.Forms.HorizontalAlignment.Center;
            this.statusBarPanel5.AutoSize = System.Windows.Forms.StatusBarPanelAutoSize.Contents;
            this.statusBarPanel5.Name = "statusBarPanel5";
            this.statusBarPanel5.Text = "DateTime";
            this.statusBarPanel5.Width = 76;
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 1000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // lblLogo
            // 
            this.lblLogo.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.lblLogo.AutoSize = false;
            this.lblLogo.BackColor = System.Drawing.Color.White;
            this.lblLogo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.lblLogo.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Bold);
            this.lblLogo.Name = "lblLogo";
            this.lblLogo.Size = new System.Drawing.Size(300, 52);
            // 
            // tsl1
            // 
            this.tsl1.AutoSize = false;
            this.tsl1.Name = "tsl1";
            this.tsl1.Size = new System.Drawing.Size(10, 50);
            // 
            // tlbMain
            // 
            this.tlbMain.AutoSize = false;
            this.tlbMain.BackColor = System.Drawing.Color.White;
            this.tlbMain.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.tlbMain.ImageScalingSize = new System.Drawing.Size(30, 30);
            this.tlbMain.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButton1,
            this.InqFunc,
            this.NewFunc,
            this.DelFunc,
            this.SaveFunc,
            this.ExcelFunc,
            this.PrintFunc,
            this.tsl1,
            this.lblLogo,
            this.CloseFunc,
            this.ExitFunc});
            this.tlbMain.Location = new System.Drawing.Point(0, 0);
            this.tlbMain.Name = "tlbMain";
            this.tlbMain.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            this.tlbMain.Size = new System.Drawing.Size(1234, 50);
            this.tlbMain.TabIndex = 94;
            this.tlbMain.Text = "Main ToolBar";
            // 
            // toolStripButton1
            // 
            this.toolStripButton1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.toolStripButton1.Font = new System.Drawing.Font("Malgun Gothic", 8F);
            this.toolStripButton1.Image = global::KSWMS.Properties.Resources.MAIN_new_34;
            this.toolStripButton1.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.toolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton1.Margin = new System.Windows.Forms.Padding(5, 1, 5, 1);
            this.toolStripButton1.Name = "toolStripButton1";
            this.toolStripButton1.Size = new System.Drawing.Size(35, 48);
            this.toolStripButton1.Tag = "InqFunc";
            this.toolStripButton1.Text = "NEW";
            this.toolStripButton1.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.toolStripButton1.TextImageRelation = System.Windows.Forms.TextImageRelation.Overlay;
            this.toolStripButton1.Click += new System.EventHandler(this.toolStripButton1_Click);
            // 
            // InqFunc
            // 
            this.InqFunc.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.InqFunc.Font = new System.Drawing.Font("Malgun Gothic", 8F);
            this.InqFunc.Image = global::KSWMS.Properties.Resources.query;
            this.InqFunc.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.InqFunc.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.InqFunc.Margin = new System.Windows.Forms.Padding(5, 1, 5, 1);
            this.InqFunc.Name = "InqFunc";
            this.InqFunc.Size = new System.Drawing.Size(34, 48);
            this.InqFunc.Tag = "InqFunc";
            this.InqFunc.Text = "조회";
            this.InqFunc.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.InqFunc.TextImageRelation = System.Windows.Forms.TextImageRelation.Overlay;
            this.InqFunc.Click += new System.EventHandler(this.InqFunc_Click);
            // 
            // NewFunc
            // 
            this.NewFunc.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.NewFunc.Font = new System.Drawing.Font("Malgun Gothic", 8F);
            this.NewFunc.Image = global::KSWMS.Properties.Resources._new;
            this.NewFunc.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.NewFunc.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.NewFunc.Margin = new System.Windows.Forms.Padding(5, 1, 5, 1);
            this.NewFunc.Name = "NewFunc";
            this.NewFunc.Size = new System.Drawing.Size(34, 48);
            this.NewFunc.Tag = "NewFunc";
            this.NewFunc.Text = "추가";
            this.NewFunc.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.NewFunc.TextImageRelation = System.Windows.Forms.TextImageRelation.Overlay;
            this.NewFunc.Click += new System.EventHandler(this.NewFunc_Click);
            // 
            // DelFunc
            // 
            this.DelFunc.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.DelFunc.Font = new System.Drawing.Font("Malgun Gothic", 8F);
            this.DelFunc.Image = global::KSWMS.Properties.Resources.row_delete;
            this.DelFunc.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.DelFunc.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.DelFunc.Margin = new System.Windows.Forms.Padding(5, 1, 5, 1);
            this.DelFunc.Name = "DelFunc";
            this.DelFunc.Size = new System.Drawing.Size(34, 48);
            this.DelFunc.Tag = "DelFunc";
            this.DelFunc.Text = "삭제";
            this.DelFunc.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.DelFunc.TextImageRelation = System.Windows.Forms.TextImageRelation.Overlay;
            // 
            // SaveFunc
            // 
            this.SaveFunc.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.SaveFunc.Font = new System.Drawing.Font("Malgun Gothic", 8F);
            this.SaveFunc.Image = global::KSWMS.Properties.Resources.save;
            this.SaveFunc.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.SaveFunc.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.SaveFunc.Margin = new System.Windows.Forms.Padding(5, 1, 5, 1);
            this.SaveFunc.Name = "SaveFunc";
            this.SaveFunc.Size = new System.Drawing.Size(34, 48);
            this.SaveFunc.Tag = "SaveFunc";
            this.SaveFunc.Text = "저장";
            this.SaveFunc.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.SaveFunc.TextImageRelation = System.Windows.Forms.TextImageRelation.Overlay;
            this.SaveFunc.Click += new System.EventHandler(this.SaveFunc_Click);
            // 
            // ExcelFunc
            // 
            this.ExcelFunc.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.ExcelFunc.Font = new System.Drawing.Font("Malgun Gothic", 8F);
            this.ExcelFunc.Image = global::KSWMS.Properties.Resources.Excel;
            this.ExcelFunc.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.ExcelFunc.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ExcelFunc.Margin = new System.Windows.Forms.Padding(5, 1, 5, 1);
            this.ExcelFunc.Name = "ExcelFunc";
            this.ExcelFunc.Size = new System.Drawing.Size(34, 48);
            this.ExcelFunc.Tag = "ExcelFunc";
            this.ExcelFunc.Text = "엑셀";
            this.ExcelFunc.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.ExcelFunc.TextImageRelation = System.Windows.Forms.TextImageRelation.Overlay;
            this.ExcelFunc.Click += new System.EventHandler(this.ExcelFunc_Click);
            // 
            // PrintFunc
            // 
            this.PrintFunc.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.PrintFunc.Font = new System.Drawing.Font("Malgun Gothic", 8F);
            this.PrintFunc.Image = global::KSWMS.Properties.Resources.print_32;
            this.PrintFunc.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.PrintFunc.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.PrintFunc.Margin = new System.Windows.Forms.Padding(5, 1, 5, 1);
            this.PrintFunc.Name = "PrintFunc";
            this.PrintFunc.Size = new System.Drawing.Size(34, 48);
            this.PrintFunc.Tag = "PrintFunc";
            this.PrintFunc.Text = "출력";
            this.PrintFunc.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.PrintFunc.TextImageRelation = System.Windows.Forms.TextImageRelation.Overlay;
            this.PrintFunc.Click += new System.EventHandler(this.PrintFunc_Click);
            // 
            // CloseFunc
            // 
            this.CloseFunc.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.CloseFunc.Font = new System.Drawing.Font("Malgun Gothic", 8F);
            this.CloseFunc.Image = global::KSWMS.Properties.Resources.Close_alt;
            this.CloseFunc.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.CloseFunc.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.CloseFunc.Margin = new System.Windows.Forms.Padding(5, 1, 5, 1);
            this.CloseFunc.Name = "CloseFunc";
            this.CloseFunc.Size = new System.Drawing.Size(34, 48);
            this.CloseFunc.Tag = "CloseFunc";
            this.CloseFunc.Text = "닫기";
            this.CloseFunc.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.CloseFunc.TextImageRelation = System.Windows.Forms.TextImageRelation.Overlay;
            this.CloseFunc.Click += new System.EventHandler(this.CloseFunc_Click);
            // 
            // ExitFunc
            // 
            this.ExitFunc.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.ExitFunc.Font = new System.Drawing.Font("Malgun Gothic", 8F);
            this.ExitFunc.Image = global::KSWMS.Properties.Resources.shutdown;
            this.ExitFunc.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.ExitFunc.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ExitFunc.Margin = new System.Windows.Forms.Padding(5, 1, 5, 1);
            this.ExitFunc.Name = "ExitFunc";
            this.ExitFunc.Size = new System.Drawing.Size(34, 48);
            this.ExitFunc.Tag = "ExitFunc";
            this.ExitFunc.Text = "종료";
            this.ExitFunc.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.ExitFunc.TextImageRelation = System.Windows.Forms.TextImageRelation.Overlay;
            this.ExitFunc.Click += new System.EventHandler(this.ExitFunc_Click);
            // 
            // treeView1
            // 
            this.treeView1.Font = new System.Drawing.Font("Gulim", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.treeView1.Location = new System.Drawing.Point(1, 80);
            this.treeView1.Name = "treeView1";
            treeNode1.Checked = true;
            treeNode1.Name = "110";
            treeNode1.NodeFont = new System.Drawing.Font("Gulim", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            treeNode1.Text = "110. 사용자 관리";
            treeNode2.Name = "100";
            treeNode2.NodeFont = new System.Drawing.Font("Gulim", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            treeNode2.Text = "100. 코드관리";
            treeNode3.Name = "210";
            treeNode3.NodeFont = new System.Drawing.Font("Gulim", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            treeNode3.Text = "210. 자동입고";
            treeNode4.Name = "220";
            treeNode4.NodeFont = new System.Drawing.Font("Gulim", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            treeNode4.Text = "220. 수동입고";
            treeNode5.Name = "230";
            treeNode5.NodeFont = new System.Drawing.Font("Gulim", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            treeNode5.Text = "230. 바코드확인";
            treeNode6.Name = "200";
            treeNode6.NodeFont = new System.Drawing.Font("Gulim", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            treeNode6.Text = "200. 입고관리";
            treeNode7.Name = "310";
            treeNode7.NodeFont = new System.Drawing.Font("Gulim", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            treeNode7.Text = "310. 오더출고";
            treeNode8.Name = "320";
            treeNode8.NodeFont = new System.Drawing.Font("Gulim", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            treeNode8.Text = "320. 수동출고";
            treeNode9.Name = "300";
            treeNode9.NodeFont = new System.Drawing.Font("Gulim", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            treeNode9.Text = "300. 출고관리";
            treeNode10.Name = "410";
            treeNode10.NodeFont = new System.Drawing.Font("Gulim", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            treeNode10.Text = "410. LOCATION 조회";
            treeNode11.Name = "420";
            treeNode11.NodeFont = new System.Drawing.Font("Gulim", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            treeNode11.Text = "420. 재고조회";
            treeNode12.Name = "430";
            treeNode12.NodeFont = new System.Drawing.Font("Gulim", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            treeNode12.Text = "430. 창고사용현황 조회";
            treeNode13.Name = "440";
            treeNode13.NodeFont = new System.Drawing.Font("Gulim", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            treeNode13.Text = "440. 적재율 조회(팝업)";
            treeNode14.Name = "450";
            treeNode14.NodeFont = new System.Drawing.Font("Gulim", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            treeNode14.Text = "450. 작업실적 조회";
            treeNode15.Name = "490";
            treeNode15.NodeFont = new System.Drawing.Font("Gulim", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            treeNode15.Text = "490. 에러기록조회";
            treeNode16.Name = "400";
            treeNode16.NodeFont = new System.Drawing.Font("Gulim", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            treeNode16.Text = "400. 조회관리";
            treeNode17.Name = "510";
            treeNode17.NodeFont = new System.Drawing.Font("Gulim", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            treeNode17.Text = "510. 상태파악/에러처리";
            treeNode18.Name = "500";
            treeNode18.NodeFont = new System.Drawing.Font("Gulim", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            treeNode18.Text = "500. 상태파악/에러처리";
            treeNode19.Name = "610";
            treeNode19.NodeFont = new System.Drawing.Font("Gulim", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            treeNode19.Text = "610. LOCATION 재고수정";
            treeNode20.Name = "620";
            treeNode20.NodeFont = new System.Drawing.Font("Gulim", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            treeNode20.Text = "620. 분포도 재고수정";
            treeNode21.Name = "600";
            treeNode21.NodeFont = new System.Drawing.Font("Gulim", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            treeNode21.Text = "600. 재고수정";
            treeNode22.Name = "710";
            treeNode22.NodeFont = new System.Drawing.Font("Gulim", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            treeNode22.Text = "710. 설비 사용정의";
            treeNode23.Name = "720";
            treeNode23.NodeFont = new System.Drawing.Font("Gulim", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            treeNode23.Text = "720. LOCATION 사용정의";
            treeNode24.Name = "700";
            treeNode24.NodeFont = new System.Drawing.Font("Gulim", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            treeNode24.Text = "700. 사용정의";
            treeNode25.Name = "990";
            treeNode25.NodeFont = new System.Drawing.Font("Gulim", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            treeNode25.Text = "990. 로그인/로그아웃";
            treeNode26.Name = "900";
            treeNode26.NodeFont = new System.Drawing.Font("Gulim", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            treeNode26.Text = "900. 기타";
            this.treeView1.Nodes.AddRange(new System.Windows.Forms.TreeNode[] {
            treeNode2,
            treeNode6,
            treeNode9,
            treeNode16,
            treeNode18,
            treeNode21,
            treeNode24,
            treeNode26});
            this.treeView1.Size = new System.Drawing.Size(245, 570);
            this.treeView1.TabIndex = 100;
            this.treeView1.NodeMouseDoubleClick += new System.Windows.Forms.TreeNodeMouseClickEventHandler(this.treeView1_NodeMouseDoubleClick);
            // 
            // Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1234, 685);
            this.Controls.Add(this.treeView1);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.statusBar1);
            this.Controls.Add(this.tlbMain);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Main";
            this.Text = "KS-WMS";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Main_FormClosed);
            this.Load += new System.EventHandler(this.Main_Load);
            this.Resize += new System.EventHandler(this.Main_Resize);
            this.panel3.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.statusBarPanel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.statusBarPanel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.statusBarPanel3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.statusBarPanel4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.statusBarPanel5)).EndInit();
            this.tlbMain.ResumeLayout(false);
            this.tlbMain.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.StatusBarPanel statusBarPanel1;
        private System.Windows.Forms.StatusBar statusBar1;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.ToolStripButton ExitFunc;
        private System.Windows.Forms.ToolStripLabel lblLogo;
        private System.Windows.Forms.ToolStripSeparator tsl1;
        private System.Windows.Forms.ToolStripButton PrintFunc;
        private System.Windows.Forms.ToolStripButton ExcelFunc;
        private System.Windows.Forms.ToolStripButton SaveFunc;
        private System.Windows.Forms.ToolStripButton DelFunc;
        private System.Windows.Forms.ToolStripButton NewFunc;
        private System.Windows.Forms.ToolStripButton InqFunc;
        private System.Windows.Forms.ToolStrip tlbMain;
        private System.Windows.Forms.ToolStripButton CloseFunc;
        private System.Windows.Forms.TreeView treeView1;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.StatusBarPanel statusBarPanel2;
        private System.Windows.Forms.StatusBarPanel statusBarPanel3;
        private System.Windows.Forms.StatusBarPanel statusBarPanel4;
        private System.Windows.Forms.StatusBarPanel statusBarPanel5;
        private System.Windows.Forms.ToolStripButton toolStripButton1;
    }
}

