﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace KSWMS
{
    public class MyTabPage : TabPage
    {
        private Form frm;
        public MyTabPage(MyFormPage frm_contenido)
        {
            if (frm_contenido.Name.ToString() == "KSWMS110")
            {
                this.frm = frm_contenido;
                this.Controls.Add(frm_contenido.pnl);
                this.Controls.Add(frm_contenido.btn1);
                this.Controls.Add(frm_contenido.btn2);
                this.Controls.Add(frm_contenido.btn3);
                this.Name = frm_contenido.Name;
                this.Text = frm_contenido.Text;
            }
            else if (frm_contenido.Name.ToString() == "KSWMS210")
            {
                this.frm = frm_contenido;
                this.Controls.Add(frm_contenido.pnl);
                this.Controls.Add(frm_contenido.btn1);
                this.Controls.Add(frm_contenido.btn2);
                this.Controls.Add(frm_contenido.btn3);
                this.Controls.Add(frm_contenido.btn4);
                this.Name = frm_contenido.Name;
                this.Text = frm_contenido.Text;
            }
            else if (frm_contenido.Name.ToString() == "KSWMS410")
            {
                this.frm = frm_contenido;
                this.Controls.Add(frm_contenido.pnl);
                this.Controls.Add(frm_contenido.btn1);
                this.Controls.Add(frm_contenido.btn2);
                this.Controls.Add(frm_contenido.btn3);
                this.Name = frm_contenido.Name;
                this.Text = frm_contenido.Text;
            }
            else if (frm_contenido.Name.ToString() == "KSWMS420")
            {
                this.frm = frm_contenido;
                this.Controls.Add(frm_contenido.pnl);
                this.Controls.Add(frm_contenido.btn1);
                this.Controls.Add(frm_contenido.btn2);
                this.Controls.Add(frm_contenido.btn3);
                this.Name = frm_contenido.Name;
                this.Text = frm_contenido.Text;
            }
            else if (frm_contenido.Name.ToString() == "KSWMS710")
            {
                this.frm = frm_contenido;
                this.Controls.Add(frm_contenido.pnl);
                this.Controls.Add(frm_contenido.btn1);
                this.Controls.Add(frm_contenido.btn2);
                this.Controls.Add(frm_contenido.btn3);
                this.Name = frm_contenido.Name;
                this.Text = frm_contenido.Text;
            }
            else
            {
                this.frm = frm_contenido;
                this.Controls.Add(frm_contenido.pnl);
                this.Controls.Add(frm_contenido.btn1);
                this.Controls.Add(frm_contenido.btn2);
                this.Name = frm_contenido.Name;
                this.Text = frm_contenido.Text;
            }
           
        }

        protected override void Dispose(bool disposing)
        {
            if(disposing)
            {
                frm.Dispose();
            }
            base.Dispose(disposing);
        }
    }

    public class MyFormPage : Form
    {
        public Panel pnl;
        public Button btn1;
        public Button btn2;
        public Button btn3;
        public Button btn4;
       
    }
}
