﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace KSWMS
{
    public partial class KSWMS420 : MyFormPage
    {
        private DataTable d1 = new DataTable();
        private Rectangle[] dataGridView1_r = new Rectangle[10];
        private int[] dataGridView1_DoubleHead_width = new int[50];
       
        public KSWMS420()
        {
            InitializeComponent();
            this.pnl = panel1;
            this.btn1 = button1;
            this.btn2 = button2;
            this.btn3 = button3;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            panel2.Size = new Size(Convert.ToInt32(panel1.Size.Width.ToString()) - 5, 25);
            //MessageBox.Show(panel1.Size.ToString());
        }
        
        private void button2_Click(object sender, EventArgs e)
        {
            dataGridView1_Main_Process();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            ToExcel.WirteExcel(dataGridView1, "재고조회");
        }
        private void button10_Click(object sender, EventArgs e)
        {
            POPUP_거래처정보 PopupForm = new POPUP_거래처정보();
            PopupForm.ShowDialog();

            textBox1.Text = PopupForm.Passvalue_거래처코드;
        }

        private void button11_Click(object sender, EventArgs e)
        {
            POPUP_제품정보 PopupForm = new POPUP_제품정보();
            PopupForm.ShowDialog();

            textBox2.Text = PopupForm.Passvalue_제품코드;
        }

        // dataGridView1 Set ===========================================================================
        private void dataGridView1_Main_Process()
        {
            Cursor.Current = Cursors.WaitCursor;

            List<string> Condition = new List<string>();
            
            Condition.Add(dateTimePicker1.Value.ToShortDateString());
            Condition.Add(dateTimePicker2.Value.ToShortDateString());
            Condition.Add(textBox1.Text);
            Condition.Add(textBox2.Text);
            Condition.Add(textBox3.Text);
            if (checkBox1.Checked == true) Condition.Add("Y");
            else Condition.Add("N");

            d1 = CommonDB.R_datatbl(Qry.KSWMS420_001(Condition));
            
            dataGridView1_Clear();
            dataGridView1Set();

            int j = 0;

            while (j < d1.Rows.Count)
            {
                int n = dataGridView1.Rows.Add();

                for (int i = 0; i < d1.Columns.Count; i++)
                {
                    dataGridView1.Rows[n].Cells[i].Value = d1.Rows[j][i].ToString();
                }
                j++;
            }
            textBox55.Text = j.ToString();
        }
        private void dataGridView1_Clear()
        {
            // 그리드 초기화
            dataGridView1.DataSource = null;
            dataGridView1.Columns.Clear();
        }

        private void dataGridView1Set()
        {
            //this.Controls.Add(dataGridView1);

            dataGridView1.ColumnCount = d1.Columns.Count;
           
            dataGridView1.Columns[0].Name = "입고일자";
            dataGridView1.Columns[1].Name = "B/L NO";
            dataGridView1.Columns[2].Name = "코드";
            dataGridView1.Columns[3].Name = "상호";
            dataGridView1.Columns[4].Name = "담당";
            dataGridView1.Columns[5].Name = "코드";
            dataGridView1.Columns[6].Name = "제품명";
            dataGridView1.Columns[7].Name = "규격";
            dataGridView1.Columns[8].Name = "수량";
            dataGridView1.Columns[9].Name = "단위";
            dataGridView1.Columns[10].Name = "구역";
            dataGridView1.Columns[11].Name = "파렛수";
            dataGridView1.Columns[12].Name = "표시";
            dataGridView1.Columns[13].Name = "비고";
            dataGridView1.Columns[14].Name = "화물관리";
            dataGridView1.Columns[15].Name = "WMS NO";
            dataGridView1.Columns[16].Name = "최초재고";
            dataGridView1.Columns[17].Name = "대표코드";
            dataGridView1.Columns[18].Name = "코드";
            dataGridView1.Columns[19].Name = "상호";
            dataGridView1.Columns[20].Name = "담당";
            dataGridView1.Columns[21].Name = "코드";
            dataGridView1.Columns[22].Name = "선박명";
            
            //this.dataGridView1.Columns["암호"].Visible = false;
            //this.dataGridView1.Columns["권한"].Visible = false;
         
            // Columns AutoSize
            //dGVProcess.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells;
         
            // Head Cell_Columns Font Style = off
            dataGridView1.EnableHeadersVisualStyles = false;
            // Head Cell_Columns HeightSizeMode = off
            dataGridView1.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
         
            // Head Cell_Columns HeightSize 임의 지정
            dataGridView1.ColumnHeadersHeight = 60;
         
            // Head Rows Control
            dataGridView1.RowHeadersVisible = false;  // Head Rows Hide
            //dataGridView1.RowHeadersWidthSizeMode = DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            // RowHeadersWidth_Size 임의 지정
            dataGridView1.RowHeadersWidth = 20;
           
            //for (int j = 0; j < d1.Columns.Count + 1; j++)
            for (int j = 0; j < d1.Columns.Count; j++)
            {
                dataGridView1.Columns[j].HeaderCell.Style.Font = new Font("맑은고딕", 10, FontStyle.Bold);
                // dGVProcess.Columns[j].HeaderCell.Style.ForeColor = Color.Red;
                //dataGridView1.Columns[j].HeaderCell.Style.BackColor = Color.Yellow;
         
                // dGVState.Columns[j].HeaderCell.Size = new Size();
         
                // Head Cell_Columns Alignment
                //dataGridView1.Columns[j].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
         
                //if (j == 1) dataGridView1.Columns[j].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
                //else dataGridView1.Columns[j].HeaderCell.Style.Alignment = DataGridViewContentAlignment.BottomCenter;
                dataGridView1.Columns[j].HeaderCell.Style.Alignment = DataGridViewContentAlignment.BottomCenter;
         
                // Cell Font Style
                //dGVProcess.Columns[j].DefaultCellStyle.Font = new Font("Tahoma", 18, FontStyle.Bold);
         
                // Cell Font Alignment
                //if (j == 0)
                //{
                //
                //}
                //else if (j == 2)
                //{
                //    dataGridView1.Columns[j].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                //}
                //else
                //{
                //    dataGridView1.Columns[j].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                //
                //}
         
                //  Cell Height 지정
                //  var dgv = new DataGridView();
                //dataGridView1.RowTemplate.Height = 30;
         
                // Head Cell_Columns Width Size 임의 지정
                DataGridViewColumn column1 = dataGridView1.Columns[0];
                column1.Width = 100;
                DataGridViewColumn column2 = dataGridView1.Columns[1];
                column2.Width = 150;
                DataGridViewColumn column3 = dataGridView1.Columns[2];
                column3.Width = 70;
                DataGridViewColumn column4 = dataGridView1.Columns[3];
                column4.Width = 100;
                DataGridViewColumn column5 = dataGridView1.Columns[4];
                column5.Width = 70;
                DataGridViewColumn column6 = dataGridView1.Columns[5];
                column6.Width = 70;
                DataGridViewColumn column7 = dataGridView1.Columns[6];
                column7.Width = 150;
                DataGridViewColumn column8 = dataGridView1.Columns[7];
                column8.Width = 70;
                DataGridViewColumn column9 = dataGridView1.Columns[8];
                column9.Width = 70;
                DataGridViewColumn column10 = dataGridView1.Columns[9];
                column10.Width = 70;
                DataGridViewColumn column11 = dataGridView1.Columns[10];
                column11.Width = 70;
                DataGridViewColumn column12 = dataGridView1.Columns[11];
                column12.Width = 80;
                DataGridViewColumn column13 = dataGridView1.Columns[12];
                column13.Width = 120;
                DataGridViewColumn column14 = dataGridView1.Columns[13];
                column14.Width = 150;
                DataGridViewColumn column15 = dataGridView1.Columns[14];
                column15.Width = 150;
                DataGridViewColumn column16 = dataGridView1.Columns[15];
                column16.Width = 100;
                DataGridViewColumn column17 = dataGridView1.Columns[16];
                column17.Width = 100;
                DataGridViewColumn column18 = dataGridView1.Columns[17];
                column18.Width = 250;
                DataGridViewColumn column19 = dataGridView1.Columns[18];
                column19.Width = 70;
                DataGridViewColumn column20 = dataGridView1.Columns[19];
                column20.Width = 100;
                DataGridViewColumn column21 = dataGridView1.Columns[20];
                column21.Width = 70;
                DataGridViewColumn column22 = dataGridView1.Columns[21];
                column22.Width = 70;
                DataGridViewColumn column23 = dataGridView1.Columns[22];
                column23.Width = 100;
             
            }

            dataGridView1_r[0] = dataGridView1.GetCellDisplayRectangle(0, -1, false);
            dataGridView1_DoubleHead_width[0] = dataGridView1.GetCellDisplayRectangle(1, -1, false).Width;
           
            dataGridView1_r[1] = dataGridView1.GetCellDisplayRectangle(2, -1, false);
            dataGridView1_DoubleHead_width[1] = dataGridView1.GetCellDisplayRectangle(3, -1, false).Width;
            dataGridView1_DoubleHead_width[2] = dataGridView1.GetCellDisplayRectangle(4, -1, false).Width;
           
            dataGridView1_r[2] = dataGridView1.GetCellDisplayRectangle(5, -1, false);
            dataGridView1_DoubleHead_width[3] = dataGridView1.GetCellDisplayRectangle(6, -1, false).Width;
            dataGridView1_DoubleHead_width[4] = dataGridView1.GetCellDisplayRectangle(7, -1, false).Width;
            dataGridView1_DoubleHead_width[5] = dataGridView1.GetCellDisplayRectangle(8, -1, false).Width;
            dataGridView1_DoubleHead_width[6] = dataGridView1.GetCellDisplayRectangle(9, -1, false).Width;

            dataGridView1_r[3] = dataGridView1.GetCellDisplayRectangle(10, -1, false);
            dataGridView1_DoubleHead_width[7] = dataGridView1.GetCellDisplayRectangle(11, -1, false).Width;
            dataGridView1_DoubleHead_width[8] = dataGridView1.GetCellDisplayRectangle(12, -1, false).Width;
            dataGridView1_DoubleHead_width[9] = dataGridView1.GetCellDisplayRectangle(13, -1, false).Width;
            dataGridView1_DoubleHead_width[10] = dataGridView1.GetCellDisplayRectangle(14, -1, false).Width;
            dataGridView1_DoubleHead_width[11] = dataGridView1.GetCellDisplayRectangle(15, -1, false).Width;
            dataGridView1_DoubleHead_width[12] = dataGridView1.GetCellDisplayRectangle(16, -1, false).Width;
            dataGridView1_DoubleHead_width[13] = dataGridView1.GetCellDisplayRectangle(17, -1, false).Width;

            dataGridView1_r[4] = dataGridView1.GetCellDisplayRectangle(18, -1, false);
            dataGridView1_DoubleHead_width[14] = dataGridView1.GetCellDisplayRectangle(19, -1, false).Width;
            dataGridView1_DoubleHead_width[15] = dataGridView1.GetCellDisplayRectangle(20, -1, false).Width;

            dataGridView1_r[5] = dataGridView1.GetCellDisplayRectangle(21, -1, false);
            dataGridView1_DoubleHead_width[16] = dataGridView1.GetCellDisplayRectangle(22, -1, false).Width;

        }
        
        private void dataGridView1_CellPainting(object sender, DataGridViewCellPaintingEventArgs e)
        {
            // DoubleHead ============================================================================

            if (e.RowIndex == -1 && e.ColumnIndex > -1)

            {
                Rectangle r = e.CellBounds;

                r.Y += e.CellBounds.Height / 2;

                r.Height = e.CellBounds.Height / 2;

                e.PaintBackground(r, true);

                e.PaintContent(r);

                e.Handled = true;
            }
        }

        private void dataGridView1_ColumnWidthChanged(object sender, DataGridViewColumnEventArgs e)
        {
            DataGridView gv = (DataGridView)sender;

            Rectangle rtHeader = gv.DisplayRectangle;

            rtHeader.Height = gv.ColumnHeadersHeight / 2;

            gv.Invalidate(rtHeader);
        }

        
        private void dataGridView1_Paint(object sender, PaintEventArgs e)
        {
            DataGridView gv = (DataGridView)sender;

            string[] strHeaders = { "재고정보", "화  주", "제  품", "재고정보",  "관  리", "선  박" };

            StringFormat format = new StringFormat();

            format.Alignment = StringAlignment.Center;

            format.LineAlignment = StringAlignment.Center;


            // Category Painting

            {
                dataGridView1_r[0].X += 1;

                dataGridView1_r[0].Y += 2;

                dataGridView1_r[0].Width = dataGridView1_r[0].Width + dataGridView1_DoubleHead_width[0] - 2;

                dataGridView1_r[0].Height = (dataGridView1_r[0].Height / 2) - 2;

                e.Graphics.DrawRectangle(new Pen(gv.BackgroundColor), dataGridView1_r[0]);

                e.Graphics.FillRectangle(new SolidBrush(gv.ColumnHeadersDefaultCellStyle.BackColor),
                //e.Graphics.FillRectangle(new SolidBrush(Color.Yellow),

                dataGridView1_r[0]);


                e.Graphics.DrawString(strHeaders[0],

                    //gv.ColumnHeadersDefaultCellStyle.Font,
                    new Font("맑은고딕", 9, FontStyle.Bold),

                    new SolidBrush(gv.ColumnHeadersDefaultCellStyle.ForeColor),

                    dataGridView1_r[0],

                    format);

            }

            // Projection Painting
            {
                dataGridView1_r[1].X += 1;

                dataGridView1_r[1].Y += 2;

                dataGridView1_r[1].Width = dataGridView1_r[1].Width + dataGridView1_DoubleHead_width[1] + 
                                          dataGridView1_DoubleHead_width[2] - 2;

                dataGridView1_r[1].Height = (dataGridView1_r[1].Height / 2) - 2;

                e.Graphics.DrawRectangle(new Pen(gv.BackgroundColor), dataGridView1_r[1]);

                e.Graphics.FillRectangle(new SolidBrush(gv.ColumnHeadersDefaultCellStyle.BackColor),
                //e.Graphics.FillRectangle(new SolidBrush(Color.Yellow),

                dataGridView1_r[1]);


                e.Graphics.DrawString(strHeaders[1],

                    //gv.ColumnHeadersDefaultCellStyle.Font,
                    new Font("맑은고딕", 9, FontStyle.Bold),

                    new SolidBrush(gv.ColumnHeadersDefaultCellStyle.ForeColor),

                    dataGridView1_r[1],

                    format);
            }

            // GROUP HEAD 매출율

            {
                dataGridView1_r[2].X += 1;

                dataGridView1_r[2].Y += 2;

                dataGridView1_r[2].Width = dataGridView1_r[2].Width + dataGridView1_DoubleHead_width[3] + 
                                         dataGridView1_DoubleHead_width[4] + dataGridView1_DoubleHead_width[5] +
                                         dataGridView1_DoubleHead_width[6]  - 2;

                dataGridView1_r[2].Height = (dataGridView1_r[2].Height / 2) - 2;

                e.Graphics.DrawRectangle(new Pen(gv.BackgroundColor), dataGridView1_r[2]);

                e.Graphics.FillRectangle(new SolidBrush(gv.ColumnHeadersDefaultCellStyle.BackColor),
                //e.Graphics.FillRectangle(new SolidBrush(Color.Yellow),

                dataGridView1_r[2]);


                e.Graphics.DrawString(strHeaders[2],

                    //gv.ColumnHeadersDefaultCellStyle.Font,
                    new Font("맑은고딕", 9, FontStyle.Bold),

                    new SolidBrush(gv.ColumnHeadersDefaultCellStyle.ForeColor),

                    dataGridView1_r[2],

                    format);
            }

            {
                dataGridView1_r[3].X += 1;

                dataGridView1_r[3].Y += 2;

                dataGridView1_r[3].Width = dataGridView1_r[3].Width +
                                         dataGridView1_DoubleHead_width[7] + 
                                         dataGridView1_DoubleHead_width[8] +
                                         dataGridView1_DoubleHead_width[9] + 
                                         dataGridView1_DoubleHead_width[10] +
                                         dataGridView1_DoubleHead_width[11] +
                                         dataGridView1_DoubleHead_width[12] +
                                         dataGridView1_DoubleHead_width[13]
                                         - 2;

                dataGridView1_r[3].Height = (dataGridView1_r[3].Height / 2) - 2;

                e.Graphics.DrawRectangle(new Pen(gv.BackgroundColor), dataGridView1_r[3]);

                e.Graphics.FillRectangle(new SolidBrush(gv.ColumnHeadersDefaultCellStyle.BackColor),
                //e.Graphics.FillRectangle(new SolidBrush(Color.Yellow),

                dataGridView1_r[3]);


                e.Graphics.DrawString(strHeaders[3],

                    //gv.ColumnHeadersDefaultCellStyle.Font,
                    new Font("맑은고딕", 9, FontStyle.Bold),

                    new SolidBrush(gv.ColumnHeadersDefaultCellStyle.ForeColor),

                    dataGridView1_r[3],

                    format);
            }

            {
                dataGridView1_r[4].X += 1;

                dataGridView1_r[4].Y += 2;

                dataGridView1_r[4].Width = dataGridView1_r[4].Width + 
                                          dataGridView1_DoubleHead_width[14] +
                                          dataGridView1_DoubleHead_width[15] - 2;

                dataGridView1_r[4].Height = (dataGridView1_r[4].Height / 2) - 2;

                e.Graphics.DrawRectangle(new Pen(gv.BackgroundColor), dataGridView1_r[4]);

                e.Graphics.FillRectangle(new SolidBrush(gv.ColumnHeadersDefaultCellStyle.BackColor),
                //e.Graphics.FillRectangle(new SolidBrush(Color.Yellow),

                dataGridView1_r[4]);


                e.Graphics.DrawString(strHeaders[4],

                    //gv.ColumnHeadersDefaultCellStyle.Font,
                    new Font("맑은고딕", 9, FontStyle.Bold),

                    new SolidBrush(gv.ColumnHeadersDefaultCellStyle.ForeColor),

                    dataGridView1_r[4],

                    format);
            }

            {
                dataGridView1_r[5].X += 1;

                dataGridView1_r[5].Y += 2;

                dataGridView1_r[5].Width = dataGridView1_r[5].Width + 
                                         dataGridView1_DoubleHead_width[16] - 2;

                dataGridView1_r[5].Height = (dataGridView1_r[5].Height / 2) - 2;

                e.Graphics.DrawRectangle(new Pen(gv.BackgroundColor), dataGridView1_r[5]);

                e.Graphics.FillRectangle(new SolidBrush(gv.ColumnHeadersDefaultCellStyle.BackColor),
                //e.Graphics.FillRectangle(new SolidBrush(Color.Yellow),

                dataGridView1_r[5]);


                e.Graphics.DrawString(strHeaders[5],

                    //gv.ColumnHeadersDefaultCellStyle.Font,
                    new Font("맑은고딕", 9, FontStyle.Bold),

                    new SolidBrush(gv.ColumnHeadersDefaultCellStyle.ForeColor),

                    dataGridView1_r[5],

                    format);
            }
            
        }

        private void dataGridView1_Scroll(object sender, ScrollEventArgs e)
        {
            dataGridView1Set();

            //DataGridView gv = (DataGridView)sender;

            Rectangle rtHeader = dataGridView1.DisplayRectangle;

            rtHeader.Height = dataGridView1.ColumnHeadersHeight / 2;

            dataGridView1.Invalidate(rtHeader);

        }

   
        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox1.Checked == true)
            {
                dateTimePicker1.Enabled = true;
                dateTimePicker2.Enabled = true;
            }
            else
            {
                dateTimePicker1.Enabled = false;
                dateTimePicker2.Enabled = false;
            }
        }

       
    }
}
