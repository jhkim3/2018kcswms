﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace KSWMS
{
    public partial class KSWMS320 : MyFormPage
    {
        private DataTable d1 = new DataTable();
        private DataTable d2 = new DataTable();
        private Rectangle[] dataGridView1_r = new Rectangle[10];
        private int[] dataGridView1_DoubleHead_width = new int[50];
        private Rectangle[] dataGridView2_r = new Rectangle[10];
        private int[] dataGridView2_DoubleHead_width = new int[50];

        public KSWMS320()
        {
            InitializeComponent();
            this.pnl = panel1;
            this.btn1 = button1;
            this.btn2 = button2;
        }
        
        private void button1_Click(object sender, EventArgs e)
        {
            dataGridView1.Size = new Size(Convert.ToInt32(panel1.Size.Width.ToString()) - 5, Convert.ToInt32(panel1.Size.Height.ToString()) / 2 - 115);
            panel3.Location = new Point(0, (Convert.ToInt32(panel1.Size.Height.ToString()) / 2) + 37);
            dataGridView2.Location = new Point(0, (Convert.ToInt32(panel1.Size.Height.ToString()) / 2) + 85);
            dataGridView2.Size = new Size(Convert.ToInt32(panel1.Size.Width.ToString()) - 5, Convert.ToInt32(panel1.Size.Height.ToString()) / 2 - 85);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Btn_Load_Process();

            if (checkBox3.Checked == false && checkBox4.Checked == false && textBox1.Text == "" && textBox2.Text == "" && textBox3.Text == "" && textBox4.Text == "")
            {
                if(dataGridView1.RowCount > 0 ) dataGridView1_Clear();
               
            }
            else 
            {
                dataGridView1_Main_Process();
                dataGridView2_Main_Process();
            }
        }
        
        private void button10_Click(object sender, EventArgs e)
        {
            POPUP_거래처정보 PopupForm = new POPUP_거래처정보();
            PopupForm.ShowDialog();

            textBox1.Text = PopupForm.Passvalue_거래처코드;
        }

        private void button11_Click(object sender, EventArgs e)
        {
            POPUP_제품정보 PopupForm = new POPUP_제품정보();
            PopupForm.ShowDialog();

            textBox2.Text = PopupForm.Passvalue_제품코드;
        }

        private void Btn_Load_Process()
        {
            d1 = CommonDB.R_datatbl(Qry.KSWMS320_001);
            int j = 0;

            while (j < d1.Rows.Count)
            {
                if (d1.Rows[j][0].ToString() == "310")
                {
                    useBtn_Chk(d1, io310, j);
                    stBtn_Chk(d1, st310, j);
                }
                else if (d1.Rows[j][0].ToString() == "320")
                {
                    useBtn_Chk(d1, io320, j);
                    stBtn_Chk(d1, st320, j);
                }
                else if (d1.Rows[j][0].ToString() == "330")
                {
                    useBtn_Chk(d1, io330, j);
                    stBtn_Chk(d1, st330, j);
                }
                else if (d1.Rows[j][0].ToString() == "340")
                {
                    useBtn_Chk(d1, io340, j);
                    stBtn_Chk(d1, st340, j);
                }

                j++;
            }
        }

        private void useBtn_Chk(DataTable dt, Button btn, int j)
        {
            if (dt.Rows[j][1].ToString() == "1")
            {
                btn.Text = "사용";
                btn.BackColor = Color.Blue;
            }
            else
            {
                btn.Text = "금지";
                btn.BackColor = Color.Red;
            }
        }

        private void stBtn_Chk(DataTable dt, Button btn, int j)
        {
            if (dt.Rows[j][2].ToString() == "I")
            {
                btn.Text = "입고";
                btn.BackColor = Color.Red;
            }
            else
            {
                btn.Text = "출고";
                btn.BackColor = Color.Blue;
            }
        }

        public static String Location_Set(NumericUpDown n1, NumericUpDown n2, NumericUpDown n3, NumericUpDown n4)
        {
            string result = n1.Value.ToString() + n2.Value.ToString();

            if (n3.Value < 10) result += "0" + n3.Value.ToString();
            else result += n3.Value.ToString();

            if (n4.Value < 10) result += "0" + n4.Value.ToString();
            else result += n4.Value.ToString();
            
            return result;
        }

        private void button12_Click(object sender, EventArgs e)
        {
            Btn_Select_Chk(button12, st310.Text, io310.Text);
        }

        private void button13_Click(object sender, EventArgs e)
        {
            Btn_Select_Chk(button13, st320.Text, io320.Text);
        }

        private void button14_Click(object sender, EventArgs e)
        {
            Btn_Select_Chk(button14, st330.Text, io330.Text);
        }

        private void button15_Click(object sender, EventArgs e)
        {
            Btn_Select_Chk(button15, st340.Text, io340.Text);
        }

        private void Btn_Select_Chk(Button btn, string io, string use)
        {
            if (io == "입고")
            {
                MessageBox.Show("선택한 출고대가 입고 모드 입니다.", "확인!", MessageBoxButtons.OK, (MessageBoxIcon)64);
                return;
            }
            else if(use == "금지")
            {
                MessageBox.Show("선택한 출고대가 사용 금지 입니다.", "확인!", MessageBoxButtons.OK, (MessageBoxIcon)64);
                return;
            }
            else
            {
                button12.BackColor = Control.DefaultBackColor;
                button13.BackColor = Control.DefaultBackColor;
                button14.BackColor = Control.DefaultBackColor;
                button15.BackColor = Control.DefaultBackColor;
                 
                btn.BackColor = Color.DodgerBlue;
            }
        }

        
        private void checkBox3_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox3.Checked == true)
            {
                dateTimePicker1.Enabled = true;
                dateTimePicker2.Enabled = true;
            }
            else
            {
                dateTimePicker1.Enabled = false;
                dateTimePicker2.Enabled = false;
            }
        }

        private void checkBox4_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox4.Checked == true)
            {
                numericUpDown1.Enabled = true;
                numericUpDown2.Enabled = true;
                numericUpDown3.Enabled = true;
                numericUpDown4.Enabled = true;
            }
            else
            {
                numericUpDown1.Enabled = false;
                numericUpDown2.Enabled = false;
                numericUpDown3.Enabled = false;
                numericUpDown4.Enabled = false;
            }
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            foreach (DataGridViewRow r in dataGridView1.Rows)
            {
                //r.Cells["X"].Value = ((CheckBox)sender).Checked;

                r.Cells[0].Value = ((CheckBox)sender).Checked;
            }

            if (checkBox1.Checked == true) textBox5.Text = textBox9.Text;
            else textBox5.Text = "0";

        }
        private void checkBox5_CheckedChanged(object sender, EventArgs e)
        {
            int val = 0;
            string keyinVal = textBox8.Text;
            if( int.TryParse(keyinVal, out val) )
            {
                int cnt = 0; 
                foreach (DataGridViewRow r in dataGridView1.Rows)
                {
                    if (cnt == val) break;
                    r.Cells[0].Value = ((CheckBox)sender).Checked;
                    cnt++;                    
                }
                if (checkBox5.Checked == true) textBox5.Text = cnt.ToString();
                else textBox5.Text = "0";
            }
            else
            {
                MessageBox.Show("선택 갯수 입력이 잘못되었습니다.", "오류!", MessageBoxButtons.OK, (MessageBoxIcon)16);
            }
        }


        private void button16_Click(object sender, EventArgs e)
        {
            if (dataGridView1.RowCount <= 0)
            {
                MessageBox.Show("출고할 자료가 없습니다.", "오류!", MessageBoxButtons.OK, (MessageBoxIcon)16);
                return;
            }
            if (button12.BackColor != Color.DodgerBlue && button13.BackColor != Color.DodgerBlue && button14.BackColor != Color.DodgerBlue && button15.BackColor != Color.DodgerBlue)
            {
                MessageBox.Show("출고대를 선택하세요!", "오류!", MessageBoxButtons.OK, (MessageBoxIcon)16);
                return;
            }
            if (textBox5.Text == "0")
            {
                MessageBox.Show("출고 작업을 체크 하세요!", "오류!", MessageBoxButtons.OK, (MessageBoxIcon)16);
                return;
            }

            DialogResult dr = MessageBox.Show("출고를 하시겠습니까?",
                      "출고여부", MessageBoxButtons.YesNo, (MessageBoxIcon)32);

            switch (dr)
            {
                case DialogResult.Yes:

                    List<string> Condition = new List<string>();

                    foreach (DataGridViewRow r in dataGridView1.Rows)
                    {
                        if (r.Cells[0].Value == null) continue;
                        else if (Convert.ToBoolean(r.Cells[0].Value) == false) continue;
                        else
                        {
                            Condition.Add(r.Cells[4].Value.ToString()); // 바코드

                            // TAB920 일반 출고 REAL_SEQ 최대값 가져오기(REAL_SEQ 300000000번 미만 긴급출고, 300000000번 이상 일반출고)

                            if (checkBox2.Checked == true)
                            {
                                d1 = CommonDB.R_datatbl(Qry.KSWMS320_010);
                                if (d1.Rows[0][0].ToString() == "") Condition.Add("1");
                                else Condition.Add((Convert.ToInt32(d1.Rows[0][0].ToString()) + 1).ToString());
                            }
                            else
                            {
                                d1 = CommonDB.R_datatbl(Qry.KSWMS320_011);
                                //MessageBox.Show(d1.Rows.Count.ToString());
                                if (d1.Rows[0][0].ToString() == "") Condition.Add("300000001");
                                else  Condition.Add((Convert.ToInt32(d1.Rows[0][0].ToString()) + 1).ToString());
                            }
                            //=======================================================================================

                            // 출고대
                            if (button12.BackColor == Color.DodgerBlue) Condition.Add("310");
                            else if (button13.BackColor == Color.DodgerBlue) Condition.Add("320");
                            else if (button14.BackColor == Color.DodgerBlue) Condition.Add("330");
                            else if (button15.BackColor == Color.DodgerBlue) Condition.Add("340");
                            
                            // TAB920 INSERT
                            CommonDB.R_datatbl(Qry.KSWMS320_003(Condition));
                            // TAB210 LOC 'G' 완료 UPDATE
                            CommonDB.R_datatbl(Qry.KSWMS320_006(Condition));

                            //리스트 초기화
                            Condition.Clear();
                        }
                        
                    }

                    break;

                case DialogResult.No:
                    break;
            }
            
            button2.PerformClick();
        }

        private void button17_Click(object sender, EventArgs e)
        {
            if (dataGridView2.RowCount <= 0)
            {
                MessageBox.Show("출고 취소할 자료가 없습니다.", "오류!", MessageBoxButtons.OK, (MessageBoxIcon)16);
                return;
            }

            DialogResult dr = MessageBox.Show("출고 취소를 하시겠습니까?",
                     "출고취소", MessageBoxButtons.YesNo, (MessageBoxIcon)32);

            switch (dr)
            {
                case DialogResult.Yes:

                    List<string> Condition = new List<string>();

                    foreach (DataGridViewRow r in dataGridView2.Rows)
                    {
                        if (r.Cells[0].Value == null) continue;
                        else if (Convert.ToBoolean(r.Cells[0].Value) == false) continue;
                        else
                        {
                            Condition.Add(r.Cells[6].Value.ToString()); // 바코드

                            d1 = CommonDB.R_datatbl(Qry.KSWMS320_05(Condition));
                            if (d1.Rows[0][0].ToString() == "1")
                            {
                                // TAB920 DELETE
                                CommonDB.R_datatbl(Qry.KSWMS320_005(Condition));
                                // TAB210 LOC 'G' 빼기 UPDATE
                                CommonDB.R_datatbl(Qry.KSWMS320_007(Condition));
                            }
                            else
                            {
                                MessageBox.Show("Barcode : "  + Condition[0] + " 출고 작업은 이미 RCP에서 진행 되었습니다." );
                            }

                            //리스트 초기화
                            Condition.Clear();
                        }
                    }
                    
                    break;

                case DialogResult.No:
                    break;
            }

            button2.PerformClick();
        }

        // dataGridView1 Set ===========================================================================

        private void dataGridView1_Main_Process()
        {
            Cursor.Current = Cursors.WaitCursor;

            List<string> Condition = new List<string>();

            if(checkBox3.Checked == true) Condition.Add("Y");
            else Condition.Add("N");
            if (checkBox4.Checked == true) Condition.Add("Y");
            else Condition.Add("N");
            Condition.Add(dateTimePicker1.Value.ToShortDateString());
            Condition.Add(dateTimePicker2.Value.ToShortDateString());
            Condition.Add(Location_Set(numericUpDown1, numericUpDown2, numericUpDown3, numericUpDown4));
            Condition.Add(textBox1.Text);
            Condition.Add(textBox2.Text);
            Condition.Add(textBox3.Text);
            Condition.Add(textBox4.Text);
            d1 = CommonDB.R_datatbl(Qry.KSWMS320_002(Condition));
            
            dataGridView1_Clear();
            dataGridView1Set();
            
            int j = 0;

            while (j < d1.Rows.Count)
            {
                int n = dataGridView1.Rows.Add();

                for (int i = 1; i <= d1.Columns.Count; i++)
                {
                    dataGridView1.Rows[j].Cells[i].Value = d1.Rows[j][i - 1].ToString();
                }

                j++;
            }
            
            textBox9.Text =  j.ToString();

        }
        private void dataGridView1_Clear()
        {
            // 그리드 초기화
            dataGridView1.DataSource = null;
            dataGridView1.Columns.Clear();

            //체크 박스 초기화
            checkBox1.Checked = false;
            checkBox2.Checked = false;
        }

        private void dataGridView1Set()
        {
            DataGridViewCheckBoxColumn checkColumn = new DataGridViewCheckBoxColumn();
            //checkColumn.Name = "X";
            //checkColumn.HeaderText = "X";
            checkColumn.Width = 30;
            //checkColumn.ReadOnly = false;
            //checkColumn.FillWeight = 10;
            //if the datagridview is resized (on form resize) the checkbox won't take up too much; value is relative to the other columns' fill values
            dataGridView1.Columns.Add(checkColumn);

            //this.Controls.Add(dataGridView1);
            dataGridView1.ColumnCount = d1.Columns.Count + 1;
            
            //dataGridView1.Columns[0].Name = "WMS NO";
            dataGridView1.Columns[1].Name = "LOCATION";
            dataGridView1.Columns[2].Name = "입고일자";
            dataGridView1.Columns[3].Name = "B/L NO";
            dataGridView1.Columns[4].Name = "바코드";
            dataGridView1.Columns[5].Name = "코드";
            dataGridView1.Columns[6].Name = "상호";
            dataGridView1.Columns[7].Name = "담당";
            dataGridView1.Columns[8].Name = "코드";
            dataGridView1.Columns[9].Name = "제품명";
            dataGridView1.Columns[10].Name = "규격";
            dataGridView1.Columns[11].Name = "수량";
            dataGridView1.Columns[12].Name = "단위";
            dataGridView1.Columns[13].Name = "표시";
            dataGridView1.Columns[14].Name = "비고";
            dataGridView1.Columns[15].Name = "통관일자";
            dataGridView1.Columns[16].Name = "화물관리번호";
            dataGridView1.Columns[17].Name = "구역";
            dataGridView1.Columns[18].Name = "WMS NO";
            dataGridView1.Columns[19].Name = "순번";
            dataGridView1.Columns[20].Name = "최초재고";
            dataGridView1.Columns[21].Name = "대표코드";
            dataGridView1.Columns[22].Name = "입고유형";
            dataGridView1.Columns[23].Name = "코드";
            dataGridView1.Columns[24].Name = "상호";
            dataGridView1.Columns[25].Name = "담당";
            dataGridView1.Columns[26].Name = "코드";
            dataGridView1.Columns[27].Name = "선박명";

            dataGridView1.Columns[0].Frozen = true;
            dataGridView1.Columns[1].Frozen = true;
            
            //this.dataGridView1.Columns["암호"].Visible = false;
            //this.dataGridView1.Columns["권한"].Visible = false;

            // Columns AutoSize
            //dGVProcess.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells;

            // Head Cell_Columns Font Style = off
            dataGridView1.EnableHeadersVisualStyles = false;
            // Head Cell_Columns HeightSizeMode = off
            dataGridView1.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.DisableResizing;

            // Head Cell_Columns HeightSize 임의 지정
            dataGridView1.ColumnHeadersHeight = 60;

            // Head Rows Control
            dataGridView1.RowHeadersVisible = false;  // Head Rows Hide
            //dataGridView1.RowHeadersWidthSizeMode = DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            // RowHeadersWidth_Size 임의 지정
            dataGridView1.RowHeadersWidth = 20;

            //for (int j = 0; j < d1.Columns.Count + 1; j++)
            for (int j = 0; j < d1.Columns.Count + 1; j++)
            {
                dataGridView1.Columns[j].HeaderCell.Style.Font = new Font("맑은고딕", 10, FontStyle.Bold);
                // dGVProcess.Columns[j].HeaderCell.Style.ForeColor = Color.Red;
                //dataGridView1.Columns[j].HeaderCell.Style.BackColor = Color.Yellow;

                // dGVState.Columns[j].HeaderCell.Size = new Size();

                // Head Cell_Columns Alignment
                //dataGridView1.Columns[j].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;

                if (j == 1) dataGridView1.Columns[j].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
                else dataGridView1.Columns[j].HeaderCell.Style.Alignment = DataGridViewContentAlignment.BottomCenter;
                //dataGridView1.Columns[j].HeaderCell.Style.Alignment = DataGridViewContentAlignment.BottomCenter;

                // Cell Font Style
                //dGVProcess.Columns[j].DefaultCellStyle.Font = new Font("Tahoma", 18, FontStyle.Bold);

                // Cell Font Alignment
                //if (j == 0)
                //{
                //
                //}
                //else if (j == 2)
                //{
                //    dataGridView1.Columns[j].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                //}
                //else
                //{
                //    dataGridView1.Columns[j].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                //
                //}

                //  Cell Height 지정
                //  var dgv = new DataGridView();
                //dataGridView1.RowTemplate.Height = 30;


                // Head Cell_Columns Width Size 임의 지정
                DataGridViewColumn column1 = dataGridView1.Columns[0];
                column1.Width = 40;
                DataGridViewColumn column2 = dataGridView1.Columns[1];
                column2.Width = 100;
                DataGridViewColumn column3 = dataGridView1.Columns[2];
                column3.Width = 100;
                DataGridViewColumn column4 = dataGridView1.Columns[3];
                column4.Width = 150;
                DataGridViewColumn column5 = dataGridView1.Columns[4];
                column5.Width = 100;
                DataGridViewColumn column6 = dataGridView1.Columns[5];
                column6.Width = 60;
                DataGridViewColumn column7 = dataGridView1.Columns[6];
                column7.Width = 90;
                DataGridViewColumn column8 = dataGridView1.Columns[7];
                column8.Width = 60;
                DataGridViewColumn column9 = dataGridView1.Columns[8];
                column9.Width = 60;
                DataGridViewColumn column10 = dataGridView1.Columns[9];
                column10.Width = 130;
                DataGridViewColumn column11 = dataGridView1.Columns[10];
                column11.Width = 60;
                DataGridViewColumn column12 = dataGridView1.Columns[11];
                column12.Width = 60;
                DataGridViewColumn column13 = dataGridView1.Columns[12];
                column13.Width = 60;
                DataGridViewColumn column14 = dataGridView1.Columns[13];
                column14.Width = 100;
                DataGridViewColumn column15 = dataGridView1.Columns[14];
                column15.Width = 120;
                DataGridViewColumn column16 = dataGridView1.Columns[15];
                column16.Width = 120;
                DataGridViewColumn column17 = dataGridView1.Columns[16];
                column17.Width = 150;
                DataGridViewColumn column18 = dataGridView1.Columns[17];
                column18.Width = 80;
                DataGridViewColumn column19 = dataGridView1.Columns[18];
                column19.Width = 100;
                DataGridViewColumn column20 = dataGridView1.Columns[19];
                column20.Width = 100;
                DataGridViewColumn column21 = dataGridView1.Columns[20];
                column21.Width = 100;
                DataGridViewColumn column22 = dataGridView1.Columns[21];
                column22.Width = 280;
                DataGridViewColumn column23 = dataGridView1.Columns[22];
                column23.Width = 100;
                DataGridViewColumn column24 = dataGridView1.Columns[23];
                column24.Width = 100;

            }

            dataGridView1_r[0] = dataGridView1.GetCellDisplayRectangle(2, -1, false);
            dataGridView1_DoubleHead_width[0] = dataGridView1.GetCellDisplayRectangle(3, -1, false).Width;
            dataGridView1_DoubleHead_width[1] = dataGridView1.GetCellDisplayRectangle(4, -1, false).Width;
            
            dataGridView1_r[1] = dataGridView1.GetCellDisplayRectangle(5, -1, false);
            dataGridView1_DoubleHead_width[2] = dataGridView1.GetCellDisplayRectangle(6, -1, false).Width;
            dataGridView1_DoubleHead_width[3] = dataGridView1.GetCellDisplayRectangle(7, -1, false).Width;
           
            dataGridView1_r[2] = dataGridView1.GetCellDisplayRectangle(8, -1, false);
            dataGridView1_DoubleHead_width[4] = dataGridView1.GetCellDisplayRectangle(9, -1, false).Width;
            dataGridView1_DoubleHead_width[5] = dataGridView1.GetCellDisplayRectangle(10, -1, false).Width;
            dataGridView1_DoubleHead_width[6] = dataGridView1.GetCellDisplayRectangle(11, -1, false).Width;
            dataGridView1_DoubleHead_width[7] = dataGridView1.GetCellDisplayRectangle(12, -1, false).Width;

            dataGridView1_r[3] = dataGridView1.GetCellDisplayRectangle(13, -1, false);
            dataGridView1_DoubleHead_width[8] = dataGridView1.GetCellDisplayRectangle(14, -1, false).Width;
            dataGridView1_DoubleHead_width[9] = dataGridView1.GetCellDisplayRectangle(15, -1, false).Width;
            dataGridView1_DoubleHead_width[10] = dataGridView1.GetCellDisplayRectangle(16, -1, false).Width;
            dataGridView1_DoubleHead_width[11] = dataGridView1.GetCellDisplayRectangle(17, -1, false).Width;

            dataGridView1_r[4] = dataGridView1.GetCellDisplayRectangle(18, -1, false);
            dataGridView1_DoubleHead_width[12] = dataGridView1.GetCellDisplayRectangle(19, -1, false).Width;
            dataGridView1_DoubleHead_width[13] = dataGridView1.GetCellDisplayRectangle(20, -1, false).Width;
            dataGridView1_DoubleHead_width[14] = dataGridView1.GetCellDisplayRectangle(21, -1, false).Width;
            dataGridView1_DoubleHead_width[15] = dataGridView1.GetCellDisplayRectangle(22, -1, false).Width;

            dataGridView1_r[5] = dataGridView1.GetCellDisplayRectangle(23, -1, false);
            dataGridView1_DoubleHead_width[16] = dataGridView1.GetCellDisplayRectangle(24, -1, false).Width;
            dataGridView1_DoubleHead_width[17] = dataGridView1.GetCellDisplayRectangle(25, -1, false).Width;

            dataGridView1_r[6] = dataGridView1.GetCellDisplayRectangle(26, -1, false);
            dataGridView1_DoubleHead_width[18] = dataGridView1.GetCellDisplayRectangle(27, -1, false).Width;
        }


        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex < 0 || e.ColumnIndex != 0)
            {
                return;
            }

            int i;

            if (dataGridView1.Rows[e.RowIndex].Cells[0].Value != null)
            {
                if (dataGridView1.Rows[e.RowIndex].Cells[0].Value.Equals(false))
                {
                    //option 1
                    (dataGridView1.CurrentRow.Cells[e.ColumnIndex] as DataGridViewCheckBoxCell).Value = true;

                    //체크 박스 카운트 +
                    i = Convert.ToInt32(textBox5.Text);
                    i += 1;
                    textBox5.Text = i.ToString();
                    i = 0;
                }
                else
                {
                    //option 1
                    (dataGridView1.CurrentRow.Cells[e.ColumnIndex] as DataGridViewCheckBoxCell).Value = false;

                    //체크 박스 카운트 -
                    if (Convert.ToInt32(textBox5.Text) > 0)
                    {
                        i = Convert.ToInt32(textBox5.Text);
                        i -= 1;
                        textBox5.Text = i.ToString();
                        i = 0;
                    }
                }
            }
            else
            {
                (dataGridView1.CurrentRow.Cells[e.ColumnIndex] as DataGridViewCheckBoxCell).Value = true;

                //체크 박스 카운트 +
                i = Convert.ToInt32(textBox5.Text);
                i += 1;
                textBox5.Text = i.ToString();
                i = 0;
            }

            //MessageBox.Show(dataGridView1.Rows[e.RowIndex].Cells[0].FormattedValue.ToString());
            //option 2
            //DataGridViewCheckBoxCell cbc = (dataGridView1.CurrentRow.Cells[e.ColumnIndex] as DataGridViewCheckBoxCell);
            //cbc.Value = true;
            //option 3
            //dataGridView1.CurrentCell.Value = true;

        }


        private void dataGridView1_CellPainting(object sender, DataGridViewCellPaintingEventArgs e)
        {
            // Head Check BOX ============================================================================
            //if (e.ColumnIndex == 0 && e.RowIndex == -1)
            // {
            // e.PaintBackground(e.ClipBounds, false);
            //
            // Point pt = e.CellBounds.Location;  // where you want the bitmap in the cell
            //
            // int nChkBoxWidth = 15;
            // int nChkBoxHeight = 15;
            // int offsetx = (e.CellBounds.Width - nChkBoxWidth) / 2;
            // int offsety = (e.CellBounds.Height - nChkBoxHeight) / 2;
            //
            // pt.X += offsetx;
            // pt.Y += offsety;
            //
            // CheckBox cb = new CheckBox();
            // cb.Size = new Size(nChkBoxWidth, nChkBoxHeight);
            // cb.Location = pt;
            // cb.CheckedChanged += new EventHandler(dataGridView1_gvSheetListCheckBox_CheckedChanged);
            //
            // ((DataGridView)sender).Controls.Add(cb);
            //
            // e.Handled = true;
            // }

            // DoubleHead ============================================================================

            if (e.RowIndex == -1 && e.ColumnIndex > -1)

            {
                Rectangle r = e.CellBounds;

                r.Y += e.CellBounds.Height / 2;

                r.Height = e.CellBounds.Height / 2;

                e.PaintBackground(r, true);

                e.PaintContent(r);

                e.Handled = true;
            }
        }

        private void dataGridView1_ColumnWidthChanged(object sender, DataGridViewColumnEventArgs e)
        {
            DataGridView gv = (DataGridView)sender;

            Rectangle rtHeader = gv.DisplayRectangle;

            rtHeader.Height = gv.ColumnHeadersHeight / 2;

            gv.Invalidate(rtHeader);
        }


        private void dataGridView1_Paint(object sender, PaintEventArgs e)
        {
            DataGridView gv = (DataGridView)sender;

            string[] strHeaders = { "작업정보", "화  주", "제  품", "작업정보", "오더정보", "관  리", "선  박"};

            StringFormat format = new StringFormat();

            format.Alignment = StringAlignment.Center;

            format.LineAlignment = StringAlignment.Center;


            // Category Painting

            {
                dataGridView1_r[0].X += 1;

                dataGridView1_r[0].Y += 2;

                dataGridView1_r[0].Width = dataGridView1_r[0].Width + dataGridView1_DoubleHead_width[0] +
                                         dataGridView1_DoubleHead_width[1] - 2;

                dataGridView1_r[0].Height = (dataGridView1_r[0].Height / 2) - 2;

                e.Graphics.DrawRectangle(new Pen(gv.BackgroundColor), dataGridView1_r[0]);

                e.Graphics.FillRectangle(new SolidBrush(gv.ColumnHeadersDefaultCellStyle.BackColor),
                //e.Graphics.FillRectangle(new SolidBrush(Color.Yellow),

                dataGridView1_r[0]);


                e.Graphics.DrawString(strHeaders[0],

                    //gv.ColumnHeadersDefaultCellStyle.Font,
                    new Font("맑은고딕", 9, FontStyle.Bold),

                    new SolidBrush(gv.ColumnHeadersDefaultCellStyle.ForeColor),

                    dataGridView1_r[0],

                    format);

            }

            // Projection Painting

            {
                dataGridView1_r[1].X += 1;

                dataGridView1_r[1].Y += 2;

                dataGridView1_r[1].Width = dataGridView1_r[1].Width + dataGridView1_DoubleHead_width[2] +
                                         dataGridView1_DoubleHead_width[3] - 2;

                dataGridView1_r[1].Height = (dataGridView1_r[1].Height / 2) - 2;

                e.Graphics.DrawRectangle(new Pen(gv.BackgroundColor), dataGridView1_r[1]);

                e.Graphics.FillRectangle(new SolidBrush(gv.ColumnHeadersDefaultCellStyle.BackColor),
                //e.Graphics.FillRectangle(new SolidBrush(Color.Yellow),

                dataGridView1_r[1]);


                e.Graphics.DrawString(strHeaders[1],

                    //gv.ColumnHeadersDefaultCellStyle.Font,
                    new Font("맑은고딕", 9, FontStyle.Bold),

                    new SolidBrush(gv.ColumnHeadersDefaultCellStyle.ForeColor),

                    dataGridView1_r[1],

                    format);
            }

            // GROUP HEAD 매출율

            {
                dataGridView1_r[2].X += 1;

                dataGridView1_r[2].Y += 2;

                dataGridView1_r[2].Width = dataGridView1_r[2].Width + dataGridView1_DoubleHead_width[4] +
                                         dataGridView1_DoubleHead_width[5] + dataGridView1_DoubleHead_width[6] +
                                         dataGridView1_DoubleHead_width[7] - 2;

                dataGridView1_r[2].Height = (dataGridView1_r[2].Height / 2) - 2;

                e.Graphics.DrawRectangle(new Pen(gv.BackgroundColor), dataGridView1_r[2]);

                e.Graphics.FillRectangle(new SolidBrush(gv.ColumnHeadersDefaultCellStyle.BackColor),
                //e.Graphics.FillRectangle(new SolidBrush(Color.Yellow),

                dataGridView1_r[2]);


                e.Graphics.DrawString(strHeaders[2],

                    //gv.ColumnHeadersDefaultCellStyle.Font,
                    new Font("맑은고딕", 9, FontStyle.Bold),

                    new SolidBrush(gv.ColumnHeadersDefaultCellStyle.ForeColor),

                    dataGridView1_r[2],

                    format);
            }

            {
                dataGridView1_r[3].X += 1;

                dataGridView1_r[3].Y += 2;

                dataGridView1_r[3].Width = dataGridView1_r[3].Width + dataGridView1_DoubleHead_width[8] +
                                         dataGridView1_DoubleHead_width[9] + dataGridView1_DoubleHead_width[10] +
                                          dataGridView1_DoubleHead_width[11] - 2;

                dataGridView1_r[3].Height = (dataGridView1_r[3].Height / 2) - 2;

                e.Graphics.DrawRectangle(new Pen(gv.BackgroundColor), dataGridView1_r[3]);

                e.Graphics.FillRectangle(new SolidBrush(gv.ColumnHeadersDefaultCellStyle.BackColor),
                //e.Graphics.FillRectangle(new SolidBrush(Color.Yellow),

                dataGridView1_r[3]);


                e.Graphics.DrawString(strHeaders[3],

                    //gv.ColumnHeadersDefaultCellStyle.Font,
                    new Font("맑은고딕", 9, FontStyle.Bold),

                    new SolidBrush(gv.ColumnHeadersDefaultCellStyle.ForeColor),

                    dataGridView1_r[3],

                    format);
            }

            {
                dataGridView1_r[4].X += 1;

                dataGridView1_r[4].Y += 2;

                dataGridView1_r[4].Width = dataGridView1_r[4].Width + dataGridView1_DoubleHead_width[12] +
                                         dataGridView1_DoubleHead_width[13] + dataGridView1_DoubleHead_width[14] +
                                          dataGridView1_DoubleHead_width[15] - 2;

                dataGridView1_r[4].Height = (dataGridView1_r[4].Height / 2) - 2;

                e.Graphics.DrawRectangle(new Pen(gv.BackgroundColor), dataGridView1_r[4]);

                e.Graphics.FillRectangle(new SolidBrush(gv.ColumnHeadersDefaultCellStyle.BackColor),
                //e.Graphics.FillRectangle(new SolidBrush(Color.Yellow),

                dataGridView1_r[4]);


                e.Graphics.DrawString(strHeaders[4],

                    //gv.ColumnHeadersDefaultCellStyle.Font,
                    new Font("맑은고딕", 9, FontStyle.Bold),

                    new SolidBrush(gv.ColumnHeadersDefaultCellStyle.ForeColor),

                    dataGridView1_r[4],

                    format);
            }

            {
                dataGridView1_r[5].X += 1;

                dataGridView1_r[5].Y += 2;

                dataGridView1_r[5].Width = dataGridView1_r[5].Width + dataGridView1_DoubleHead_width[16] +
                                         dataGridView1_DoubleHead_width[17] - 2;

                dataGridView1_r[5].Height = (dataGridView1_r[5].Height / 2) - 2;

                e.Graphics.DrawRectangle(new Pen(gv.BackgroundColor), dataGridView1_r[5]);

                e.Graphics.FillRectangle(new SolidBrush(gv.ColumnHeadersDefaultCellStyle.BackColor),
                //e.Graphics.FillRectangle(new SolidBrush(Color.Yellow),

                dataGridView1_r[5]);


                e.Graphics.DrawString(strHeaders[5],

                    //gv.ColumnHeadersDefaultCellStyle.Font,
                    new Font("맑은고딕", 9, FontStyle.Bold),

                    new SolidBrush(gv.ColumnHeadersDefaultCellStyle.ForeColor),

                    dataGridView1_r[5],

                    format);
            }

            {
                dataGridView1_r[6].X += 1;

                dataGridView1_r[6].Y += 2;

                dataGridView1_r[6].Width = dataGridView1_r[6].Width + dataGridView1_DoubleHead_width[18] - 2;

                dataGridView1_r[6].Height = (dataGridView1_r[6].Height / 2) - 2;

                e.Graphics.DrawRectangle(new Pen(gv.BackgroundColor), dataGridView1_r[6]);

                e.Graphics.FillRectangle(new SolidBrush(gv.ColumnHeadersDefaultCellStyle.BackColor),
                //e.Graphics.FillRectangle(new SolidBrush(Color.Yellow),

                dataGridView1_r[6]);


                e.Graphics.DrawString(strHeaders[6],

                    //gv.ColumnHeadersDefaultCellStyle.Font,
                    new Font("맑은고딕", 9, FontStyle.Bold),

                    new SolidBrush(gv.ColumnHeadersDefaultCellStyle.ForeColor),

                    dataGridView1_r[6],

                    format);
            }
        }

        private void dataGridView1_Scroll(object sender, ScrollEventArgs e)
        {
            dataGridView1Set();

            //DataGridView gv = (DataGridView)sender;

            Rectangle rtHeader = dataGridView1.DisplayRectangle;

            rtHeader.Height = dataGridView1.ColumnHeadersHeight / 2;

            dataGridView1.Invalidate(rtHeader);

        }


        // dataGridView2 Set ===========================================================================

        private void dataGridView2_Main_Process()
        {
            Cursor.Current = Cursors.WaitCursor;

            //List<string> Condition = new List<string>();
            //if (checkBox3.Checked == true) Condition.Add("Y");
            //else Condition.Add("N");
            //if (checkBox4.Checked == true) Condition.Add("Y");
            //else Condition.Add("N");
            //Condition.Add(dateTimePicker1.Value.ToShortDateString());
            //Condition.Add(dateTimePicker2.Value.ToShortDateString());
            //Condition.Add(Location_Set(numericUpDown1, numericUpDown2, numericUpDown3, numericUpDown4));
            //Condition.Add(textBox1.Text);
            //Condition.Add(textBox2.Text);
            //Condition.Add(textBox3.Text);
            //Condition.Add(textBox4.Text);
            //d2 = CommonDB.R_datatbl(Qry.KSWMS320_004(Condition));

            d2 = CommonDB.R_datatbl(Qry.KSWMS320_004);

            dataGridView2_Clear();
            dataGridView2Set();

            int j = 0;

            while (j < d2.Rows.Count)
            {
                int n = dataGridView2.Rows.Add();

                for (int i = 1; i <= d2.Columns.Count; i++)
                {
                    dataGridView2.Rows[j].Cells[i].Value = d2.Rows[j][i - 1].ToString();

                    //if (i == 0)
                    //{
                    //    dataGridView2.Rows[n].Cells[i].Value = (j + 1).ToString();
                    //}
                    //else if (i == 18)
                    //{
                    //    if (d2.Rows[j][14].ToString() != "")
                    //    {
                    //        dataGridView2.Rows[n].Cells[i].Value = "0";
                    //    }
                    //    else
                    //    {
                    //        dataGridView2.Rows[n].Cells[i].Value = d2.Rows[j][i - 1].ToString();
                    //    }
                    //}
                    //else
                    //{
                    //    dataGridView2.Rows[n].Cells[i].Value = d2.Rows[j][i - 1].ToString();
                    //}

                    /*
                    if (dataGridView2.Rows[n].Cells[i].Value.Equals("EST"))
                    {
                        dataGridView2.Rows[n].DefaultCellStyle.BackColor = Color.Red;
                        dataGridView2.Rows[n].DefaultCellStyle.ForeColor = Color.White;
                    }
                    */
                }

                j++;
            }

            label12.Text = j.ToString();

        }
        private void dataGridView2_Clear()
        {
            // 그리드 초기화
            dataGridView2.DataSource = null;
            dataGridView2.Columns.Clear();
            
        }

        private void dataGridView2Set()
        {
            DataGridViewCheckBoxColumn checkColumn = new DataGridViewCheckBoxColumn();
            //checkColumn.Name = "X";
            //checkColumn.HeaderText = "X";
            checkColumn.Width = 30;
            //checkColumn.ReadOnly = false;
            //checkColumn.FillWeight = 10;
            //if the datagridview is resized (on form resize) the checkbox won't take up too much; value is relative to the other columns' fill values
            dataGridView2.Columns.Add(checkColumn);

            //this.Controls.Add(dataGridView2);
            dataGridView2.ColumnCount = d2.Columns.Count + 1;

            //dataGridView2.Columns[0].Name = "WMS NO";
            dataGridView2.Columns[1].Name = "LOCATION";
            dataGridView2.Columns[2].Name = "독";
            dataGridView2.Columns[3].Name = "차량";
            dataGridView2.Columns[4].Name = "입고일자";
            dataGridView2.Columns[5].Name = "B/L NO";
            dataGridView2.Columns[6].Name = "바코드";
            dataGridView2.Columns[7].Name = "코드";
            dataGridView2.Columns[8].Name = "상호";
            dataGridView2.Columns[9].Name = "담당";
            dataGridView2.Columns[10].Name = "코드";
            dataGridView2.Columns[11].Name = "제품명";
            dataGridView2.Columns[12].Name = "규격";
            dataGridView2.Columns[13].Name = "수량";
            dataGridView2.Columns[14].Name = "단위";
            dataGridView2.Columns[15].Name = "표시";
            dataGridView2.Columns[16].Name = "비고";
            dataGridView2.Columns[17].Name = "화물관리번호";
            dataGridView2.Columns[18].Name = "구역";
            dataGridView2.Columns[19].Name = "통관일자";
            dataGridView2.Columns[20].Name = "WMS NO";
            dataGridView2.Columns[21].Name = "순번";
            dataGridView2.Columns[22].Name = "최초재고";
            dataGridView2.Columns[23].Name = "대표코드";
            dataGridView2.Columns[24].Name = "입고유형";
            dataGridView2.Columns[25].Name = "코드";
            dataGridView2.Columns[26].Name = "상호";
            dataGridView2.Columns[27].Name = "담당";
            dataGridView2.Columns[28].Name = "코드";
            dataGridView2.Columns[29].Name = "선박명";

            dataGridView2.Columns[0].Frozen = true;
            dataGridView2.Columns[1].Frozen = true;

            //this.dataGridView2.Columns["암호"].Visible = false;
            //this.dataGridView2.Columns["권한"].Visible = false;

            // Columns AutoSize
            //dGVProcess.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells;

            // Head Cell_Columns Font Style = off
            dataGridView2.EnableHeadersVisualStyles = false;
            // Head Cell_Columns HeightSizeMode = off
            dataGridView2.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.DisableResizing;

            // Head Cell_Columns HeightSize 임의 지정
            dataGridView2.ColumnHeadersHeight = 60;

            // Head Rows Control
            dataGridView2.RowHeadersVisible = false;  // Head Rows Hide
            //dataGridView2.RowHeadersWidthSizeMode = DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            // RowHeadersWidth_Size 임의 지정
            dataGridView2.RowHeadersWidth = 20;

            //for (int j = 0; j < d2.Columns.Count + 1; j++)
            for (int j = 0; j < d2.Columns.Count + 1; j++)
            {
                dataGridView2.Columns[j].HeaderCell.Style.Font = new Font("맑은고딕", 10, FontStyle.Bold);
                // dGVProcess.Columns[j].HeaderCell.Style.ForeColor = Color.Red;
                //dataGridView2.Columns[j].HeaderCell.Style.BackColor = Color.Yellow;

                // dGVState.Columns[j].HeaderCell.Size = new Size();

                // Head Cell_Columns Alignment
                //dataGridView2.Columns[j].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;

                if (j == 1) dataGridView2.Columns[j].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
                else dataGridView2.Columns[j].HeaderCell.Style.Alignment = DataGridViewContentAlignment.BottomCenter;
                //dataGridView2.Columns[j].HeaderCell.Style.Alignment = DataGridViewContentAlignment.BottomCenter;

                // Cell Font Style
                //dGVProcess.Columns[j].DefaultCellStyle.Font = new Font("Tahoma", 18, FontStyle.Bold);

                // Cell Font Alignment
                //if (j == 0)
                //{
                //
                //}
                //else if (j == 2)
                //{
                //    dataGridView2.Columns[j].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                //}
                //else
                //{
                //    dataGridView2.Columns[j].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                //
                //}

                //  Cell Height 지정
                //  var dgv = new DataGridView();
                //dataGridView2.RowTemplate.Height = 30;


                // Head Cell_Columns Width Size 임의 지정
                DataGridViewColumn column1 = dataGridView2.Columns[0];
                column1.Width = 40;
                DataGridViewColumn column2 = dataGridView2.Columns[1];
                column2.Width = 100;
                DataGridViewColumn column3 = dataGridView2.Columns[2];
                column3.Width = 70;
                DataGridViewColumn column4 = dataGridView2.Columns[3];
                column4.Width = 70;
                DataGridViewColumn column5 = dataGridView2.Columns[4];
                column5.Width = 100;
                DataGridViewColumn column6 = dataGridView2.Columns[5];
                column6.Width = 100;
                DataGridViewColumn column7 = dataGridView2.Columns[6];
                column7.Width = 150;
                DataGridViewColumn column8 = dataGridView2.Columns[7];
                column8.Width = 60;
                DataGridViewColumn column9 = dataGridView2.Columns[8];
                column9.Width = 100;
                DataGridViewColumn column10 = dataGridView2.Columns[9];
                column10.Width = 60;
                DataGridViewColumn column11 = dataGridView2.Columns[10];
                column11.Width = 60;
                DataGridViewColumn column12 = dataGridView2.Columns[11];
                column12.Width = 100;
                DataGridViewColumn column13 = dataGridView2.Columns[12];
                column13.Width = 60;
                DataGridViewColumn column14 = dataGridView2.Columns[13];
                column14.Width = 60;
                DataGridViewColumn column15 = dataGridView2.Columns[14];
                column15.Width = 60;
                DataGridViewColumn column16 = dataGridView2.Columns[15];
                column16.Width = 100;
                DataGridViewColumn column17 = dataGridView2.Columns[16];
                column17.Width = 120;
                DataGridViewColumn column18 = dataGridView2.Columns[17];
                column18.Width = 150;
                DataGridViewColumn column19 = dataGridView2.Columns[18];
                column19.Width = 70;
                DataGridViewColumn column20 = dataGridView2.Columns[19];
                column20.Width = 100;
                DataGridViewColumn column21 = dataGridView2.Columns[20];
                column21.Width = 100;
                DataGridViewColumn column22 = dataGridView2.Columns[21];
                column22.Width = 70;
                DataGridViewColumn column23 = dataGridView2.Columns[22];
                column23.Width = 90;
                DataGridViewColumn column24 = dataGridView2.Columns[23];
                column24.Width = 280;
                DataGridViewColumn column25 = dataGridView2.Columns[24];
                column25.Width = 90;
                DataGridViewColumn column26 = dataGridView2.Columns[25];
                column26.Width = 70;
                DataGridViewColumn column27 = dataGridView2.Columns[26];
                column27.Width = 90;
                DataGridViewColumn column28 = dataGridView2.Columns[27];
                column28.Width = 70;
                DataGridViewColumn column29 = dataGridView2.Columns[28];
                column29.Width = 70;
                DataGridViewColumn column30 = dataGridView2.Columns[29];
                column30.Width = 90;

            }

            dataGridView2_r[0] = dataGridView2.GetCellDisplayRectangle(2, -1, false);
            dataGridView2_DoubleHead_width[0] = dataGridView2.GetCellDisplayRectangle(3, -1, false).Width;
            
            dataGridView2_r[1] = dataGridView2.GetCellDisplayRectangle(4, -1, false);
            dataGridView2_DoubleHead_width[1] = dataGridView2.GetCellDisplayRectangle(5, -1, false).Width;
            dataGridView2_DoubleHead_width[2] = dataGridView2.GetCellDisplayRectangle(6, -1, false).Width;
            
            dataGridView2_r[2] = dataGridView2.GetCellDisplayRectangle(7, -1, false);
            dataGridView2_DoubleHead_width[3] = dataGridView2.GetCellDisplayRectangle(8, -1, false).Width;
            dataGridView2_DoubleHead_width[4] = dataGridView2.GetCellDisplayRectangle(9, -1, false).Width;
          
            dataGridView2_r[3] = dataGridView2.GetCellDisplayRectangle(10, -1, false);
            dataGridView2_DoubleHead_width[5] = dataGridView2.GetCellDisplayRectangle(11, -1, false).Width;
            dataGridView2_DoubleHead_width[6] = dataGridView2.GetCellDisplayRectangle(12, -1, false).Width;
            dataGridView2_DoubleHead_width[7] = dataGridView2.GetCellDisplayRectangle(13, -1, false).Width;
            dataGridView2_DoubleHead_width[8] = dataGridView2.GetCellDisplayRectangle(14, -1, false).Width;
           
            dataGridView2_r[4] = dataGridView2.GetCellDisplayRectangle(15, -1, false);
            dataGridView2_DoubleHead_width[9] = dataGridView2.GetCellDisplayRectangle(16, -1, false).Width;
            dataGridView2_DoubleHead_width[10] = dataGridView2.GetCellDisplayRectangle(17, -1, false).Width;
            dataGridView2_DoubleHead_width[11] = dataGridView2.GetCellDisplayRectangle(18, -1, false).Width;
            dataGridView2_DoubleHead_width[12] = dataGridView2.GetCellDisplayRectangle(19, -1, false).Width;
           
            dataGridView2_r[5] = dataGridView2.GetCellDisplayRectangle(20, -1, false);
            dataGridView2_DoubleHead_width[13] = dataGridView2.GetCellDisplayRectangle(21, -1, false).Width;
            dataGridView2_DoubleHead_width[14] = dataGridView2.GetCellDisplayRectangle(22, -1, false).Width;
            dataGridView2_DoubleHead_width[15] = dataGridView2.GetCellDisplayRectangle(23, -1, false).Width;
            dataGridView2_DoubleHead_width[16] = dataGridView2.GetCellDisplayRectangle(24, -1, false).Width;
           

            dataGridView2_r[6] = dataGridView2.GetCellDisplayRectangle(25, -1, false);
            dataGridView2_DoubleHead_width[17] = dataGridView2.GetCellDisplayRectangle(26, -1, false).Width;
            dataGridView2_DoubleHead_width[18] = dataGridView2.GetCellDisplayRectangle(27, -1, false).Width;

            dataGridView2_r[7] = dataGridView2.GetCellDisplayRectangle(28, -1, false);
            dataGridView2_DoubleHead_width[19] = dataGridView2.GetCellDisplayRectangle(29, -1, false).Width;
        }


        private void dataGridView2_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex < 0 || e.ColumnIndex != 0) return;

            if (dataGridView2.Rows[e.RowIndex].Cells[0].Value != null)
            {
                if (dataGridView2.Rows[e.RowIndex].Cells[0].Value.Equals(false)) (dataGridView2.CurrentRow.Cells[e.ColumnIndex] as DataGridViewCheckBoxCell).Value = true; //option 1
                else (dataGridView2.CurrentRow.Cells[e.ColumnIndex] as DataGridViewCheckBoxCell).Value = false; //option 1
            }
            else (dataGridView2.CurrentRow.Cells[e.ColumnIndex] as DataGridViewCheckBoxCell).Value = true;

            //MessageBox.Show(dataGridView1.Rows[e.RowIndex].Cells[0].FormattedValue.ToString());
            //option 2
            //DataGridViewCheckBoxCell cbc = (dataGridView1.CurrentRow.Cells[e.ColumnIndex] as DataGridViewCheckBoxCell);
            //cbc.Value = true;
            //option 3
            //dataGridView1.CurrentCell.Value = true;
        }


        private void dataGridView2_CellPainting(object sender, DataGridViewCellPaintingEventArgs e)
        {
            // Head Check BOX ============================================================================
            //if (e.ColumnIndex == 0 && e.RowIndex == -1)
            // {
            // e.PaintBackground(e.ClipBounds, false);
            //
            // Point pt = e.CellBounds.Location;  // where you want the bitmap in the cell
            //
            // int nChkBoxWidth = 15;
            // int nChkBoxHeight = 15;
            // int offsetx = (e.CellBounds.Width - nChkBoxWidth) / 2;
            // int offsety = (e.CellBounds.Height - nChkBoxHeight) / 2;
            //
            // pt.X += offsetx;
            // pt.Y += offsety;
            //
            // CheckBox cb = new CheckBox();
            // cb.Size = new Size(nChkBoxWidth, nChkBoxHeight);
            // cb.Location = pt;
            // cb.CheckedChanged += new EventHandler(dataGridView2_gvSheetListCheckBox_CheckedChanged);
            //
            // ((DataGridView)sender).Controls.Add(cb);
            //
            // e.Handled = true;
            // }

            // DoubleHead ============================================================================

            if (e.RowIndex == -1 && e.ColumnIndex > -1)

            {
                Rectangle r = e.CellBounds;

                r.Y += e.CellBounds.Height / 2;

                r.Height = e.CellBounds.Height / 2;

                e.PaintBackground(r, true);

                e.PaintContent(r);

                e.Handled = true;
            }
        }

        private void dataGridView2_ColumnWidthChanged(object sender, DataGridViewColumnEventArgs e)
        {
            DataGridView gv = (DataGridView)sender;

            Rectangle rtHeader = gv.DisplayRectangle;

            rtHeader.Height = gv.ColumnHeadersHeight / 2;

            gv.Invalidate(rtHeader);
        }


        private void dataGridView2_Paint(object sender, PaintEventArgs e)
        {
            DataGridView gv = (DataGridView)sender;

            string[] strHeaders = { "출하정보", "작업정보", "화  주", "제  품", "작업정보", "오더정보", "관  리", "선  박" };

            StringFormat format = new StringFormat();

            format.Alignment = StringAlignment.Center;

            format.LineAlignment = StringAlignment.Center;


            // Category Painting

            {
                dataGridView2_r[0].X += 1;

                dataGridView2_r[0].Y += 2;

                dataGridView2_r[0].Width = dataGridView2_r[0].Width + dataGridView2_DoubleHead_width[0] - 2;

                dataGridView2_r[0].Height = (dataGridView2_r[0].Height / 2) - 2;

                e.Graphics.DrawRectangle(new Pen(gv.BackgroundColor), dataGridView2_r[0]);

                e.Graphics.FillRectangle(new SolidBrush(gv.ColumnHeadersDefaultCellStyle.BackColor),
                //e.Graphics.FillRectangle(new SolidBrush(Color.Yellow),

                dataGridView2_r[0]);


                e.Graphics.DrawString(strHeaders[0],

                    //gv.ColumnHeadersDefaultCellStyle.Font,
                    new Font("맑은고딕", 9, FontStyle.Bold),

                    new SolidBrush(gv.ColumnHeadersDefaultCellStyle.ForeColor),

                    dataGridView2_r[0],

                    format);

            }

            // Projection Painting

            {
                dataGridView2_r[1].X += 1;

                dataGridView2_r[1].Y += 2;

                dataGridView2_r[1].Width = dataGridView2_r[1].Width + dataGridView2_DoubleHead_width[1] + 
                                         dataGridView2_DoubleHead_width[2] - 2;

                dataGridView2_r[1].Height = (dataGridView2_r[1].Height / 2) - 2;

                e.Graphics.DrawRectangle(new Pen(gv.BackgroundColor), dataGridView2_r[1]);

                e.Graphics.FillRectangle(new SolidBrush(gv.ColumnHeadersDefaultCellStyle.BackColor),
                //e.Graphics.FillRectangle(new SolidBrush(Color.Yellow),

                dataGridView2_r[1]);


                e.Graphics.DrawString(strHeaders[1],

                    //gv.ColumnHeadersDefaultCellStyle.Font,
                    new Font("맑은고딕", 9, FontStyle.Bold),

                    new SolidBrush(gv.ColumnHeadersDefaultCellStyle.ForeColor),

                    dataGridView2_r[1],

                    format);
            }

            // GROUP HEAD 매출율

            {
                dataGridView2_r[2].X += 1;

                dataGridView2_r[2].Y += 2;

                dataGridView2_r[2].Width = dataGridView2_r[2].Width + dataGridView2_DoubleHead_width[3] + 
                                         dataGridView2_DoubleHead_width[4] - 2;

                dataGridView2_r[2].Height = (dataGridView2_r[2].Height / 2) - 2;

                e.Graphics.DrawRectangle(new Pen(gv.BackgroundColor), dataGridView2_r[2]);

                e.Graphics.FillRectangle(new SolidBrush(gv.ColumnHeadersDefaultCellStyle.BackColor),
                //e.Graphics.FillRectangle(new SolidBrush(Color.Yellow),

                dataGridView2_r[2]);


                e.Graphics.DrawString(strHeaders[2],

                    //gv.ColumnHeadersDefaultCellStyle.Font,
                    new Font("맑은고딕", 9, FontStyle.Bold),

                    new SolidBrush(gv.ColumnHeadersDefaultCellStyle.ForeColor),

                    dataGridView2_r[2],

                    format);
            }

            {
                dataGridView2_r[3].X += 1;

                dataGridView2_r[3].Y += 2;

                dataGridView2_r[3].Width = dataGridView2_r[3].Width + dataGridView2_DoubleHead_width[5] + 
                                         dataGridView2_DoubleHead_width[6] + dataGridView2_DoubleHead_width[7] + 
                                         dataGridView2_DoubleHead_width[8] - 2;

                dataGridView2_r[3].Height = (dataGridView2_r[3].Height / 2) - 2;

                e.Graphics.DrawRectangle(new Pen(gv.BackgroundColor), dataGridView2_r[3]);

                e.Graphics.FillRectangle(new SolidBrush(gv.ColumnHeadersDefaultCellStyle.BackColor),
                //e.Graphics.FillRectangle(new SolidBrush(Color.Yellow),

                dataGridView2_r[3]);


                e.Graphics.DrawString(strHeaders[3],

                    //gv.ColumnHeadersDefaultCellStyle.Font,
                    new Font("맑은고딕", 9, FontStyle.Bold),

                    new SolidBrush(gv.ColumnHeadersDefaultCellStyle.ForeColor),

                    dataGridView2_r[3],

                    format);
            }

            {
                dataGridView2_r[4].X += 1;

                dataGridView2_r[4].Y += 2;

                dataGridView2_r[4].Width = dataGridView2_r[4].Width + dataGridView2_DoubleHead_width[9] + 
                                          dataGridView2_DoubleHead_width[10] + dataGridView2_DoubleHead_width[11] + 
                                          dataGridView2_DoubleHead_width[12] - 2;

                dataGridView2_r[4].Height = (dataGridView2_r[4].Height / 2) - 2;

                e.Graphics.DrawRectangle(new Pen(gv.BackgroundColor), dataGridView2_r[4]);

                e.Graphics.FillRectangle(new SolidBrush(gv.ColumnHeadersDefaultCellStyle.BackColor),
                //e.Graphics.FillRectangle(new SolidBrush(Color.Yellow),

                dataGridView2_r[4]);


                e.Graphics.DrawString(strHeaders[4],

                    //gv.ColumnHeadersDefaultCellStyle.Font,
                    new Font("맑은고딕", 9, FontStyle.Bold),

                    new SolidBrush(gv.ColumnHeadersDefaultCellStyle.ForeColor),

                    dataGridView2_r[4],

                    format);
            }

            {
                dataGridView2_r[5].X += 1;

                dataGridView2_r[5].Y += 2;

                dataGridView2_r[5].Width = dataGridView2_r[5].Width + dataGridView2_DoubleHead_width[13] + 
                                          dataGridView2_DoubleHead_width[14] + dataGridView2_DoubleHead_width[15] + 
                                          dataGridView2_DoubleHead_width[16] - 2;

                dataGridView2_r[5].Height = (dataGridView2_r[5].Height / 2) - 2;

                e.Graphics.DrawRectangle(new Pen(gv.BackgroundColor), dataGridView2_r[5]);

                e.Graphics.FillRectangle(new SolidBrush(gv.ColumnHeadersDefaultCellStyle.BackColor),
                //e.Graphics.FillRectangle(new SolidBrush(Color.Yellow),

                dataGridView2_r[5]);


                e.Graphics.DrawString(strHeaders[5],

                    //gv.ColumnHeadersDefaultCellStyle.Font,
                    new Font("맑은고딕", 9, FontStyle.Bold),

                    new SolidBrush(gv.ColumnHeadersDefaultCellStyle.ForeColor),

                    dataGridView2_r[5],

                    format);
            }

            {
                dataGridView2_r[6].X += 1;

                dataGridView2_r[6].Y += 2;

                dataGridView2_r[6].Width = dataGridView2_r[6].Width + dataGridView2_DoubleHead_width[17] + 
                                          dataGridView2_DoubleHead_width[18] - 2;

                dataGridView2_r[6].Height = (dataGridView2_r[6].Height / 2) - 2;

                e.Graphics.DrawRectangle(new Pen(gv.BackgroundColor), dataGridView2_r[6]);

                e.Graphics.FillRectangle(new SolidBrush(gv.ColumnHeadersDefaultCellStyle.BackColor),
                //e.Graphics.FillRectangle(new SolidBrush(Color.Yellow),

                dataGridView2_r[6]);


                e.Graphics.DrawString(strHeaders[6],

                    //gv.ColumnHeadersDefaultCellStyle.Font,
                    new Font("맑은고딕", 9, FontStyle.Bold),

                    new SolidBrush(gv.ColumnHeadersDefaultCellStyle.ForeColor),

                    dataGridView2_r[6],

                    format);
            }
           
            {
                dataGridView2_r[7].X += 1;

                dataGridView2_r[7].Y += 2;

                dataGridView2_r[7].Width = dataGridView2_r[7].Width + dataGridView2_DoubleHead_width[19] - 2;

                dataGridView2_r[7].Height = (dataGridView2_r[7].Height / 2) - 2;

                e.Graphics.DrawRectangle(new Pen(gv.BackgroundColor), dataGridView2_r[7]);

                e.Graphics.FillRectangle(new SolidBrush(gv.ColumnHeadersDefaultCellStyle.BackColor),
                //e.Graphics.FillRectangle(new SolidBrush(Color.Yellow),

                dataGridView2_r[7]);


                e.Graphics.DrawString(strHeaders[7],

                    //gv.ColumnHeadersDefaultCellStyle.Font,
                    new Font("맑은고딕", 9, FontStyle.Bold),

                    new SolidBrush(gv.ColumnHeadersDefaultCellStyle.ForeColor),

                    dataGridView2_r[7],

                    format);
            }
           
        }

        private void dataGridView2_Scroll(object sender, ScrollEventArgs e)
        {
            dataGridView2Set();

            //DataGridView gv = (DataGridView)sender;

            Rectangle rtHeader = dataGridView2.DisplayRectangle;

            rtHeader.Height = dataGridView2.ColumnHeadersHeight / 2;

            dataGridView2.Invalidate(rtHeader);
        }


    }
}
