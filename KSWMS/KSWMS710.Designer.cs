﻿namespace KSWMS
{
    partial class KSWMS710
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel5 = new System.Windows.Forms.Panel();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.button3 = new System.Windows.Forms.Button();
            this.comboBox4 = new System.Windows.Forms.ComboBox();
            this.comboBox3 = new System.Windows.Forms.ComboBox();
            this.comboBox2 = new System.Windows.Forms.ComboBox();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.label17 = new System.Windows.Forms.Label();
            this.st340 = new System.Windows.Forms.Button();
            this.st330 = new System.Windows.Forms.Button();
            this.st320 = new System.Windows.Forms.Button();
            this.st310 = new System.Windows.Forms.Button();
            this.io340 = new System.Windows.Forms.Button();
            this.io330 = new System.Windows.Forms.Button();
            this.io320 = new System.Windows.Forms.Button();
            this.io310 = new System.Windows.Forms.Button();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.o240 = new System.Windows.Forms.Button();
            this.o230 = new System.Windows.Forms.Button();
            this.o220 = new System.Windows.Forms.Button();
            this.o210 = new System.Windows.Forms.Button();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.i140 = new System.Windows.Forms.Button();
            this.i130 = new System.Windows.Forms.Button();
            this.i120 = new System.Windows.Forms.Button();
            this.i110 = new System.Windows.Forms.Button();
            this.panel3 = new System.Windows.Forms.Panel();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.btnInputLimit = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.panel5);
            this.panel1.Controls.Add(this.button3);
            this.panel1.Controls.Add(this.comboBox4);
            this.panel1.Controls.Add(this.comboBox3);
            this.panel1.Controls.Add(this.comboBox2);
            this.panel1.Controls.Add(this.comboBox1);
            this.panel1.Controls.Add(this.label18);
            this.panel1.Controls.Add(this.label19);
            this.panel1.Controls.Add(this.label20);
            this.panel1.Controls.Add(this.label21);
            this.panel1.Controls.Add(this.panel4);
            this.panel1.Controls.Add(this.st340);
            this.panel1.Controls.Add(this.st330);
            this.panel1.Controls.Add(this.st320);
            this.panel1.Controls.Add(this.st310);
            this.panel1.Controls.Add(this.io340);
            this.panel1.Controls.Add(this.io330);
            this.panel1.Controls.Add(this.io320);
            this.panel1.Controls.Add(this.io310);
            this.panel1.Controls.Add(this.label13);
            this.panel1.Controls.Add(this.label14);
            this.panel1.Controls.Add(this.label15);
            this.panel1.Controls.Add(this.label16);
            this.panel1.Controls.Add(this.o240);
            this.panel1.Controls.Add(this.o230);
            this.panel1.Controls.Add(this.o220);
            this.panel1.Controls.Add(this.o210);
            this.panel1.Controls.Add(this.label9);
            this.panel1.Controls.Add(this.label10);
            this.panel1.Controls.Add(this.label11);
            this.panel1.Controls.Add(this.label12);
            this.panel1.Controls.Add(this.i140);
            this.panel1.Controls.Add(this.i130);
            this.panel1.Controls.Add(this.i120);
            this.panel1.Controls.Add(this.i110);
            this.panel1.Controls.Add(this.panel3);
            this.panel1.Controls.Add(this.label7);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Controls.Add(this.button2);
            this.panel1.Controls.Add(this.button1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(969, 566);
            this.panel1.TabIndex = 1;
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.btnInputLimit);
            this.panel5.Controls.Add(this.textBox1);
            this.panel5.Controls.Add(this.label1);
            this.panel5.Location = new System.Drawing.Point(777, 380);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(180, 51);
            this.panel5.TabIndex = 98;
            this.panel5.Visible = false;
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(70, 15);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(53, 21);
            this.textBox1.TabIndex = 97;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(11, 18);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 12);
            this.label1.TabIndex = 96;
            this.label1.Text = "입고율 : ";
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(765, 531);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(75, 23);
            this.button3.TabIndex = 5;
            this.button3.Text = "button3";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // comboBox4
            // 
            this.comboBox4.Font = new System.Drawing.Font("굴림", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.comboBox4.FormattingEnabled = true;
            this.comboBox4.Location = new System.Drawing.Point(290, 480);
            this.comboBox4.Name = "comboBox4";
            this.comboBox4.Size = new System.Drawing.Size(120, 24);
            this.comboBox4.TabIndex = 95;
            // 
            // comboBox3
            // 
            this.comboBox3.Font = new System.Drawing.Font("굴림", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.comboBox3.FormattingEnabled = true;
            this.comboBox3.Location = new System.Drawing.Point(290, 445);
            this.comboBox3.Name = "comboBox3";
            this.comboBox3.Size = new System.Drawing.Size(120, 24);
            this.comboBox3.TabIndex = 94;
            // 
            // comboBox2
            // 
            this.comboBox2.Font = new System.Drawing.Font("굴림", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.comboBox2.FormattingEnabled = true;
            this.comboBox2.Location = new System.Drawing.Point(290, 410);
            this.comboBox2.Name = "comboBox2";
            this.comboBox2.Size = new System.Drawing.Size(120, 24);
            this.comboBox2.TabIndex = 93;
            // 
            // comboBox1
            // 
            this.comboBox1.Font = new System.Drawing.Font("굴림", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(290, 375);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(120, 24);
            this.comboBox1.TabIndex = 92;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("굴림", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label18.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label18.Location = new System.Drawing.Point(140, 485);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(134, 16);
            this.label18.TabIndex = 91;
            this.label18.Text = "4번 입고대(340)";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("굴림", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label19.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label19.Location = new System.Drawing.Point(140, 450);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(132, 16);
            this.label19.TabIndex = 90;
            this.label19.Text = "3번 입고대(330)";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("굴림", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label20.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label20.Location = new System.Drawing.Point(140, 415);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(132, 16);
            this.label20.TabIndex = 89;
            this.label20.Text = "2번 입고대(320)";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("굴림", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label21.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label21.Location = new System.Drawing.Point(140, 380);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(132, 16);
            this.label21.TabIndex = 88;
            this.label21.Text = "1번 입고대(310)";
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.panel4.Controls.Add(this.label17);
            this.panel4.Location = new System.Drawing.Point(0, 340);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(968, 25);
            this.panel4.TabIndex = 87;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("굴림", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label17.ForeColor = System.Drawing.Color.Navy;
            this.label17.Location = new System.Drawing.Point(30, 5);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(179, 16);
            this.label17.TabIndex = 21;
            this.label17.Text = "▶ 입고대 스태커 설정";
            // 
            // st340
            // 
            this.st340.BackColor = System.Drawing.Color.WhiteSmoke;
            this.st340.Font = new System.Drawing.Font("굴림", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.st340.ForeColor = System.Drawing.SystemColors.ControlText;
            this.st340.Location = new System.Drawing.Point(420, 305);
            this.st340.Name = "st340";
            this.st340.Size = new System.Drawing.Size(120, 30);
            this.st340.TabIndex = 86;
            this.st340.Text = "입고";
            this.st340.UseVisualStyleBackColor = false;
            // 
            // st330
            // 
            this.st330.BackColor = System.Drawing.Color.LawnGreen;
            this.st330.Font = new System.Drawing.Font("굴림", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.st330.ForeColor = System.Drawing.SystemColors.ControlText;
            this.st330.Location = new System.Drawing.Point(420, 270);
            this.st330.Name = "st330";
            this.st330.Size = new System.Drawing.Size(120, 30);
            this.st330.TabIndex = 85;
            this.st330.Text = "출고";
            this.st330.UseVisualStyleBackColor = false;
            // 
            // st320
            // 
            this.st320.BackColor = System.Drawing.Color.WhiteSmoke;
            this.st320.Font = new System.Drawing.Font("굴림", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.st320.ForeColor = System.Drawing.SystemColors.ControlText;
            this.st320.Location = new System.Drawing.Point(420, 235);
            this.st320.Name = "st320";
            this.st320.Size = new System.Drawing.Size(120, 30);
            this.st320.TabIndex = 84;
            this.st320.Text = "입고";
            this.st320.UseVisualStyleBackColor = false;
            // 
            // st310
            // 
            this.st310.BackColor = System.Drawing.Color.LawnGreen;
            this.st310.Font = new System.Drawing.Font("굴림", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.st310.ForeColor = System.Drawing.SystemColors.ControlText;
            this.st310.Location = new System.Drawing.Point(420, 200);
            this.st310.Name = "st310";
            this.st310.Size = new System.Drawing.Size(120, 30);
            this.st310.TabIndex = 83;
            this.st310.Text = "출고";
            this.st310.UseVisualStyleBackColor = false;
            // 
            // io340
            // 
            this.io340.BackColor = System.Drawing.Color.Blue;
            this.io340.Font = new System.Drawing.Font("굴림", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.io340.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.io340.Location = new System.Drawing.Point(290, 305);
            this.io340.Name = "io340";
            this.io340.Size = new System.Drawing.Size(120, 30);
            this.io340.TabIndex = 82;
            this.io340.Text = "사용";
            this.io340.UseVisualStyleBackColor = false;
            this.io340.Click += new System.EventHandler(this.buttonCheck_Click);
            // 
            // io330
            // 
            this.io330.BackColor = System.Drawing.Color.Blue;
            this.io330.Font = new System.Drawing.Font("굴림", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.io330.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.io330.Location = new System.Drawing.Point(290, 270);
            this.io330.Name = "io330";
            this.io330.Size = new System.Drawing.Size(120, 30);
            this.io330.TabIndex = 81;
            this.io330.Text = "사용";
            this.io330.UseVisualStyleBackColor = false;
            this.io330.Click += new System.EventHandler(this.buttonCheck_Click);
            // 
            // io320
            // 
            this.io320.BackColor = System.Drawing.Color.Blue;
            this.io320.Font = new System.Drawing.Font("굴림", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.io320.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.io320.Location = new System.Drawing.Point(290, 235);
            this.io320.Name = "io320";
            this.io320.Size = new System.Drawing.Size(120, 30);
            this.io320.TabIndex = 80;
            this.io320.Text = "사용";
            this.io320.UseVisualStyleBackColor = false;
            this.io320.Click += new System.EventHandler(this.buttonCheck_Click);
            // 
            // io310
            // 
            this.io310.BackColor = System.Drawing.Color.Blue;
            this.io310.Font = new System.Drawing.Font("굴림", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.io310.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.io310.Location = new System.Drawing.Point(290, 200);
            this.io310.Name = "io310";
            this.io310.Size = new System.Drawing.Size(120, 30);
            this.io310.TabIndex = 79;
            this.io310.Text = "사용";
            this.io310.UseVisualStyleBackColor = false;
            this.io310.Click += new System.EventHandler(this.buttonCheck_Click);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("굴림", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label13.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label13.Location = new System.Drawing.Point(120, 315);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(158, 16);
            this.label13.TabIndex = 78;
            this.label13.Text = "4번 입/출고대(340)";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("굴림", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label14.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label14.Location = new System.Drawing.Point(120, 280);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(156, 16);
            this.label14.TabIndex = 77;
            this.label14.Text = "3번 입/출고대(330)";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("굴림", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label15.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label15.Location = new System.Drawing.Point(120, 245);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(156, 16);
            this.label15.TabIndex = 76;
            this.label15.Text = "2번 입/출고대(320)";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("굴림", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label16.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label16.Location = new System.Drawing.Point(120, 210);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(156, 16);
            this.label16.TabIndex = 75;
            this.label16.Text = "1번 입/출고대(310)";
            // 
            // o240
            // 
            this.o240.BackColor = System.Drawing.Color.Blue;
            this.o240.Font = new System.Drawing.Font("굴림", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.o240.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.o240.Location = new System.Drawing.Point(720, 135);
            this.o240.Name = "o240";
            this.o240.Size = new System.Drawing.Size(120, 30);
            this.o240.TabIndex = 74;
            this.o240.Text = "사용";
            this.o240.UseVisualStyleBackColor = false;
            this.o240.Click += new System.EventHandler(this.buttonCheck_Click);
            // 
            // o230
            // 
            this.o230.BackColor = System.Drawing.Color.Blue;
            this.o230.Font = new System.Drawing.Font("굴림", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.o230.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.o230.Location = new System.Drawing.Point(720, 100);
            this.o230.Name = "o230";
            this.o230.Size = new System.Drawing.Size(120, 30);
            this.o230.TabIndex = 73;
            this.o230.Text = "사용";
            this.o230.UseVisualStyleBackColor = false;
            this.o230.Click += new System.EventHandler(this.buttonCheck_Click);
            // 
            // o220
            // 
            this.o220.BackColor = System.Drawing.Color.Blue;
            this.o220.Font = new System.Drawing.Font("굴림", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.o220.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.o220.Location = new System.Drawing.Point(720, 65);
            this.o220.Name = "o220";
            this.o220.Size = new System.Drawing.Size(120, 30);
            this.o220.TabIndex = 72;
            this.o220.Text = "사용";
            this.o220.UseVisualStyleBackColor = false;
            this.o220.Click += new System.EventHandler(this.buttonCheck_Click);
            // 
            // o210
            // 
            this.o210.BackColor = System.Drawing.Color.Blue;
            this.o210.Font = new System.Drawing.Font("굴림", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.o210.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.o210.Location = new System.Drawing.Point(720, 30);
            this.o210.Name = "o210";
            this.o210.Size = new System.Drawing.Size(120, 30);
            this.o210.TabIndex = 71;
            this.o210.Text = "사용";
            this.o210.UseVisualStyleBackColor = false;
            this.o210.Click += new System.EventHandler(this.buttonCheck_Click);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("굴림", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label9.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label9.Location = new System.Drawing.Point(570, 145);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(134, 16);
            this.label9.TabIndex = 70;
            this.label9.Text = "4호기 출고(240)";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("굴림", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label10.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label10.Location = new System.Drawing.Point(570, 110);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(132, 16);
            this.label10.TabIndex = 69;
            this.label10.Text = "3호기 출고(230)";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("굴림", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label11.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label11.Location = new System.Drawing.Point(570, 75);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(132, 16);
            this.label11.TabIndex = 68;
            this.label11.Text = "2호기 출고(220)";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("굴림", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label12.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label12.Location = new System.Drawing.Point(570, 40);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(132, 16);
            this.label12.TabIndex = 67;
            this.label12.Text = "1호기 출고(210)";
            // 
            // i140
            // 
            this.i140.BackColor = System.Drawing.Color.Blue;
            this.i140.Font = new System.Drawing.Font("굴림", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.i140.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.i140.Location = new System.Drawing.Point(290, 135);
            this.i140.Name = "i140";
            this.i140.Size = new System.Drawing.Size(120, 30);
            this.i140.TabIndex = 66;
            this.i140.Text = "사용";
            this.i140.UseVisualStyleBackColor = false;
            this.i140.Click += new System.EventHandler(this.buttonCheck_Click);
            // 
            // i130
            // 
            this.i130.BackColor = System.Drawing.Color.Blue;
            this.i130.Font = new System.Drawing.Font("굴림", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.i130.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.i130.Location = new System.Drawing.Point(290, 100);
            this.i130.Name = "i130";
            this.i130.Size = new System.Drawing.Size(120, 30);
            this.i130.TabIndex = 65;
            this.i130.Text = "사용";
            this.i130.UseVisualStyleBackColor = false;
            this.i130.Click += new System.EventHandler(this.buttonCheck_Click);
            // 
            // i120
            // 
            this.i120.BackColor = System.Drawing.Color.Blue;
            this.i120.Font = new System.Drawing.Font("굴림", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.i120.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.i120.Location = new System.Drawing.Point(290, 65);
            this.i120.Name = "i120";
            this.i120.Size = new System.Drawing.Size(120, 30);
            this.i120.TabIndex = 64;
            this.i120.Text = "사용";
            this.i120.UseVisualStyleBackColor = false;
            this.i120.Click += new System.EventHandler(this.buttonCheck_Click);
            // 
            // i110
            // 
            this.i110.BackColor = System.Drawing.Color.Blue;
            this.i110.Font = new System.Drawing.Font("굴림", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.i110.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.i110.Location = new System.Drawing.Point(290, 30);
            this.i110.Name = "i110";
            this.i110.Size = new System.Drawing.Size(120, 30);
            this.i110.TabIndex = 63;
            this.i110.Text = "사용";
            this.i110.UseVisualStyleBackColor = false;
            this.i110.Click += new System.EventHandler(this.buttonCheck_Click);
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.panel3.Controls.Add(this.label8);
            this.panel3.Location = new System.Drawing.Point(0, 170);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(968, 25);
            this.panel3.TabIndex = 62;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("굴림", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label8.ForeColor = System.Drawing.Color.Navy;
            this.label8.Location = new System.Drawing.Point(30, 5);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(146, 16);
            this.label8.TabIndex = 21;
            this.label8.Text = "▶ 입/출고대 설정";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("굴림", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label7.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label7.Location = new System.Drawing.Point(140, 145);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(134, 16);
            this.label7.TabIndex = 61;
            this.label7.Text = "4호기 입고(140)";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("굴림", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label6.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label6.Location = new System.Drawing.Point(140, 110);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(132, 16);
            this.label6.TabIndex = 60;
            this.label6.Text = "3호기 입고(130)";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("굴림", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label5.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label5.Location = new System.Drawing.Point(140, 75);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(132, 16);
            this.label5.TabIndex = 59;
            this.label5.Text = "2호기 입고(120)";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("굴림", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label4.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label4.Location = new System.Drawing.Point(140, 40);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(132, 16);
            this.label4.TabIndex = 58;
            this.label4.Text = "1호기 입고(110)";
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(968, 25);
            this.panel2.TabIndex = 57;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("굴림", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label2.ForeColor = System.Drawing.Color.Navy;
            this.label2.Location = new System.Drawing.Point(30, 5);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(162, 16);
            this.label2.TabIndex = 21;
            this.label2.Text = "▶ 스태크 입고 설정";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("굴림", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label3.ForeColor = System.Drawing.Color.Navy;
            this.label3.Location = new System.Drawing.Point(460, 5);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(162, 16);
            this.label3.TabIndex = 5;
            this.label3.Text = "▶ 스태크 출고 설정";
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(684, 531);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 4;
            this.button2.Text = "button2";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(603, 531);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 2;
            this.button1.Text = "button1";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // btnInputLimit
            // 
            this.btnInputLimit.Location = new System.Drawing.Point(129, 11);
            this.btnInputLimit.Name = "btnInputLimit";
            this.btnInputLimit.Size = new System.Drawing.Size(37, 26);
            this.btnInputLimit.TabIndex = 98;
            this.btnInputLimit.Text = "Set";
            this.btnInputLimit.UseVisualStyleBackColor = true;
            this.btnInputLimit.Click += new System.EventHandler(this.btnInputLimit_Click);
            // 
            // KSWMS710
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(969, 566);
            this.Controls.Add(this.panel1);
            this.Name = "KSWMS710";
            this.Text = "710. 설비 사용정의";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button i140;
        private System.Windows.Forms.Button i130;
        private System.Windows.Forms.Button i120;
        private System.Windows.Forms.Button i110;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Button o240;
        private System.Windows.Forms.Button o230;
        private System.Windows.Forms.Button o220;
        private System.Windows.Forms.Button o210;
        private System.Windows.Forms.Button io340;
        private System.Windows.Forms.Button io330;
        private System.Windows.Forms.Button io320;
        private System.Windows.Forms.Button io310;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Button st340;
        private System.Windows.Forms.Button st330;
        private System.Windows.Forms.Button st320;
        private System.Windows.Forms.Button st310;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.ComboBox comboBox4;
        private System.Windows.Forms.ComboBox comboBox3;
        private System.Windows.Forms.ComboBox comboBox2;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Button btnInputLimit;
    }
}