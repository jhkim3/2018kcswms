﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KSWMS
{
    class Qry
    {
        public static String KSWMS110_001
        {
            get
            {
                return "SELECT USER_ID, USER_NAME, UPD_DATE, PASSWD, ATH_GBN FROM TABUSR";
            }
        }

        public static String KSWMS110_002(List<string> Condition)
        {
            string result = "SELECT COUNT(*)" + 
                            " FROM TABUSR" + 
                            " WHERE USER_ID= '" + Condition[0] + "'";
            return result;
        }

        public static String KSWMS110_003(List<string> Condition)
        {
            string result = "SELECT COUNT(*)" +
                            " FROM TABUSR" +
                            " WHERE USER_ID= '" + Condition[0] +
                            "' AND PASSWD = '" + Condition[1] +
                            "' AND USER_NAME = '" + Condition[2] +
                            "' AND ATH_GBN = '" + Condition[3] + "'";
            return result;
        }
        
        public static String KSWMS110_004(List<string> Condition)
        {
            string result = "INSERT INTO TABUSR" +
                            "(USER_ID, USER_NAME, PASSWD, USER_LANG, ATH_GBN, BACK_FLG, CRT_DATE, UPD_DATE, COM_NAME, USR_ID)" +
                            " VALUES('" + Condition[0] + "', '" + Condition[2] + "', '" +  
                            Condition[1] + "', 'K', '" + Condition[3] + "', 'Y', SYSDATE, SYSDATE, NULL, 'AUTO_LOGIN')";
            return result;
        }

        public static String KSWMS110_005(List<string> Condition)
        {
            string result = "UPDATE TABUSR" +
                            " SET  USER_NAME = '" + Condition[2] + "', PASSWD = '" + Condition[1] +
                            "', ATH_GBN = '" + Condition[3] + "', UPD_DATE = SYSDATE" +
                            " WHERE USER_ID= '" + Condition[0] + "'";
            return result;
        }

        public static String KSWMS110_006(List<string> Condition)
        {
            string result = "DELETE FROM TABUSR" +
                            " WHERE USER_ID= '" + Condition[0] +
                            "' AND PASSWD = '" + Condition[1] +
                            "' AND USER_NAME = '" + Condition[2] +
                            "' AND ATH_GBN = '" + Condition[3] + "'";
                         
            return result;
        }

        public static String KSWMS210_001(List<string> Condition)
        {
            string result = "SELECT a.WMS_NO, a.SSTOCKNO, TO_CHAR(a.IPDATE, 'YYYY/MM/DD')," +
                            //" (SELECT FLDCON FROM FLD_MA WHERE (SELECT ZONE FROM TAB200 WHERE a.WMS_NO = SER_NO) = FLDVAL AND TBL_CD = 'TAB200' AND FLD_CD = 'ZONE')," + 
                            " (CASE a.PGUBUN" +
                            "  WHEN '82' THEN '축산'" +
                            "  ELSE '식품'" +
                            "  END) AS PGUBUN," +
                            " (SELECT FLDCON FROM FLD_MA WHERE a.WRKSTS = FLDVAL AND TBL_CD = 'TABINORD' AND FLD_CD = 'WRKSTS') AS WRKSTS," +
                            " (SELECT FLDCON FROM FLD_MA WHERE a.WRKTYP = FLDVAL AND TBL_CD = 'TAB200' AND FLD_CD = 'IN_GBN') AS WRKTYP," +
                            " TO_CHAR(a.IPQTY1, 'FM9,999,999') AS IPQTY1," +
                            " a.BLNO1, a.HCODE, a.HNAME, a.HDAM, a.PCODE, a.PNAME," +
                            " a.PDESC, a.PUNIT, a.HANG, a.BIGO, a.HOAMLNO, a.TRUCK, a.JHCODE, a.JHNAME, a.JHDAM, a.SCODE," +
                            " SNAME, TO_CHAR(a.TGDATE, 'YYYY/MM/DD')" +
                            " FROM TABINORD a" +
                            " WHERE IPDATE = TO_DATE('" + Condition[0] + "'||'/" + Condition[1] + "/" + Condition[2] + "','YYYY/MM/DD')";

                            //" WHERE WMS_NO = '17C120013' OR  WMS_NO = '17C120002'";


            if (Condition[3] != "A")
            {
                result += " AND WRKSTS ='" + Condition[3] + "'";
            }

            if (Condition[4] != "A")
            {
                result += " AND WRKTYP ='" + Condition[4] + "'";
            }

            result += " ORDER BY WMS_NO ASC";

            return result;

        }

        public static String KSWMS210_002(List<string> Condition)
        {
            string result = "SELECT a.WMS_NO, TO_CHAR(a.IPDATE, 'YYYY/MM/DD'), a.BLNO1," +
                       //" (SELECT FLDCON FROM FLD_MA WHERE (SELECT ZONE FROM TAB200 WHERE a.WMS_NO = SER_NO) = FLDVAL AND TBL_CD = 'TAB200' AND FLD_CD = 'ZONE')," + 
                       " a.HCODE, a.HNAME, a.HDAM, a.PCODE, a.PNAME, 0, TO_CHAR(a.IPQTY1, 'FM9,999,999') AS IPQTY1,  a.PUNIT," +
                       " a.HANG, a.BIGO, a.HOAMLNO, a.SSTOCKNO, 0," +
                       " (SELECT FLDCON FROM FLD_MA WHERE a.WRKSTS = FLDVAL AND TBL_CD = 'TABINORD' AND FLD_CD = 'WRKSTS') AS WRKSTS," +
                       " (SELECT FLDCON FROM FLD_MA WHERE a.WRKTYP = FLDVAL AND TBL_CD = 'TAB200' AND FLD_CD = 'IN_GBN') AS WRKTYP," +
                       " a.TRUCK, a.JHCODE, a.JHNAME, a.JHDAM, SCODE, SNAME" +
                       " FROM TABINORD a" +
                       "  WHERE IPDATE = TO_DATE('" + Condition[0] + "'||'/" + Condition[1] + "/" + Condition[2] + "','YYYY/MM/DD') AND WRKSTS = '0'";
               
            if (Condition[3] != "A")
            {
                result += " AND a.WRKTYP ='" + Condition[3] + "'";
            }

            result += " ORDER BY a.WMS_NO ASC";
            
            return result;
        }

        public static String KSWMS210_003(List<string> Condition)
        {
            string result = "SELECT COUNT(*)" +
                            " FROM TABINORD" +
                            " WHERE WMS_NO  = '" + Condition[25] + "'" +
                            " AND IPDATE    = TO_DATE('" + Condition[0] + "' || '/" + Condition[1] + "/" + Condition[2] + "', 'YYYY/MM/DD')" +
                            " AND JHCODE    = '" + Condition[6] + "'" +
                            " AND JHNAME    = '" + Condition[7] + "'" +
                            " AND JHDAM     = '" + Condition[8] + "'" +
                            " AND HCODE     = '" + Condition[9] + "'" +
                            " AND HNAME     = '" + Condition[10] + "'" +
                            " AND HDAM      = '" + Condition[11] + "'" +
                            " AND PCODE     = '" + Condition[12] + "'" +
                            " AND PNAME     = '" + Condition[13] + "'" +
                           // " AND PDESC     = '" + Condition[14] + "'" +
                            " AND PUNIT     = '" + Condition[15] + "'" +
                            " AND SCODE     = '" + Condition[16] + "'" +
                            " AND SNAME     = '" + Condition[17] + "'" +
                            " AND IPQTY1    = " + Regex.Replace(Condition[18], @"\D", "") +
                            " AND BLNO1     = '" + Condition[19] + "'" +
                            " AND HOAMLNO   = '" + Condition[20] + "'" +
                            " AND HANG      = '" + Condition[21] + "'" +
                            " AND TRUCK     = '" + Condition[22] + "'" +
                            " AND BIGO      = '" + Condition[23] + "'";
                         
            return result;
            
            /* GRID COL NO.
             0   a.WMS_NO,
             1   a.SSTOCKNO, 
             2   TO_CHAR(a.IPDATE, 'YYYY/MM/DD'),
             3   0,
             4   (SELECT FLDCON FROM FLD_MA WHERE a.WRKSTS = FLDVAL AND TBL_CD = 'TABINORD' AND FLD_CD = 'WRKSTS') AS WRKSTS,
             5   (SELECT FLDCON FROM FLD_MA WHERE a.WRKTYP = FLDVAL AND TBL_CD = 'TAB200' AND FLD_CD = 'IN_GBN') AS WRKTYP,
             6   TO_CHAR(a.IPQTY1, 'FM9,999,999') AS IPQTY1,
             7   a.BLNO1,
             8   a.HCODE
             9   a.HNAME,
            10   a.HDAM,
            11   a.PCODE,
            12   a.PNAME,
            13   0,
            14   a.PUNIT,
            15   a.HANG,
            16   a.BIGO,
            17   a.HOAMLNO,
            18   a.TRUCK,
            19   a.JHCODE,
            20   a.JHNAME,
            21   a.JHDAM,
            22   a.SCODE,
            23   SNAME,
            24   TO_CHAR(a.TGDATE, 'YYYY/MM/DD')
            */
        }

        public static String KSWMS210_004(List<string> Condition)
        {
            string result = "UPDATE TABINORD" +
                            " SET IPDATE     = TO_DATE('" + Condition[0] + "/" + Condition[1] + "/" + Condition[2] + "', 'YYYY/MM/DD')" +
                            ", JHCODE    = '" + Condition[6] + "'" +
                            ", JHNAME    = '" + Condition[7] + "'" +
                            ", JHDAM     = '" + Condition[8] + "'" +
                            ", HCODE     = '" + Condition[9] + "'" +
                            ", HNAME     = '" + Condition[10] + "'" +
                            ", HDAM      = '" + Condition[11] + "'" +
                            ", PCODE     = '" + Condition[12] + "'" +
                            ", PNAME     = '" + Condition[13] + "'" +
                            ", PDESC     = '" + Condition[14] + "'" +
                            ", PUNIT     = '" + Condition[15] + "'" +
                            ", SCODE     = '" + Condition[16] + "'" +
                            ", SNAME     = '" + Condition[17] + "'" +
                            ", IPQTY1    = " + Regex.Replace(Condition[18], @"\D", "") +
                            ", BLNO1     = '" + Condition[19] + "'" +
                            ", HOAMLNO   = '" + Condition[20] + "'" +
                            ", HANG      = '" + Condition[21] + "'" +
                            ", TRUCK     = '" + Condition[22] + "'" +
                            ", BIGO      = '" + Condition[23] + "'" +
                            ", UPDUID    = '" + Condition[24] + "'" +
                            " WHERE WMS_NO  = '" + Condition[25] + "'";

            return result;
        }

        public static String KSWMS210_005(List<string> Condition)
        {
            string result = "INSERT INTO TABINORD" +
                            "(WMS_NO, SSTOCKNO, STOCKNO, IPDATE, TGDATE, MDATE, JHCODE, JHNAME, JHDAM," +
                            " HCODE, HNAME, HDAM, SCODE, SNAME, PCODE, PNAME, PDESC, PUNIT, ROOM1, IPNO, IPQTY1," +
                            " NGUBUN, MGUBUN, HANG, BLNO1, HOAMLNO, PGUBUN, TRUCK, BIGO, ITIME1, WRKSTS, WRKTYP," +
                            " CRT_DT, CRTCOM, CRTUID, UPD_DT, UPDCOM, UPDUID, INDATE)" +
                            " VALUES('" + Condition[25] + "', '000000000', '000000000'," +
                            " TO_DATE('" + Condition[0] + "/" + Condition[1] + "/" + Condition[2] + "', 'YYYY/MM/DD')," +
                            " TO_DATE('" + Condition[3] + "/" + Condition[4] + "/" + Condition[5] + "', 'YYYY/MM/DD')," +
                            " SYSDATE,'" + Condition[6] + "', '" + Condition[7] + "', '" + Condition[8] + "'," +
                            " '" + Condition[9] + "', '" + Condition[10] + "', '" + Condition[11] + "'," +
                            " '" + Condition[16] + "', '" + Condition[17] + "'," +
                            " '" + Condition[12] + "', '" + Condition[13] + "', '" + Condition[14] + "', '" + Condition[15] + "'," +
                            " '8층', '000000000', " + Condition[18] + ", NULL, NULL, '" + Condition[21] + "'," +
                            " '" + Condition[19] + "', '" + Condition[20] + "', 'pg', '" + Condition[22] + "'," +
                            " '" + Condition[23] + "', NULL, '0', '12', SYSDATE, 'admin', '" + Condition[24] + "'," +
                            " SYSDATE, 'upusr', '" + Condition[24] + "', NULL)";
            
            return result;
        }

        public static String KSWMS210_006(List<string> Condition)
        {

            string result = "DELETE FROM TABINORD" +
                            " WHERE WMS_NO = '" + Condition[0] +
                            "' AND WRKSTS = '" + Condition[1] + "'";

            return result;
        }

        public static String KSWMS210_007(List<string> Condition)
        {
            string result = "SELECT a.WMS_NO, a.WMS_SQ, a.IPDATE," + 
                            " (CASE a.ORDSTS " +
                            "  WHEN '0' THEN '예약'" +
                            //"  ELSE CONCAT(a.ORDSTS, '번')" +
                            "  ELSE a.ORDSTS || '번'" +
                            "  END) AS ORDSTS," +
                            " (SELECT FLDCON FROM FLD_MA WHERE a.ORDTYP = FLDVAL AND TBL_CD = 'TABINORD' AND FLD_CD = 'WRKTYP') AS ORDTYP," +
                            " TO_CHAR(a.INQTY, 'FM9,999,999') AS INQTY," +
                            " TO_CHAR(a.ORDQTY, 'FM9,999,999') AS ORDQTY," +
                            " a.BLNO," +
                            " a.HCODE," +
                            " (SELECT HNAME FROM TAB120 WHERE a.HCODE = HCODE) AS HNAME," +
                            " (SELECT HDAMDANG FROM TAB120 WHERE a.HCODE = HCODE) AS HDAMDANG," +
                            " a.PCODE," +
                            " (SELECT PNAME FROM TAB110 WHERE a.PCODE = PCODE) AS PNAME," +
                            " (SELECT PDESC FROM TAB110 WHERE a.PCODE = PCODE) AS PDESC," +
                            " (SELECT PUNIT FROM TAB110 WHERE a.PCODE = PCODE) AS PUNIT," +
                            " a.HANG, a.BIGO, a.SER_NO, a.HOAMLNO," +
                            " SUBSTR(a.ITEM_CD, 8, 22)," +
                            " (CASE a.PGUBUN" +
                            "  WHEN '82' THEN '축산'" +
                            "  ELSE '식품'" +
                            "  END) AS PGUBUN," +
                            " STOCKNO, TRUCK," +
                            " a.MCODE," +
                            " (SELECT HNAME FROM TAB120 WHERE a.MCODE = HCODE) AS MNAME," +
                            " (SELECT HDAMDANG FROM TAB120 WHERE a.MCODE = HCODE) AS MDAMDANG," +
                            " SCODE," +
                            " (SELECT SNAME FROM TAB130 WHERE a.SCODE = SCODE) AS SNAME" +
                            " FROM TABINRSV a" +
                            " WHERE IPDATE = TO_DATE('" + Condition[0] + "'||'/" + Condition[1] + "/" + Condition[2] + "','YYYY/MM/DD')";

            if (Condition[3] == "0")  result += " AND a.ORDSTS ='" + Condition[3] + "'";
            else  result += " AND a.ORDSTS != '0'";

            result += "AND a.WMS_NO LIKE '%" + Condition[4] + "%'";
            result += "AND a.SER_NO LIKE '%" + Condition[5] + "%'";

            result += " ORDER BY a.WMS_NO, a.WMS_SQ ASC";

            return result;
        }

        public static String KSWMS210_008(List<string> Condition)
        {
            string result = "INSERT INTO TABINRSV " +
                            "(AREAKY, WMS_NO, WMS_SQ, STOCKNO, ITEM_CD, SER_NO, ORDSTS, ORDTYP, IPDATE," +
                            " TGDATE, ZONECD, MCODE, HCODE, PCODE, SCODE,INQTY, ORDQTY, BLNO, HOAMLNO," + 
                            " HANG, PGUBUN, TRUCK, BIGO, CRT_DT, CRTCOM, CRTUID, UPD_DT, UPDCOM, UPDUID," +
                            " INDATE)" +
                            "SELECT " +
                            "'1210' AS AREAKY, WMS_NO, LPAD('" + Condition[1] + "', 3, 0) AS WMS_SQ, STOCKNO, " +
                            "('KSC' || SUBSTR(WMS_NO, 0, 5) || 'H' || HCODE || 'P' || PCODE ||" +
                            " 'B' || SUBSTR(BLNO1, -4, 4) || 'R' || LPAD(HANG, 10, 0)) AS ITEM_CD," +
                            "'" + Condition[3] + "' AS SER_NO," +
                            " '0' AS ORDSTS," +
                            " WRKTYP," +
                            "TO_CHAR(IPDATE, 'YYYY/MM/DD')AS IPDATE," +
                            "TO_CHAR(TGDATE, 'YYYY/MM/DD')AS TGDATE," +
                            "(CASE PGUBUN WHEN '82' THEN '2' ELSE '1' END) AS ZONECD," +
                            " JHCODE, HCODE, PCODE, SCODE, " + Condition[2] + "," +
                            " IPQTY1, BLNO1, HOAMLNO, HANG, PGUBUN, TRUCK, BIGO," +
                            " SYSDATE, 'crtcom', '" + Condition[4] + "'," +
                            " SYSDATE, 'updcom', '" + Condition[4] + "'," +
                            " TO_CHAR(SYSDATE, 'YYYY/MM/DD')AS INDATE" +
                            " FROM TABINORD" +
                            " WHERE WMS_NO = '" + Condition[0] + "'";

            return result;
        }

        public static String KSWMS210_008_01(List<string> Condition)
        {
            string result = "UPDATE TABINORD" +
                            " SET WRKSTS   = '" + Condition[1] + "', " +
                            "UPD_DT = SYSDATE" +
                            " WHERE WMS_NO  = '" + Condition[0] + "'";

            return result;
        }

        public static String KSWMS210_008_02(List<string> Condition)
        {

            string result = "DELETE FROM TABINRSV" +
                            " WHERE WMS_NO = '" + Condition[0] +
                            "' AND ORDSTS = '" + Condition[1] + "'";

            return result;
        }

        public static String KSWMS210_009
        {
            get
            {
                return "SELECT a.WMS_NO, a.SSTOCKNO, TO_CHAR(a.IPDATE, 'YYYY/MM/DD')," +
                       //" (SELECT FLDCON FROM FLD_MA WHERE (SELECT ZONE FROM TAB200 WHERE a.WMS_NO = SER_NO) = FLDVAL AND TBL_CD = 'TAB200' AND FLD_CD = 'ZONE')," + 
                       " 0," +
                       " (SELECT FLDCON FROM FLD_MA WHERE a.WRKSTS = FLDVAL AND TBL_CD = 'TABINORD' AND FLD_CD = 'WRKSTS') AS WRKSTS," +
                       " (SELECT FLDCON FROM FLD_MA WHERE a.WRKTYP = FLDVAL AND TBL_CD = 'TAB200' AND FLD_CD = 'IN_GBN') AS WRKTYP," +
                       " TO_CHAR(a.IPQTY1, 'FM9,999,999') AS IPQTY1," +
                       " a.BLNO1, a.HCODE, a.HNAME, a.HDAM, a.PCODE, a.PNAME," +
                       " a.PDESC, a.PUNIT, a.HANG, a.BIGO, a.HOAMLNO, a.TRUCK, a.JHCODE, a.JHNAME, a.JHDAM, SCODE," +
                       " SNAME" +
                       " FROM TABINORD a" +
                       " WHERE IPDATE = TO_DATE('2017'||'/12/13','YYYY/MM/DD')" +
                       //" WHERE WMS_NO = '17C120013' OR  WMS_NO = '17C120002'";
                       " ORDER BY WMS_NO ASC";
            }
        }

        public static String KSWMS210_010(List<string> Condition)
        {
            string result = "SELECT MAX(WMS_NO)" +
                            " FROM TABINORD" +
                            " WHERE WMS_NO LIKE '" + Condition[0] + "%'";
            return result;
        }

        public static String KSWMS210_011(List<string> Condition)
        {
            string result = "SELECT MAX(SER_NO)" +
                            " FROM TABINRSV" +
                            " WHERE SER_NO LIKE '" + Condition[0] + "%'";
            return result;
        }

        public static String KSWMS210_012  // 빈(공)렉
        {
            get
            {
                return " SELECT COUNT(*)" +
                       " FROM TAB200" +
                       " WHERE USE = 'O' and LUG = '0'";
            }
        }

        public static String KSWMS210_013(List<string> Condition)
        {
            string result = "UPDATE TABINORD" +
                            " SET WRKSTS   = '2'," +
                            " UPD_DT = SYSDATE," +
                            " UPDUID = '"+ Condition[0] + "'" +
                            " WHERE WMS_NO  = '" + Condition[1] + "'" +
                            " AND WRKSTS = '1'";
            return result;
        }

        public static String KSWMS210_014(List<string> Condition)
        {
            string result = "UPDATE TABINRSV" +
                            " SET ORDSTS   = '1'," +
                            " UPD_DT = SYSDATE," +
                            " UPDUID = '" + Condition[0] + "'" +
                            " WHERE WMS_NO  = '" + Condition[1] + "'" +
                            " AND ORDSTS = '0'";
            return result;
        }

        public static String KSWMS210_015(List<string> Condition)
        {
            string result = "UPDATE TABINRSV" +
                           " SET ORDSTS   = '" + Condition[3] + "'," +
                           " UPD_DT = SYSDATE," +
                           " UPDUID = '" + Condition[0] + "'" +
                           " WHERE SER_NO  = '" + Condition[1] + "'" +
                           " AND ORDSTS = '" + Condition[2] + "'";
            return result;
        }

        public static String KSWMS210_025
        {
            get
            {
                return " SELECT" +
                        " MDATE," +
                        " TO_CHAR(IPDATE, 'YYYY/MM/DD')AS IPDATE," +
                        " BLNO1, HCODE, HNAME, HDAM," +
                        " PCODE, PNAME, PDESC," +
                        " TO_CHAR(IPQTY1, 'FM9,999,999') AS IPQTY1," +
                        " PUNIT, IF_FLAG, HANG," +
                        " BIGO, WMSNO, SSTOCKNO, HOAMLNO" +
                        " FROM TABINWRK" +
                        " WHERE IPDATE >= TO_DATE('17/12/13', 'YY/MM/DD')" +
                        " AND IPDATE < TO_DATE('17/12/13', 'YY/MM/DD') + 1" +
                        " ORDER BY IPDATE ASC";
            }
        }

        public static String KSWMS320_001
        {
            get
            {
                return "SELECT " +
                       " STN_NO, CHKDEF, IO_DEF" +
                       " FROM TABGATE";
            }
        }

        public static String KSWMS320_002(List<string> Condition)
        {
            string result = "SELECT " +
                            " (SUBSTR(a.LOC, 1, 1) || '-' || SUBSTR(a.LOC, 2, 1) || '-' || SUBSTR(a.LOC, 3, 2)|| '-' || SUBSTR(a.LOC, 5, 2)) AS LOC," +
                            " a.IPDATE," +
                            " a.BLNO, a.SER_NO," +
                            " a.HCODE," +
                            " (SELECT HNAME FROM TAB120 WHERE a.HCODE = HCODE) AS HNAME," +
                            " (SELECT HDAMDANG FROM TAB120 WHERE a.HCODE = HCODE) AS HDAMDANG," +
                            " a.PCODE," +
                            " (SELECT PNAME FROM TAB110 WHERE a.PCODE = PCODE) AS PNAME," +
                            " (SELECT PDESC FROM TAB110 WHERE a.PCODE = PCODE) AS PDESC," +
                            " a.INQTY," +
                            " (SELECT PUNIT FROM TAB110 WHERE a.PCODE = PCODE) AS PUNIT," +
                            " a.HANG, a.BIGO, a.TGDATE, a.HOAMLNO," +
                            " (CASE a.PGUBUN" +
                            "  WHEN '82' THEN '축산'" +
                            "  ELSE '식품'" +
                            "  END) AS PGUBUN," +
                            " a.WMS_NO, a.WMS_SQ, a.STOCKNO, a.ITEM_CD," +
                            " (CASE a.ORDTYP" +
                            "  WHEN '11' THEN '고려오더'" +
                            "  WHEN '12' THEN '생성오더'" +
                            "  ELSE '수동오더'" +
                            "  END) AS ORDTYP," +
                            " a.MCODE," +
                            " (SELECT HNAME FROM TAB120 WHERE a.MCODE = HCODE) AS HNAME," +
                            " (SELECT HDAMDANG FROM TAB120 WHERE a.MCODE = HCODE) AS HDAMDANG," +
                            " a.SCODE," +
                            " (SELECT SNAME FROM TAB130 WHERE a.SCODE = SCODE) AS SNAME" +
                            " FROM TAB210 a" +
                            " WHERE SUBSTR(a.LOC, 0, 1) != 'G'";

            if (Condition[0] == "Y")
                result += " AND IPDATE >= TO_DATE('" + Condition[2] + "','YYYY/MM/DD') AND IPDATE < TO_DATE('" + Condition[3] + "', 'YYYY/MM/DD') + 1";
            if (Condition[1] == "Y")
                result += " AND LOC = '" + Condition[4] + "'";
            if (Condition[5] != "") result += " AND MCODE = '" + Condition[5] + "'";
            if (Condition[6] != "") result += " AND PCODE = '" + Condition[6] + "'";
            if (Condition[7] != "") result += " AND BLNO = '" + Condition[7] + "'";
            if (Condition[8] != "") result += " AND SER_NO = '" + Condition[8] + "'";
            
            result += " ORDER BY a.IPDATE ASC";
            return result;
        }

        public static String KSWMS320_003(List<string> Condition)
        {
            string result = "INSERT INTO TAB920 " +
                            "(REAL_DATE, REAL_SEQ, JOB_NO, JOB_DATE, JOB_GBN1, JOB_GBN2, LOC," + 
            	            " SRC_SITE, DST_SITE, ITEM_CODE, ITEM_STS, SER_NO, SER_NO1, SER_NO2," + 
            	            " MV_CODE, IN_GBN, IN_DATE, REMARK, JOB_TIME_S, JOB_TIME_E, SMS_GROUP," +
            	            " JOB_GBN2_NAME, T310_RCV_DATE, T310_RCV_SEQ, T310_ALTERKEY, ORD_GROUP," +
            	            " HOST_STS, HOST_FLG, COMMIT_NO, SERIAL_NO, REC_GBN, BACK_FLG, COM_NAME," +
            	            " USR_ID, PLC_CODE1, LUGG_SIZE)" +

                            "SELECT " +
                            " SYSDATE, '" + Condition[1] + "', '0', SYSDATE, '0', '0', LOC, LOC," +
                            " '" + Condition[2] + "', ITEM_CD, '0', SER_NO, SER_NO, '0', '0', '0'," +
                            " SYSDATE, '0', SYSDATE, SYSDATE, '0', '0', '0', 0, '0', '0', '0', '0'," +
                            " '0', '0', '0', '0', '0', 'UserId', '0', '0'" +
                            " FROM TAB210" +
                            " WHERE SUBSTR(LOC, 0, 1) != 'G' AND SER_NO = '" + Condition[0] + "'";
            return result;
        }

        public static String KSWMS320_004
        {
            get
            {
                return "SELECT" +
                       " (SUBSTR(p.LOC, 2, 1) || '-' || SUBSTR(p.LOC, 3, 1) || '-' || SUBSTR(p.LOC, 4, 2) || '-' || SUBSTR(p.LOC, 6, 2)) AS LOC," +
                       " p.DOCKNO, p.TRUCK, p.IPDATE, p.BLNO, p.SER_NO," +
                       " p.HCODE," +
                       " (SELECT HNAME FROM TAB120 WHERE p.HCODE = HCODE) AS HNAME," +
                       " (SELECT HDAMDANG FROM TAB120 WHERE p.HCODE = HCODE) AS HDAMDANG," +
                       " p.PCODE," +
                       " (SELECT PNAME FROM TAB110 WHERE p.PCODE = PCODE) AS PNAME," +
                       " (SELECT PDESC FROM TAB110 WHERE p.PCODE = PCODE) AS PDESC," +
                       " p.INQTY," +
                       " (SELECT PUNIT FROM TAB110 WHERE p.PCODE = PCODE) AS PUNIT," +
                       " p.HANG, p.BIGO, p.HOAMLNO," +
                       " (CASE p.PGUBUN" +
                       "  WHEN '82' THEN '축산'" +
                       "  ELSE '식품'" +
                       "  END) AS PGUBUN," +
                       "  p.TGDATE," +
                       "  p.WMS_NO, p.WMS_SQ, p.STOCKNO, p.ITEM_CD," +
                       " (CASE p.ORDTYP" +
                       "  WHEN '11' THEN '고려오더'" +
                       "  WHEN '12' THEN '생성오더'" +
                       "  ELSE '수동오더'" +
                       "  END) AS ORDTYP," +
                       "  p.MCODE," +
                       " (SELECT HNAME FROM TAB120 WHERE p.MCODE = HCODE) AS HNAME," +
                       " (SELECT HDAMDANG FROM TAB120 WHERE p.MCODE = HCODE) AS HDAMDANG," +
                       "  p.SCODE," +
                       " (SELECT SNAME FROM TAB130 WHERE p.SCODE = SCODE) AS SNAME" +
                       " FROM TAB920 a" +
                       " INNER JOIN TAB210 p" +
                       " ON a.SER_NO = p.SER_NO" +
                       " WHERE a.IN_GBN = '0'" +
                       " ORDER BY a.REAL_SEQ ASC";
            }
        }

        public static String KSWMS320_05(List<string> Condition)  // 진한 추가
        {
            string result = "Select Count(*) FROM TAB920" +
                            " WHERE SER_NO = '" + Condition[0] + "'"
                            + " and IN_GBN = 0" 
                            ;
            return result;
        }

        public static String KSWMS320_005(List<string> Condition)
        {

            string result = "DELETE FROM TAB920" +
                            " WHERE SER_NO = '"+ Condition[0] + "'"
                            + " and IN_GBN = 0"   // 진한수정 
                            ;
            return result;
        }

        public static String KSWMS320_006(List<string> Condition)
        {
            string result = "UPDATE TAB210" +
                            " SET LOC = CONCAT('G', LOC), " +
                            "UPD_DT = SYSDATE" +
                            " WHERE SER_NO  = '" + Condition[0] + "'";
            return result;
        }
        
        public static String KSWMS320_007(List<string> Condition)
        {
            string result = "UPDATE TAB210" +
                            " SET LOC = SUBSTR(LOC, 2, 6), " +
                            "UPD_DT = SYSDATE" +
                            " WHERE SER_NO  = '" + Condition[0] + "'"
                            ;
            return result;
        }

        public static String KSWMS320_010
        {
            get
            {
                return "SELECT MAX(REAL_SEQ)" +
                       " FROM TAB920" +
                       " WHERE REAL_SEQ < '300000000'";
            }
        }

        public static String KSWMS320_011
        {
            get
            {
                return "SELECT MAX(REAL_SEQ)" +
                       " FROM TAB920" +
                       " WHERE REAL_SEQ > '300000000'";
            }
        }

        public static String KSWMS410_001(List<string> Condition)
        {
            string result = "SELECT" +
                            " (SUBSTR(a.LOC, 1, 1) || '-' || SUBSTR(a.LOC, 2, 1) || '-' || SUBSTR(a.LOC, 3, 2) || '-' || SUBSTR(a.LOC, 5, 2)) AS LOC," +
                            " (CASE a.STS WHEN '0' THEN '정상' ELSE '비정상' END) AS STS," +
                            " (CASE a.LUG WHEN '1' THEN '재고' ELSE '공랙' END) AS LUG," +
                            " (CASE a.USE WHEN 'O' THEN '사용' ELSE '미사용' END) AS USE," +
                            " (CASE a.IN_GBN WHEN '1' THEN '자동입고' ELSE '수동입고' END) AS IN_GBN," +
                           " (CASE p.ZONEKD WHEN '1' THEN '식품' ELSE '축산' END) AS ZONE," +   //진한 수정
                            " TO_CHAR(a.IN_DATE, 'YYYY/MM/DD')," +
                            " p.IPDATE, p.BLNO, p.SER_NO," +
                            " p.HCODE," +
                            " (SELECT HNAME FROM TAB120 WHERE p.HCODE = HCODE) AS HNAME," +
                            " (SELECT HDAMDANG FROM TAB120 WHERE p.HCODE = HCODE) AS HDAMDANG," +
                            " p.PCODE," +
                            " (SELECT PNAME FROM TAB110 WHERE p.PCODE = PCODE) AS PNAME," +
                            " (SELECT PDESC FROM TAB110 WHERE p.PCODE = PCODE) AS PDESC," +
                            " p.INQTY," +
                            " (SELECT PUNIT FROM TAB110 WHERE p.PCODE = PCODE) AS PUNIT," +
                            " p.HANG, p.BIGO, p.INQTY, p.HOAMLNO, p.TGDATE," +
                            " p.WMS_NO, p.WMS_SQ, p.STOCKNO, p.ITEM_CD," +
                            " p.MCODE," +
                            " (SELECT HNAME FROM TAB120 WHERE p.MCODE = HCODE) AS HNAME," +
                            " (SELECT HDAMDANG FROM TAB120 WHERE p.MCODE = HCODE) AS HDAMDANG," +
                            " p.SCODE," +
                            " (SELECT SNAME FROM TAB130 WHERE p.SCODE = SCODE) AS SNAME" +
                            " FROM TAB200 a" +
                            " LEFT JOIN TAB210 p" +
                            " ON a.SER_NO = p.SER_NO AND a.LOC = p.LOC" +
                            " WHERE a.LOC IS NOT NULL";
                            
                          
            if (Condition[0] == "N")
                result += " AND p.IPDATE >= TO_DATE('" + Condition[1] + "','YYYY/MM/DD') AND p.IPDATE < TO_DATE('" + Condition[2] + "', 'YYYY/MM/DD') + 1";
            if (Condition[3] == "N")
                result += " AND a.LOC = '" + Condition[4] + "'" ;
            if (Condition[5] != "")
                result += " AND p.HCODE LIKE '" + Condition[5] + "%'";
            if (Condition[6] != "")
                result += " AND p.PCODE LIKE '" + Condition[6] + "%'";
            if (Condition[7] != "")
                result += " AND p.BLNO LIKE '" + Condition[7] + "%'";
            if (Condition[8] != "")
                result += " AND p.SER_NO LIKE '" + Condition[8] + "%'";
            if (Condition[9] != "A")
                result += " AND a.LUG ='" + Condition[9] + "'";
            if (Condition[10] != "A")
                result += " AND a.USE ='" + Condition[10] + "'";
            if (Condition[11] != "A")
                result += " AND a.STS ='" + Condition[11] + "'";

            //result += " ORDER BY a.SER_NO ASC";
            return result;
        }

        public static String KSWMS420_001(List<string> Condition)
        {
            string result = "SELECT" +
                            " a.IPDATE, a.BLNO," +
                            " a.HCODE," +
                            " (SELECT HNAME FROM TAB120 WHERE a.HCODE = HCODE) AS HNAME," +
                            " (SELECT HDAMDANG FROM TAB120 WHERE a.HCODE = HCODE) AS HDAMDANG," +
                            " a.PCODE," +
                            " (SELECT PNAME FROM TAB110 WHERE a.PCODE = PCODE) AS PNAME," +
                            " (SELECT PDESC FROM TAB110 WHERE a.PCODE = PCODE) AS PDESC," +
                            " SUM(a.INQTY)," +
                            " (SELECT PUNIT FROM TAB110 WHERE a.PCODE = PCODE) AS PUNIT," +
                            " (CASE a.PGUBUN" +
                            "  WHEN '82' THEN '축산'" +
                            " ELSE '식품'" +
                            " END) AS PGUBUN," +
                            " COUNT(*)," +
                            " a.HANG, a.BIGO, a.HOAMLNO," +
                            " a.WMS_NO, a.STOCKNO, a.ITEM_CD," +
                            " a.MCODE," +
                            " (SELECT HNAME FROM TAB120 WHERE a.MCODE = HCODE) AS HNAME," +
                            " (SELECT HDAMDANG FROM TAB120 WHERE a.MCODE = HCODE) AS HDAMDANG," +
                            " a.SCODE," +
                            " (SELECT SNAME FROM TAB130 WHERE a.SCODE = SCODE) AS SNAME" +

                            " FROM TAB210 a" +

                            " WHERE SUBSTR(LOC, 0, 1) != 'G'" +
                            " AND MCODE LIKE '" + Condition[2] + "%'" +
                            " AND PCODE LIKE '" + Condition[3] + "%'" +
                            " AND BLNO LIKE '" + Condition[4] + "%'";

            if (Condition[5] == "Y")
                result += " AND a.IPDATE >= TO_DATE('" + Condition[0] + "','YYYY/MM/DD') AND a.IPDATE < TO_DATE('" + Condition[1] + "', 'YYYY/MM/DD') + 1";


            result += " GROUP BY a.IPDATE, a.BLNO, a.HCODE, a.PCODE, a.PGUBUN," +
                      " a.HANG, a.BIGO, a.HOAMLNO, a.WMS_NO, a.STOCKNO," +
                      " a.ITEM_CD, a.MCODE, a.SCODE";

            //" ORDER BY a.SER_NO ASC";
            return result;
        }

        public static String KSWMS430_001(List<string> Condition)
        {
            string result = "SELECT LOC, LUG, STS, USE" +
                            " FROM TAB200" +
                            " WHERE LOC LIKE '" + Condition[0] + "%'";

            return result;
        }

        public static String POPUP_랙적재율정보_001
        {
            get
            {
                return "SELECT LOC, LUG" +
                       " FROM TAB200" +
                       " WHERE 1=1" +
                       " ORDER BY LOC ASC";
            }
        }

        public static String POPUP_거래처정보_001(List<string> Condition)
        {
            string result = "SELECT HCODE, HNAME, HDAMDANG FROM TAB120" +
                            " WHERE HCODE LIKE '%" + Condition[0] + "%'";
            //if (Condition[0] != "")
            //{
            //    result += " AND HCODE LIKE '%" + Condition[0] + "%'";
            //}
            return result;
        }

        public static String POPUP_제품정보_001(List<string> Condition)
        {
            string result = "SELECT PCODE, PNAME, PDESC, PUNIT, PGUBUN, BIGO FROM TAB110" +
                            " WHERE PCODE LIKE '%" + Condition[0] + "%'";
            //if (Condition[0] != "")
            //{
            //    result += " AND HCODE LIKE '%" + Condition[0] + "%'";
            //}
            return result;
        }

        public static String POPUP_선박정보_001(List<string> Condition)
        {
            string result = "SELECT SCODE, SNAME, SBIGO FROM TAB130" +
                            " WHERE SCODE LIKE '%" + Condition[0] + "%'";
            //if (Condition[0] != "")
            //{
            //    result += " AND HCODE LIKE '%" + Condition[0] + "%'";
            //}
            return result;
        }

        public static String KSWMS710_001
        {
            get
            {
                return "SELECT STN_NO, CHKDEF, IO_DEF" +
                       " FROM TABGATE" +
                       " ORDER BY STN_NO ASC";
            }
        }

        public static String KSWMS710_002(List<string> Condition)
        {
            string result;

            if (Condition[0] == "310" || Condition[0] == "320" || Condition[0] == "330" || Condition[0] == "340")
            {
                result = "UPDATE TABGATE" +
                         " SET CHKDEF = '" + Condition[1] + "', IO_DEF = '" + Condition[2] + "', STCDEF = '" + Condition[3] + "'" +
                         " WHERE STN_NO = '" + Condition[0] + "'";
            }
            else
            {
                result = "UPDATE TABGATE" +
                          " SET CHKDEF = '" + Condition[1] + "'" +
                          " WHERE STN_NO = '" + Condition[0] + "'";
            }

            return result;
        }
    }
}
