﻿using System;
using System.Data;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Oracle.ManagedDataAccess.Client;

namespace KSWMS
{
    class CommonDB
    {
        public static DataTable R_datatbl(string sql)
        {
            DataTable dt = new DataTable();
            try
            {
                OracleConnection OraConn = new OracleConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);
                OraConn.Open();
                OracleDataAdapter oda = new OracleDataAdapter();
                oda.SelectCommand = new OracleCommand(sql, OraConn);
                
                oda.Fill(dt);
                OraConn.Close();
                //dataGridView1.DataSource = d1;
            }
            catch(Exception ex)
            {

            }

            return dt;
        }

    }
}
