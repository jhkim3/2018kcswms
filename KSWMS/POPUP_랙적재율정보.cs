﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;

namespace KSWMS
{
    public partial class POPUP_랙적재율정보 : Form
    {
        DataTable d1 = new DataTable();
        private decimal [,] arraysum = new decimal[21, 6];
        private int COL_Count = 10;

        public POPUP_랙적재율정보()
        {
            InitializeComponent();
           
        }
        
        private void button2_Click(object sender, EventArgs e)
        {
            Main_Process();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Application.OpenForms["POPUP_랙적재율정보"].Close();
        }

        private void Main_Process()
        {
            Clear();
            dataGridView1Set();

            // Cursor.Current = Cursors.WaitCursor;

            d1 = CommonDB.R_datatbl(Qry.POPUP_랙적재율정보_001);
            
            for (int i = 0; i <= arraysum.GetLength(0) - 1; i += 1)
            {
                if(i < 5) arraysum[i, 0] = 1;
                else if (i > 4 && i < 10) arraysum[i, 0] = 2;
                else if (i > 9 && i < 15) arraysum[i, 0] = 3;
                else if (i > 14 && i < 20) arraysum[i, 0] = 4;
                else if (i == 20) arraysum[i, 0] = 9;
            
                if (i < 4) arraysum[i, 1] = i + 1;
                else if (i > 4 && i < 9) arraysum[i, 1] = i - 4;
                else if (i > 9 && i < 14) arraysum[i, 1] = i - 9;
                else if (i > 14 && i < 19) arraysum[i, 1] = i - 14;
                else if (i == 4 || i == 9 || i == 14 || i == 19 || i == 20) arraysum[i, 1] = 8;
                
                arraysum[i, 3] = 0;
            }
            
            int j = 0;
            while (j < d1.Rows.Count)
            {
                if (d1.Rows[j][0].ToString().Substring(0, 2) == "11")
                {
                    if (d1.Rows[j][1].ToString() == "1") arraysum[0, 2] += 1;          //제품 count
                    else if (d1.Rows[j][1].ToString() == "0") arraysum[0, 4] += 1;     //공랙 count
                    arraysum[0, 5] += 1; // 총랙 count
                }
                if (d1.Rows[j][0].ToString().Substring(0, 2) == "12")
                {
                    if (d1.Rows[j][1].ToString() == "1") arraysum[1, 2] += 1;          //제품 count
                    else if (d1.Rows[j][1].ToString() == "0") arraysum[1, 4] += 1;     //공랙 count
                    arraysum[1, 5] += 1; // 총랙 count
                }
                if (d1.Rows[j][0].ToString().Substring(0, 2) == "13")
                {
                    if (d1.Rows[j][1].ToString() == "1") arraysum[2, 2] += 1;          //제품 count
                    else if (d1.Rows[j][1].ToString() == "0") arraysum[2, 4] += 1;     //공랙 count
                    arraysum[2, 5] += 1; // 총랙 count
                }
                if (d1.Rows[j][0].ToString().Substring(0, 2) == "14")
                {
                    if (d1.Rows[j][1].ToString() == "1") arraysum[3, 2] += 1;          //제품 count
                    else if (d1.Rows[j][1].ToString() == "0") arraysum[3, 4] += 1;     //공랙 count
                    arraysum[3, 5] += 1; // 총랙 count
                }
                if (d1.Rows[j][0].ToString().Substring(0, 2) == "21")
                {
                    if (d1.Rows[j][1].ToString() == "1") arraysum[5, 2] += 1;          //제품 count
                    else if (d1.Rows[j][1].ToString() == "0") arraysum[5, 4] += 1;     //공랙 count
                    arraysum[5, 5] += 1; // 총랙 count
                }
                if (d1.Rows[j][0].ToString().Substring(0, 2) == "22")
                {
                    if (d1.Rows[j][1].ToString() == "1") arraysum[6, 2] += 1;          //제품 count
                    else if (d1.Rows[j][1].ToString() == "0") arraysum[6, 4] += 1;     //공랙 count
                    arraysum[6, 5] += 1; // 총랙 count
                }
                if (d1.Rows[j][0].ToString().Substring(0, 2) == "23")
                {
                    if (d1.Rows[j][1].ToString() == "1") arraysum[7, 2] += 1;          //제품 count
                    else if (d1.Rows[j][1].ToString() == "0") arraysum[7, 4] += 1;     //공랙 count
                    arraysum[7, 5] += 1; // 총랙 count
                }
                if (d1.Rows[j][0].ToString().Substring(0, 2) == "24")
                {
                    if (d1.Rows[j][1].ToString() == "1") arraysum[8, 2] += 1;          //제품 count
                    else if (d1.Rows[j][1].ToString() == "0") arraysum[8, 4] += 1;     //공랙 count
                    arraysum[8, 5] += 1; // 총랙 count
                }
                if (d1.Rows[j][0].ToString().Substring(0, 2) == "31")
                {
                    if (d1.Rows[j][1].ToString() == "1") arraysum[10, 2] += 1;          //제품 count
                    else if (d1.Rows[j][1].ToString() == "0") arraysum[10, 4] += 1;     //공랙 count
                    arraysum[10, 5] += 1; // 총랙 count
                }
                if (d1.Rows[j][0].ToString().Substring(0, 2) == "32")
                {
                    if (d1.Rows[j][1].ToString() == "1") arraysum[11, 2] += 1;          //제품 count
                    else if (d1.Rows[j][1].ToString() == "0") arraysum[11, 4] += 1;     //공랙 count
                    arraysum[11, 5] += 1; // 총랙 count
                }
                if (d1.Rows[j][0].ToString().Substring(0, 2) == "33")
                {
                    if (d1.Rows[j][1].ToString() == "1") arraysum[12, 2] += 1;          //제품 count
                    else if (d1.Rows[j][1].ToString() == "0") arraysum[12, 4] += 1;     //공랙 count
                    arraysum[12, 5] += 1; // 총랙 count
                }
                if (d1.Rows[j][0].ToString().Substring(0, 2) == "34")
                {
                    if (d1.Rows[j][1].ToString() == "1") arraysum[13, 2] += 1;          //제품 count
                    else if (d1.Rows[j][1].ToString() == "0") arraysum[13, 4] += 1;     //공랙 count
                    arraysum[13, 5] += 1; // 총랙 count
                }
                if (d1.Rows[j][0].ToString().Substring(0, 2) == "41")
                {
                    if (d1.Rows[j][1].ToString() == "1") arraysum[15, 2] += 1;          //제품 count
                    else if (d1.Rows[j][1].ToString() == "0") arraysum[15, 4] += 1;     //공랙 count
                    arraysum[15, 5] += 1; // 총랙 count
                }
                if (d1.Rows[j][0].ToString().Substring(0, 2) == "42")
                {
                    if (d1.Rows[j][1].ToString() == "1") arraysum[16, 2] += 1;          //제품 count
                    else if (d1.Rows[j][1].ToString() == "0") arraysum[16, 4] += 1;     //공랙 count
                    arraysum[16, 5] += 1; // 총랙 count
                }
                if (d1.Rows[j][0].ToString().Substring(0, 2) == "43")
                {
                    if (d1.Rows[j][1].ToString() == "1") arraysum[17, 2] += 1;          //제품 count
                    else if (d1.Rows[j][1].ToString() == "0") arraysum[17, 4] += 1;     //공랙 count
                    arraysum[17, 5] += 1; // 총랙 count
                }
                if (d1.Rows[j][0].ToString().Substring(0, 2) == "44")
                {
                    if (d1.Rows[j][1].ToString() == "1") arraysum[18, 2] += 1;          //제품 count
                    else if (d1.Rows[j][1].ToString() == "0") arraysum[18, 4] += 1;     //공랙 count
                    arraysum[18, 5] += 1; // 총랙 count
                }

                arraysum[4, 2] = arraysum[0, 2] + arraysum[1, 2] + arraysum[2, 2] + arraysum[3, 2];
                arraysum[4, 4] = arraysum[0, 4] + arraysum[1, 4] + arraysum[2, 4] + arraysum[3, 4];
                arraysum[4, 5] = arraysum[0, 5] + arraysum[1, 5] + arraysum[2, 5] + arraysum[3, 5];

                arraysum[9, 2] = arraysum[5, 2] + arraysum[6, 2] + arraysum[7, 2] + arraysum[8, 2];
                arraysum[9, 4] = arraysum[5, 4] + arraysum[6, 4] + arraysum[7, 4] + arraysum[8, 4];
                arraysum[9, 5] = arraysum[5, 5] + arraysum[6, 5] + arraysum[7, 5] + arraysum[8, 5];

                arraysum[14, 2] = arraysum[10, 2] + arraysum[11, 2] + arraysum[12, 2] + arraysum[13, 2];
                arraysum[14, 4] = arraysum[10, 4] + arraysum[11, 4] + arraysum[12, 4] + arraysum[13, 4];
                arraysum[14, 5] = arraysum[10, 5] + arraysum[11, 5] + arraysum[12, 5] + arraysum[13, 5];

                arraysum[19, 2] = arraysum[15, 2] + arraysum[16, 2] + arraysum[17, 2] + arraysum[18, 2];
                arraysum[19, 4] = arraysum[15, 4] + arraysum[16, 4] + arraysum[17, 4] + arraysum[18, 4];
                arraysum[19, 5] = arraysum[15, 5] + arraysum[16, 5] + arraysum[17, 5] + arraysum[18, 5];

                arraysum[20, 2] = arraysum[4, 2] + arraysum[9, 2] + arraysum[14, 2] + arraysum[19, 2];
                arraysum[20, 4] = arraysum[4, 4] + arraysum[9, 4] + arraysum[14, 4] + arraysum[19, 4];

                arraysum[20, 5] = arraysum[20, 2] + arraysum[20, 4];

                j++;
            }
            
            dataGridView1DataSet();
            dataGridView1GraphSet();
            //statusBar1.Panels[0].Text = "전체 " + j.ToString() + " 건이 검색 되었습니다.";
        }

        private void Clear()
        {
            // 배열 초기화
            for (int i1 = 0; i1 <= arraysum.GetLength(0) - 1; i1 += 1)
            {
                for (int j1 = 0; j1 <= arraysum.GetLength(1) - 1; j1 += 1)
                {
                    arraysum[i1, j1] = 0;
                }
            }

            // CHART 초기화
            foreach (var series in chart1.Series)
            {
                series.Points.Clear();
            }
            chart1.Series.Clear();

            // 그리드 초기화
            dataGridView1.DataSource = null;
            dataGridView1.Columns.Clear();
        }
        
        private void dataGridView1Set()
        {
            //this.Controls.Add(dataGridView1);

            dataGridView1.ColumnCount = COL_Count;

            dataGridView1.Columns[0].Name = "호\n기";
            dataGridView1.Columns[1].Name = "열";
            dataGridView1.Columns[2].Name = "수";
            dataGridView1.Columns[3].Name = "%";
            dataGridView1.Columns[4].Name = "수";
            dataGridView1.Columns[5].Name = "%";
            dataGridView1.Columns[6].Name = "수";
            dataGridView1.Columns[7].Name = "%";
            dataGridView1.Columns[8].Name = "수";
            dataGridView1.Columns[9].Name = "%";

            //this.dataGridView1.Columns["암호"].Visible = false;
            //this.dataGridView1.Columns["권한"].Visible = false;

            // Columns AutoSize
            //dGVProcess.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells;

            // Head Cell_Columns Font Style = off
            dataGridView1.EnableHeadersVisualStyles = false;
            // Head Cell_Columns HeightSizeMode = off
            dataGridView1.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.DisableResizing;

            // Head Cell_Columns Width Size 임의 지정
            DataGridViewColumn column1 = dataGridView1.Columns[0];
            column1.Width = 30;

            DataGridViewColumn column2 = dataGridView1.Columns[1];
            column2.Width = 30;

            DataGridViewColumn column3 = dataGridView1.Columns[2];
            column3.Width = 50;

            DataGridViewColumn column4 = dataGridView1.Columns[3];
            column4.Width = 50;
            
            DataGridViewColumn column5 = dataGridView1.Columns[4];
            column5.Width = 50;

            DataGridViewColumn column6 = dataGridView1.Columns[5];
            column6.Width = 50;
            
            DataGridViewColumn column7 = dataGridView1.Columns[6];
            column7.Width = 50;
            
            DataGridViewColumn column8 = dataGridView1.Columns[7];
            column8.Width = 50;

            DataGridViewColumn column9 = dataGridView1.Columns[8];
            column9.Width = 50;

            DataGridViewColumn column10 = dataGridView1.Columns[9];
            column10.Width = 50;
            
            // Head Cell_Columns HeightSize 임의 지정
            dataGridView1.ColumnHeadersHeight = 60;
            // Head Row Width 임의 지정
            dataGridView1.RowHeadersWidth = 10;

            for (int j = 0; j < COL_Count; j++)
            {
                dataGridView1.Columns[j].HeaderCell.Style.Font = new Font("맑은고딕", 10, FontStyle.Bold);
                // dGVProcess.Columns[j].HeaderCell.Style.ForeColor = Color.Red;
                //dataGridView1.Columns[j].HeaderCell.Style.BackColor = Color.Yellow;

                // dGVState.Columns[j].HeaderCell.Size = new Size();
                
                // Head Cell_Columns Alignment
                //dataGridView1.Columns[j].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
                if (j < 2) dataGridView1.Columns[j].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
                else dataGridView1.Columns[j].HeaderCell.Style.Alignment = DataGridViewContentAlignment.BottomCenter;
              
                // Cell Font Style
                //dGVProcess.Columns[j].DefaultCellStyle.Font = new Font("Tahoma", 18, FontStyle.Bold);

                // Cell Font Alignment
                if (j < 2) dataGridView1.Columns[j].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                else dataGridView1.Columns[j].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                
                //  Cell Height 지정
                //  var dgv = new DataGridView();
                //dataGridView1.RowTemplate.Height = 30;
            }
        }

        private void dataGridView1DataSet()
        {
            //배열(arraysum) 데이타 값을 dataGridView1 에 표시하기

            for (int i = 0; i <= arraysum.GetLength(0) - 1; i += 1)
            {
                i = dataGridView1.Rows.Add();

                for (int j = 0; j <= COL_Count -1; j += 1)
                {
                    if (j == 0)
                    {
                        if (arraysum[i, j] == 9) dataGridView1.Rows[i].Cells[j].Value = "총";
                        else dataGridView1.Rows[i].Cells[j].Value = arraysum[i, j].ToString();
                    }
                    else if (j == 1)
                    {
                        if (arraysum[i, j] == 8) dataGridView1.Rows[i].Cells[j].Value = "합";
                        else dataGridView1.Rows[i].Cells[j].Value = arraysum[i, j].ToString();
                    }
                    else if (j == 2) dataGridView1.Rows[i].Cells[j].Value = string.Format("{0}", arraysum[i, j].ToString("#,##0"));
                    else if (j == 3)
                    {
                        if (arraysum[i, 2] == 0 || arraysum[i, 5] == 0) dataGridView1.Rows[i].Cells[j].Value = "0.0 %";
                        else dataGridView1.Rows[i].Cells[j].Value = string.Format("{0}", (arraysum[i, 2] / arraysum[i, 5] * 100).ToString("0.##")) + " %";
                    }
                    else if (j == 4) dataGridView1.Rows[i].Cells[j].Value = arraysum[i, j - 1].ToString();
                    else if (j == 5) dataGridView1.Rows[i].Cells[j].Value = "0.0 %";
                    else if (j == 6) dataGridView1.Rows[i].Cells[j].Value = string.Format("{0}", arraysum[i, j - 2].ToString("#,##0"));
                    else if (j == 7)
                    {
                        if (arraysum[i, 4] == 0 || arraysum[i, 5] == 0) dataGridView1.Rows[i].Cells[j].Value = "0.0 %";
                        else dataGridView1.Rows[i].Cells[j].Value = string.Format("{0}", (arraysum[i, 4] / arraysum[i, 5] * 100).ToString("0.##")) + " %";
                    }
                    else if (j == 8) dataGridView1.Rows[i].Cells[j].Value = string.Format("{0}", arraysum[i, j - 3].ToString("#,##0"));
                    else if (j == 9) dataGridView1.Rows[i].Cells[j].Value = "100.0 %";
                }
            }

            //dataGridView1 포커스(FOCUS)위치 세팅
            dataGridView1.CurrentCell = dataGridView1.Rows[20].Cells[1];
            //dataGridView1.FirstDisplayedScrollingRowIndex = 0;
        }

        private void dataGridView1GraphSet()
        {
            chart1.ChartAreas["ChartArea1"].AxisX.Interval = 1; // X축 격자간격 설정
            chart1.ChartAreas["ChartArea1"].AxisY.LabelStyle.Format = "{0:0,0}%";
            
            chart1.Series.Add("적재율");
            chart1.Series["적재율"].ChartType = SeriesChartType.Column;
            chart1.Series["적재율"].Color = Color.Black;
            chart1.Series["적재율"].BorderWidth = 5;
            
            chart1.Series["적재율"].Points.AddXY(arraysum[4, 0] + "호기 (" + (arraysum[4, 2] / arraysum[4, 5] * 100).ToString("0.##") + "%)", arraysum[4, 2]/ arraysum[4, 5]*100);
            chart1.Series["적재율"].Points.AddXY(arraysum[9, 0] + "호기 (" + (arraysum[9, 2] / arraysum[9, 5] * 100).ToString("0.##") + "%)", arraysum[9, 2] / arraysum[9, 5] * 100);
            chart1.Series["적재율"].Points.AddXY(arraysum[14, 0] + "호기 (" + (arraysum[14, 2] / arraysum[14, 5] * 100).ToString("0.##") + "%)", arraysum[14, 2] / arraysum[14, 5] * 100);
            chart1.Series["적재율"].Points.AddXY(arraysum[19, 0] + "호기 (" + (arraysum[19, 2] / arraysum[19, 5] * 100).ToString("0.##") + "%)", arraysum[19, 2] / arraysum[19, 5] * 100);
            chart1.Series["적재율"].Points.AddXY("합계 (" + (arraysum[20, 2] / arraysum[20, 5] * 100).ToString("0.##") + "%)", arraysum[20, 2] / arraysum[20, 5] * 100);

        }

        private void dataGridView1_Paint(object sender, PaintEventArgs e)
        {
            DataGridView gv = (DataGridView)sender;

            string[] strHeaders = { "제품", "파레트", "공랙", "총랙" };

            StringFormat format = new StringFormat();

            format.Alignment = StringAlignment.Center;

            format.LineAlignment = StringAlignment.Center;

            // Category Painting

            {
                Rectangle r1 = gv.GetCellDisplayRectangle(2, -1, false);
                int width1 = gv.GetCellDisplayRectangle(3, -1, false).Width;
               
                r1.X += 1;

                r1.Y += 2;

                r1.Width = r1.Width + width1 - 2;

                r1.Height = (r1.Height / 2) - 2;

                e.Graphics.DrawRectangle(new Pen(gv.BackgroundColor), r1);

                e.Graphics.FillRectangle(new SolidBrush(gv.ColumnHeadersDefaultCellStyle.BackColor),
                //e.Graphics.FillRectangle(new SolidBrush(Color.Yellow),

                r1);


                e.Graphics.DrawString(strHeaders[0],

                    //gv.ColumnHeadersDefaultCellStyle.Font,
                    new Font("맑은고딕", 9, FontStyle.Bold),

                    new SolidBrush(gv.ColumnHeadersDefaultCellStyle.ForeColor),

                    r1,

                    format);

            }

            // Projection Painting

            {
                Rectangle r2 = gv.GetCellDisplayRectangle(4, -1, false);
                int width1 = gv.GetCellDisplayRectangle(5, -1, false).Width;
              
                r2.X += 1;

                r2.Y += 2;

                r2.Width = r2.Width + width1 - 2;

                r2.Height = (r2.Height / 2) - 2;

                e.Graphics.DrawRectangle(new Pen(gv.BackgroundColor), r2);

                e.Graphics.FillRectangle(new SolidBrush(gv.ColumnHeadersDefaultCellStyle.BackColor), 
                //e.Graphics.FillRectangle(new SolidBrush(Color.Yellow),

                r2);


                e.Graphics.DrawString(strHeaders[1],

                    //gv.ColumnHeadersDefaultCellStyle.Font,
                    new Font("맑은고딕", 9, FontStyle.Bold),

                    new SolidBrush(gv.ColumnHeadersDefaultCellStyle.ForeColor),

                    r2,

                    format);
            }

            {
                Rectangle r3 = gv.GetCellDisplayRectangle(6, -1, false);
                int width1 = gv.GetCellDisplayRectangle(7, -1, false).Width;
              
                r3.X += 1;

                r3.Y += 2;

                r3.Width = r3.Width + width1 - 2;

                r3.Height = (r3.Height / 2) - 2;

                e.Graphics.DrawRectangle(new Pen(gv.BackgroundColor), r3);

                e.Graphics.FillRectangle(new SolidBrush(gv.ColumnHeadersDefaultCellStyle.BackColor), 
                //e.Graphics.FillRectangle(new SolidBrush(Color.GreenYellow),

                r3);


                e.Graphics.DrawString(strHeaders[2],

                    //gv.ColumnHeadersDefaultCellStyle.Font,
                    new Font("맑은고딕", 9, FontStyle.Bold),

                    new SolidBrush(gv.ColumnHeadersDefaultCellStyle.ForeColor),

                    r3,

                    format);
            }

            {
                Rectangle r4 = gv.GetCellDisplayRectangle(8, -1, false);
                int width1 = gv.GetCellDisplayRectangle(9, -1, false).Width;

                r4.X += 1;

                r4.Y += 2;

                r4.Width = r4.Width + width1 - 2;

                r4.Height = (r4.Height / 2) - 2;

                e.Graphics.DrawRectangle(new Pen(gv.BackgroundColor), r4);

                e.Graphics.FillRectangle(new SolidBrush(gv.ColumnHeadersDefaultCellStyle.BackColor), 
                //e.Graphics.FillRectangle(new SolidBrush(Color.GreenYellow),

                r4);


                e.Graphics.DrawString(strHeaders[3],

                    //gv.ColumnHeadersDefaultCellStyle.Font,
                    new Font("맑은고딕", 9, FontStyle.Bold),

                    new SolidBrush(gv.ColumnHeadersDefaultCellStyle.ForeColor),

                    r4,

                    format);
            }

        }

        private void dataGridView1_CellPainting(object sender, DataGridViewCellPaintingEventArgs e)
        {
            if (e.RowIndex == -1 && e.ColumnIndex > -1)

            {
                Rectangle r = e.CellBounds;

                r.Y += e.CellBounds.Height / 2;

                r.Height = e.CellBounds.Height / 2;

                e.PaintBackground(r, true);

                e.PaintContent(r);

                e.Handled = true;
            }

        }

        private void dataGridView1_Scroll(object sender, ScrollEventArgs e)
        {
            DataGridView gv = (DataGridView)sender;

            Rectangle rtHeader = gv.DisplayRectangle;

            rtHeader.Height = gv.ColumnHeadersHeight / 2;

            gv.Invalidate(rtHeader);
        }



        private void dataGridView1_ColumnWidthChanged(object sender, DataGridViewColumnEventArgs e)
        {
            DataGridView gv = (DataGridView)sender;

            Rectangle rtHeader = gv.DisplayRectangle;

            rtHeader.Height = gv.ColumnHeadersHeight / 2;

            gv.Invalidate(rtHeader);

        }

        private void POPUP_랙적재율정보_Load(object sender, EventArgs e)
        {
            Main_Process();
        }
    }
}
