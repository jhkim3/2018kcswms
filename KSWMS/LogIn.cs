﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace KSWMS
{
    public partial class LogIn : Form
    {
        public LogIn()
        {
            InitializeComponent();
            Common.setTextBoxEnter(textBox1, button1);
            Common.setTextBoxEnter(textBox2, button1);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Application.OpenForms["LogIn"].Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            DataTable d1 = new DataTable();

            //string qry = "SELECT COUNT(*) FROM TABUSR WHERE USER_ID= '" + textBox1.Text + "' AND PASSWD = '" + textBox2.Text + "'";
            string qry = "SELECT USER_ID, USER_NAME, ATH_GBN FROM TABUSR WHERE USER_ID= '" + textBox1.Text + "' AND PASSWD = '" + textBox2.Text + "'";
            d1 = CommonDB.R_datatbl(qry);

            //if (d1.Rows[0][0].ToString() == "1")
            if (d1.Rows.Count > 0)
            {
                List<string> User_Para = new List<string>();
                User_Para.Add(d1.Rows[0][0].ToString());
                User_Para.Add(d1.Rows[0][1].ToString());
                User_Para.Add(d1.Rows[0][2].ToString());

                User_Para.Add("TE");
            User_Para.Add("st");
            this.Hide();
                Main M1 = new Main(User_Para);
                M1.Show();
                
            }
            else if (textBox1.Text == "") MessageBox.Show("사용자 ID를 입력하세요.");
            else if (textBox2.Text == "") MessageBox.Show("패스워드를 입력하세요.");
            else MessageBox.Show("등록되지 않는 사용자 ID 이거나 패스워드가 틀렸습니다.");
        }
    }
}
